<?

$pagetitle = 'Custom & group tours';
$metadesc = 'Grand Canyon tours, as well as Monument Valley tours and Bryce Canyon tour service. Yellowstone tours from Salt Lake City, by bus to Grand Canyon, Monument Valley, tours, Yellowstone, Bryce Canyon tour';
include_once('header.php');

echo '<CENTER><BR>

<FONT FACE="Arial" SIZE="5" COLOR="#000080"><B>'.gettrans('Bundu Bashers custom and group tours').'</B></FONT><BR>

                    <table border="0" cellpadding="3" cellspacing="0" id="table2">

                      <tr>

                        <td style="font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 12px; color: #666666"><a target="_blank" href="images/Bergeron%20Kanab%203.jpg">

                        <img class="imgbord" src="images/Bergeron%20Kanab%203_small.jpg" xthumbnail-orig-image="Bergeron Kanab 3.jpg" width="93" height="70"></a></td>

                        <td style="font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 12px; color: #666666"><a target="_blank" href="images/Hayem%20Monument%20Valley%202.jpg">

                        <img class="imgbord" src="images/Hayem%20Monument%20Valley%202_small.jpg" xthumbnail-orig-image="Hayem Monument Valley 2.jpg" width="103" height="70"></a></td>

                        <td style="font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 12px; color: #666666"><a target="_blank" href="images/American%20photo%2017.jpg">

                        <img class="imgbord" src="images/American%20photo%2017_small.jpg" xthumbnail-orig-image="American photo 17.jpg" width="103" height="70"></a></td>

                        <td style="font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 12px; color: #666666"><a target="_blank" href="images/KIPP%205.jpg">

                        <img class="imgbord" src="images/KIPP%205_small.jpg" xthumbnail-orig-image="KIPP 5.jpg" width="92" height="70"></a></td>

                        <td style="font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 12px; color: #666666"><a target="_blank" href="images/Yellowstone%20rafting%202.jpg">

                        <img class="imgbord" src="images/Yellowstone%20rafting%202_small.jpg" xthumbnail-orig-image="Yellowstone rafting 2.jpg" width="106" height="70"></a></td>

                        <td style="font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 12px; color: #666666"><a target="_blank" href="images/Yellowstone%20shared%2016.jpg">

                        <img class="imgbord" src="images/Yellowstone%20shared%2016_small.jpg" xthumbnail-orig-image="Yellowstone shared 16.jpg" width="106" height="70"></a></td>

                        <td style="font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 12px; color: #666666"><a target="_blank" href="images/Lake%20Powell%20Antelope%20Canyon%202.jpg">

                        <img class="imgbord" src="images/Lake%20Powell%20Antelope%20Canyon%202_small.jpg" xthumbnail-orig-image="Lake Powell Antelope Canyon 2.jpg" width="103" height="70"></a></td>

                      </tr>

                    </table>

                  </center>

<p align="justify"> <font face="Tahoma" size="2"> <font color="#000080">'.gettrans('In addition to our packaged tours, Bundu Bashers can also design a tour to your specifications.').'&nbsp; '.gettrans('We have had a lot of experience in planning great excursions across the southwest and elsewhere.').'&nbsp; '.gettrans('Our staff know the best places to stay, the fun things to do and the unusual, off the beaten path, places to visit.').'</font></p>

                  <p><font face="Tahoma" size="2" color="#000080">'.gettrans('Whether you have a small family reunion or a large corporate group, our expertise will enable you to have a great time at an affordable price.').'</font></p>

                  <p><font face="Tahoma" size="2" color="#000080">'.gettrans('For more details, or for a quotation, please contact us using the information on the left.').'</font></p>'."\n\n";

include('footer.php'); ?>