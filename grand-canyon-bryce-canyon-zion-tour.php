<? include_once('header.php'); ?>
<style type="text/css">
<!--
.style1 {font-family: Verdana, Arial, Helvetica, sans-serif}
.style3 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #FF0000;
}
.style6 {
	font-size: 12px;
	font-style: italic;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
}
.style9 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #FF0000;
}
.style10 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; }
-->
</style>
<title>Grand Canyon tours</title><table width="800" border="0" cellpadding="0">
  <tr>
    <td><p class="style10"><a href="http://www.bundubashers.com/tour.php?id=grand-canyon-tours" target="_blank">One day Grand Canyon bus tour</a> for just $19.95 when you buy a <a href="http://www.bundubashers.com/tour.php?id=las-vegas-bryce-canyon-tours" target="_blank">Bryce Canyon and Zion tour</a> for its discounted price of $149! </p>
    <p class="style10">But if you'd rather not see Bryce and Zion, we will still give you the <span class="style3">lowest price</span> Grand Canyon tour anywhere! Just <span class="style3"><a href="http://www.bundubashers.com/tour.php?id=south_rim_one_day_grand_canyon_bus_tour" target="_blank">$69.95!</a></span></p></td>
  </tr>
  <tr>
    <td width="796"><table width="650" border="0" align="center" cellpadding="1">
      <tr>
        <td width="259"><div align="center"><img src="bryce-canyon-tours-21a.jpg" alt="Bryce Canyon tours" width="214" height="144" border="1" /></div></td>
        <td width="321"><div align="center"><img src="bryce-canyon-tours-49.jpg" alt="Bryce Canyon tours" width="214" height="144" border="1" /></div></td>
        <td width="56"><div align="center"><img src="bryce-canyon-tour-19f.jpg" alt="Bryce Canyon tour" width="214" height="144" border="1" /></div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="796" border="0" cellpadding="2">
      <tr>
        <td width="788"><div align="center"><span class="style3">Limited time offer!</span> <span class="style3"> <a href="http://www.bundubashers.com/reserve_special.php?t">Order here</a></span></div></td>
      </tr>
      <tr>
        <td><p align="justify" class="style10">People say that Bryce Canyon is much more awesome than the Grand Canyon, plus you get a tour of Zion, Utah's most popular  Park, thrown in for free. So on one day you'll take a tour to the Grand Canyon, and then on a separate day you'll take a <a href="http://www.bundubashers.com/tour.php?id=las-vegas-bryce-canyon-tours" target="_blank">tour to Bryce Canyon and Zion</a>! </p></td>
      </tr>
      <tr>
        <td><div align="center"><span class="style1"><a href="http://www.bundubashers.com/reserve_special.php?t">Order this special offer here!</a></span></div></td>
      </tr>
      <tr>
        <td><div align="center"></div></td>
      </tr>
      <tr>
        <td class="style1"><div align="center"><a href="http://www.bundubashers.com/tour.php?id=grand-canyon-tours" target="_blank">Grand Canyon tour details</a> <span class="style6">(opens in new window - return here to get the discounted rate)</span></div></td>
      </tr>
      <tr>
        <td class="style1"><div align="center"><a href="http://www.bundubashers.com/tour.php?id=las-vegas-bryce-canyon-tours" target="_blank">Bryce Canyon and Zion tour details</a> <span class="style6">(opens in new window - return here for the special offer)</span></div></td>
      </tr>
      <tr>
        <td class="style1"><div align="center"><a href="#Please">Offer details. Important! Please read.</a></div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="650" border="0" align="center" cellpadding="1">
      <tr>
        <td><img src="zion-tours-44h.jpg" alt="Zion tours" width="214" height="144" border="1" /></td>
        <td><img src="bryce-canyon-tour-100d.jpg" alt="Bryce Canyon tour" width="214" height="144" border="1" /></td>
        <td><img src="zion-tours-100-c.jpg" alt="Zion tours" width="214" height="144" border="1" /></td>
      </tr>
    </table>    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="style8"><span class="style8"><a name="Please" id="Please"></a><strong>Please read the following. It is important!</strong></span></td>
  </tr>
  <tr>
    <td class="style8"><div align="justify">This offer is subject to availability on both the Grand Canyon and the Bryce Canyon/Zion tours, on the dates you have ordered them. Availability is not guaranteed. The fact that the system allows you to order does not mean that your order is confirmed. </div></td>
  </tr>
  <tr>
    <td class="style8"><div align="justify">If you leave it to a couple of days prior to the first tour date, the chances are there will not be availability.</div></td>
  </tr>
  <tr>
    <td class="style8"><div align="justify">If there is no availability, your credit card will not be charged unless you agree to a different date for one or both of the tours.</div></td>
  </tr>
  <tr>
    <td class="style9"><div align="justify">Once we confirm your order we will charge your card and we will not under any circumstances accept a cancellation! </div></td>
  </tr>
  <tr>
    <td class="style8"><div align="justify">We reserve the right to switch your tour dates. In other words, if you have ordered the Bryce tour for the 14th of a month and the Grand Canyon tour for the 15th, we reserve the right to give you the Bryce tour on the 15th and the Grand Canyon tour on the 14th. </div></td>
  </tr>
  <tr>
    <td class="style8">&nbsp;</td>
  </tr>
  <tr>
    <td class="style8"><div align="center"><a href="#TOP">TOP</a></div></td>
  </tr>
  <tr>
    <td class="style8">&nbsp;</td>
  </tr>
  <tr>
    <td class="style8">&nbsp;</td>
  </tr>
  <tr>
    <td class="style8">&nbsp;</td>
  </tr>
  <tr>
    <td class="style8">&nbsp;</td>
  </tr>
  <tr>
    <td class="style8">&nbsp;</td>
  </tr>
  <tr>
    <td class="style8">&nbsp;</td>
  </tr>
  <tr>
    <td class="style8">&nbsp;</td>
  </tr>
  <tr>
    <td class="style8">&nbsp;</td>
  </tr>
  <tr>
    <td class="style8">&nbsp;</td>
  </tr>
  <tr>
    <td class="style8">&nbsp;</td>
  </tr>
  <tr>
    <td class="style8">&nbsp;</td>
  </tr>
</table>
<p class="style1">&nbsp;</p>
<p class="style1">&nbsp;</p>
<p class="style1">&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>









<? include('footer.php'); ?>