<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_locations";
require("validate.php");
require("header.php");

if(!isset($_SESSION['locations']['p'])): $_SESSION['locations']['p'] = 1; endif;
if(!isset($_SESSION['locations']['sortby'])): $_SESSION['locations']['sortby'] = "locations.`name`"; endif;
	$sortby = array('locations.`id`'=>'ID','locations.`name`'=>'Name','locations.`desc`'=>'Description');
if(!isset($_SESSION['locations']['sortdir'])): $_SESSION['locations']['sortdir'] = "ASC"; endif;
if(!isset($_SESSION['locations']['limit'])): $_SESSION['locations']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['locations']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['locations']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['locations']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['locations']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['locations']['p'] = '1';
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	if($_POST['edit'] == "*new*"){
	//INSERT NEW
	$query = 'INSERT INTO `locations`(`name`,`desc`,`address`,`order_weight`,`ez_start`,`ez_destination`,`timezone_id`,`stop_lat`,`stop_lon`,`stop_url`)'; //,`timezone`,`dst`
		$query .= ' VALUES("'.$_POST['name'].'","'.$_POST['desc'].'","'.$_POST['address'].'","'.$_POST['order_weight'].'","'.$_POST['ez_start'].'","'.$_POST['ez_destination'].'","'.$_POST['timezone_id'].'","'.$_POST['stop_lat'].'","'.$_POST['stop_lon'].'","'.$_POST['stop_url'].'")'; //,"'.$_POST['timezone'].'","'.$_POST['dst'].'"
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new location "'.$_POST['name'].'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

	} else {
	//UPDATE
	$query = 'UPDATE `locations` SET `name` = "'.$_POST['name'].'", `desc` = "'.$_POST['desc'].'", `address` = "'.$_POST['address'].'", `order_weight` = "'.$_POST['order_weight'].'", `ez_start` = "'.$_POST['ez_start'].'", `ez_destination` = "'.$_POST['ez_destination'].'", `timezone_id` = "'.$_POST['timezone_id'].'", `stop_lat` = "'.$_POST['stop_lat'].'", `stop_lon` = "'.$_POST['stop_lon'].'", `stop_url` = "'.$_POST['stop_url'].'"'; //, `timezone` = "'.$_POST['timezone'].'", `dst` = "'.$_POST['dst'].'"
		$query .= ' WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Saved location "'.$_POST['name'].'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

	}

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_POST['selitems']) && count($_POST['selitems']) > 0){
/*
	$query = 'DELETE FROM `locations` WHERE `id` = "'.implode('" OR `id` = "',$_POST['selitems']).'"';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,mysql_affected_rows().' locations were deleted.'); else: array_push($errormsg,$thiserror); endif;
*/
}




echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Stops/Locations</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);



if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){

if($_REQUEST['edit'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['edit'] == "*new*"){
	$fillform = array(
		'id' => '*new*',
		'timezone' => '5'
		);
	} else {
	$query = 'SELECT * FROM `locations` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
	$result = mysql_query($query);
	$fillform = mysql_fetch_assoc($result);
	}

//GET TIMEZONES
$timezones = array();
$query = 'SELECT * FROM `timezones` ORDER BY `offset_std` DESC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$timezones['t'.$row['id']] = $row;
	}



bgcolor('');

echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";

echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Name</TD><TD><INPUT TYPE="text" NAME="name" STYLE="width:400px;" VALUE="'.getval('name').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Description</TD><TD><TEXTAREA NAME="desc" STYLE="width:400px; height:80px;">'.getval('desc').'</TEXTAREA></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Address</TD><TD><TEXTAREA NAME="address" STYLE="width:400px; height:90px;">'.getval('address').'</TEXTAREA></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">EZ Order weight</TD><TD><SELECT ID="order_weight" NAME="order_weight">';
	$oweights = array(
		"1" => "1 - Highest priority",
		"2" => "2",
		"3" => "3 - High priority",
		"4" => "4",
		"5" => "5 - Normal priority",
		"6" => "6",
		"7" => "7 - Low priority",
		"8" => "8",
		"9" => "9 - Lowest priority"
		);
	foreach($oweights as $key => $val){
		echo '<OPTION VALUE="'.$key.'"';
		if($key == getval('order_weight')): echo ' SELECTED'; endif;
		echo '>'.$val.'</OPTION>';
		}
	echo '</SELECT></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">EZ Starting City<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Include as a starting city on EZ Tour Finder?</SPAN></TD><TD><SELECT NAME="ez_start">';
		echo '<OPTION VALUE="1"'; if(getval('ez_start') == "1"): echo ' SELECTED'; endif; echo '>Yes</OPTION>';
		echo '<OPTION VALUE="0"'; if(getval('ez_start') == "0"): echo ' SELECTED'; endif; echo '>No</OPTION>';
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">EZ Destination<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Include as a destination on EZ Tour Finder?</SPAN></TD><TD><SELECT NAME="ez_destination">';
		echo '<OPTION VALUE="1"'; if(getval('ez_destination') == "1"): echo ' SELECTED'; endif; echo '>Yes</OPTION>';
		echo '<OPTION VALUE="0"'; if(getval('ez_destination') == "0"): echo ' SELECTED'; endif; echo '>No</OPTION>';
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Time zone</TD><TD><SELECT NAME="timezone_id" STYLE="width:200px;">';
	    $timezone_identifiers = DateTimeZone::listIdentifiers();
	    foreach( $timezone_identifiers as $value ){
	        if ( preg_match( '/^(America)\//', $value ) ){
	            $ex=explode("/",$value,2);//obtain continent,city   
	            if ($continent!=$ex[0]){
	                if ($continent!="") echo '</optgroup>';
	                echo '<optgroup label="'.$ex[0].'">';
	            }
	   
	            $city=$ex[1];
	            $continent=$ex[0];
	            echo '<option value="'.$value.'"';
				if(getval('timezone_id') == $value): echo ' SELECTED'; endif;
				echo '>'.$city.'</option>';               
	        }
	    }
		echo '</SELECT></TD></TR>'."\n";
	/*echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Time zone</TD><TD><SELECT NAME="timezone" STYLE="width:200px;">';
		foreach($timezones as $timezone){
		echo '<OPTION VALUE="'.$timezone['id'].'"';
		if(getval('timezone') == $timezone['id']): echo ' SELECTED'; endif;
		echo '>'.$timezone['name'].'&nbsp;&nbsp;('.$timezone['abbr_std'].'/'.$timezone['abbr_dst'].')</OPTION>';
		}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Observe DST<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Does this location observe daylight savings time?</SPAN></TD><TD><SELECT NAME="dst">';
		echo '<OPTION VALUE="1"'; if(getval('dst') == "1"): echo ' SELECTED'; endif; echo '>Yes</OPTION>';
		echo '<OPTION VALUE="0"'; if(getval('dst') == "0"): echo ' SELECTED'; endif; echo '>No</OPTION>';
		echo '</SELECT></TD></TR>'."\n";*/
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Latitude</TD><TD><INPUT TYPE="text" NAME="stop_lat" STYLE="width:100px;" VALUE="'.getval('stop_lat').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Longitude</TD><TD><INPUT TYPE="text" NAME="stop_lon" STYLE="width:100px;" VALUE="'.getval('stop_lon').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">URL</TD><TD><INPUT TYPE="text" NAME="stop_url" STYLE="width:400px;" VALUE="'.getval('stop_url').'"></TD></TR>'."\n";
	echo '</TABLE><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";


} else {


?><SCRIPT><!--

function selectall(){
	i=0;
	while(document.getElementById("sel"+i)){
		document.getElementById("sel"+i).checked = document.getElementById("selall").checked;
		i++;
		}
}

function del(){
	var r=confirm("Delete checked locations?");
	return r;
}

//--></SCRIPT><?


//GET LOCATIONS
$locations = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS locations.*, timezones.`name` AS `timezone` FROM `locations`';
	$query .= ' LEFT JOIN `timezones` ON locations.`timezone` = timezones.`id`';
	$query .= ' ORDER BY '.$_SESSION['locations']['sortby'].' '.$_SESSION['locations']['sortdir'].', locations.`name` ASC';
	$query .= ' LIMIT '.(($_SESSION['locations']['p']-1)*$_SESSION['locations']['limit']).','.$_SESSION['locations']['limit'];
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($locations,$row);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['locations']['limit']);
if($numpages > 0 && $_SESSION['locations']['p'] > $numpages): $_SESSION['locations']['p'] = $numpages; endif;


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['locations']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['locations']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['locations']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['locations']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['locations']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['locations']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['locations']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['locations']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['locations']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Name</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Description</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Time Zone</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($locations as $key => $row){
	if($row['timezone_id'] != ""){ $row['timezone'] = $row['timezone_id']; }
echo '<TR STYLE="background:#'.bgcolor('').'">';
	echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'">'.$row['name'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:9pt;">'.$row['desc'].'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:9pt;">'.$row['timezone'].'</TD>';
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['locations']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['locations']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['locations']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['locations']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['locations']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR><INPUT TYPE="button" VALUE="New Location" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" VALUE="Delete Selected" STYLE="color:#FF0000;" DISABLED>';
	echo '<BR><BR>'."\n\n";

} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";



//echo '</CENTER>';
//$timezone_abbreviations = DateTimeZone::listAbbreviations();
//echo '<PRE>'; print_r($timezone_abbreviations); echo '</PRE>';

//$timezone_identifiers = DateTimeZone::listIdentifiers();
//echo '<PRE>'; print_r($timezone_identifiers); echo '</PRE>';



require("footer.php");

?>