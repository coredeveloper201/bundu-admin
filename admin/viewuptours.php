<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

//$_REQUEST['print'] = "y";

if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	$pageid = "3_viewuptours";
	require("validate.php");
	}

if(!isset($_REQUEST['start']) || trim($_REQUEST['start']) == ""){ $_REQUEST['start'] = $today; }
if(!isset($_REQUEST['end']) || trim($_REQUEST['end']) == ""){ $_REQUEST['end'] = strtotime("+ 3 months"); }
if($_REQUEST['end'] < $_REQUEST['start']){ $_REQUEST['end'] = $_REQUEST['start']; }

$assoc = array();
$query = '(SELECT reservations_assoc.`date` AS `startdate`, reservations_assoc.`tourid`, SUM(reservations_assoc.`adults`+reservations_assoc.`seniors`+reservations_assoc.`children`) AS `totalpax`,
		(SELECT MAX(tours_assoc.`day`) FROM `tours_assoc` WHERE tours_assoc.`tourid` = reservations_assoc.`tourid` LIMIT 1) AS `numdays`, tours.`title` AS `tourtitle`,
		driver_info.`driver` AS `driver_info`, tours_dates.`id` AS `dateid`, tours_dates.`confirmed` AS `confirmed`,
		GROUP_CONCAT(reservations_assoc.reservation) AS res_ids
		#, (SELECT tours_dates.`dir` FROM `tours_dates` WHERE tours_dates.`tourid` = reservations_assoc.`tourid` AND reservations_assoc.`date` = tours_dates.`date` LIMIT 1) AS `dir`
	FROM `reservations_assoc`
	JOIN `reservations` ON reservations_assoc.`reservation` = reservations.`id`
	#LEFT JOIN `tours_assoc` ON reservations_assoc.`tourid` = tours_assoc.`tourid`
	LEFT JOIN `tours` ON reservations_assoc.`tourid` = tours.`id`
	LEFT JOIN `driver_info` ON reservations_assoc.`date` = driver_info.`date` AND reservations_assoc.`tourid` = driver_info.`tourid`
	LEFT JOIN `tours_dates` ON reservations_assoc.`date` = tours_dates.`date` AND reservations_assoc.`tourid` = tours_dates.`tourid`
	WHERE (reservations_assoc.`canceled` = 0 OR tours_dates.`confirmed` = 1) AND (reservations.`canceled` = 0 OR tours_dates.`confirmed` = 1) AND tours.`vendor` = 1
		AND reservations_assoc.`date` >= "'.$_REQUEST['start'].'" AND reservations_assoc.`date` <= "'.$_REQUEST['end'].'"';
		if(isset($_REQUEST['filter']) && trim($_REQUEST['filter']) != "" && is_numeric($_REQUEST['filter'])){
			$query .= ' AND (reservations_assoc.`type` = "t" OR reservations_assoc.`type` = "o") AND reservations_assoc.`tourid` = "'.$_REQUEST['filter'].'"';
			} elseif(isset($_REQUEST['filter']) && trim($_REQUEST['filter']) != "" && $_REQUEST['filter'] != "*all*"){
			$query .= ' AND reservations_assoc.`type` = "'.$_REQUEST['filter'].'"';
			} else {
			$query .= ' AND (reservations_assoc.`type` = "t" OR reservations_assoc.`type` = "o") AND reservations_assoc.`tourid` > 1000';
			}
		$query .= ' GROUP BY reservations_assoc.`date`, reservations_assoc.`tourid`)

	UNION

	(SELECT reservations_assoc.`date` AS `startdate`, reservations_assoc.`id` AS `tourid`, SUM(reservations_assoc.`adults`+reservations_assoc.`seniors`+reservations_assoc.`children`) AS `totalpax`, 1 AS `numdays`, reservations_assoc.`reference` AS `tourtitle`,
		driver_info.`driver` AS `driver_info`, "prv" AS `dateid`, "prv" AS `confirmed`,
		GROUP_CONCAT(reservations_assoc.reservation) AS res_ids
	FROM `reservations_assoc`
	JOIN `reservations` ON reservations_assoc.`reservation` = reservations.`id` AND reservations.`canceled` = 0
	LEFT JOIN `driver_info` ON reservations_assoc.`date` = driver_info.`date` AND reservations_assoc.`id` = driver_info.`tourid`
	WHERE reservations_assoc.`canceled` = 0';
		$query .= ' AND reservations_assoc.`date` >= "'.$_REQUEST['start'].'" AND reservations_assoc.`date` <= "'.$_REQUEST['end'].'"';
		if(isset($_REQUEST['filter']) && trim($_REQUEST['filter']) != "" && is_numeric($_REQUEST['filter'])){
			$query .= ' AND (reservations_assoc.`type` = "t" OR reservations_assoc.`type` = "o") AND reservations_assoc.`tourid` = "'.$_REQUEST['filter'].'"';
			} elseif(isset($_REQUEST['filter']) && trim($_REQUEST['filter']) != "" && $_REQUEST['filter'] != "*all*"){
			$query .= ' AND reservations_assoc.`type` = "'.$_REQUEST['filter'].'"';
			} else {
			$query .= ' AND (reservations_assoc.`type` = "t" OR reservations_assoc.`type` = "o") AND reservations_assoc.`tourid` = 1';
			}
		$query .= ' GROUP BY reservations_assoc.`id`)

	ORDER BY `startdate` ASC, `numdays` ASC, `tourid` ASC';

	if($_SERVER['REMOTE_ADDR'] == "98.255.68.8") {
		//echo '<PRE STYLE="text-align:left;">'.$query.'</PRE>';	
	}

$result = mysql_query($query);
//echo mysql_error();
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($assoc,$row);
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($assoc); echo '</PRE>';


if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	echo '<HTML>'."\n\n";
	echo '<HEAD><TITLE>Schedule</TITLE><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><LINK HREF="stylesheet.css" REL="stylesheet" TYPE="text/css"></HEAD>'."\n\n";
	echo '<BODY BGCOLOR="#FFFFFF" TOPMARGIN="0" LEFTMARGIN="0" onLoad="javascript:window.print();">'."\n\n";
	echo '<CENTER>'."\n\n";
	}


echo '<TABLE BORDER="0" WIDTH="94%" CELLSPACING="0" CELLPADDING="2" STYLE="empty-cells:show">'."\n";

if($num_results == 0){

echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
	echo '<TD ALIGN="left" STYLE="padding:5px; border-bottom:solid 1px #000000; font-family:Arial; font-size:9pt; font-weight:bold;"><A HREF="'.$_SERVER['PHP_SELF'].'?start='.mktime(0,0,0,date("n",$_REQUEST['start']),(date("j",$_REQUEST['start'])-1),date("Y",$_REQUEST['start'])).'">&lt; Previous Day</A></TD>';
	echo '<TD ALIGN="center" STYLE="padding:5px; border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold;">'.date("l, F j, Y",$_REQUEST['start']);
		if($_REQUEST['end'] != $_REQUEST['start']){ echo ' - '.date("l, F j, Y",$_REQUEST['end']); }
		echo '</TD>';
	echo '<TD ALIGN="right" STYLE="padding:5px; border-bottom:solid 1px #000000; font-family:Arial; font-size:9pt; font-weight:bold;"><A HREF="'.$_SERVER['PHP_SELF'].'?start='.mktime(0,0,0,date("n",$_REQUEST['end']),(date("j",$_REQUEST['end'])+1),date("Y",$_REQUEST['end'])).'">Next Day &gt;</A></TD>';
	echo '</TR>'."\n\n";

echo '<TR><TD COLSPAN="3" ALIGN="center" STYLE="font-family:Arial; font-size:12pt;"><I>- No reservations found for this time period. -</I></TD></TR>'."\n\n";

} else {

echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
	echo '<TD ALIGN="center" CLASS="viewsched" STYLE="border-bottom:solid 1px #000000; font-weight:bold;">Start</TD>';
	echo '<TD ALIGN="center" CLASS="viewsched" STYLE="border-bottom:solid 1px #000000; font-weight:bold;">End</TD>';
	echo '<TD ALIGN="center" CLASS="viewsched" STYLE="border-bottom:solid 1px #000000; font-weight:bold;">Tours</TD>';
	//echo '<TD ALIGN="center" CLASS="viewsched" STYLE="border-bottom:solid 1px #000000; font-weight:bold;">Weekdays</TD>';
	echo '<TD ALIGN="center" CLASS="viewsched" STYLE="border-bottom:solid 1px #000000; font-weight:bold;">Pax</TD>';
	echo '<TD ALIGN="center" CLASS="viewsched" STYLE="border-bottom:solid 1px #000000; font-weight:bold;">Driver info.</TD>';
	//echo '<TD ALIGN="center" CLASS="viewsched" STYLE="border-bottom:solid 1px #000000; font-weight:bold;">Notes</TD>';
	echo '<TD ALIGN="center" CLASS="viewsched" STYLE="border-bottom:solid 1px #000000; font-weight:bold;">Confirmed</TD>';
	echo '</TR>'."\n\n";

foreach($assoc as $key => $row){
	$days = ($row['numdays']-1);
	if($days < 0){ $days = 0; }
	$row['enddate'] = strtotime("+ ".$days." days",$row['startdate']);

	echo '<TR STYLE="background:#'.bgcolor('').'">';
		//echo '<td class="viewsched">'.$row['dateid'].'</td>';
		echo '<TD ALIGN="center" VALIGN="top" CLASS="viewsched" STYLE="padding-left:4px;">'.date("n/d/Y",$row['startdate']).'<BR>'.date("D",$row['startdate']).'</TD>';
		echo '<TD ALIGN="center" VALIGN="top" CLASS="viewsched" STYLE="padding-left:4px;">'.date("n/d/Y",$row['enddate']).'<BR>'.date("D",$row['enddate']).'</TD>';
		echo '<TD VALIGN="top" CLASS="viewsched" STYLE="font-size:9pt; padding-left:4px;">';
			if($row['tourid'] < 20000){ echo '['.$row['tourid'].'] '; }
			echo $row['tourtitle'];
				echo '<div style="font-size:10px; font-style:italic;">';
				$resIDs = explode(',', $row['res_ids']);
				$links = array();
				foreach($resIDs as $id_reservation) {
					$links[] = '<a href="/admin/3_reservations.php?view='.$id_reservation.'">'.$id_reservation.'</a>';
				}
				echo implode(', ', $links);
				echo '</div>';
			echo '</TD>';
		//echo '<TD VALIGN="top" CLASS="viewsched" STYLE="padding-left:4px;">Weekdays</TD>';
		echo '<TD ALIGN="right" VALIGN="top" CLASS="viewsched" STYLE="padding-left:4px;">'.$row['totalpax'].'</TD>';
		echo '<TD VALIGN="top" CLASS="viewsched" STYLE="padding-left:8px;">';
			if(!isset($_REQUEST['print']) || $_REQUEST['print'] != "y"){
				echo '<INPUT TYPE="hidden" NAME="date[]" VALUE="'.$row['startdate'].'">';
				echo '<INPUT TYPE="hidden" NAME="tourid[]" VALUE="'.$row['tourid'].'">';
				echo '<TEXTAREA NAME="driver_info[]" STYLE="width:160px; height:30px; font-size:8pt;">'.$row['driver_info'].'</TEXTAREA>';
				} else {
				echo $row['driver_info'];
				}
			echo '</TD>';
		/*echo '<TD VALIGN="top" CLASS="viewsched" STYLE="padding-left:4px;">';
			if(!isset($_REQUEST['print']) || $_REQUEST['print'] != "y"){
				echo '<TEXTAREA STYLE="width:180px; height:100%; min-height:50px;">'.$row['notes'].'</TEXTAREA>';	
				} else {
				echo $row['notes'];
				}
			echo '</TD>';*/
		/*if(!isset($_REQUEST['print']) || $_REQUEST['print'] != "y"){
			echo '<TD VALIGN="top" ALIGN="center" CLASS="viewsched">';
				echo '<A HREF="3_reservations.php?view='.$row['reservation'].'" TITLE="View Reservation"><IMG SRC="img/viewico.gif" BORDER="0"></A>';
				echo '<BR>';
				echo '<A HREF="3_reservations.php?edit='.$row['reservation'].'" TITLE="Edit Reservation"><IMG SRC="img/editico.gif" BORDER="0"></A>';
				echo '</TD>';
			}*/
		echo '<TD ALIGN="center" VALIGN="top" CLASS="viewsched" STYLE="padding-left:8px;">';
			if(!isset($_REQUEST['print']) || $_REQUEST['print'] != "y"){
				if($row['dateid'] == "prv"){
					echo '<img src="img/check.png" alt="check" width="16" height="16" />';
					} else {
					echo '<INPUT TYPE="hidden" NAME="dateids[]" VALUE="'.$row['dateid'].'"><INPUT TYPE="checkbox" NAME="confirmed[]" VALUE="'.$row['dateid'].'"';
					if($row['confirmed'] == 1){ echo ' CHECKED'; }
					echo '>';
					}
				} else {
				if($row['confirmed'] == "1" || $row['confirmed'] == "prv"){ echo '<img src="img/check.png" alt="check" width="16" height="16" />'; }
				}
			echo '</TD>';
		echo '</TR>'."\n";

	} //End ForEach


} //End $num_results if statement

echo '</TABLE><BR>'."\n\n";


if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	echo '</CENTER>'."\n\n";
	echo '</BODY>'."\n\n";
	echo '</HTML>'."\n\n";
	}


?>