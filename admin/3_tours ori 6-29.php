<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

//$_REQUEST['edit'] = 1153; echo 'REMOVE THIS LINE FOR PRODUCTION';

$working = 0;

$pageid = "3_tours";
require("validate.php");
if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""): $onload = array('checkend()'); endif;
require("header.php");

//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($_POST,true)).'</PRE>'; exit;

if(!isset($_SESSION['tours']['p'])): $_SESSION['tours']['p'] = 1; endif;
if(!isset($_SESSION['tours']['filter'])): $_SESSION['tours']['filter'] = "available"; endif;
	$filters = array('available'=>'Available','archived'=>'Archived');
if(!isset($_SESSION['tours']['sortby'])): $_SESSION['tours']['sortby'] = "tours.`id`"; endif;
	$sortby = array('tours.`id`'=>'ID','tours.`title`'=>'Title','tours.`numdays`'=>'Days','upcoming'=>'Dates','vendors.`name`'=>'Vendor');
if(!isset($_SESSION['tours']['sortdir'])): $_SESSION['tours']['sortdir'] = "DESC"; endif;
if(!isset($_SESSION['tours']['showhidden'])): $_SESSION['tours']['showhidden'] = "0"; endif;
if(!isset($_SESSION['tours']['limit'])): $_SESSION['tours']['limit'] = "200"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['tours']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['filter']) && $_REQUEST['filter'] != ""): $_SESSION['tours']['filter'] = $_REQUEST['filter']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['tours']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['tours']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['showhidden']) && $_REQUEST['showhidden'] != ""): $_SESSION['tours']['showhidden'] = $_REQUEST['showhidden']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['tours']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort") {
	if(!isset($_REQUEST['showhidden'])): $_SESSION['tours']['showhidden'] = '0'; endif;
	$_SESSION['tours']['p'] = '1';
}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';
//echo '<PRE>'; print_r($_POST['runs']); echo '</PRE>';

$tourlog = '';
if(isset($_POST) && count($_POST) > 0){
	$tourlog .= "\n".'//!'.date("M j, Y g:ia").' ---------------------------------------------------------------------------------------------------------------------------------------------'."\n\n";
	$tourlog .= print_r($_POST,true)."\n\n";
	}

//!FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){
	if(!isset($_POST['check_complete']) || $_POST['check_complete'] != "1"){
	array_push($errormsg,'The form you submitted was incomplete.  No changes were saved.<BR>To avoid loosing data, be sure to let the page load completely before submitting changes.');
	} else {

	//Find/Format tour info
	$_POST['scity'] = 0;
	$_POST['numdays'] = 1;
	$_POST['days'] = 1;
	$route1 = 0;
	foreach($_POST['tour_id'] as $key => $id){ if($id != "" && $_POST['tour_type'][$key] == 'r' && $_POST['tour_dir'][$key] == 'f'){
		$route1 = $id;
		break;
		}}
	$result = @mysqlQuery('SELECT `dep_loc` FROM `routes` WHERE `id` = "'.$route1.'" LIMIT 1'); $route1 = mysql_fetch_assoc($result);
	if(isset($route1['dep_loc'])){
		$_POST['scity'] = $route1['dep_loc'];
		}
	$key = (count($_POST['tour_day'])-1);
	while(isset($_POST['tour_day'][$key]) && $_POST['tour_day'][$key] > 0){
		if($_POST['tour_dir'][$key] == 'f'){
			$_POST['numdays'] = $_POST['tour_day'][$key];
			if($_POST['numdays'] > 1){ $_POST['days'] = 2; }
			break;
			}
		$key--;
		}
	if(!isset($_POST['hidden']) || $_POST['hidden'] != "1"): $_POST['hidden'] = "0"; endif;
	if(!isset($_POST['pricesheet']) || $_POST['pricesheet'] != "1"): $_POST['pricesheet'] = "0"; endif;
	if(!isset($_POST['toptour']) || $_POST['toptour'] != "1"): $_POST['toptour'] = "0"; endif;
	$_POST['short_desc'] = trim($_POST['short_desc']);
	//$_POST['pricingdesc'] = trim($_POST['pricingdesc']);
	$_POST['highlights'] = trim($_POST['highlights']);
	$_POST['details'] = trim($_POST['details']);
	if(!isset($_POST['zion_warning']) || $_POST['zion_warning'] != "1"){ $_POST['zion_warning'] = "0"; }
	$_POST['pleasenote'] = trim($_POST['pleasenote']);
	$_POST['toptitle'] = trim($_POST['toptitle']);
	$_POST['topdesc'] = trim($_POST['topdesc']);

	if(!isset($_POST['fuelsurcharge']) || @$_POST['fuelsurcharge'] < 0){ $_POST['fuelsurcharge'] = "0"; }
	if(!isset($_POST['getweights']) || $_POST['getweights'] != "y"){ $_POST['getweights'] = 'n'; }
	if(!isset($_POST['getlunch']) || $_POST['getlunch'] != "y"){ $_POST['getlunch'] = 'n'; }
	if(!isset($_POST['getpreftime']) || $_POST['getpreftime'] != "y"){ $_POST['getpreftime'] = 'n'; }

	$ext_pricing = array();
	//$findper = array();
	if(count($_POST['ext_guest']) > 0) {
		foreach($_POST['ext_guest'] as $i => $val) {
			if(is_numeric($_POST['ext_guest'][$i]) && $_POST['ext_guest'][$i] > 0 && is_numeric($_POST['ext_price'][$i]) && $_POST['ext_price'][$i]){
				$_POST['ext_price'][$i] = number_format($_POST['ext_price'][$i],2,'.','');
				array_push($ext_pricing,trim($_POST['ext_guest'][$i]).':'.$_POST['ext_price'][$i]);
				//array_push($findper,array('guest'=>$_POST['ext_guest'][$i],'price'=>$_POST['ext_price'][$i]));
			}
		}
		if(count($ext_pricing) > 0) {
			array_multisort($_POST['ext_guest'], SORT_ASC, $ext_pricing);
		}
	}
	$ext_pricing = implode('|',$ext_pricing);
	//if($_POST['perguest'] == "0" && count($findper) > 0){
	//	array_multisort($_POST['ext_guest'], SORT_ASC, $findper);
	//	$findper = array_shift($findper);
	//	$_POST['perguest'] = ($findper['price']/$findper['guest']);
	//	}

	if($_POST['edit'] == "*new*"){
	//!INSERT NEW -------------------------
	$query = 'INSERT INTO `tours`(`alias`,`title`,`eztitle`,`nickname`,`numdays`,`short_desc`,`scity`,`days`,`perguest`,`single`,`triple`,`quad`,`ext_pricing`,`fuelsurcharge`,`agent1_per`,`agent2_per`,`getweights`,`getpreftime`,`getlunch`,`highlights`,`details`,`youtube_title`,`youtube`,`pleasenote`,`zion_warning`,`bunducosts`,`upsell_headline`,`upsell_desc`,`meta_title`,`meta_desc`,`vendor`,`toptour`,`toptitle`,`topdesc`,`toporder`,`order_weight`,`pricesheet`,`hidden`)';
		$query .= ' VALUES("'.mysql_real_escape_string($_POST['alias']).'","'.mysql_real_escape_string($_POST['title']).'","'.mysql_real_escape_string($_POST['eztitle']).'","'.mysql_real_escape_string($_POST['nickname']).'","'.mysql_real_escape_string($_POST['numdays']).'","'.mysql_real_escape_string($_POST['short_desc']).'","'.mysql_real_escape_string($_POST['scity']).'","'.mysql_real_escape_string($_POST['days']).'","'.mysql_real_escape_string($_POST['perguest']).'","'.mysql_real_escape_string($_POST['single']).'","'.mysql_real_escape_string($_POST['triple']).'","'.mysql_real_escape_string($_POST['quad']).'","'.$ext_pricing.'","'.mysql_real_escape_string($_POST['fuelsurcharge']).'","'.mysql_real_escape_string($_POST['agent1_per']).'","'.mysql_real_escape_string($_POST['agent2_per']).'","'.mysql_real_escape_string($_POST['getweights']).'","'.mysql_real_escape_string($_POST['getpreftime']).'","'.mysql_real_escape_string($_POST['getlunch']).'","'.mysql_real_escape_string($_POST['highlights']).'","'.mysql_real_escape_string($_POST['details']).'","'.mysql_real_escape_string($_POST['youtube_title']).'","'.mysql_real_escape_string($_POST['youtube']).'","'.mysql_real_escape_string($_POST['pleasenote']).'","'.mysql_real_escape_string($_POST['zion_warning']).'","'.mysql_real_escape_string($_POST['bunducosts']).'","'.mysql_real_escape_string($_POST['upsell_headline']).'","'.mysql_real_escape_string($_POST['upsell_desc']).'","'.mysql_real_escape_string($_POST['meta_title']).'","'.mysql_real_escape_string($_POST['meta_desc']).'","'.mysql_real_escape_string($_POST['vendor']).'","'.mysql_real_escape_string($_POST['toptour']).'","'.mysql_real_escape_string($_POST['toptitle']).'","'.mysql_real_escape_string($_POST['topdesc']).'","'.mysql_real_escape_string($_POST['toporder']).'","'.mysql_real_escape_string($_POST['order_weight']).'","'.mysql_real_escape_string($_POST['pricesheet']).'","'.mysql_real_escape_string($_POST['hidden']).'")';
		@mysqlQuery($query);
	$thiserror = $GLOBALS['mysql_error'];
	if($thiserror == ""): $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new tour "'.stripslashes($_POST['title']).'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;
	} else {

	//!UPDATE -------------------------
	$query = 'UPDATE `tours` SET `title` = "'.mysql_real_escape_string($_POST['title']).'", `eztitle` = "'.mysql_real_escape_string($_POST['eztitle']).'", `short_desc` = "'.mysql_real_escape_string($_POST['short_desc']).'", `highlights` = "'.mysql_real_escape_string($_POST['highlights']).'", `details` = "'.mysql_real_escape_string($_POST['details']).'", `youtube_title` = "'.mysql_real_escape_string($_POST['youtube_title']).'", `pleasenote` = "'.mysql_real_escape_string($_POST['pleasenote']).'", `upsell_headline` = "'.mysql_real_escape_string($_POST['upsell_headline']).'", `upsell_desc` = "'.mysql_real_escape_string($_POST['upsell_desc']).'", `meta_title` = "'.mysql_real_escape_string($_POST['meta_title']).'", `meta_desc` = "'.mysql_real_escape_string($_POST['meta_desc']).'", `toptitle` = "'.mysql_real_escape_string($_POST['toptitle']).'", `topdesc` = "'.mysql_real_escape_string($_POST['topdesc']).'"';
		$query .= ' WHERE `id` = "'.mysql_real_escape_string($_POST['edit']).'" LIMIT 1';
		@mysqlQuery($query);
	$thiserror = $GLOBALS['mysql_error'];
	$thisaffected = mysql_affected_rows();
		if($thisaffected > 0){ array_push($successmsg,'Tour '.$_POST['edit'].' has been marked for translation updates.'); }
	if($thiserror != ""){ array_push($errormsg,$thiserror); }

	$query = 'UPDATE `tours` SET `alias` = "'.mysql_real_escape_string($_POST['alias']).'", `nickname` = "'.mysql_real_escape_string($_POST['nickname']).'", `numdays` = "'.mysql_real_escape_string($_POST['numdays']).'", `scity` = "'.mysql_real_escape_string($_POST['scity']).'", `days` = "'.mysql_real_escape_string($_POST['days']).'", `perguest` = "'.mysql_real_escape_string($_POST['perguest']).'", `single` = "'.mysql_real_escape_string($_POST['single']).'", `triple` = "'.mysql_real_escape_string($_POST['triple']).'", `quad` = "'.mysql_real_escape_string($_POST['quad']).'", `ext_pricing` = "'.$ext_pricing.'", `fuelsurcharge` = "'.mysql_real_escape_string($_POST['fuelsurcharge']).'", `agent1_per` = "'.mysql_real_escape_string($_POST['agent1_per']).'", `agent2_per` = "'.mysql_real_escape_string($_POST['agent2_per']).'", `getweights` = "'.mysql_real_escape_string($_POST['getweights']).'", `getpreftime` = "'.mysql_real_escape_string($_POST['getpreftime']).'", `getlunch` = "'.mysql_real_escape_string($_POST['getlunch']).'", `youtube` = "'.mysql_real_escape_string($_POST['youtube']).'", `zion_warning` = "'.mysql_real_escape_string($_POST['zion_warning']).'", `bunducosts` = "'.mysql_real_escape_string($_POST['bunducosts']).'", `vendor` = "'.mysql_real_escape_string($_POST['vendor']).'", `toptour` = "'.mysql_real_escape_string($_POST['toptour']).'", `toporder` = "'.mysql_real_escape_string($_POST['toporder']).'", `order_weight` = "'.mysql_real_escape_string($_POST['order_weight']).'", `pricesheet` = "'.mysql_real_escape_string($_POST['pricesheet']).'", `hidden` = "'.mysql_real_escape_string($_POST['hidden']).'"';
		if($thisaffected > 0){ $query .= ', `eng_modified` = CURRENT_TIMESTAMP'; }
		$query .= ' WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
		@mysqlQuery($query);
	$thiserror = $GLOBALS['mysql_error'];
	if($thiserror == ""): array_push($successmsg,'Saved tour "'.stripslashes($_POST['title']).'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;
	}

	$tourlog .= $query."\n";
	$tourlog .= $thiserror."\n";

	//!TOUR STEPS/OPTIONS -------------------------
	@mysqlQuery('DELETE FROM `tours_assoc` WHERE `tourid` = "'.$_REQUEST['edit'].'"');
	if(isset($_POST['tour_id'])){
	foreach($_POST['tour_id'] as $key => $id){ if($id != ""){
		$query = 'INSERT INTO `tours_assoc`(`tourid`,`dir`,`type`,`typeid`,`adjust`,`content`,`day`,`order`) VALUES("'.mysql_real_escape_string($_REQUEST['edit']).'","'.mysql_real_escape_string($_POST['tour_dir'][$key]).'","'.mysql_real_escape_string($_POST['tour_type'][$key]).'","'.mysql_real_escape_string($_POST['tour_id'][$key]).'","'.mysql_real_escape_string($_POST['tour_adj'][$key]).'","'.mysql_real_escape_string(htmlspecialchars($_POST['tour_content'][$key],ENT_COMPAT,'UTF-8',0)).'","'.mysql_real_escape_string($_POST['tour_day'][$key]).'","'.mysql_real_escape_string($key).'")';
		@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror != ""): array_push($errormsg,$thiserror); endif;
		$tourlog .= $query."\n";
		$tourlog .= $thiserror."\n";
		}} //End foreach
		} //End if statement

	//!UPDATE DATES -------------------------
	if(isset($_POST['runs']) && count($_POST['runs']) > 0){ if((include_once 'sups/tourvaliddates.php') !== false && (include_once 'sups/dates.php') !== false){

		//Separate dates into working categories
		$deldates = array();
		$adddates = array('f'=>array(),'r'=>array());
		foreach($_POST['runs'] as $run){
			$run = explode('|',$run);
			$date = mktime(0,0,0,$run[1],$run[2],$run[0]);
				if($run[3] == "0"){
					array_push($deldates,$date);
				} elseif(strtolower($run[3]) == "r") {
					array_push($adddates['r'],$date);
				} else {
					array_push($adddates['f'],$date);
				}
			}

		//Find all valid dates for tour based on routes
		$testroutes = array('f'=>array(),'r'=>array());
		if(isset($_POST['tour_id'])){
		foreach($_POST['tour_id'] as $key => $id){ if($id != "" && $_POST['tour_type'][$key] == "r"){
			$addroute = array('typeid'=>$_POST['tour_id'][$key],'day'=>$_POST['tour_day'][$key]);
			array_push($testroutes[$_POST['tour_dir'][$key]],$addroute);
			}} //End foreach
			} //End if statement
		$validdates = tourvaliddates($_REQUEST['edit'],$testroutes,'n');

		//Validate and add or set pending approval
		$numadded = 0;
		$appdates = array('f'=>array(),'r'=>array());
		foreach($adddates as $dir => $dates) {
			foreach($dates as $date) {
				if((count($testroutes['f'])+count($testroutes['r'])) == 0 || in_array($date,$validdates[$dir])){
					$query = 'INSERT INTO `tours_dates`(`tourid`,`date`,`dir`) VALUES("'.mysql_real_escape_string($_REQUEST['edit']).'","'.mysql_real_escape_string($date).'","'.mysql_real_escape_string($dir).'") ON DUPLICATE KEY UPDATE `dir` = "'.mysql_real_escape_string($dir).'"';
					@mysqlQuery($query);
					$thiserror = $GLOBALS['mysql_error'];
					if(mysql_affected_rows() > 0){ $numadded++; }
					if($thiserror != ""){ array_push($errormsg,$thiserror); }
					$tourlog .= $query."\n";
					$tourlog .= $thiserror."\n";

				} elseif($date >= strtotime("today")) {
					array_push($appdates[$dir], $date);
				}
			}
		}
		if($numadded > 0){ array_push($successmsg,$numadded.' dates were added for tour '.$_REQUEST['edit'].'.'); }

		//DELETE DATES
		$tour_was_using = array();
		if(count($deldates) > 0){
		$tour_was_using = tour_using($_REQUEST['edit'],$deldates);
		$result = rmv_tour_dates($_REQUEST['edit'],$deldates);
			//if($result['affected'] > 0){ array_push($successmsg,$result['affected'].' dates were removed.'); }
			$successmsg = array_merge($successmsg,$result['success']);
			$errormsg = array_merge($errormsg,$result['error']);
			}

		//$numadded = 0;
		//foreach($adddates as $dir => $dates){
		//$result = add_tour_dates($_REQUEST['edit'],$dir,$dates);
		//	$numadded = ($numadded + $result['affected']);
		//	$successmsg = array_merge($successmsg,$result['success']);
		//	$errormsg = array_merge($errormsg,$result['error']);
		//	}

	} else {
		array_push($errormsg,'Unable to update tour dates.  Could not load date processing functions.');
	} } //End count $_POST['runs'] if statement

	//!TRANS TYPES -------------------------
	@mysqlQuery('DELETE FROM `tours_transtypes_assoc` WHERE `tourid` = "'.mysql_real_escape_string($_REQUEST['edit']).'"');
	if(isset($_POST['means'])){
	foreach($_POST['means'] as $id){ if($id != ""){
		$query = 'INSERT INTO `tours_transtypes_assoc`(`tourid`,`transid`) VALUES("'.mysql_real_escape_string($_REQUEST['edit']).'","'.mysql_real_escape_string($id).'") ON DUPLICATE KEY UPDATE `transid` = "'.mysql_real_escape_string($id).'"';
		@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror != ""): array_push($errormsg,$thiserror); endif;
		$tourlog .= $query."\n";
		$tourlog .= $thiserror."\n";
		}} //End foreach
		} //End if statement

	//!SIMILAR TOURS -------------------------
	@mysqlQuery('DELETE FROM `tours_similar` WHERE `tourid` = "'.mysql_real_escape_string($_REQUEST['edit']).'"');
	if(isset($_POST['similar'])){
	foreach($_POST['similar'] as $order => $id){ if($id != ""){
		$query = 'INSERT INTO `tours_similar`(`tourid`,`simid`,`order`) VALUES("'.mysql_real_escape_string($_REQUEST['edit']).'","'.mysql_real_escape_string($id).'","'.mysql_real_escape_string($order).'") ON DUPLICATE KEY UPDATE `order` = "'.mysql_real_escape_string($order).'"';
		@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror != ""): array_push($errormsg,$thiserror); endif;
		$tourlog .= $query."\n";
		$tourlog .= $thiserror."\n";
		}} //End foreach
		} //End if statement

	//!UPSELL TOURS -------------------------
	@mysqlQuery('DELETE FROM `tours_upsell` WHERE `tourid` = "'.mysql_real_escape_string($_REQUEST['edit']).'"');
	if(isset($_POST['upsell'])){
	foreach($_POST['upsell'] as $order => $id){ if($id != ""){
		$query = 'INSERT INTO `tours_upsell`(`tourid`,`upsellid`,`discount`,`order`) VALUES("'.mysql_real_escape_string($_REQUEST['edit']).'","'.mysql_real_escape_string($id).'","'.mysql_real_escape_string($_POST['upsell_discount'][$order]).'","'.mysql_real_escape_string($order).'") ON DUPLICATE KEY UPDATE `discount` = "'.mysql_real_escape_string(@$_POST['upsell_discount'][$order]).'", `order` = "'.mysql_real_escape_string($order).'"';
		@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror != ""): array_push($errormsg,$thiserror); endif;
		$tourlog .= $query."\n";
		$tourlog .= $thiserror."\n";
		}} //End foreach
		} //End if statement

	//!IMAGES -------------------------
	@mysqlQuery('DELETE FROM `images_assoc_tours` WHERE `tourid` = "'.mysql_real_escape_string($_REQUEST['edit']).'"');
	if(isset($_POST['images'])){
	foreach($_POST['images'] as $order => $id){ if($id != ""){
		$query = 'INSERT INTO `images_assoc_tours`(`tourid`,`imgid`,`order`) VALUES("'.mysql_real_escape_string($_REQUEST['edit']).'","'.mysql_real_escape_string($id).'","'.mysql_real_escape_string($order).'") ON DUPLICATE KEY UPDATE `order` = "'.mysql_real_escape_string($order).'"';
		@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror != ""): array_push($errormsg,$thiserror); endif;
		$tourlog .= $query."\n";
		$tourlog .= $thiserror."\n";
		}} //End foreach
		} //End if statement

	} //End check complete
} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "dates" && isset($_POST['edit']) && $_POST['edit'] != ""){

	//!APPROVED DATES
	@include_once('sups/dates.php');
	//echo '<PRE>'; print_r($_POST); echo '</PRE>';
	if(isset($_POST['submit']) && $_POST['submit'] == "Approve selected"){ $go = false; } else { $go = true; }

	//Add approved
	$adddates = array('f'=>array(),'r'=>array());
	if(isset($_POST['app_dates'])){
		foreach($_POST['app_dates'] as $key => $date){
			if($go || $_POST['choice_'.$date] == "y"){
					array_push($adddates[$_POST['app_dir'][$key]],$date);
				}
			}
		}
	$numadded = 0; $numradded = 0;
	foreach($adddates as $dir => $dates){
	$result = add_tour_dates($_POST['edit'],$dir,$dates,1);
		$numadded = ($numadded + $result['affected']);
		$numradded = ($numradded + $result['routes_affected']);
		//$successmsg = array_merge($successmsg,$result['success']);
		$errormsg = array_merge($errormsg,$result['error']);
		}
	if($numradded > 0){ array_push($successmsg,$numradded.' route dates were added.'); }
	if($numadded > 0){ array_push($successmsg,$numadded.' dates were added for tour '.$_REQUEST['edit'].'.'); }

	//Remove approved route dates
	$numremoved = 0;
	if(isset($_POST['del_tourdate'])){
		foreach($_POST['del_tourdate'] as $key => $date){
			if($go || $_POST['choice_'.$date] == "y"){
			$result = rmv_route_dates($_POST['del_routeid'][$key],$_POST['del_routedate'][$key]);
				$numremoved = ($numremoved + $result['affected']);
				//$successmsg = array_merge($successmsg,$result['success']);
				$errormsg = array_merge($errormsg,$result['error']);
				}
			}
		}
	if($numremoved > 0){ array_push($successmsg,$numremoved.' unused route dates were removed.'); }

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "copy" && isset($_POST['edit']) && $_POST['edit'] != ""){

	$old = array();
	//COPY BASIC INFO
	if(isset($_POST['basic']) && $_POST['basic'] == "y"){
		//GET OLD INFO
		$query = 'SELECT * FROM `tours` WHERE `id` = "'.mysql_real_escape_string($_POST['edit']).'" LIMIT 1';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
		$old = @mysql_fetch_assoc($result);
		unset($old['id']);
		$old['alias'] = 'COPY['.rand(0,999).'] '.$old['alias'];
		$old['title'] = 'COPY: '.$old['title'];
		}
	$old_keys = array();
	$old_values = array();
	foreach($old as $key => $value){
		array_push($old_keys,$key);
		array_push($old_values,str_replace('"','\"',$value));
		}
	$query = 'INSERT INTO `tours`(';
		if(count($old_keys) > 0): $query .= '`'.implode('`,`',$old_keys).'`'; endif;
		$query .= ') VALUES(';
		if(count($old_values) > 0): $query .= '"'.implode('","',$old_values).'"'; endif;
		$query .= ')';
		@mysqlQuery($query);
		$newid = mysql_insert_id();
		$thiserror = $GLOBALS['mysql_error'];

	if($thiserror != ""){
		array_push($errormsg,$thiserror);
	} else {
		array_push($successmsg,'Saved new tour "'.$old['title'].'" ('.$newid.').');

	//COPY ASSOC
		if(isset($_POST['assoc']) && $_POST['assoc'] == "y"){
		$numsaved = 0;
		$old = array();
		$query = 'SELECT * FROM `tours_assoc` WHERE `tourid` = "'.$_POST['edit'].'"';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = @mysql_fetch_assoc($result);
			unset($row['id']);
			$row['tourid'] = $newid;
			array_push($old,$row);
			} //End for loop
		foreach($old as $row){
			$old_keys = array();
			$old_values = array();
			foreach($row as $key => $value){
				array_push($old_keys,$key);
				array_push($old_values,str_replace('"','\"',$value));
				}
			$query = 'INSERT INTO `tours_assoc`(`'.implode('`,`',$old_keys).'`) VALUES("'.implode('","',$old_values).'")';
				@mysqlQuery($query);
				$thiserror = $GLOBALS['mysql_error'];
				if($thiserror == ""): $numsaved=($numsaved+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
			} //End foreach
		if($numsaved > 0): array_push($successmsg,$numsaved.' associated routes were copied.'); endif;
		} //End dates if statement

	//COPY IMAGES
		if(isset($_POST['images']) && $_POST['images'] == "y"){
		$numsaved = 0;
		$old = array();
		$query = 'SELECT * FROM `images_assoc_tours` WHERE `tourid` = "'.$_POST['edit'].'"';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = @mysql_fetch_assoc($result);
			$row['tourid'] = $newid;
			array_push($old,$row);
			} //End for loop
		foreach($old as $row){
			$old_keys = array();
			$old_values = array();
			foreach($row as $key => $value){
				array_push($old_keys,$key);
				array_push($old_values,str_replace('"','\"',$value));
				}
			$query = 'INSERT INTO `images_assoc_tours`(`'.implode('`,`',$old_keys).'`) VALUES("'.implode('","',$old_values).'")';
				@mysqlQuery($query);
				$thiserror = $GLOBALS['mysql_error'];
				if($thiserror == ""): $numsaved=($numsaved+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
			} //End foreach
		if($numsaved > 0): array_push($successmsg,$numsaved.' tour images were copied.'); endif;
		} //End dates if statement

	//COPY DATES
		if(isset($_POST['dates']) && $_POST['dates'] == "y"){
		$numsaved = 0;
		$old = array();
		$query = 'SELECT * FROM `tours_dates` WHERE `tourid` = "'.$_POST['edit'].'"';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = @mysql_fetch_assoc($result);
			unset($row['id']);
			$row['tourid'] = $newid;
			array_push($old,$row);
			} //End for loop
		foreach($old as $row){
			$old_keys = array();
			$old_values = array();
			foreach($row as $key => $value){
				array_push($old_keys,$key);
				array_push($old_values,str_replace('"','\"',$value));
				}
			$query = 'INSERT INTO `tours_dates`(`'.implode('`,`',$old_keys).'`) VALUES("'.implode('","',$old_values).'")';
				@mysqlQuery($query);
				$thiserror = $GLOBALS['mysql_error'];
				if($thiserror == ""): $numsaved=($numsaved+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
			} //End foreach
		if($numsaved > 0): array_push($successmsg,$numsaved.' dates were copied.'); endif;
		} //End dates if statement

	//COPY SIMILAR TOURS
		if(isset($_POST['similar']) && $_POST['similar'] == "y"){
		$numsaved = 0;
		$old = array();
		$query = 'SELECT * FROM `tours_similar` WHERE `tourid` = "'.$_POST['edit'].'"';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = @mysql_fetch_assoc($result);
			$row['tourid'] = $newid;
			array_push($old,$row);
			} //End for loop
		foreach($old as $row){
			$old_keys = array();
			$old_values = array();
			foreach($row as $key => $value){
				array_push($old_keys,$key);
				array_push($old_values,str_replace('"','\"',$value));
				}
			$query = 'INSERT INTO `tours_similar`(`'.implode('`,`',$old_keys).'`) VALUES("'.implode('","',$old_values).'")';
				@mysqlQuery($query);
				$thiserror = $GLOBALS['mysql_error'];
				if($thiserror == ""): $numsaved=($numsaved+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
			} //End foreach
		if($numsaved > 0): array_push($successmsg,$numsaved.' similar tour associations were copied.'); endif;
		} //End dates if statement

	//COPY UPSELL TOURS
		if(isset($_POST['upsell']) && $_POST['upsell'] == "y"){
		$numsaved = 0;
		$old = array();
		$query = 'SELECT * FROM `tours_upsell` WHERE `tourid` = "'.$_POST['edit'].'"';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = @mysql_fetch_assoc($result);
			$row['tourid'] = $newid;
			array_push($old,$row);
			} //End for loop
		foreach($old as $row){
			$old_keys = array();
			$old_values = array();
			foreach($row as $key => $value){
				array_push($old_keys,$key);
				array_push($old_values,str_replace('"','\"',$value));
				}
			$query = 'INSERT INTO `tours_upsell`(`'.implode('`,`',$old_keys).'`) VALUES("'.implode('","',$old_values).'")';
				@mysqlQuery($query);
				$thiserror = $GLOBALS['mysql_error'];
				if($thiserror == ""): $numsaved=($numsaved+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
			} //End foreach
		if($numsaved > 0): array_push($successmsg,$numsaved.' upsell tour associations were copied.'); endif;
		} //End dates if statement

	//COPY MEANS OF TRANSPORTATION
		if(isset($_POST['transtypes']) && $_POST['transtypes'] == "y"){
		$numsaved = 0;
		$old = array();
		$query = 'SELECT * FROM `tours_transtypes_assoc` WHERE `tourid` = "'.$_POST['edit'].'"';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = @mysql_fetch_assoc($result);
			$row['tourid'] = $newid;
			array_push($old,$row);
			} //End for loop
		foreach($old as $row){
			$old_keys = array();
			$old_values = array();
			foreach($row as $key => $value){
				array_push($old_keys,$key);
				array_push($old_values,str_replace('"','\"',$value));
				}
			$query = 'INSERT INTO `tours_transtypes_assoc`(`'.implode('`,`',$old_keys).'`) VALUES("'.implode('","',$old_values).'")';
				@mysqlQuery($query);
				$thiserror = $GLOBALS['mysql_error'];
				if($thiserror == ""): $numsaved=($numsaved+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
			} //End foreach
		if($numsaved > 0): array_push($successmsg,$numsaved.' means of transportation were copied.'); endif;
		} //End dates if statement

	//COPY TRANSLATIONS
		if(isset($_POST['translations']) && $_POST['translations'] == "y"){
		$numsaved = 0;
		$old = array();
		$query = 'SELECT * FROM `tours_translations` WHERE `tourid` = "'.$_POST['edit'].'"';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = @mysql_fetch_assoc($result);
			unset($row['id']);
			$row['tourid'] = $newid;
			array_push($old,$row);
			} //End for loop
		foreach($old as $row){
			$old_keys = array();
			$old_values = array();
			foreach($row as $key => $value){
				array_push($old_keys,$key);
				array_push($old_values,str_replace('"','\"',$value));
				}
			$query = 'INSERT INTO `tours_translations`(`'.implode('`,`',$old_keys).'`) VALUES("'.implode('","',$old_values).'")';
				@mysqlQuery($query);
				$thiserror = $GLOBALS['mysql_error'];
				if($thiserror == ""): $numsaved=($numsaved+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
			} //End foreach
		if($numsaved > 0): array_push($successmsg,$numsaved.' translations were copied.'); endif;
		} //End dates if statement

	$_REQUEST['edit'] = $newid;
	} //End initial new route error if statement

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "archive" && isset($_POST['edit']) && $_POST['edit'] != ""){

	//Clear old archived entries for tour
	@mysqlQuery('DELETE images_assoc_itinerary_archive FROM `itinerary_archive`,`images_assoc_itinerary_archive` WHERE itinerary_archive.`tourid` = "'.$_POST['edit'].'" AND images_assoc_itinerary_archive.`itid` = itinerary_archive.`id`');
		$thiserror = $GLOBALS['mysql_error']; if($thiserror != ""){ array_push($errormsg,$thiserror); }
	@mysqlQuery('DELETE itinerary_archive_translations FROM `itinerary_archive`,`itinerary_archive_translations` WHERE itinerary_archive.`tourid` = "'.$_POST['edit'].'" AND itinerary_archive_translations.`itid` = itinerary_archive.`id`');
		$thiserror = $GLOBALS['mysql_error']; if($thiserror != ""){ array_push($errormsg,$thiserror); }
	@mysqlQuery('DELETE FROM `itinerary_archive` WHERE `tourid` = "'.$_POST['edit'].'"');
		$thiserror = $GLOBALS['mysql_error']; if($thiserror != ""){ array_push($errormsg,$thiserror); }

	function fromsecs($secs=0){
		$out = array('h'=>0,'m'=>0,'s'=>0);
		$out['h'] = intval($secs / 3600);
			$secs_left = ($secs - ($out['h'] * 3600));
		$out['m'] = intval($secs_left / 60);
		$out['s'] = ($secs_left - ($out['m'] * 60));
		return $out;
		}

	//Build itinerary as it would appear on the site
	$itinerary = array();
	$query = 'SELECT tours_assoc.`tourid`, tours_assoc.`type`, tours_assoc.`day`, tours_assoc.`order` AS `step`, tours_assoc.`content`, routes.`id` AS `routeid`, routes.`name` AS `route_name`, routes.`miles` AS `route_miles`, routes.`dep_loc`, routes.`dep_time`, routes.`arr_loc`, routes.`arr_time`, tours_assoc.`adjust`, tours_assoc.`day`, tours_assoc.`order`, itinerary.`id`, itinerary.`title`, itinerary.`map`, itinerary.`ml`, itinerary.`time`, itinerary.`comments`, itinerary.`youtube_title`, itinerary.`youtube`';
		$query .= ' FROM (`tours_assoc` LEFT JOIN `routes` ON tours_assoc.`type` = "r" AND tours_assoc.`typeid` = routes.`id`)';
		$query .= ' LEFT JOIN `itinerary` ON itinerary.`routeid` = routes.`id`';
		$query .= ' WHERE (tours_assoc.`tourid` = "'.$_POST['edit'].'" AND tours_assoc.`dir` = "f")';
		$query .= ' AND ((tours_assoc.`type` = "r" AND itinerary.`id` > 0) OR (tours_assoc.`type` = "i" AND tours_assoc.`content` != ""))';
		$query .= ' ORDER BY tours_assoc.`day` ASC, tours_assoc.`order` ASC, itinerary.`step` ASC';
		//echo $query;
	$result = @mysqlQuery($query);

	$num_results = @mysql_num_rows($result);
	$curroute = 0; $curday = 1;
	for($i=0; $i<$num_results; $i++){
		$row = @mysql_fetch_assoc($result);

		if($row['title'] == "" && $curroute != $row['routeid'] && $row['route_name'] != ""){
			$row['title'] = $row['route_name'];
			}
		if($curroute != $row['routeid'] && $row['ml'] == 0){
			$row['ml'] = $row['route_miles'];
			}
		if($curroute != $row['routeid'] && $row['time'] === ""){
			$timedesc = fromsecs($row['travel_time']);
			$row['time'] = '';
			if($timedesc['h'] > 0 || $timedesc['m'] > 0){ $row['time'] .= gettrans('About').' '; }
			if($timedesc['h'] > 0){ $row['time'] .= $timedesc['h'].' '.gettrans('hours'); }
			if($timedesc['h'] > 0 && $timedesc['m'] > 0){ $row['time'] .= ', '; }
			if($timedesc['m'] > 0){ $row['time'] .= $timedesc['m'].' '.gettrans('minutes'); }
			}
		if($row['time'] != ""){
			if(is_int($row['time'])){
				$addtotime = 'hour';
				if($row['time'] > 1){ $addtotime .= 's'; }
				$row['time'] .= ' '.gettrans($addtotime);
				}
			}
		$curroute = $row['routeid'];
		$curday = $row['day'];
		array_push($itinerary,$row);
		} //End for loop

	//Copy itinerary into itinerary archive table
	$numsaved = 0;
	foreach($itinerary as $step => $row){
		//echo '<PRE STYLE="text-align:left;">'; print_r($row); echo '</PRE>';
		$row['day'] = str_replace('"','\\"',$row['day']);
		$row['title'] = str_replace('"','\\"',$row['title']);
		$row['ml'] = str_replace('"','\\"',$row['ml']);
		$row['time'] = str_replace('"','\\"',$row['time']);
		$row['comments'] = str_replace('"','\\"',$row['comments']);
		$row['youtube_title'] = str_replace('"','\\"',$row['youtube_title']);
		$row['youtube'] = str_replace('"','\\"',$row['youtube']);

		$query = 'INSERT INTO `itinerary_archive`(`tourid`,`day`,`title`,`map`,`ml`,`time`,`comments`,`youtube_title`,`youtube`,`step`) VALUES("'.$row['tourid'].'","'.$row['day'].'","'.$row['title'].'","'.$row['map'].'","'.$row['ml'].'","'.$row['time'].'","'.$row['comments'].'","'.$row['youtube_title'].'","'.$row['youtube'].'","'.$step.'")';
			@mysqlQuery($query);
		$newid = mysql_insert_id();
		$thiserror = $GLOBALS['mysql_error'];
		//echo $query.'<BR>'.$thiserror.'<BR><BR>';

		if($thiserror == ""){
			$numsaved=($numsaved+mysql_affected_rows());
			//Copy images and translations
			if($row['id'] != NULL){
				@mysqlQuery('INSERT INTO `images_assoc_itinerary_archive`(`itid`,`imgid`,`order`) SELECT "'.$newid.'",`imgid`,`order` FROM `images_assoc_itinerary` WHERE `itid` = "'.$row['id'].'"');
					$thiserror = $GLOBALS['mysql_error']; if($thiserror != ""){ array_push($errormsg,$thiserror); }
				@mysqlQuery('INSERT INTO `itinerary_archive_translations`(`itid`,`lang`,`title`,`time`,`comments`) SELECT "'.$newid.'",`lang`,`title`,`time`,`comments` FROM `itinerary_translations` WHERE `itid` = "'.$row['id'].'"');
					$thiserror = $GLOBALS['mysql_error']; if($thiserror != ""){ array_push($errormsg,$thiserror); }
				}
			} else {
			array_push($errormsg,$thiserror);
			}
		} //End ForEach

	if(count($errormsg) < 1){
		$query = 'UPDATE `tours` SET `archived` = "'.$time.'" WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
			@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror == ""){ array_push($successmsg,'Archived tour ('.$_REQUEST['edit'].').'); } else { array_push($errormsg,$thiserror); }
		}

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "cur_tour" && isset($_POST['edit']) && $_POST['edit'] != ""){

	$query = 'UPDATE `tours` SET `cur_tour` = "'.$_POST['cur_tour'].'", `cur_link` = "'.$_POST['cur_link'].'" WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
		@mysqlQuery($query);
	$thiserror = $GLOBALS['mysql_error'];
	if($thiserror == ""): array_push($successmsg,'Saved tour link for archived tour ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "restore" && isset($_POST['edit']) && $_POST['edit'] != ""){

	@mysqlQuery('UPDATE `tours` SET `archived` = "0" WHERE `id` = "'.$_POST['edit'].'" LIMIT 1');
	if($thiserror != ""){ array_push($errormsg,$thiserror); }
	if(count($errormsg) < 1): array_push($successmsg,'Restored tour '.$_POST['edit'].'.'); endif;

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_POST['edit']) && $_POST['edit'] != ""){

	@include_once('sups/dates.php');

	//!DELETE TOUR
	$deldates = array();
	$query = 'SELECT * FROM `tours_dates` WHERE `tourid` = "'.$_POST['edit'].'" AND `tourid` != 0 ORDER BY `date` ASC';
	$result = mysqlQuery($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		array_push($deldates,$row['date']);
		}
	//echo '<PRE>'; print_r($deldates); echo '</PRE>';

	//DELETE DATES
	$tour_was_using = array();
	if(count($deldates) > 0){
	$tour_was_using = tour_using($_POST['edit'],$deldates);
	$result = rmv_tour_dates($_REQUEST['edit'],$deldates);
		$successmsg = array_merge($successmsg,$result['success']);
		$errormsg = array_merge($errormsg,$result['error']);
		}

	//Any unused route dates?
	$unusedroutes = false;
	foreach($tour_was_using as $key1 => $date){
		foreach($date as $key2 => $route){
			$tour_was_using[$key1][$key2]['usedby'] = route_usedby($route['routeid'],$route['route_date'],$route['route_date']);
			//echo '<PRE>'; print_r($tour_was_using[$key1][$key2]['usedby']); echo '</PRE>';
			if(count($tour_was_using[$key1][$key2]['usedby']) == 0){ $unusedroutes = true; }
			}
		}

	if(count($errormsg) == 0){

		$query = 'DELETE FROM `tours_assoc` WHERE `tourid` = "'.$_POST['edit'].'"';
			@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror == ""): array_push($successmsg,mysql_affected_rows().' tour steps were deleted for tour '.$_POST['edit'].'.'); else: array_push($errormsg,$thiserror); endif;
	
		$query = 'DELETE FROM `tours_transtypes_assoc` WHERE `tourid` = "'.$_POST['edit'].'"';
			@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror != ""): array_push($errormsg,$thiserror); endif;
	
		$query = 'DELETE FROM `tours_similar` WHERE `tourid` = "'.$_POST['edit'].'"';
			@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror != ""): array_push($errormsg,$thiserror); endif;

		$query = 'DELETE FROM `tours_upsell` WHERE `tourid` = "'.$_POST['edit'].'"';
			@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror != ""): array_push($errormsg,$thiserror); endif;
	
		$query = 'DELETE FROM `tours_translations` WHERE `tourid` = "'.$_POST['edit'].'"';
			@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror == ""): array_push($successmsg,mysql_affected_rows().' tour translations were deleted for tour '.$_POST['edit'].'.'); else: array_push($errormsg,$thiserror); endif;
	
		$query = 'DELETE FROM `images_assoc_tours` WHERE `tourid` = "'.$_POST['edit'].'"';
			@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror == ""): array_push($successmsg,mysql_affected_rows().' tour images were deleted for tour '.$_POST['edit'].'.'); else: array_push($errormsg,$thiserror); endif;
	
		$query = 'DELETE FROM `tours` WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
			@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror == ""){
			array_push($successmsg,'Tour '.$_POST['edit'].' has been deleted.');
			$_POST['edit'] = '';
			$_REQUEST['edit'] = '';
			} else {
			array_push($errormsg,$thiserror);
			}

	} else {
		array_push($errormsg,'Unable to delete tour.');
	}


} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "rmvroutedates" && isset($_POST['del_routeid']) && is_array($_POST['del_routeid']) && count($_POST['del_routeid']) > 0){

	//!REMOVE APPROVED ROUTE DATES
	@include_once('sups/dates.php');
	//echo '<PRE>'; print_r($_POST); echo '</PRE>';
	if(isset($_POST['submit']) && $_POST['submit'] == "Approve selected"){ $go = false; } else { $go = true; }

	$numremoved = 0;
	foreach($_POST['del_tourdate'] as $key => $date){
		if($go || $_POST['choice_'.$date] == "y"){
		$result = rmv_route_dates($_POST['del_routeid'][$key],$_POST['del_routedate'][$key]);
			$numremoved = ($numremoved + $result['affected']);
			//$successmsg = array_merge($successmsg,$result['success']);
			$errormsg = array_merge($errormsg,$result['error']);
			}
		}
	if($numremoved > 0){ array_push($successmsg,$numremoved.' unused route dates were removed.'); }

}


if(isset($tourlog) && $tourlog != ""){
	//touch('reslog/tourlog');
	//@file_put_contents('reslog/tourlog',$tourlog,FILE_APPEND | LOCK_EX);
}

echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Tours</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


if(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_POST['edit']) && $_POST['edit'] != "" && $unusedroutes && count($tour_was_using) > 0){
//!DELETED TOUR - APPROVED ROUTE DATE REMOVAL **************************************************************//


echo '<SPAN STYLE="font-family:Arial; font-size:12pt;">Would you like to remove these unused route dates?</SPAN><BR><BR>'."\n\n";

echo '<FORM METHOD="post" NAME="editform" ID="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="rmvroutedates">'."\n\n";

//echo '<PRE>'; print_r($tour_was_using); echo '</PRE>';


//GET ROUTE NAMES
$routeids = array();
foreach($tour_was_using as $date){
	foreach($date as $route){
		array_push($routeids,$route['routeid']);
		}
	}
$routenames = array();
	$query = 'SELECT DISTINCT `id`,`name` FROM `routes` WHERE `id` = "'.implode('" OR `id` = "',$routeids).'" ORDER BY `id` ASC';
	$result = mysqlQuery($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$routenames[$row['id']] = $row['name'];
		}
	//echo '<PRE>'; print_r($routenames); echo '</PRE>';

echo '<INPUT TYPE="submit" NAME="submit" VALUE="**Approve all listed**" STYLE="width:200px; color:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" VALUE="Approve none listed" STYLE="color:red;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="submit" VALUE="Approve selected"><BR><BR>'."\n\n";


//Print out dates for "Approve all listed"
foreach($tour_was_using as $thisdate){
	foreach($thisdate as $route){
		if(count($route['usedby']) == 0){
			echo '<INPUT TYPE="hidden" NAME="del_tourdate[]" VALUE="'.$route['tour_date'].'"><INPUT TYPE="hidden" NAME="del_routeid[]" VALUE="'.$route['routeid'].'"><INPUT TYPE="hidden" NAME="del_routedate[]" VALUE="'.$route['route_date'].'">'."\n";
			}
		}
	}
	echo "\n";

//Print out dates for approval
echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n\n";
foreach($tour_was_using as $thisdate){

	bgcolor('reset');
	$delroutes = 0;
	foreach($thisdate as $route){ if(count($route['usedby']) == 0){ $delroutes++; } }
	if($delroutes > 0){
		echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-left:10px;">'.date("F j, Y",$thisdate[0]['tour_date']).': Has been removed</TD></TR>'."\n";
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="width:20px; padding:6px;" ALIGN="center" VALIGN="top"><INPUT TYPE="radio" NAME="choice_'.$thisdate[0]['tour_date'].'" ID="choice_'.$thisdate[0]['tour_date'].'_y" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:11pt; cursor:pointer;" onClick="document.getElementById(\'choice_'.$thisdate[0]['tour_date'].'_y\').checked=1;';
			if(count($tour_was_using) > 5 && count($thisdate) > 4){ echo ' document.getElementById(\'detlink_'.$thisdate[0]['tour_date'].'\').style.display=\'none\'; document.getElementById(\'details_'.$thisdate[0]['tour_date'].'\').style.display=\'\';'; }
			echo '"><B>'.$delroutes.' route dates</B> are not being used by any other tours, and can safely be removed.';
			if(count($tour_was_using) > 5 && count($thisdate) > 4){ echo '<DIV ID="detlink_'.$thisdate[0]['tour_date'].'" onClick="this.style.display=\'none\'; document.getElementById(\'details_'.$thisdate[0]['tour_date'].'\').style.display=\'\';" STYLE="padding-left:20px; color:#000099;">Click here to see details...</DIV>'; }
			echo '<DIV ID="details_'.$thisdate[0]['tour_date'].'" STYLE="font-size:9pt;';
				if(count($tour_was_using) > 5 && count($thisdate) > 4){ echo ' display:none;'; }
				echo '">';
			foreach($thisdate as $route){
				if(count($route['usedby']) > 0){
					echo '<LI STYLE="margin-left:16px;">Keep '.date("D, n/j/Y",$route['route_date']).' for route "'.$routenames[$route['routeid']].'" (used by '.count($route['usedby']).' tours)<BR>';
					} else {
					echo '<LI STYLE="margin-left:16px;"><SPAN STYLE="color:#339900;">Remove '.date("D, n/j/Y",$route['route_date']).' from route "'.$routenames[$route['routeid']].'".</SPAN><BR>';
					}
				}
			echo '</DIV>';
			echo '</TD></TR>'."\n";
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="width:20px; padding:6px; border-bottom:5px solid #FFFFFF;" ALIGN="center" VALIGN="top"><INPUT TYPE="radio" NAME="choice_'.$thisdate[0]['tour_date'].'" ID="choice_'.$thisdate[0]['tour_date'].'_n" VALUE="n"></TD><TD STYLE="font-family:Arial; font-size:11pt; border-bottom:5px solid #FFFFFF; cursor:pointer;" onClick="document.getElementById(\'choice_'.$thisdate[0]['tour_date'].'_n\').checked=1;">Do not remove the '.$delroutes.' unused route dates.</TD></TR>'."\n\n";
		} //End delroutes if statement

	} //End For Each
	echo '</TABLE><BR>'."\n\n";


echo '<INPUT TYPE="submit" NAME="submit" VALUE="**Approve all listed**" STYLE="width:200px; color:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" VALUE="Approve none listed" STYLE="color:red;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="submit" VALUE="Approve selected"><BR><BR>'."\n\n";

?><SCRIPT><!--
function checkend(){ }
//--></SCRIPT><? echo "\n\n";



} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != "" && count($_POST['runs']) > 0 && (count($appdates['f']) + count($appdates['r']) + count($tour_was_using)) > 0){
//!UNAPPROVED DATES PAGE **************************************************************//


//!COME BACK AND ADDRESS THIS SOMEDAY - Sometimes when deleting a tour date, the system will display a blank page of "changes to be approved".
	//This is related to the "count($tour_was_using)" method of testing whether or not to show this page above.
	//Instead of testing the count of the array, it should test that there are actually unused route dates that can be removed.
	//echo '<PRE STYLE="text-align:left;">'; print_r($tour_was_using); echo '</PRE>';
	//echo count($appdates['f']) .' '. count($appdates['r']) .' '. count($tour_was_using);


?><SCRIPT><!--
function checkend(){ }
//--></SCRIPT><? echo "\n\n";

echo '<SPAN STYLE="font-family:Arial; font-size:12pt;">Some changes to tour/route dates need your approval...</SPAN><BR><BR>'."\n\n";

echo '<FORM METHOD="post" NAME="editform" ID="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="dates">'."\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.$_REQUEST['edit'].'">'."\n\n";

//GET ROUTE NAMES
$routeids = array();
foreach($testroutes['f'] as $route){
	array_push($routeids,$route['typeid']);
	}
foreach($testroutes['r'] as $route){
	array_push($routeids,$route['typeid']);
	}
$routenames = array();
	$query = 'SELECT DISTINCT `id`,`name` FROM `routes` WHERE `id` = "'.implode('" OR `id` = "',$routeids).'" ORDER BY `id` ASC';
	$result = mysqlQuery($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$routenames[$row['id']] = $row['name'];
		}

echo '<INPUT TYPE="submit" NAME="submit" VALUE="**Approve all listed**" STYLE="width:200px; color:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" VALUE="Approve none listed" STYLE="color:red;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit='.$_REQUEST['edit'].'\';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="submit" VALUE="Approve selected"><BR><BR>'."\n\n";


@include_once('sups/tourvaliddates.php');
@include_once('sups/dates.php');

//echo '<PRE>'; print_r($appdates); echo '</PRE>';
//echo '<PRE>'; print_r($tour_was_using); echo '</PRE>';

//Organize dates and get prereqs
$fdates = array();
if(isset($appdates['f']) && count($appdates['f']) > 0){
	$fdates = propose_tour($_REQUEST['edit'],'f',$appdates['f'],1);
	//echo '<PRE>'; print_r($fdates); echo '</PRE>';
	}
$rdates = array();
if(isset($appdates['r']) && count($appdates['r']) > 0){
	$rdates = propose_tour($_REQUEST['edit'],'r',$appdates['r'],1);
	//echo '<PRE>'; print_r($rdates); echo '</PRE>';
	}
foreach($tour_was_using as $key1 => $date){
	foreach($date as $key2 => $route){
		$tour_was_using[$key1][$key2]['usedby'] = route_usedby($route['routeid'],$route['route_date'],$route['route_date']);
		//echo '<PRE>'; print_r($tour_was_using[$key1][$key2]['usedby']); echo '</PRE>';
		}
	}

//Print out dates for "Approve all listed"
$appdates = array_merge($fdates,$rdates);
ksort($appdates);
foreach($appdates as $thisdate){
	echo '<INPUT TYPE="hidden" NAME="app_dates[]" VALUE="'.$thisdate[0]['tour_date'].'"><INPUT TYPE="hidden" NAME="app_dir[]" VALUE="'.$thisdate[0]['dir'].'">'."\n";
	}
foreach($tour_was_using as $thisdate){
	foreach($thisdate as $route){
		if(count($route['usedby']) == 0){
			echo '<INPUT TYPE="hidden" NAME="del_tourdate[]" VALUE="'.$route['tour_date'].'"><INPUT TYPE="hidden" NAME="del_routeid[]" VALUE="'.$route['routeid'].'"><INPUT TYPE="hidden" NAME="del_routedate[]" VALUE="'.$route['route_date'].'">'."\n";
			}
		}
	}
	echo "\n";

//Merge all arrays for listing
$appdates = array_merge($fdates,$rdates,$tour_was_using);
	ksort($appdates);
	//echo '<DIV STYLE="text-align:left;"><PRE>'; print_r($appdates); echo '</PRE></DIV>';

//Print out dates for approval
echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n\n";
foreach($appdates as $thisdate){

	bgcolor('reset');
	if(isset($thisdate[0]['usedby'])){
		$delroutes = 0;
		foreach($thisdate as $route){ if(count($route['usedby']) == 0){ $delroutes++; } }
		if($delroutes > 0){
			echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-left:10px;">'.date("F j, Y",$thisdate[0]['tour_date']).': Has been removed</TD></TR>'."\n";
			echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="width:20px; padding:6px;" ALIGN="center" VALIGN="top"><INPUT TYPE="radio" NAME="choice_'.$thisdate[0]['tour_date'].'" ID="choice_'.$thisdate[0]['tour_date'].'_y" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:11pt; cursor:pointer;" onClick="document.getElementById(\'choice_'.$thisdate[0]['tour_date'].'_y\').checked=1;';
				if(count($appdates) > 5 && $delroutes > 4){ echo ' document.getElementById(\'detlink_'.$thisdate[0]['tour_date'].'\').style.display=\'none\'; document.getElementById(\'details_'.$thisdate[0]['tour_date'].'\').style.display=\'\';'; }
				echo '"><B>'.$delroutes.' route dates</B> are not being used by any other tours, and can safely be removed.';
				if(count($appdates) > 5 && $delroutes > 4){ echo '<DIV ID="detlink_'.$thisdate[0]['tour_date'].'" onClick="this.style.display=\'none\'; document.getElementById(\'details_'.$thisdate[0]['tour_date'].'\').style.display=\'\';" STYLE="padding-left:20px; color:#000099;">Click here to see details...</DIV>'; }
				echo '<DIV ID="details_'.$thisdate[0]['tour_date'].'" STYLE="font-size:9pt;';
					if(count($appdates) > 5 && $delroutes > 4){ echo ' display:none;'; }
					echo '">';
				foreach($thisdate as $route){
					if(count($route['usedby']) > 0){
						echo '<LI STYLE="margin-left:16px;">Keep '.date("D, n/j/Y",$route['route_date']).' for route "'.$routenames[$route['routeid']].'" (used by '.count($route['usedby']).' tours)<BR>';
						} else {
						echo '<LI STYLE="margin-left:16px;"><SPAN STYLE="color:#339900;">Remove '.date("D, n/j/Y",$route['route_date']).' from route "'.$routenames[$route['routeid']].'".</SPAN><BR>';
						}
					}
				echo '</DIV>';
				echo '</TD></TR>'."\n";
			echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="width:20px; padding:6px; border-bottom:5px solid #FFFFFF;" ALIGN="center" VALIGN="top"><INPUT TYPE="radio" NAME="choice_'.$thisdate[0]['tour_date'].'" ID="choice_'.$thisdate[0]['tour_date'].'_n" VALUE="n"></TD><TD STYLE="font-family:Arial; font-size:11pt; border-bottom:5px solid #FFFFFF; cursor:pointer;" onClick="document.getElementById(\'choice_'.$thisdate[0]['tour_date'].'_n\').checked=1;">Do not remove the '.$delroutes.' unused route dates.</TD></TR>'."\n\n";
			} //End delroutes if statement
	} else {
		echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-left:10px;">'.date("F j, Y",$thisdate[0]['tour_date']).': Run';
			if($thisdate[0]['dir'] == 'r'){ echo ' in reverse'; } else { echo ' forward'; }
			echo '</TD></TR>'."\n";
		$addroutes = 0;
		foreach($thisdate as $route){ if($route['route_date'] != $route['proposed_route']){ $addroutes++; } }
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="width:20px; padding:6px;" ALIGN="center" VALIGN="top"><INPUT TYPE="radio" NAME="choice_'.$thisdate[0]['tour_date'].'" ID="choice_'.$thisdate[0]['tour_date'].'_y" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:11pt; cursor:pointer;" onClick="document.getElementById(\'choice_'.$thisdate[0]['tour_date'].'_y\').checked=1;';
			if(count($appdates) > 5 && count($thisdate) > 4){ echo ' document.getElementById(\'detlink_'.$thisdate[0]['tour_date'].'\').style.display=\'none\'; document.getElementById(\'details_'.$thisdate[0]['tour_date'].'\').style.display=\'\';'; }
			echo '">To run the tour on this date, <B>add '.$addroutes.' route dates</B>.';
			if(count($appdates) > 5 && count($thisdate) > 4){ echo '<DIV ID="detlink_'.$thisdate[0]['tour_date'].'" onClick="this.style.display=\'none\'; document.getElementById(\'details_'.$thisdate[0]['tour_date'].'\').style.display=\'\';" STYLE="padding-left:20px; color:#000099;">Click here to see details...</DIV>'; }
			echo '<DIV ID="details_'.$thisdate[0]['tour_date'].'" STYLE="font-size:9pt;';
				if(count($appdates) > 5 && count($thisdate) > 4){ echo ' display:none;'; }
				echo '">';
			foreach($thisdate as $route){
				if($route['route_date'] != $route['proposed_route']){
					echo '<LI STYLE="margin-left:16px;">Add '.date("D, n/j/Y",$route['proposed_route']).' to route "'.$routenames[$route['routeid']].'"<BR>';
					} else {
					echo '<LI STYLE="margin-left:16px;"><SPAN STYLE="color:#339900;">On '.date("D, n/j/Y",$route['proposed_route']).', route "'.$routenames[$route['routeid']].'" is already running.</SPAN><BR>';
					}
				}
			echo '</DIV>';
			echo '</TD></TR>'."\n";
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="width:20px; padding:6px; border-bottom:5px solid #FFFFFF;" ALIGN="center" VALIGN="top"><INPUT TYPE="radio" NAME="choice_'.$thisdate[0]['tour_date'].'" ID="choice_'.$thisdate[0]['tour_date'].'_n" VALUE="n"></TD><TD STYLE="font-family:Arial; font-size:11pt; border-bottom:5px solid #FFFFFF; cursor:pointer;" onClick="document.getElementById(\'choice_'.$thisdate[0]['tour_date'].'_n\').checked=1;">Do not add the '.$addroutes.' needed route dates. Do not add this date to the tour.</TD></TR>'."\n\n";
	} //End 'usedby' if statment

	} //End For Each
	echo '</TABLE><BR>'."\n\n";


echo '<INPUT TYPE="submit" NAME="submit" VALUE="**Approve all listed**" STYLE="width:200px; color:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" VALUE="Approve none listed" STYLE="color:red;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit='.$_REQUEST['edit'].'\';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="submit" VALUE="Approve selected"><BR><BR>'."\n\n";


} elseif(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){
//!EDIT TOUR PAGE **************************************************************//


if($_REQUEST['edit'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['edit'] == "*new*"){
	$fillform = array(
		'id' => '*new*',
		'getweights' => 'n',
		'getlunch' => 'n',
		'getpreftime' => 'n'
		);
	} else {
	$query = 'SELECT * FROM `tours` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
	$result = mysqlQuery($query);
	$fillform = mysql_fetch_assoc($result);
	}



if(getval('archived') > 0){
//!ARCHIVED ******************************


//GET TOUR LIST
$tourlist = array();
$query = 'SELECT `id`,`title` FROM `tours` ORDER BY `id` DESC, `title` ASC';
$result = @mysqlQuery($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['title'] = str_replace("'",'',$row['title']);
	if(strlen($row['title']) > 100){ $row['title'] = trim(substr($row['title'],0,49)).' ... '.trim(substr($row['title'],-49)); }
	array_push($tourlist,$row);
	}

echo '<FORM METHOD="post" NAME="editform" ID="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="cur_tour">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";

echo '<DIV STYLE="font-family:Arial; font-size:12pt; width:93%;">';
	echo getval('title').'<BR><BR>';
	echo '<I>This tour has been archived.</I><BR><BR>';
	echo '</DIV>'."\n\n";

echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	bgcolor('reset');
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">On site, link to newer tour?</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
		echo '<SELECT NAME="cur_tour" STYLE="font-size:8pt; width:400px;"><OPTION VALUE="0">No</OPTION>'."\n";
				foreach($tourlist as $row){
					echo "\t".'<OPTION VALUE="'.$row['id'].'"';
					if($row['id'] == getval('cur_tour')): echo ' SELECTED'; endif;
					echo '>'.$row['id'].' : '.$row['title'].'</OPTION>'."\n";
					}
				echo '</SELECT>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Link text<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">If left blank, "There is a new and much better version of this tour!" will be used.</SPAN></TD><TD><INPUT TYPE="text" NAME="cur_link" STYLE="width:400px;" VALUE="'.getval('cur_link').'"></TD></TR>'."\n";
	echo '</TABLE><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '</FORM>'."\n\n";

echo '<HR SIZE="1" WIDTH="94%"><BR>'."\n\n";

echo '<FORM NAME="restoreform" ID="restoreform" METHOD="POST" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" ID="utaction" VALUE="restore">'."\n";
	echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.$_REQUEST['edit'].'">'."\n";
	echo '<INPUT TYPE="submit" VALUE="Restore '.$_REQUEST['edit'].'" STYLE="color:#FF0000; width:180px;">'."\n";
	echo '<BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";

?><SCRIPT><!-- function checkend(){ } //--></SCRIPT><? echo "\n\n";


} else {
//!NORMAL ******************************


include_once('sups/timezones.php');

//GET LOCATIONS
$locations = array();
$query = 'SELECT * FROM `locations` ORDER BY `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$locations['l'.$row['id']] = $row;
	}

//GET ROUTES
$routes = array();
$query = 'SELECT * FROM `routes` ORDER BY `name` ASC, `dep_loc` ASC, `arr_loc` ASC, `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['description'] = ''; $row['details'] = '';
	if($row['anchor'] == 'arr'){
		$dep_time = ($today+$row['arr_time']-$row['travel_time']+tz_offset($row['arr_loc'],$row['dep_loc']));
		$arr_time = ($today+$row['arr_time']);
		$row['arr_time'] = ($today+$row['arr_time']+tz_offset($row['arr_loc'],9));
		$row['dep_time'] = ($row['arr_time']-$row['travel_time']);
		} else {
		$dep_time = ($today+$row['dep_time']);
		$arr_time = ($today+$row['dep_time']+$row['travel_time']+tz_offset($row['dep_loc'],$row['arr_loc']));
		$row['dep_time'] = ($today+$row['dep_time']+tz_offset($row['dep_loc'],9));
		$row['arr_time'] = ($row['dep_time']+$row['travel_time']);
		}

	$row['subhead'] = $locations['l'.$row['dep_loc']]['name'].' '.date("g:ia",$dep_time).' <I>to</I> '.$locations['l'.$row['arr_loc']]['name'].' '.date("g:ia",$arr_time);
	$routes['r'.$row['id']] = $row;
	//array_push($routes,$row);
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($routes); echo '</PRE>';

//GET VENDORS
$vendors = array();
$query = 'SELECT * FROM `vendors` ORDER BY `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$vendors['v'.$row['id']] = $row;
	}

//GET LODGING
$lodging = array();
$query = 'SELECT lodging.`id`, lodging.`name`, vendors.`name` as `vendor` FROM `lodging`';
		$query .= ' LEFT JOIN `vendors` ON lodging.`vendor` = vendors.`id`';
		$query .= ' WHERE lodging.`type` = "t"';
		$query .= ' ORDER BY vendors.`name` ASC, lodging.`name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['subhead'] = 'Lodging';
		if($row['vendor'] != ""){ $row['subhead'] .= ' &#149; <B>'.$row['vendor'].'</B>'; }
	$lodging['l'.$row['id']] = $row;
	//array_push($lodging,$row);
	}

//GET EXTENDED LODGING
$extlodging = array();
$query = 'SELECT tours_extlodging.`id`, tours_extlodging.`name`, tours_extlodging.`perguest` as `price`, lodging.`name` as `lodge_name` FROM `tours_extlodging`';
		$query .= ' LEFT JOIN `lodging` ON tours_extlodging.`lodgeid` = lodging.`id`';
		$query .= ' ORDER BY tours_extlodging.`name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['subhead'] = 'Lodging extension';
		if($row['lodge_name'] != ""){ $row['name'] = $row['lodge_name'].' ('.$row['name'].')'; }
		if($row['price'] != 0){ $row['subhead'] .= ' &#149; <B>$'.$row['price'].'/night</B>'; }
	$extlodging['e'.$row['id']] = $row;
	//array_push($lodging,$row);
	}

//GET TRANSPORT
$transport = array();
$query = 'SELECT tours_transport.*, vendors.`name` AS `vendor_name` FROM `tours_transport` LEFT JOIN `vendors` ON tours_transport.`vendor` = vendors.`id`';
	$query .= ' ORDER BY tours_transport.`reference` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['name'] = $row['reference'];
	$row['subhead'] = 'Transport &#149; <B>'.$row['vendor_name'].'</B>';
	$transport['p'.$row['id']] = $row;
	}

//GET ACTIVITIES
$activities = array();
$query = 'SELECT activities.*, vendors.`name` as `vendor` FROM `activities` LEFT JOIN `vendors` ON activities.`vendor` = vendors.`id` WHERE activities.`disable` != 1 ORDER BY activities.`name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['subhead'] = 'Activity &#149; ';
		if($row['price'] != 0){
			$row['subhead'] .= '<B>$'.$row['price'].'</B> &#149; ';
			}
	$row['subhead'] .= $row['vendor'];
	$activities['a'.$row['id']] = $row;
	//array_push($activities,$row);
	}

//GET ITINERARY
$itinerary = array();
$query = 'SELECT `id`,`nickname`,`title`,`comments` FROM `itinerary`';
	$query .= ' WHERE `routeid` = 0';
	$query .= ' ORDER BY `nickname` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if($row['nickname'] != ""){
		$row['name'] = $row['nickname'];
		} else {
		$row['name'] = '[No nickname]';
		}
	if($row['title'] != ""){ $row['comments'] = $row['title'].' - '.$row['comments']; }
	$row['comments'] = strip_tags($row['comments']);
	$row['comments'] = str_replace("\r",'',$row['comments']);
	$row['comments'] = str_replace("\n",' ',$row['comments']);
	$row['comments'] = str_replace('"','&quot;',$row['comments']);
	if(strlen($row['comments']) > 180){
		$row['comments'] = trim(substr($row['comments'],0,89)).' ... '.trim(substr($row['comments'],-89));
		}
	$row['subhead'] = $row['comments'];
	$itinerary['i'.$row['id']] = $row;
	}

//GET MEANS OF TRAVEL
$means = array();
$query = 'SELECT * FROM `tours_transtypes` ORDER BY `name` ASC';
$result = @mysqlQuery($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($means,$row);
	}
$fillform['means'] = array();
$query = 'SELECT tours_transtypes_assoc.* FROM `tours_transtypes_assoc`,`tours_transtypes` WHERE tours_transtypes_assoc.`transid` = tours_transtypes.`id` AND tours_transtypes_assoc.`tourid` = "'.getval('id').'" AND `tourid` != 0 ORDER BY tours_transtypes.`name` ASC';
$result = @mysqlQuery($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($fillform['means'],$row);
	}

$tour_types = array(
	'r' => 'routes',
	'l' => 'lodging',
	'e' => 'extlodging',
	'p' => 'transport',
	'a' => 'activities',
	'i' => 'itinerary'
	);
function valid_step($type,$id){
	global $tour_types;
	global ${$tour_types[$type]};
	if( isset( ${$tour_types[$type]}[$type.$id] ) ){
		return true;
		}
	return false;
	}

//GET STEPS OF TOUR
$fillform['steps'] = array('f'=>array(),'r'=>array());
$query = 'SELECT * FROM `tours_assoc` WHERE `tourid` = "'.getval('id').'" AND `tourid` != 0 ORDER BY `dir` ASC, `day` ASC, `order` ASC';
$result = @mysqlQuery($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if( valid_step($row['type'],$row['typeid']) ){
		array_push($fillform['steps'][$row['dir']],$row);
		//} else {
		//echo 'not<BR>';
		}
	}

//GET TOUR LIST FOR SIMILAR/UPSELL
$tourlist = array();
$query = 'SELECT `id`,`title` FROM `tours` WHERE `id` != "'.getval('id').'" ORDER BY `id` DESC, `title` ASC';
$result = @mysqlQuery($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['title'] = str_replace("'",'',$row['title']);
	//if(strlen($row['title']) > 100){ $row['title'] = substr($row['title'],0,100).'...'; }
	if(strlen($row['title']) > 100){ $row['title'] = trim(substr($row['title'],0,49)).' ... '.trim(substr($row['title'],-49)); }
	array_push($tourlist,$row);
	}

//GET SIMILAR TOURS
$fillform['similar'] = array();
$query = 'SELECT tours_similar.* FROM `tours_similar`,`tours` WHERE tours_similar.`simid` = tours.`id` AND tours_similar.`tourid` = "'.getval('id').'" AND `tourid` != 0';
	$query .= ' ORDER BY tours_similar.`order` ASC, tours.`order_weight` ASC, tours.`numdays` ASC, tours.`perguest` ASC, tours.`title` ASC';
$result = @mysqlQuery($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($fillform['similar'],$row);
	}

//GET UPSELL TOURS
$fillform['upsell'] = array();
$query = 'SELECT tours_upsell.* FROM `tours_upsell`,`tours` WHERE tours_upsell.`upsellid` = tours.`id` AND tours_upsell.`tourid` = "'.getval('id').'" AND `tourid` != 0';
	$query .= ' ORDER BY tours_upsell.`order` ASC, tours.`order_weight` ASC, tours.`numdays` ASC, tours.`perguest` ASC, tours.`title` ASC';
$result = @mysqlQuery($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($fillform['upsell'],$row);
	}

//GET TOUR DATES
$dates = array();
$query = 'SELECT * FROM `tours_dates` WHERE `tourid` = "'.getval('id').'" AND `tourid` != 0 ORDER BY `date` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(!isset($dates[date("Y",$row['date'])])){ $dates[date("Y",$row['date'])] = array(); }
	if(!isset($dates[date("Y",$row['date'])][date("n",$row['date'])])){ $dates[date("Y",$row['date'])][date("n",$row['date'])] = array(); }
	$dates[date("Y",$row['date'])][date("n",$row['date'])][date("j",$row['date'])] = $row['dir'];
	//array_push($dates,$row);
	}
$availdates = array();
if( include_once('sups/tourvaliddates.php') ){
	$founddates = tourvaliddates(getval('id'),'','n');
	foreach($founddates as $key => $thisdir){
		foreach($thisdir as $thisdate){
			if(!isset($availdates[date("Y",$thisdate)])){ $availdates[date("Y",$thisdate)] = array(); }
			if(!isset($availdates[date("Y",$thisdate)][date("n",$thisdate)])){ $availdates[date("Y",$thisdate)][date("n",$thisdate)] = array(); }
			$availdates[date("Y",$thisdate)][date("n",$thisdate)][date("j",$thisdate)] = $key;
			}
		}
	}
	//echo '<PRE>'; print_r($availdates); echo '</PRE>';

//GET BLOCKED
$tours_blocked = array();
include_once('../getblocked.php');
	$blocked = getblocked_tours(getval('id'),'*','*');
	//echo '</CENTER><PRE>'; print_r($blocked); echo '</PRE>';
	if(isset($blocked['t'.getval('id')])){
	foreach($blocked['t'.getval('id')] as $key => $row){
		if(!isset($tours_blocked[date("Y",$row['date'])])){ $tours_blocked[date("Y",$row['date'])] = array(); }
		if(!isset($tours_blocked[date("Y",$row['date'])][date("n",$row['date'])])){ $tours_blocked[date("Y",$row['date'])][date("n",$row['date'])] = array(); }
		$tours_blocked[date("Y",$row['date'])][date("n",$row['date'])][date("j",$row['date'])] = $row['blocked'];
		}
		}
	//echo '<PRE>Blocked'; print_r($tours_blocked); echo '</PRE>';

//GET IMAGES
$fillform['images'] = array();
$imgids = array();
$query = 'SELECT images.* FROM `images`,`images_assoc_tours` WHERE images.`id` = images_assoc_tours.`imgid` AND images_assoc_tours.`tourid` = "'.getval('id').'" AND `tourid` != 0 ORDER BY images_assoc_tours.`order` ASC';
$result = @mysqlQuery($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['data'] = imgform($row['filename'],180,90);
	array_push($fillform['images'],$row);
	array_push($imgids,$row['id']);
	}


//echo '<INPUT TYPE="button" VALUE="TEST" onClick="test()"><BR><BR>';


?><SCRIPT><!--

function test(){
	//waitforcal();

	var out = '';

	var x = document.getElementById('editform').elements;
	for(i in x){ if(x[i] != undefined){
		if(x[i]['name'] == "tour_type[]" || x[i]['name'] == "tour_id[]" || x[i]['name'] == "tour_adj[]" || x[i]['name'] == "tour_day[]"){ // || x[i]['name'] == "tour_content[]"
			out += i+' : '+x[i]['id']+' : '+x[i]['name']+' : '+x[i]['value']+"\n";
			if(x[i]['name'] == "tour_day[]"){ out += "\n"; }
			}
		}}

	alert(out);

	//alert('Tour length: '+tour.length+"\n\n"+out);

	//alert(dropTargets['days']);

	//alert('1:'+dayindex(1) +', 2:'+dayindex(2) +', 3:'+dayindex(3) +', 4:'+dayindex(4) +', 5:'+dayindex(5));

	}

var tour = new Array();
<?
	echo "\t".'tour[\'f\'] = new Array();'."\n";
	foreach($fillform['steps']['f'] as $i => $step){
		$step['content'] = str_replace("\n",'\n',$step['content']);
		$step['content'] = str_replace("\r",'',$step['content']);
		echo "\t".'tour[\'f\']['.$i.'] = new Array(); tour[\'f\']['.$i.']["type"] = "'.$step['type'].'"; tour[\'f\']['.$i.']["id"] = "'.$step['typeid'].'"; tour[\'f\']['.$i.']["adjust"] = "'.$step['adjust'].'"; tour[\'f\']['.$i.']["content"] = "'.$step['content'].'"; tour[\'f\']['.$i.']["day"] = "'.$step['day'].'";'."\n";
		}
	echo "\t".'tour[\'r\'] = new Array();'."\n";
	foreach($fillform['steps']['r'] as $i => $step){
		$step['content'] = str_replace("\n",'\n',$step['content']);
		$step['content'] = str_replace("\r",'',$step['content']);
		echo "\t".'tour[\'r\']['.$i.'] = new Array(); tour[\'r\']['.$i.']["type"] = "'.$step['type'].'"; tour[\'r\']['.$i.']["id"] = "'.$step['typeid'].'"; tour[\'r\']['.$i.']["adjust"] = "'.$step['adjust'].'"; tour[\'r\']['.$i.']["content"] = "'.$step['content'].'"; tour[\'r\']['.$i.']["day"] = "'.$step['day'].'";'."\n";
		}
	?>

var optinfo = new Array();
<?
	foreach($routes as $row){
		echo "\t".'optinfo["r'.$row['id'].'"] = new Array(); optinfo["r'.$row['id'].'"]["type"] = "r"; optinfo["r'.$row['id'].'"]["adjust"] = 0; optinfo["r'.$row['id'].'"]["sel"] = 0; optinfo["r'.$row['id'].'"]["hlp"] = 0;';
			foreach($row as $key => $val){
				echo ' optinfo["r'.$row['id'].'"]["'.$key.'"] = "'.str_replace('"','\\"',$val).'";';
				}
			echo "\n";
		}
	foreach($lodging as $row){
		echo "\t".'optinfo["l'.$row['id'].'"] = new Array(); optinfo["l'.$row['id'].'"]["type"] = "l"; optinfo["l'.$row['id'].'"]["adjust"] = 0; optinfo["l'.$row['id'].'"]["sel"] = 0;';
			foreach($row as $key => $val){
				echo ' optinfo["l'.$row['id'].'"]["'.$key.'"] = "'.str_replace('"','\\"',$val).'";';
				}
			echo "\n";
		}
	foreach($extlodging as $row){
		echo "\t".'optinfo["e'.$row['id'].'"] = new Array(); optinfo["e'.$row['id'].'"]["type"] = "e"; optinfo["e'.$row['id'].'"]["adjust"] = 0; optinfo["e'.$row['id'].'"]["sel"] = 0;';
			foreach($row as $key => $val){
				echo ' optinfo["e'.$row['id'].'"]["'.$key.'"] = "'.str_replace('"','\\"',$val).'";';
				}
			echo "\n";
		}
	foreach($transport as $row){
		echo "\t".'optinfo["p'.$row['id'].'"] = new Array(); optinfo["p'.$row['id'].'"]["type"] = "p"; optinfo["p'.$row['id'].'"]["adjust"] = 0; optinfo["p'.$row['id'].'"]["sel"] = 0;';
			foreach($row as $key => $val){
				echo ' optinfo["p'.$row['id'].'"]["'.$key.'"] = "'.str_replace('"','\\"',$val).'";';
				}
			echo "\n";
		}
	foreach($activities as $row){
		echo "\t".'optinfo["a'.$row['id'].'"] = new Array(); optinfo["a'.$row['id'].'"]["type"] = "a"; optinfo["a'.$row['id'].'"]["adjust"] = 0; optinfo["a'.$row['id'].'"]["sel"] = 0;';
			foreach($row as $key => $val){
				echo ' optinfo["a'.$row['id'].'"]["'.$key.'"] = "'.str_replace('"','\\"',$val).'";';
				}
			echo "\n";
		}
	foreach($itinerary as $row){
		echo "\t".'optinfo["i'.$row['id'].'"] = new Array(); optinfo["i'.$row['id'].'"]["type"] = "i"; optinfo["i'.$row['id'].'"]["adjust"] = 0; optinfo["i'.$row['id'].'"]["sel"] = 0;';
			foreach($row as $key => $val){
				echo ' optinfo["i'.$row['id'].'"]["'.$key.'"] = "'.str_replace('"','\\"',$val).'";';
				}
			echo "\n";
		}
	?>

var colors = new Array(); colors['r'] = 'blue'; colors['l'] = 'green'; colors['e'] = 'ltblue'; colors['p'] = 'orange'; colors['a'] = 'red'; colors['i'] = 'grey';

function show_hide(c){
	var state = document.getElementById('choose_'+c).style.display;
	var sections = new Array('Routes','Lodging','Lodging extensions','Transport','Activities','Itinerary');
	for(i in sections){
		document.getElementById('choose_'+sections[i]).style.display = 'none';
		document.getElementById('label_'+sections[i]).innerHTML = '&nbsp;+&nbsp;'+sections[i];
		}
	if(state == 'none'){
		document.getElementById('choose_'+c).style.display = '';
		document.getElementById('label_'+c).innerHTML = '&nbsp;-&nbsp;'+c;
		//window.location = '#designtop';
		hl_left();
		}
	adjlcon();
	}

function hl_left(){
	var di = document.getElementById('tourdir').value;

	//Clear all highlighted
	for(i in optinfo){
		optinfo[i]['sel'] = 0;
		optinfo[i]['hlp'] = 0;
		}

	//Find included
	for(t in tour[di]){
		//if(tour[di][t]['type'] != "i"){
		optinfo[tour[di][t]['type']+tour[di][t]['id']]['sel'] = 1;
		}

	//Find next routes
	lastr = findlastr(tour[di].length);
	if(lastr > -1){
		for(i in optinfo){
			if(optinfo[i]['type'] == 'r' && optinfo[i]['dep_loc'] == optinfo['r'+tour[di][lastr]['id']]['arr_loc']){
				optinfo[i]['hlp'] = 1;
				}
			}
		}

	//Highlight all
	for(i in optinfo){
		hl(optinfo[i]['type'],optinfo[i]['id'],'b');
		}
	}

function hl(type,id,o){
	var x = document.getElementById(type+'_'+id);
	var sel = 0; if(optinfo[type+id] != undefined){ sel = optinfo[type+id]['sel']; }
	var hlp = 0; if(type == "r" && optinfo['r'+id] != undefined){ hlp = optinfo['r'+id]['hlp']; }

	if(o == 1){
		x.style.color = '#0000FF';
		} else if(o == 0) {
		x.style.color = '#000000';
		}
	if(o == 1 || sel == 1){
		x.style.backgroundImage = 'url(\'img/fade_r_'+colors[type]+'.jpg\')';
		x.style.backgroundPosition = 'center right';
		x.style.backgroundRepeat = 'repeat-y';
		} else if(hlp == 1){
		x.style.backgroundImage = 'url(\'img/fade_r_gold.jpg\')';
		x.style.backgroundPosition = 'center right';
		x.style.backgroundRepeat = 'repeat-y';
		} else {
		x.style.backgroundImage = '';
		x.style.backgroundPosition = '';
		x.style.backgroundRepeat = '';
		}
	}

function adjlcon(){
	document.getElementById('lcon').style.height = (document.getElementById('toverview').offsetHeight + 20)+'px';
	}

function addtotour(type,id){
	var di = document.getElementById('tourdir').value;

	/*if(type == "i"){
		onday = tour[di][ eval(tour[di].length-1) ]['day'];

		var newcont = document.getElementById('icontent_new').value.replace(/"/ig,'&quot;');

		var add = new Array();
			add['id'] = id;
			add['type'] = type;
			add['adjust'] = '';
			add['content'] = newcont;
			add['day'] = onday;

		tour[di].push(add);
		buildtour();
	} else {*/

	var adj = 0;
	if(type == "a"){
		var suggestadj = 0;
		if(optinfo[type+id]['adjust'] == 0 && optinfo[type+id]['price'] != undefined){
			suggestadj = eval(optinfo[type+id]['price']);
			} else {
			suggestadj = eval(optinfo[type+id]['adjust']);
			}
		//adj = prompt("Enter extra cost or savings. Negative values ok. 0 = included.",suggestadj.toFixed(2));
		adj = suggestadj.toFixed(2);
		}

	if(adj != null){
		optinfo[type+id]['adjust'] = eval(adj);
		onday = 1;
		lastr = findlastr(tour[di].length);
		if(lastr > -1 && type == "r" && optinfo['r'+id]['dep_time'] < optinfo['r'+tour[di][lastr]['id']]['arr_time']){
			onday = eval(eval(tour[di][lastr]['day'])+1);
			} else if(tour[di].length > 0){
			onday = tour[di][ eval(tour[di].length-1) ]['day'];
			}

		var add = new Array();
			add['id'] = id;
			add['type'] = type;
			add['adjust'] = eval(adj);
			add['content'] = '';
			add['day'] = onday;

		tour[di].push(add);
		buildtour();
		} //End adj if satement
	//} //End Type == i if statement
	}

function delsetup(obj){
	var x = document.getElementById('fordel');
	x.style.position = 'absolute';
	x.style.display = '';
	x.style.padding = '0px';
	x.innerHTML = obj.innerHTML;
	x.style.zIndex = 1000;

	var boxpos = findPos(obj);
	x.style.top = eval(boxpos[1])+'px';
	x.style.left = eval(boxpos[0])+'px';
	}

function rmvfromtour(key){
	var di = document.getElementById('tourdir').value;
	tour[di].splice(key,1);
	buildtour();
	}

function buildtour(){
	var di = document.getElementById('tourdir').value;

	//Check/adjust days
	if(tour[di].length > 0 && tour[di][0]['day'] != 1){
		var dif = eval(1 - eval(tour[di][0]['day']));
		for(i in tour[di]){
			var oday = eval(tour[di][i]['day']);
			tour[di][i]['day'] = eval(oday + dif);
			}
		}

	var t = document.getElementById('toverview');
	var onday = 1;

	t.innerHTML = '';
	if(tour[di].length > 0){
		var code = '<DIV ID="tourday_'+onday+'" STYLE="background:#FFFFFF url(\'img/fade_top.jpg\') bottom center repeat-x; border:1px solid #333333; padding:2px; text-align:center; font-size:12pt; font-weight:bold;">Day '+onday+'</DIV>'+"\n\n";
		for(i in tour[di]){
			lastr = findlastr(i);
			while(tour[di][i]['day'] > onday){
				onday++;
				code += '<DIV ID="tourday_'+onday+'" STYLE="background:#FFFFFF url(\'img/fade_top.jpg\') bottom center repeat-x; border:1px solid #333333; padding:2px; text-align:center; font-size:12pt; font-weight:bold;">Day '+onday+'</DIV>'+"\n\n";
				}
			var warning = new Array();
			if(i > 0 && lastr > -1 && tour[di][i]['type'] == "r" && optinfo['r'+tour[di][i]['id']]['dep_loc'] != optinfo['r'+tour[di][lastr]['id']]['arr_loc']){
				warning.push('The following route departs from a different location than the previous route arrived.');
				}
			if(i > 0 && lastr > -1 && tour[di][i]['type'] == "r" && tour[di][i]['day'] <= tour[di][lastr]['day'] && optinfo['r'+tour[di][i]['id']]['dep_time'] < optinfo['r'+tour[di][lastr]['id']]['arr_time']){
				warning.push('The following route departs at a time earlier than the previous route arrived.');
				}
			if(warning.length > 0){
				//code += '<DIV STYLE="padding:1px; cursor:help;" TITLE="Warning: '+warning.join(' ')+'"><IMG SRC="img/brokenchain.jpg" BORDER="0"></DIV>'+"\n\n";
				code += '<DIV STYLE="padding:1px; font-family:Arial; font-size:10px;"><SPAN STYLE="color:#FF0000;">Warning:</SPAN> '+warning.join('<BR>')+'</DIV>'+"\n\n";
				}

			/*if(tour[di][i]['type'] == "i"){
			code += '<DIV ID="tstep_'+i+'" STYLE="border-bottom:1px solid #DDDDDD; background:#FFFFFF url(\'img/fade_r_grey.jpg\') center right repeat-y; padding:2px; text-align:left;" onMouseOver="makeDraggable(0,this);">'+"\n";
				code += '<SPAN STYLE="padding:0px; float:right;" onMouseOver="delsetup(this);"><INPUT TYPE="button" VALUE="-" TITLE="Remove from tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="rmvfromtour(\''+i+'\');"></SPAN>'+"\n";
				code += '&#149; <SPAN STYLE="font-size:11pt;">Itinerary addition</SPAN>';
					code += '<BR><SPAN STYLE="font-size:11px; color:#333333;">"'+tour[di][i]['content'].replace(/\n/ig,'<BR>')+'"</SPAN>'+"\n";
				//code += '<SPAN STYLE="font-family:Arial; font-size:9pt; height:50px;">'+tour[di][i]['content']+'</SPAN>';
				code += '</DIV>'+"\n\n";
			} else {*/
			var optid = tour[di][i]['type']+tour[di][i]['id'];
			code += '<DIV ID="tstep_'+i+'" STYLE="border-bottom:1px solid #DDDDDD; background:#FFFFFF url(\'img/fade_r_'+colors[tour[di][i]['type']]+'.jpg\') center right repeat-y; padding:2px; text-align:left;" onMouseOver="makeDraggable(0,this);">'+"\n";
				//code += '<INPUT TYPE="hidden" NAME="tour_type[]" VALUE="'+tour[di][i]['type']+'"><INPUT TYPE="hidden" NAME="tour_id[]" VALUE="'+tour[di][i]['id']+'"><INPUT TYPE="hidden" NAME="tour_adj[]" VALUE="'+tour[di][i]['adjust']+'"><INPUT TYPE="hidden" NAME="tour_day[]" VALUE="'+onday+'">'+"\n";
				code += '<SPAN STYLE="padding:0px; float:right;" onMouseOver="delsetup(this);"><INPUT TYPE="button" VALUE="-" TITLE="Remove from tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="rmvfromtour(\''+i+'\');"></SPAN>'+"\n";
				code += '&#149; '+optinfo[optid]['name'];
					//if(tour[di][i]['adjust'] != undefined && tour[di][i]['adjust'] != 0){
					//	code += '&nbsp;<SPAN STYLE="font-size:10pt; font-weight:bold;">';
					//	if(tour[di][i]['adjust'] > 0){ code += '+'; }
					//	code += '$'+eval(tour[di][i]['adjust']).toFixed(2)+'</SPAN>';
					//	}
					code += '<BR><SPAN STYLE="font-size:10px; color:#666666;">'+optinfo[optid]['subhead']+'</SPAN>'+"\n";
				code += '</DIV>'+"\n\n";
			//} //End Type If Statement
			}
			code += '<DIV ID="tourday_'+eval(eval(onday)+1)+'" STYLE="background:#FFFFFF url(\'img/fade_top.jpg\') bottom center repeat-x; border:1px solid #333333; padding:2px; text-align:center; font-size:9pt; color:#333333;">Next Day - Drag/drop an item here to move it to a new day</DIV>'+"\n\n";
		} else {
		var code = '<SPAN STYLE="font-family:Arial; font-size:14pt; color:#999999;"><BR>Add items here<BR>from the left column<BR><BR></SPAN>';
		}
	//code += '<INPUT TYPE="button" VALUE="Test" onClick="alert( findlastr(tour[di].length) );">';
	t.innerHTML = code;

	buildtourfields();
	CreateDragContainer();
	document.getElementById('fordel').display = 'none';
	hl_left();
	db_getavail();
	adjlcon();
	}

function buildtourfields(){
	var di = document.getElementById('tourdir').value;

	var f = document.getElementById('tourfields');
	f.innerHTML = '';

	for(i in tour['f']){
		f.innerHTML += '<INPUT TYPE="hidden" NAME="tour_dir[]" VALUE="f"><INPUT TYPE="hidden" NAME="tour_type[]" VALUE="'+tour['f'][i]['type']+'"><INPUT TYPE="hidden" NAME="tour_id[]" VALUE="'+tour['f'][i]['id']+'"><INPUT TYPE="hidden" NAME="tour_adj[]" VALUE="'+tour['f'][i]['adjust']+'"><INPUT TYPE="hidden" NAME="tour_content[]" VALUE="'+tour['f'][i]['content']+'"><INPUT TYPE="hidden" NAME="tour_day[]" VALUE="'+tour['f'][i]['day']+'">'+"\n";
		}
	for(i in tour['r']){
		f.innerHTML += '<INPUT TYPE="hidden" NAME="tour_dir[]" VALUE="r"><INPUT TYPE="hidden" NAME="tour_type[]" VALUE="'+tour['r'][i]['type']+'"><INPUT TYPE="hidden" NAME="tour_id[]" VALUE="'+tour['r'][i]['id']+'"><INPUT TYPE="hidden" NAME="tour_adj[]" VALUE="'+tour['r'][i]['adjust']+'"><INPUT TYPE="hidden" NAME="tour_content[]" VALUE="'+tour['r'][i]['content']+'"><INPUT TYPE="hidden" NAME="tour_day[]" VALUE="'+tour['r'][i]['day']+'">'+"\n";
		}
	}

function findlastr(i){
	var di = document.getElementById('tourdir').value;

	findit = eval(i-1);
	if(findit > -1){
		while(tour[di][findit] != undefined && tour[di][findit]['type'] != 'r'){ findit--; }
		} else if(findit == 0 && tour[di][findit]['type'] != 'r'){
		findit = -1;
		}
	return findit;
	}

function findPos(obj){
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return [curleft,curtop];
	}

//BEGIN DRAG AND DROP FUNCTIONS
window.onload = CreateDragContainer;
document.onmousemove = mouseMove;
document.onmouseup   = mouseUp;

var dragObject  = null;
var dragOrigin = new Array('n','n');
var dragTarget = new Array('n','n');
var dropTargets = new Array(); dropTargets[0] = new Array(); dropTargets[1] = new Array(); dropTargets['days'] = new Array();
var landonday = 0;
var mouseOffset = null;
var cur = 0;

function testIsValidObject(objToTest){
	if(null == objToTest){
		return false;
	}
	if("undefined" == typeof(objToTest) ){
		return false;
	}
	return true;
}

function CreateDragContainer(){
	var di = document.getElementById('tourdir').value;

	dropTargets = new Array();
	dropTargets[0] = new Array();
	for(i in tour[di]){
		addDropTarget(0,document.getElementById('tstep_'+i));
		}
	dropTargets[1] = new Array();
	for(i in images){
		addDropTarget(1,document.getElementById('img_'+i));
		}
	dropTargets['days'] = new Array();
	var i = 1;
	while( testIsValidObject(document.getElementById('tourday_'+i)) ){
		addDropTarget('days',document.getElementById('tourday_'+i));
		i++;
		}
}

function addDropTarget(t,obj){
	dropTargets[t].push(obj);
}

function mouseCoords(ev){
	if(ev.pageX || ev.pageY){
		return {x:ev.pageX, y:ev.pageY};
	}
	return {
		x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		y:ev.clientY + document.body.scrollTop  - document.body.clientTop
	};
}

function mouseMove(ev){
	ev           = ev || window.event;
	var mousePos = mouseCoords(ev);

	if(dragObject && dropTargets[cur].length > 0){
		dragObject.style.position = 'absolute';
		dragObject.style.top      = mousePos.y - mouseOffset.y;
		dragObject.style.left     = mousePos.x - mouseOffset.x;
		dragObject.style.opacity  = '.70';
		dragObject.style.filter   = 'alpha(opacity=70)';
		dragObject.style.display = '';

		findtarget(ev);

		return false;
	}
}

function makeDraggable(t,item){
	if(!item) return;
	cur = t;
	item.onmousedown = function(ev){

		dragObject = document.getElementById("forshadow");

		if(cur == 1){
		dragObject.innerHTML = '<IMG SRC="'+item.src+'" WIDTH="'+eval(item.offsetWidth-8)+'" HEIGHT="'+eval(item.offsetHeight-8)+'" BORDER="0">';
		dragObject.style.width = item.offsetWidth;
		dragObject.style.height = item.offsetHeight;
		dragObject.style.padding = '0px';

		dragObject.style.backgroundColor = 'transparent';
		dragObject.style.backgroundImage = 'none';
		} else {
		dragObject.innerHTML = item.innerHTML;
		dragObject.style.width = item.offsetWidth;
		dragObject.style.height = item.offsetHeight;
		dragObject.style.padding = item.style.padding;

		dragObject.style.backgroundColor = item.style.backgroundColor;
		dragObject.style.backgroundImage = item.style.backgroundImage;
		dragObject.style.backgroundPosition = item.style.backgroundPosition;
		dragObject.style.backgroundRepeat = item.style.backgroundRepeat;

		dragObject.style.textAlign = 'left';
		dragObject.style.fontFamily = document.getElementById('toverview').style.fontFamily;
		dragObject.style.fontSize = document.getElementById('toverview').style.fontSize;
		}

		mouseOffset = getMouseOffset(item, ev);

		dragOrigin[cur] = 'n';
		for(i=0; i<dropTargets[cur].length; i++){
			if(item.id == dropTargets[cur][i].id){
			dragOrigin[cur] = i;
			break;
			}
		}
		return false;
	}
	item.style.cursor = "pointer";
}

function getMouseOffset(target, ev){
	ev = ev || window.event;

	var docPos    = getPosition(target);
	var mousePos  = mouseCoords(ev);
	return {x:mousePos.x - docPos.x, y:mousePos.y - docPos.y};
}

function getPosition(e){
	var left = 0;
	var top  = 0;

	while (e.offsetParent){
		left += e.offsetLeft;
		top  += e.offsetTop;
		e     = e.offsetParent;
	}

	left += e.offsetLeft;
	top  += e.offsetTop;

	return {x:left, y:top};
}

function mouseUp(ev){
	ev           = ev || window.event;
	findtarget(ev);
	movem();

	document.getElementById("forshadow").style.display = 'none';
	dragObject   = null;
	dragOrigin[cur]   = 'n';
	cleartarg();
}

function movem(){
	var di = document.getElementById('tourdir').value;

	if(dragOrigin[cur] != 'n' && dragTarget[cur] != 'n'){
	//alert(dragOrigin[cur]+' '+dragTarget[cur]);

	if(cur == 1){
		var sv = images[dragOrigin[cur]];
		images.splice(dragOrigin[cur],1);
		images.splice(dragTarget[cur],0,sv);
		rebuildimgs();
		} else {
		var sv = tour[di][dragOrigin[cur]];
		if(landonday > 0 && landonday <= sv['day']){
			sv['day'] = eval(landonday-1);
			} else if(landonday > 0 && landonday > sv['day']){
			sv['day'] = landonday;
			} else {
			sv['day'] = tour[di][dragTarget[cur]]['day'];
			}
		tour[di].splice(dragOrigin[cur],1);
		tour[di].splice(dragTarget[cur],0,sv);
		buildtour();
		}
	}
}

function findtarget(ev){
	var mousePos = mouseCoords(ev);
	for(i=0; i<dropTargets[cur].length; i++){
		var curTarget  = dropTargets[cur][i];
		var targPos    = getPosition(curTarget);
		var targWidth  = parseInt(curTarget.offsetWidth);
		var targHeight = parseInt(curTarget.offsetHeight);
		var newTarget  = 'n';
		if(
			(mousePos.x > targPos.x)                &&
			(mousePos.x < (targPos.x + targWidth))  &&
			(mousePos.y > targPos.y)                &&
			(mousePos.y < (targPos.y + targHeight))){
				// dragObject was dropped onto curTarget!
				newTarget = i;
				break;
		}
	}

	if(newTarget != "n"){
		if(newTarget != dragTarget[cur]){
		cleartarg();
		dragTarget[cur] = newTarget;
			if(cur == 1){
			if(newTarget < dragOrigin[cur]){
				document.getElementById( dropTargets[cur][newTarget].id ).style.borderLeft = "4px solid #0000FF";
				} else if(newTarget > dragOrigin[cur])  {
				document.getElementById( dropTargets[cur][newTarget].id ).style.borderRight = "4px solid #0000FF";
				}
			} else {
			if(newTarget < dragOrigin[cur]){
				document.getElementById( dropTargets[cur][newTarget].id ).style.borderTop = "4px solid #0000FF";
				} else if(newTarget > dragOrigin[cur])  {
				document.getElementById( dropTargets[cur][newTarget].id ).style.borderBottom = "4px solid #0000FF";
				}
			}
		}
	} else {
		cleartarg();
		if(cur == 0){ findtday(ev); }
	}
}

function findtday(ev){
	var di = document.getElementById('tourdir').value;

	var mousePos = mouseCoords(ev);
	for(i=0; i<dropTargets['days'].length; i++){
		var curTarget  = dropTargets['days'][i];
		var targPos    = getPosition(curTarget);
		var targWidth  = parseInt(curTarget.offsetWidth);
		var targHeight = parseInt(curTarget.offsetHeight);
		var newTarget  = 'n';
		if(
			(mousePos.x > targPos.x)                &&
			(mousePos.x < (targPos.x + targWidth))  &&
			(mousePos.y > targPos.y)                &&
			(mousePos.y < (targPos.y + targHeight))){
				// dragObject was dropped onto curTarget!
				newTarget = i;
				break;
		}
	}

	if(newTarget != "n" && dragOrigin[cur] != "n"){
		cleartarg();
		landonday = eval(eval(newTarget)+1);
		if(landonday <= tour[di][dragOrigin[cur]]['day']){ //newTarget > 0 &&
			dragTarget[cur] = dayindex(landonday);
			document.getElementById( dropTargets['days'][newTarget].id ).style.borderTop = "4px solid #0000FF";
			} else if(landonday > tour[di][dragOrigin[cur]]['day']){ //newTarget > 0 &&
			var dindex = eval(dayindex(landonday));
			if(dindex > -1){
				dragTarget[cur] = eval(dindex-1);
				} else {
				dragTarget[cur] = eval(tour[di].length-1);
				}
			document.getElementById( dropTargets['days'][newTarget].id ).style.borderBottom = "4px solid #0000FF";
			}

	} else {
		cleartarg();
	}
}

function cleartarg(){
	if(dragTarget[cur] != 'n' && testIsValidObject( dropTargets[cur][dragTarget[cur]] )){
		if(cur == 1){
			document.getElementById( dropTargets[cur][dragTarget[cur]].id ).style.borderLeft = "4px solid #FFFFFF";
			document.getElementById( dropTargets[cur][dragTarget[cur]].id ).style.borderRight = "4px solid #FFFFFF";
			} else {
			document.getElementById( dropTargets[cur][dragTarget[cur]].id ).style.borderTop = "0px solid #FFFFFF";
			document.getElementById( dropTargets[cur][dragTarget[cur]].id ).style.borderBottom = "1px solid #DDDDDD";
			}
		}
	dragTarget[cur] = 'n';

	d = 1;
	while(testIsValidObject( document.getElementById('tourday_'+d) )){
		document.getElementById('tourday_'+d).style.borderTop = "1px solid #333333";
		document.getElementById('tourday_'+d).style.borderBottom = "1px solid #333333";
		d++;
		}
	landonday = 0;
}

function dayindex(find){
	var di = document.getElementById('tourdir').value;

	var ind = -1;
	for(i in tour[di]){
		if(tour[di][i]['day'] == find || tour[di][i]['day'] > find){ ind = i; break; }
		}
	return ind;
	}

function addmeans(){
	var t = document.getElementById('meanstable');
		var r = t.insertRow(t.rows.length);

		var d = r.insertCell(0);
		d.style.verticalAlign = 'middle';
		d.innerHTML = '<SELECT NAME="means[]" STYLE="font-size:8pt;"><OPTION VALUE="">n/a</OPTION><?
		foreach($means as $row){
			echo '<OPTION VALUE="'.$row['id'].'">'.$row['name'].'</OPTION>';
				}
			echo '</SELECT>'; ?>';

		var d = r.insertCell(1);
		d.style.verticalAlign = 'middle';
		d.style.paddingLeft = '3px';
		d.innerHTML = '<INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);">';
}

function addsimilar(){
	var t = document.getElementById('similartable');
		var r = t.insertRow(t.rows.length);

		var d = r.insertCell(0);
		d.style.verticalAlign = 'middle';
		d.innerHTML = '<SELECT NAME="similar[]" STYLE="font-size:8pt; width:400px;"><OPTION VALUE="">n/a</OPTION><?
		foreach($tourlist as $row){
			echo '<OPTION VALUE="'.$row['id'].'">'.$row['id'].' : '.$row['title'].'</OPTION>';
			}
			echo '</SELECT>'; ?>';

		var d = r.insertCell(1);
		d.style.verticalAlign = 'middle';
		d.style.paddingLeft = '3px';
		d.innerHTML = '<INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);">';
}

function addupsell(){
	var t = document.getElementById('upselltable');
		var r = t.insertRow(t.rows.length);

		var d = r.insertCell(0);
		d.style.verticalAlign = 'middle';
		d.innerHTML = '<SELECT NAME="upsell[]" STYLE="font-size:8pt; width:320px;"><OPTION VALUE="">n/a</OPTION><?
		foreach($tourlist as $row){
			echo '<OPTION VALUE="'.$row['id'].'">'.$row['id'].' : '.$row['title'].'</OPTION>';
			}
			echo '</SELECT>'; ?>';

		var d = r.insertCell(1);
		d.style.verticalAlign = 'middle';
		d.style.fontFamily = 'Helvetica';
		d.style.fontSize = '10pt';
		d.innerHTML = '<INPUT TYPE="text" NAME="upsell_discount[]" VALUE="100.0" STYLE="margin-left:4px; text-align:right; width:60px;">%';

		var d = r.insertCell(2);
		d.style.verticalAlign = 'middle';
		d.style.paddingLeft = '3px';
		d.innerHTML = '<INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);">';
}

function addspeccost(){
	var t = document.getElementById('speccosts');
		var r = t.insertRow(t.rows.length);

		var d = r.insertCell(0);
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '9pt';
		d.style.verticalAlign = 'middle';
		d.style.paddingBottom = '2px';
		d.innerHTML = 'For <INPUT TYPE="text" STYLE="font-size:9pt; width:40px;" NAME="ext_guest[]" VALUE=""> guests, set cost to $<INPUT TYPE="text" STYLE="font-size:9pt; width:60px; text-align:right;" NAME="ext_price[]" VALUE="">';

		var d = r.insertCell(1);
		d.style.verticalAlign = 'middle';
		d.style.paddingLeft = '3px';
		d.style.paddingBottom = '2px';
		d.innerHTML = '<INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);">';
}

//CALENDAR FUNCTIONS
<?	//Find soonest upcoming month
	$query = 'SELECT `date` FROM `tours_dates` WHERE `tourid` = "'.getval('id').'" AND `date` > '.$time.' ORDER BY `date` ASC LIMIT 1';
	$result = mysqlQuery($query);
	$num_results = mysql_num_rows($result);
	if($num_results > 0){
	$soonest = mysql_fetch_assoc($result);
	$soonest = $soonest['date'];
	$calfirst = mktime(0,0,0,date("n",$soonest),1,date("Y",$soonest));
	} else {
	$calfirst = mktime(0,0,0,date("n",$time),1,date("Y",$time));
	}
	$calendar = array(
		"first" => $calfirst,
		"start" => date("w",$calfirst),
		"days" => date("t",$calfirst),
		"m" => date("n",$calfirst),
		"y" => date("Y",$calfirst)
		);
$calw = (7*42);

echo 'var calw = '.$calw.';'."\n";
?>

var notoggle = 'The tour can not start on this date.'+"\n"+'Available dates are determined by the routes you choose for the tour.'+"\n"+'All routes on Day 1 must be running on the date you choose to start the tour.'+"\n"+'All routes on Day 2 must be running the next day.'+"\n"+'...and so on.';

var bg = 'DDDDDD';
function bgcolor(){
	if(bg == "FFFFFF"){ bg = "DDDDDD"; } else { bg = "FFFFFF"; }
	return bg;
	}

var cal = new Array();
<? for($y=2009; $y<(date("Y",$time)+6); $y++){
	echo "\t".'cal['.$y.'] = new Array();';
		for($m=1; $m<13; $m++){
		$first = mktime(0,0,0,$m,1,$y);
		echo ' cal['.$y.']['.$m.'] = new Array('.date("w",$first).','.date("t",$first).');';
		}
		echo "\n";
	} ?>

var runs = new Array();
<? foreach($dates as $y => $years){
	echo "\t".'runs['.$y.'] = new Array();'."\n";
	foreach($years as $m => $months){
		echo "\t".'runs['.$y.']['.$m.'] = new Array();';
		foreach($months as $d => $dir){
			echo ' runs['.$y.']['.$m.']['.$d.'] = \''.$dir.'\';';
			}
		echo "\n";
		}
	echo "\n";
	} ?>

var availdates = new Array();
<? foreach($availdates as $y => $years){
	echo "\t".'availdates['.$y.'] = new Array();'."\n";
	foreach($years as $m => $months){
		echo "\t".'availdates['.$y.']['.$m.'] = new Array();';
		foreach($months as $d => $day){
			echo ' availdates['.$y.']['.$m.']['.$d.'] = \''.$day.'\';';
			}
		echo "\n";
		}
	echo "\n";
	} ?>

var blocked = new Array();
<? foreach($tours_blocked as $y => $years){
	echo "\t".'blocked['.$y.'] = new Array();'."\n";
	foreach($years as $m => $months){
		echo "\t".'blocked['.$y.']['.$m.'] = new Array();';
		foreach($months as $d => $day){
			echo ' blocked['.$y.']['.$m.']['.$d.'] = \''.$day.'\';';
			}
		echo "\n";
		}
	echo "\n";
	} ?>

function movecal(i){
	var newmonth = new Date();
	newmonth.setFullYear(document.getElementById("choose_year").value,eval(document.getElementById("choose_month").value - 1 + i),1);
	document.getElementById('choose_month').value = eval(newmonth.getMonth()+1);
	document.getElementById('choose_year').value = newmonth.getFullYear();
	buildcal(document.getElementById('choose_year').value,document.getElementById('choose_month').value);
	}

function waitforcal(){
	while(document.getElementById('calendar').rows.length > 2){ document.getElementById('calendar').deleteRow( eval(document.getElementById('calendar').rows.length-1) ) }

	var r = document.getElementById('calendar').insertRow(document.getElementById('calendar').rows.length);
	r.style.backgroundColor = '#FFFFFF';

	var c = r.insertCell(r.cells.length);
	c.colSpan = '7';
	c.style.backgroundColor = 'FFFFFF';
	c.style.height = Math.ceil(calw/7)+'px';
	c.style.fontFamily = 'Arial';
	c.style.fontSize = '10pt';
	c.style.color = '#333333';
	c.style.textAlign = 'center';
	c.style.cursor = 'wait';
	c.innerHTML = 'Please wait...<BR>The system is finding available dates and rebulding the calendar.<BR>If the calendar does not reappear shortly, please click "Save" at the bottom of the page to save your work and see an accurate calendar.';
	}

function buildcal(y,m){
	while(document.getElementById('calendar').rows.length > 2){ document.getElementById('calendar').deleteRow( eval(document.getElementById('calendar').rows.length-1) ) }

	var col = cal[y][m][0];

	//bg = 'DDDDDD';
	var r = document.getElementById('calendar').insertRow(document.getElementById('calendar').rows.length);
	r.style.backgroundColor = 'FFFFFF'; //bgcolor();

	if(cal[y][m][0] > 0){
		var c = r.insertCell(r.cells.length);
		c.colSpan = cal[y][m][0];
		c.style.backgroundColor = 'DDDDDD';
		c.style.borderTop = '1px solid #999999';
		c.style.borderRight = '1px solid #999999';
		c.style.height = Math.ceil(calw/7)+'px';
		c.style.fontSize = '4px';
		c.innerHTML = '&nbsp;';
		}

	for(day=1; day<=cal[y][m][1]; day++){
		col++;

		var c = r.insertCell(r.cells.length);
		c.id = 'd'+day;
		c.style.borderTop = '1px solid #999999';
		if(col < 7){
			c.style.borderRight = '1px solid #999999';
			}
		c.style.textAlign = 'left';
		c.style.verticalAlign = 'top';
		c.style.padding = '3px';
		c.style.cursor = 'pointer';
		c.style.width = Math.ceil(calw/7)+'px';
		c.style.height = Math.ceil(calw/7)+'px';
		c.style.fontFamily = 'Arial';
		c.style.fontSize = '11px';
		c.abbr = day;
		c.innerHTML = day;
		c.onclick = function(){ toggle_run(y,m,this.abbr); hl_runs(y,m); update_runs(); }

	if(col == 7){
		var r = document.getElementById('calendar').insertRow(document.getElementById('calendar').rows.length);
		r.style.backgroundColor = 'FFFFFF'; //bgcolor();
		col = 0;
		}

	} //End For Loop

	if(col > 0 && col < 7){
		var c = r.insertCell(r.cells.length);
		c.colSpan = eval(7-col);
		c.style.backgroundColor = 'DDDDDD';
		c.style.borderTop = '1px solid #999999';
		c.style.height = Math.ceil(calw/7)+'px';
		c.style.fontSize = '4px';
		c.innerHTML = '&nbsp;';
		}

	hl_runs(y,m);
	if(runs[y] != undefined){ sum_show_hide(y); }
	}

function toggle_run(y,m,d){
	if(runs[y] != undefined && runs[y][m] != undefined && runs[y][m][d] != undefined && runs[y][m][d] == 'f'){
		set_run(y,m,d,'r');
		} else if(runs[y] != undefined && runs[y][m] != undefined && runs[y][m][d] != undefined && runs[y][m][d] == 'r'){
		set_run(y,m,d,0);
		} else {
		set_run(y,m,d,'f');
		}
	}

function set_run(y,m,d,s){
	if(runs[y] == undefined){
		runs[y] = new Array();
		}
	if(runs[y][m] == undefined){
		runs[y][m] = new Array();
		}
	if(blocked[y] == undefined || blocked[y][m] == undefined || blocked[y][m][d] == undefined || blocked[y][m][d] == "0"){ runs[y][m][d] = s; }
	}

function hl_runs(y,m){
for(d=1; d<=cal[y][m][1]; d++){
	var td = document.getElementById('d'+d);
	var code = d;
	if(blocked[y] != undefined && blocked[y][m] != undefined && blocked[y][m][d] != undefined && blocked[y][m][d] != "0"){
		//td.onclick = '';
		td.style.cursor = 'help';
		//td.title = blocked[y][m][d]+' seats booked.  Can\'t be removed.';
		$(td).bind('mouseover', function(){
			show_booked(this);
		});
		$(td).bind('mouseout', function(){
			hide_booked();
		});
	} else {
		td.style.cursor = 'pointer';
	}
	if(availdates[y] != undefined && availdates[y][m] != undefined && availdates[y][m][d] != undefined && availdates[y][m][d] != 0){
		code += '<DIV STYLE="float:right; color:#339900; vertical-align:top; text-transform:uppercase;">'+availdates[y][m][d]+'</DIV><BR STYLE="clear:both;">';
		} else {
		td.style.color = '#000000';
		}
	code += '<DIV STYLE="text-align:center; margin-top:3px; font-size:12px;">';
	if(blocked[y] != undefined && blocked[y][m] != undefined && blocked[y][m][d] != undefined && blocked[y][m][d] != "0"){
		code += '<SPAN STYLE="color:#666666;">(<SPAN STYLE="color:#000000; font-weight:bold;">'+blocked[y][m][d]+'</SPAN>)</SPAN> ';
		}
	if(runs[y] != undefined && runs[y][m] != undefined && runs[y][m][d] != undefined && runs[y][m][d] != 0){
		td.style.backgroundColor = '#a5c7ff';
		if(runs[y][m][d] == 'r'){
			code += '<SPAN TITLE="Runs in reverse">R</SPAN>';
			} else {
			code += '<SPAN TITLE="Runs forward">F</SPAN>';
			}
		} else {
		td.style.backgroundColor = td.parentNode.style.backgroundColor;
		}
	code += '</DIV>';
	td.innerHTML = code;
	} //End for loop
	}

function bulkchange(){
	var sdate = new Date();
	sdate.setFullYear(document.getElementById("bulk_syear").value,eval(document.getElementById("bulk_smonth").value-1),document.getElementById("bulk_sday").value); sdate.setHours(0,0,0,0);
	var edate = new Date();
	edate.setFullYear(document.getElementById("bulk_eyear").value,eval(document.getElementById("bulk_emonth").value-1),document.getElementById("bulk_eday").value); edate.setHours(0,0,0,0);

	var freq = document.getElementById("bulk_frequency").value;
	var f = 0;
	var i = sdate;
	while(i >= sdate && i <= edate){
		f++;
		if(freq == "sun" && i.getDay() == 0 || freq == "mon" && i.getDay() == 1 || freq == "tue" && i.getDay() == 2 || freq == "wed" && i.getDay() == 3 || freq == "thu" && i.getDay() == 4 || freq == "fri" && i.getDay() == 5 || freq == "sat" && i.getDay() == 6 || freq != "sun" && freq != "mon" && freq != "tue" && freq != "wed" && freq != "thu" && freq != "fri" && freq != "sat" && i == sdate || f == freq){
			//if(document.getElementById("bulk_setto").value == 0 || availdates[i.getFullYear()] != undefined && availdates[i.getFullYear()][eval(i.getMonth()+1)] != undefined && availdates[i.getFullYear()][eval(i.getMonth()+1)][i.getDate()] != undefined && availdates[i.getFullYear()][eval(i.getMonth()+1)][i.getDate()] == 1){
			set_run(i.getFullYear(),eval(i.getMonth()+1),i.getDate(),document.getElementById("bulk_setto").value);
			//	}
			f = 0;
			}
		var n = new Date();
		n.setFullYear(i.getFullYear(),i.getMonth(),eval(i.getDate()+1)); n.setHours(0,0,0,0);
		i = n;
		}
	//alert(i+"\n"+edate);
	hl_runs(document.getElementById("choose_year").value,document.getElementById("choose_month").value);
	update_runs();
	}

function runs_allavail(){
	runs_rmvall();
	for(y in availdates){
		for(m in availdates[y]){
			for(d in availdates[y][m]){
				set_run(y,m,d,availdates[y][m][d]);
				}
			}
		}
	hl_runs(document.getElementById("choose_year").value,document.getElementById("choose_month").value);
	update_runs();
	}

function runs_rmvall(){
	for(y in runs){
		for(m in runs[y]){
			for(d in runs[y][m]){
				set_run(y,m,d,0);
				}
			}
		}
	hl_runs(document.getElementById("choose_year").value,document.getElementById("choose_month").value);
	update_runs();
	}

function sortNumber(a,b){ return a - b; }

var month_names = new Array(''<? for($i=1; $i<13; $i++){ echo ',\''.date("M",mktime(0,0,0,$i,1,2009)).'\''; } ?>);

function update_runs(){
	var totalruns = 0;
	var fields = '';
	var summary = '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100px;">';

	var years = new Array();
	for(y in runs){
		years.push(y);
		}
		years.sort(sortNumber);

	for(i in years){
		y = years[i];
		var ysubtotal = 0;
		for(m in runs[y]){
			for(d in runs[y][m]){
				if(runs[y][m][d] != 0){ ysubtotal++; }
				}
			}
		summary += '<TR BGCOLOR="#DDDDDD" onClick="sum_show_hide(\''+y+'\');"><TD STYLE="font-family:Arial; font-size:9pt; border-bottom:1px solid #999999; cursor:pointer;">'+y+'</TD><TD ALIGN="right" STYLE="font-family:Arial; font-size:10px; color:#666666; border-bottom:1px solid #999999; cursor:pointer;">'+ysubtotal+' dates</TD></TR>';
		for(m in runs[y]){
			var countruns = 0;
			for(d in runs[y][m]){
				fields += '<INPUT TYPE="hidden" NAME="runs[]" VALUE="'+y+'|'+m+'|'+d+'|'+runs[y][m][d]+'">';
				if(runs[y][m][d] != 0){ countruns++; }
				}
			if(countruns > 0){
				summary += '<TR><TD ID="rs_'+y+'_'+m+'_1" STYLE="font-family:Arial; font-size:10px; text-align:right; padding-left:10px; padding-right:18px; cursor:pointer;';
				if(y != document.getElementById("choose_year").value){ summary += ' display:none;'; }
				summary += '" onMouseOver="hl_sumline(\'rs_'+y+'_'+m+'_\',\'#0000FF\');" onMouseOut="hl_sumline(\'rs_'+y+'_'+m+'_\',\'#000000\');" onClick="chgcal_sumline(\''+y+'\',\''+m+'\');">'+month_names[m]+'</TD><TD ID="rs_'+y+'_'+m+'_2" STYLE="font-family:Arial; font-size:10px; text-align:right; cursor:pointer;';
				if(y != document.getElementById("choose_year").value){ summary += ' display:none;'; }
				summary += '" onMouseOver="hl_sumline(\'rs_'+y+'_'+m+'_\',\'#0000FF\');" onMouseOut="hl_sumline(\'rs_'+y+'_'+m+'_\',\'#000000\');" onClick="chgcal_sumline(\''+y+'\',\''+m+'\');">'+countruns+' dates</TD></TR>';
				}
			totalruns = eval(totalruns+countruns);
			}
		}
	summary += '<TR><TD STYLE="border-top:1px solid #666666; font-family:Arial; font-size:9pt; text-align:right; padding-right:18px;">Total</TD><TD ALIGN="right" STYLE="border-top:1px solid #666666; font-family:Arial; font-size:10px;">'+totalruns+' dates</TD></TR>';
	summary += '<TR><TD COLSPAN="2"><INPUT TYPE="button" VALUE="Clear all dates" onClick="runs_rmvall();" STYLE="font-size:8pt;"></TD></TR>';
	summary += '</TABLE>';
	document.getElementById('run_fields').innerHTML = fields;
	document.getElementById('run_summary').innerHTML = summary;
	}

function adjustend(adddays){
	var startdate = new Date();
	startdate.setFullYear(document.getElementById("bulk_syear").value,eval(document.getElementById("bulk_smonth").value-1),document.getElementById("bulk_sday").value);

	var findend = startdate.getTime();
	findend = eval(findend + (86400000*eval(adddays)));

	var end = new Date();
	end.setTime(findend);

	document.getElementById("bulk_emonth").value = eval(end.getMonth()+1);
	document.getElementById("bulk_eday").value = end.getDate();
	document.getElementById("bulk_eyear").value = end.getFullYear();
	}

function checkend(){
	//Adjust days available for month/year chosen
	var sday = document.getElementById("bulk_sday");
	if(sday.length < cal[document.getElementById("bulk_syear").value][document.getElementById("bulk_smonth").value][1]){
		while(sday.length < cal[document.getElementById("bulk_syear").value][document.getElementById("bulk_smonth").value][1]){
			var newday = document.createElement('option');
				newday.text = eval(sday.length+1);
				newday.value = eval(sday.length+1);
				try { sday.add(newday,null); } catch(ex) { sday.add(newday); }
			}
		} else if(sday.length > cal[document.getElementById("bulk_syear").value][document.getElementById("bulk_smonth").value][1]) {
		while(sday.length > cal[document.getElementById("bulk_syear").value][document.getElementById("bulk_smonth").value][1]){
			sday.remove( eval(sday.length-1) );
			}
		}
	var eday = document.getElementById("bulk_eday");
	if(eday.length < cal[document.getElementById("bulk_eyear").value][document.getElementById("bulk_emonth").value][1]){
		while(eday.length < cal[document.getElementById("bulk_eyear").value][document.getElementById("bulk_emonth").value][1]){
			var newday = document.createElement('option');
				newday.text = eval(eday.length+1);
				newday.value = eval(eday.length+1);
				try { eday.add(newday,null); } catch(ex) { eday.add(newday); }
			}
		} else if(eday.length > cal[document.getElementById("bulk_eyear").value][document.getElementById("bulk_emonth").value][1]) {
		while(eday.length > cal[document.getElementById("bulk_eyear").value][document.getElementById("bulk_emonth").value][1]){
			eday.remove( eval(eday.length-1) );
			}
		}

	//Adjust ending date if later starting date is chosen
	var startdate = new Date();
	startdate.setFullYear(document.getElementById("bulk_syear").value,eval(document.getElementById("bulk_smonth").value-1),document.getElementById("bulk_sday").value);
	var chkdate = startdate.getTime();

	var enddate = new Date();
	enddate.setFullYear(document.getElementById("bulk_eyear").value,eval(document.getElementById("bulk_emonth").value-1),document.getElementById("bulk_eday").value);
	var chkend = enddate.getTime();

	if(chkend < chkdate){ adjustend(0); }
	}

var xmlhttp;

function db_getavail(){
	var di = document.getElementById('tourdir').value;

	url = 'sups/tourvaliddates.php?get_dates_for_tour=<? echo getval('id'); ?>';
		var step = 1;
		for(i in tour['f']){ if(tour['f'][i]['type'] == "r"){
			url += '&useroutes'+step+'='+tour['f'][i]['id']+'|'+tour['f'][i]['day']+'|f';
			step++;
			}}
		for(i in tour['r']){ if(tour['r'][i]['type'] == "r"){
			url += '&useroutes'+step+'='+tour['r'][i]['id']+'|'+tour['r'][i]['day']+'|r';
			step++;
			}}
		url += '&'+Math.random();
	xmlhttp=db_GetXmlHttpObject();
	xmlhttp.onreadystatechange=db_process;
	xmlhttp.open('GET',url,true);
	xmlhttp.send(null);
	//waitforcal();
	}

function db_GetXmlHttpObject(){
	if(window.XMLHttpRequest){
		return new XMLHttpRequest();
		}
	if(window.ActiveXObject){
		return new ActiveXObject("Microsoft.XMLHTTP");
		}
	return null;
	}

function db_process(){ if(xmlhttp.readyState == 4){
	var results = xmlhttp.responseText;
	fromroute = results.split("\n");

	availdates = new Array();

	var read = 0;
	for(y in fromroute){
		if(fromroute[y] == '||END||'){ read = 0; }
		if(read == 1){
			var add = fromroute[y].split('|');
			if(availdates[add[0]] == undefined){
				availdates[add[0]] = new Array();
				}
			if(availdates[add[0]][add[1]] == undefined){
				availdates[add[0]][add[1]] = new Array();
				}
			availdates[add[0]][add[1]][add[2]] = add[3];
			}
		if(fromroute[y] == '||BEGIN||'){ read = 1; }
		}

	buildcal(document.getElementById('choose_year').value,document.getElementById('choose_month').value);
	}}

//IMAGE FUNCTIONS

var images = new Array(<? if(count($imgids) > 0): echo '\''.implode("','",$imgids).'\''; endif; ?>);
var image_data = new Array();
<?
	foreach($fillform['images'] as $img){
		$img['filename'] = str_replace("'",'\\\'',$img['filename']);
		$img['data']['filename'] = str_replace("'",'\\\'',$img['data']['filename']);
		$img['caption'] = str_replace("'",'\\\'',$img['caption']);
		echo "\t".'image_data[\'i'.$img['id'].'\'] = new Array();';
		echo ' image_data[\'i'.$img['id'].'\'][\'large\']=\''.$img['filename'].'\';';
		foreach($img['data'] as $key => $val){
			echo ' image_data[\'i'.$img['id'].'\'][\''.$key.'\']=\''.$val.'\';';
			}
		echo ' image_data[\'i'.$img['id'].'\'][\'caption\']=\''.$img['caption'].'\';';
		echo "\n";
		}
	?>

function findimgs(choose,func){
	var win = window.open("3_imagewin.php?choose="+choose+"&func="+func,"findimages","width=790,height=600,scrollbars=yes,resizable=yes");
	}

function addimg(file,thumb,w,h,id){
	image_data['i'+id] = new Array();
		image_data['i'+id]['large'] = file;
		image_data['i'+id]['filename'] = thumb;
		image_data['i'+id]['w'] = w;
		image_data['i'+id]['h'] = h;
		image_data['i'+id]['caption'] = '';

	if(testIsValidObject(images)){
		images.push(id);
		} else {
		images = new Array(id);
		}

	rebuildimgs();
	CreateDragContainer();
	}

function rmvimg(i){
	document.getElementById('delimgbtn').style.display = 'none';
	images.splice(i,1);
	rebuildimgs();
	CreateDragContainer();
	}

function rebuildimgs(){
	x = document.getElementById('images_assoc');
	var code = '';
	for(i in images){
		var id = images[i];
		code += ' <INPUT TYPE="hidden" NAME="images[]" VALUE="'+id+'">';
		code += '<IMG SRC="../images/'+image_data['i'+id]['filename']+'" BORDER="0" WIDTH="'+image_data['i'+id]['w']+'" HEIGHT="'+image_data['i'+id]['h']+'" ID="img_'+i+'" ALT="'+image_data['i'+id]['caption']+'" STYLE="border:4px solid #FFFFFF;" onMouseOver="makeDraggable(1,this); showdelimg(this,\''+i+'\');" onMouseOut="document.getElementById(\'delimgbtn\').style.display=\'none\';">';
		}
	x.innerHTML = code;
	CreateDragContainer();
	}

function showdelimg(obj,i){
	var x = document.getElementById('delimgbtn');

	var newcode = '<INPUT TYPE="button" VALUE="Remove" STYLE="font-size:8pt;" onClick="rmvimg(\''+i+'\')">';
	x.innerHTML = newcode;
	x.style.display = "";
	x.style.position = "absolute";

	var boxpos = findPos(obj);
	x.style.top = eval(boxpos[1]) + 'px';
	x.style.left = eval(boxpos[0] - 2) + 'px';
	}

function cal_showme(i,name){
	var x = document.getElementById(name);
	if(i == 'toggle' && x.style.display == 'none'){ i = ''; } else if(i == 'toggle'){ i = 'none'; }
	x.style.display = i;

	if(i == 'none'){ i = ''; } else { i = 'none'; }
	document.getElementById(name+'_s').style.display = i;
	}

function sum_show_hide(show){
	for(y in runs){
	var disp = '';
	if(y != show){ disp = 'none'; }
		for(m in runs[y]){
			if(document.getElementById('rs_'+y+'_'+m+'_1') != undefined){ document.getElementById('rs_'+y+'_'+m+'_1').style.display = disp; }
			if(document.getElementById('rs_'+y+'_'+m+'_2') != undefined){ document.getElementById('rs_'+y+'_'+m+'_2').style.display = disp; }
			}
		} //End y for loop
	}

function hl_sumline(name,c){
	document.getElementById(name+'1').style.color = c;
	document.getElementById(name+'2').style.color = c;
	}

function chgcal_sumline(y,m){
	document.getElementById('choose_month').value = m;
	document.getElementById('choose_year').value = y;
	buildcal(y,m);
	}

function is_int(value){
	if((parseFloat(value) == parseInt(value)) && !isNaN(parseInt(value))){
    	return true;
		} else {
		return false;
		}
	}

function IsNumeric(sText){
	var ValidChars = "0123456789.";
	var IsNumber = true;
	var Char;
	for(i=0; i<sText.length && IsNumber == true; i++){ 
		Char = sText.charAt(i);
		if(ValidChars.indexOf(Char) == -1){ IsNumber = false; }
		}
	return IsNumber;
	}

function cl_single(){
	var pp_single = 0;
	var pp_a = parseFloat(document.getElementById('perguest').value);
	var pp_b = parseFloat(document.getElementById('single').value);
	if(!isNaN(pp_a) && !isNaN(pp_b)){
		pp_single = (pp_a + pp_b).toFixed(2);
		} else {
		pp_single = document.getElementById('perguest').value;
		}
	document.getElementById('pp_single').innerHTML = '$'+pp_single;
	}

function cl_triple(){
	var pp_triple = 0;
	var pp_a = parseFloat(document.getElementById('perguest').value);
	var pp_b = parseFloat(document.getElementById('triple').value);
	if(!isNaN(pp_a) && !isNaN(pp_b)){
		pp_triple = (((pp_a*3)-pp_b)/3).toFixed(2);
		} else {
		pp_triple = document.getElementById('perguest').value;
		}
	document.getElementById('pp_triple').innerHTML = '$'+pp_triple;
	}

function cl_quad(){
	var pp_quad = 0;
	var pp_a = parseFloat(document.getElementById('perguest').value);
	var pp_b = parseFloat(document.getElementById('quad').value);
	if(!isNaN(pp_a) && !isNaN(pp_b)){
		pp_quad = (((pp_a*4)-pp_b)/4).toFixed(2);
		} else {
		pp_quad = document.getElementById('perguest').value;
		}
	document.getElementById('pp_quad').innerHTML = '$'+pp_quad;
	}

function finalconfirm(){
	var r = confirm('Are you sure you\'d like to save?');
	return r;
	}


var booked_showing = 0;
function show_booked(obj){
	if(booked_showing == $(obj).attr('id')){
		$('#cal_info').stop(true, true).fadeTo(50,1);
	} else {
		var offset = $(obj).offset();
		$('#cal_info').html('<img src="img/load.gif" border="0" class="show_booked_spinner">');
		$('#cal_info').css('left', (parseInt(offset.left) + parseInt($(obj).width()) + 6)+'px');
		$('#cal_info').css('top', offset.top);
		$('#cal_info').delay(500).stop(true, true).fadeTo(200,1);
	
		var day = $(obj).attr('id').replace(/d/,'');
	
		$.ajax({
			url: "sups/calassist.php",
			type: "GET",
			data: 'get=tour_booked&id_tour=<? echo getval('id'); ?>&date='+$('#choose_year').val()+'-'+$('#choose_month').val()+'-'+day,
			dataType: 'json',
			cache: false,
			error: function(){
				$('.show_booked_spinner').attr('src','img/caution16x16.png');
			},
			success: function(result){
				//alert(result);
				if(typeof result.error != "undefined"){
					$('.show_booked_spinner').attr('src','img/caution16x16.png');
				} else {
					var html = '<span style="font-size:11pt;">'+result.date_display+'</span><br>';
						html += 'Seats blocked: '+result.blocked+'<br>';
						html += '<div style="margin-top:6px;">';
						html += '	Reservations:<br>';
						for(i in result.reservations){
							html += '	<a href="3_reservations.php?view='+result.reservations[i]+'">'+result.reservations[i]+'</a><br>';
						}
						//html += result.reservations+'<br>';
						html += '</div>';
					$('#cal_info').html(html);
				}
			}
		});
	}
	booked_showing = $(obj).attr('id');
}

function hide_booked(){
	$('#cal_info').delay(500).stop(true).fadeTo(50, 0, function(){
		$('#cal_info').hide();
	});
}

function keep_booked(){
	$('#cal_info').show();
}

//-->
</SCRIPT>

<div id="cal_info" style="display:none; position:absolute; background-color:#FFFFFF; border:1px solid grey; padding:8px; font-family:Helvetica; font-size:9pt; text-align:left;" onmousemove="keep_booked();" onmouseout="hide_booked();"></div>

<? echo "\n\n";


//!BASIC
echo '<FORM METHOD="post" NAME="editform" ID="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n"; // onSubmit="return finalconfirm();"
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";


echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Basic</TD>';
		echo '</TR>'."\n\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">ID</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.getval('id').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Alias</TD><TD><INPUT TYPE="text" NAME="alias" STYLE="width:400px;" VALUE="'.getval('alias').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Title</TD><TD><INPUT TYPE="text" NAME="title" STYLE="width:400px;" VALUE="'.getval('title').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Vendor</TD><TD><SELECT NAME="vendor" STYLE="width:200px;">';
		foreach($vendors as $vendor){
		echo '<OPTION VALUE="'.$vendor['id'].'"';
		if(getval('vendor') == $vendor['id']): echo ' SELECTED'; endif;
		echo '>'.$vendor['name'].'</OPTION>';
		}
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Order weight</TD><TD><SELECT ID="order_weight" NAME="order_weight">';
	$oweights = array(
		"1" => "1 - Highest priority",
		"2" => "2",
		"3" => "3 - High priority",
		"4" => "4",
		"5" => "5 - Normal priority",
		"6" => "6",
		"7" => "7 - Low priority",
		"8" => "8",
		"9" => "9 - Lowest priority"
		);
	foreach($oweights as $key => $val){
		echo '<OPTION VALUE="'.$key.'"';
		if($key == getval('order_weight')): echo ' SELECTED'; endif;
		echo '>'.$val.'</OPTION>';
		}
	echo '</SELECT></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="text-align:right; padding-right:10px;"><INPUT TYPE="checkbox" NAME="hidden" ID="hidden" VALUE="1"';
		if(getval('hidden') == "1"): echo ' CHECKED'; endif;
		echo '></TD><TD STYLE="font-family:Arial; font-size:11pt;"><LABEL FOR="hidden">Hide this tour from public view</LABEL></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Nickname/Short name</TD><TD><INPUT TYPE="text" NAME="nickname" STYLE="width:400px;" VALUE="'.getval('nickname').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="text-align:right; padding-right:10px;"><INPUT TYPE="checkbox" NAME="pricesheet" ID="pricesheet" VALUE="1"';
		if(getval('pricesheet') == "1"): echo ' CHECKED'; endif;
		echo '></TD><TD STYLE="font-family:Arial; font-size:11pt;"><LABEL FOR="pricesheet">Include in tour price sheets</LABEL></TD></TR>'."\n";

//!DESIGN TOUR
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;"><A NAME="designtop"></A>Design Tour</TD>';
		echo '</TR>'."\n\n";
	echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="padding-left:0px; padding-right:0px;">';


//!LEFT SIDE - OPTIONS
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;"><TR>';
		echo '<TD VALIGN="top" ALIGN="center" STYLE="width:48%; padding-right:6px; border-right:2px solid #333333; text-align:center; font-family:Arial; font-size:9pt;" ID="lconcon">';
		echo '<DIV STYLE="overflow:scroll; overflow-x:hidden;" ID="lcon">';
		echo '<SPAN STYLE="font-family:Arial; font-size:11px;">Click on "Routes", "Lodging", "Lodging extensions", "Transport","Activities", or "Itinerary" to expand, choose and add to the tour in the right column.</SPAN>'."\n";
			echo '<A NAME="pl_Routes"></A><DIV ID="label_Routes" STYLE="text-align:left; background:#CCCCCC url(\'img/fade_top.jpg\') center center repeat-x; border-top:4px solid #FFFFFF; border-bottom:1px solid #333333; font-size:10pt; color:#000000; font-weight:bold; cursor:pointer;" onMouseOver="this.style.color=\'#0000FF\';" onMouseOut="this.style.color=\'#000000\';" onClick="show_hide(\'Routes\');">&nbsp;+&nbsp;Routes</DIV>'."\n"; // url(\'img/topbar.jpg\') bottom center repeat-x
			echo '<DIV ID="choose_Routes" STYLE="text-align:right; display:none;">'."\n";
				foreach($routes as $row){
				echo '<DIV ID="r_'.$row['id'].'" STYLE="border-bottom:1px solid #DDDDDD; padding:2px; text-align:left; cursor:pointer;" onMouseOver="hl(\'r\',\''.$row['id'].'\',1);" onMouseOut="hl(\'r\',\''.$row['id'].'\',0);" onDblClick="addtotour(\'r\',\''.$row['id'].'\');">';
					echo '<SPAN STYLE="float:right"><INPUT TYPE="button" VALUE="+" TITLE="Add to tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="addtotour(\'r\',\''.$row['id'].'\');"></SPAN>';
					echo '<SPAN ID="rhtml_'.$row['id'].'">';
						echo '&#149; '.$row['name'].'<BR>';
						echo '<SPAN STYLE="font-size:10px; color:#666666;">'.$row['subhead'].'</SPAN>';
						echo '</SPAN>';
					echo '</DIV>'."\n";
				}
				echo '</DIV>'."\n\n";

			echo '<A NAME="pl_Lodging"></A><DIV ID="label_Lodging" STYLE="text-align:left; background:#CCCCCC url(\'img/fade_top.jpg\') center center repeat-x; border-top:4px solid #FFFFFF; border-bottom:1px solid #333333; font-size:10pt; color:#000000; font-weight:bold; cursor:pointer;" onMouseOver="this.style.color=\'#0000FF\';" onMouseOut="this.style.color=\'#000000\';" onClick="show_hide(\'Lodging\');">&nbsp;+&nbsp;Lodging</DIV>'."\n";
			echo '<DIV ID="choose_Lodging" STYLE="text-align:right; display:none;">'."\n";
				foreach($lodging as $row){
				echo '<DIV ID="l_'.$row['id'].'" STYLE="border-bottom:1px solid #DDDDDD; padding:2px; text-align:left; cursor:pointer;" onMouseOver="hl(\'l\',\''.$row['id'].'\',1);" onMouseOut="hl(\'l\',\''.$row['id'].'\',0);" onDblClick="addtotour(\'l\',\''.$row['id'].'\');">';
					echo '<SPAN STYLE="float:right"><INPUT TYPE="button" VALUE="+" TITLE="Add to tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="addtotour(\'l\',\''.$row['id'].'\');"></SPAN>';
					echo '<SPAN ID="lhtml_'.$row['id'].'">';
						echo '&#149; '.$row['name'].'<BR>';
						echo '<SPAN STYLE="font-size:10px; color:#666666;">'.$row['subhead'].'</SPAN>';
						echo '</SPAN>';
					echo '</DIV>'."\n";
				}
				echo '</DIV>'."\n\n";

			echo '<A NAME="pl_Lodging extensions"></A><DIV ID="label_Lodging extensions" STYLE="text-align:left; background:#CCCCCC url(\'img/fade_top.jpg\') center center repeat-x; border-top:4px solid #FFFFFF; border-bottom:1px solid #333333; font-size:10pt; color:#000000; font-weight:bold; cursor:pointer;" onMouseOver="this.style.color=\'#0000FF\';" onMouseOut="this.style.color=\'#000000\';" onClick="show_hide(\'Lodging extensions\');">&nbsp;+&nbsp;Lodging extensions</DIV>'."\n";
			echo '<DIV ID="choose_Lodging extensions" STYLE="text-align:right; display:none;">'."\n";
				foreach($extlodging as $row){
				echo '<DIV ID="e_'.$row['id'].'" STYLE="border-bottom:1px solid #DDDDDD; padding:2px; text-align:left; cursor:pointer;" onMouseOver="hl(\'e\',\''.$row['id'].'\',1);" onMouseOut="hl(\'e\',\''.$row['id'].'\',0);" onDblClick="addtotour(\'e\',\''.$row['id'].'\');">';
					echo '<SPAN STYLE="float:right"><INPUT TYPE="button" VALUE="+" TITLE="Add to tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="addtotour(\'e\',\''.$row['id'].'\');"></SPAN>';
					echo '<SPAN ID="ehtml_'.$row['id'].'">';
						echo '&#149; '.$row['name'].'<BR>';
						echo '<SPAN STYLE="font-size:10px; color:#666666;">'.$row['subhead'].'</SPAN>';
						echo '</SPAN>';
					echo '</DIV>'."\n";
				}
				echo '</DIV>'."\n\n";

			echo '<A NAME="pl_Transport"></A><DIV ID="label_Transport" STYLE="text-align:left; background:#CCCCCC url(\'img/fade_top.jpg\') center center repeat-x; border-top:4px solid #FFFFFF; border-bottom:1px solid #333333; font-size:10pt; color:#000000; font-weight:bold; cursor:pointer;" onMouseOver="this.style.color=\'#0000FF\';" onMouseOut="this.style.color=\'#000000\';" onClick="show_hide(\'Transport\');">&nbsp;+&nbsp;Transport</DIV>'."\n";
			echo '<DIV ID="choose_Transport" STYLE="text-align:right; display:none;">'."\n";
				foreach($transport as $row){
				echo '<DIV ID="p_'.$row['id'].'" STYLE="border-bottom:1px solid #DDDDDD; padding:2px; text-align:left; cursor:pointer;" onMouseOver="hl(\'p\',\''.$row['id'].'\',1);" onMouseOut="hl(\'p\',\''.$row['id'].'\',0);" onDblClick="addtotour(\'p\',\''.$row['id'].'\');">';
					echo '<SPAN STYLE="float:right"><INPUT TYPE="button" VALUE="+" TITLE="Add to tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="addtotour(\'p\',\''.$row['id'].'\');"></SPAN>';
					echo '<SPAN ID="phtml_'.$row['id'].'">';
						echo '&#149; '.$row['name'].'<BR>';
						echo '<SPAN STYLE="font-size:10px; color:#666666;">'.$row['subhead'].'</SPAN>';
						echo '</SPAN>';
					echo '</DIV>'."\n";
				}
				echo '</DIV>'."\n\n";

			echo '<A NAME="pl_Activities"></A><DIV ID="label_Activities" STYLE="text-align:left; background:#CCCCCC url(\'img/fade_top.jpg\') center center repeat-x; border-top:4px solid #FFFFFF; border-bottom:1px solid #333333; font-size:10pt; color:#000000; font-weight:bold; cursor:pointer;" onMouseOver="this.style.color=\'#0000FF\';" onMouseOut="this.style.color=\'#000000\';" onClick="show_hide(\'Activities\');">&nbsp;+&nbsp;Activities</DIV>'."\n";
			echo '<DIV ID="choose_Activities" STYLE="text-align:right; display:none;">'."\n";
				foreach($activities as $row){
				echo '<DIV ID="a_'.$row['id'].'" STYLE="border-bottom:1px solid #DDDDDD; padding:2px; text-align:left; cursor:pointer;" onMouseOver="hl(\'a\',\''.$row['id'].'\',1);" onMouseOut="hl(\'a\',\''.$row['id'].'\',0);" onDblClick="addtotour(\'a\',\''.$row['id'].'\');">';
					echo '<SPAN STYLE="float:right"><INPUT TYPE="button" VALUE="+" TITLE="Add to tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="addtotour(\'a\',\''.$row['id'].'\');"></SPAN>';
					echo '<SPAN ID="ahtml_'.$row['id'].'">';
						echo '&#149; '.$row['name'].'<BR>';
						echo '<SPAN STYLE="font-size:10px; color:#666666;">'.$row['subhead'].'</SPAN>';
						echo '</SPAN>';
					echo '</DIV>'."\n";
				}
				echo '</DIV>'."\n\n";

			/*echo '<A NAME="pl_Itinerary"></A><DIV ID="label_Itinerary" STYLE="text-align:left; background:#CCCCCC url(\'img/fade_top.jpg\') center center repeat-x; border-top:4px solid #FFFFFF; border-bottom:1px solid #333333; font-size:10pt; color:#000000; font-weight:bold; cursor:pointer;" onMouseOver="this.style.color=\'#0000FF\';" onMouseOut="this.style.color=\'#000000\';" onClick="show_hide(\'Itinerary\');">&nbsp;+&nbsp;Itinerary</DIV>'."\n";
			echo '<DIV ID="choose_Itinerary" STYLE="text-align:right; background:#FFFFFF url(\'img/fade_r_grey.jpg\') center right repeat-y; display:none;">'."\n";
				echo '<DIV ID="i_*new*" STYLE="border-bottom:1px solid #DDDDDD; padding:2px; text-align:left;">';
					echo '<SPAN STYLE="float:right"><INPUT TYPE="button" VALUE="+" TITLE="Add to tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="addtotour(\'i\',\'new\');"></SPAN>';
					echo '<TEXTAREA STYLE="font-size:9pt; width:92%; height:140px;" ID="icontent_new"></TEXTAREA>';
					echo '</DIV>'."\n";
				echo '</DIV>'."\n\n";*/

			echo '<A NAME="pl_Itinerary"></A><DIV ID="label_Itinerary" STYLE="text-align:left; background:#CCCCCC url(\'img/fade_top.jpg\') center center repeat-x; border-top:4px solid #FFFFFF; border-bottom:1px solid #333333; font-size:10pt; color:#000000; font-weight:bold; cursor:pointer;" onMouseOver="this.style.color=\'#0000FF\';" onMouseOut="this.style.color=\'#000000\';" onClick="show_hide(\'Itinerary\');">&nbsp;+&nbsp;Itinerary</DIV>'."\n";
			echo '<DIV ID="choose_Itinerary" STYLE="text-align:right; display:none;">'."\n";
				foreach($itinerary as $row){
				echo '<DIV ID="i_'.$row['id'].'" STYLE="border-bottom:1px solid #DDDDDD; padding:2px; text-align:left; cursor:pointer;" onMouseOver="hl(\'i\',\''.$row['id'].'\',1);" onMouseOut="hl(\'i\',\''.$row['id'].'\',0);" onDblClick="addtotour(\'i\',\''.$row['id'].'\');">';
					echo '<SPAN STYLE="float:right"><INPUT TYPE="button" VALUE="+" TITLE="Add to tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="addtotour(\'i\',\''.$row['id'].'\');"></SPAN>';
					echo '<SPAN ID="ihtml_'.$row['id'].'">';
						echo '&#149; '.$row['name'].'<BR>';
						echo '<SPAN STYLE="font-size:10px; color:#666666;">'.$row['subhead'].'</SPAN>';
						echo '</SPAN>';
					echo '</DIV>'."\n";
				}
				echo '</DIV>'."\n\n";

			echo '</DIV>';
			echo '</TD>';


//!RIGHT SIDE - TOUR OVERVIEW
		echo '<TD VALIGN="top" ALIGN="center" STYLE="width:52%; padding-left:6px; text-align:center; font-family:Arial; font-size:11pt;">'."\n\n";

		if(!isset($_REQUEST['tourdir']) || $_REQUEST['tourdir'] != 'r'){ $_REQUEST['tourdir'] = 'f'; }

			echo '<SELECT NAME="tourdir" ID="tourdir" STYLE="font-family:Arial; font-size:12pt; text-align:center; width:100%;" onChange="buildtour();">';
				echo '<OPTION VALUE="f">Editing FORWARD version</OPTION>';
				echo '<OPTION VALUE="r"'; if($_REQUEST['tourdir'] == 'r'){ echo ' SELECTED'; } echo '>Editing REVERSE version</OPTION>';
				echo '</SELECT>';

			echo '<DIV ID="toverview" STYLE="padding-top:4px; font-family:Arial; font-size:12pt; text-align:center; min-height:300px;">'."\n";
			if(isset($fillform['steps'][$_REQUEST['tourdir']]) && count($fillform['steps'][$_REQUEST['tourdir']]) > 0){
			$colors = array('r'=>'blue','l'=>'green','e'=>'ltblue','p'=>'orange','a'=>'red','i'=>'grey');
			$onday = 1;
			$lastr = -1;
			echo '<DIV ID="tourday_'.$onday.'" STYLE="background:#FFFFFF url(\'img/fade_top.jpg\') bottom center repeat-x; border:1px solid #333333; padding:2px; text-align:center; font-size:12pt; font-weight:bold;">Day '.$onday.'</DIV>'."\n";
			foreach($fillform['steps'][$_REQUEST['tourdir']] as $i => $step){
				while($step['day'] > $onday){
					$onday++;
					echo '<DIV ID="tourday_'.$onday.'" STYLE="background:#FFFFFF url(\'img/fade_top.jpg\') bottom center repeat-x; border:1px solid #333333; padding:2px; text-align:center; font-size:12pt; font-weight:bold;">Day '.$onday.'</DIV>'."\n";
					}

				/*if($step['type'] == "i"){
				echo '<DIV ID="tstep_'.$i.'" STYLE="border-bottom:1px solid #DDDDDD; background:#FFFFFF url(\'img/fade_r_grey.jpg\') center right repeat-y; padding:2px; text-align:left;" onMouseOver="makeDraggable(0,this);">';
					echo '<SPAN STYLE="padding:0px; float:right;" onMouseOver="delsetup(this);"><INPUT TYPE="button" VALUE="-" TITLE="Remove from tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="rmvfromtour(\''.$i.'\');"></SPAN>';
					echo '&#149; <SPAN STYLE="font-size:11pt;">Itinerary addition</SPAN>';
						echo '<BR><SPAN STYLE="font-size:11px; color:#333333;">"'.nl2br($step['content']).'"</SPAN>';
					echo '</DIV>'."\n";
				} else {*/

				$warning = array();
				if($step['type'] == "l"){
					$name = $lodging['l'.$step['typeid']]['name'];
					$subhead = $lodging['l'.$step['typeid']]['subhead'];
					} elseif($step['type'] == "e"){
					$name = $extlodging['e'.$step['typeid']]['name'];
					$subhead = $extlodging['e'.$step['typeid']]['subhead'];
					} elseif($step['type'] == "p"){
					$name = $transport['p'.$step['typeid']]['reference'];
					$subhead = $transport['p'.$step['typeid']]['subhead'];
					} elseif($step['type'] == "a"){
					$name = $activities['a'.$step['typeid']]['name'];
					$subhead = $activities['a'.$step['typeid']]['subhead'];
					} elseif($step['type'] == "i"){
					$name = $itinerary['i'.$step['typeid']]['name'];
					$subhead = $itinerary['i'.$step['typeid']]['subhead'];
					} else {
					$name = $routes['r'.$step['typeid']]['name'];
					$subhead = $routes['r'.$step['typeid']]['subhead'];
					if($i > 0 && $lastr > -1 && $routes['r'.$step['typeid']]['dep_loc'] != $routes['r'.$fillform['steps'][$_REQUEST['tourdir']][$lastr]['typeid']]['arr_loc']){
						array_push($warning,'The following route departs from a different location than the previous route arrived.');
						}
					if($i > 0 && $lastr > -1 && $step['day'] <= $fillform['steps'][$_REQUEST['tourdir']][$lastr]['day'] && $routes['r'.$step['typeid']]['dep_time'] < $routes['r'.$fillform['steps'][$_REQUEST['tourdir']][$lastr]['typeid']]['arr_time']){
						array_push($warning,'The following route departs at a time earlier than the previous route arrived.');
						}
					if(count($warning) > 0){
						echo '<DIV STYLE="padding:1px; font-family:Arial; font-size:10px;"><SPAN STYLE="color:#FF0000;">Warning:</SPAN> '.implode('<BR>',$warning).'</DIV>'."\n\n";
						}
					}

				echo '<DIV ID="tstep_'.$i.'" STYLE="border-bottom:1px solid #DDDDDD; background:#FFFFFF url(\'img/fade_r_'.$colors[$step['type']].'.jpg\') center right repeat-y; padding:2px; text-align:left;" onMouseOver="makeDraggable(0,this);">';
					echo '<SPAN STYLE="padding:0px; float:right;" onMouseOver="delsetup(this);"><INPUT TYPE="button" VALUE="-" TITLE="Remove from tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="rmvfromtour(\''.$i.'\');"></SPAN>';
					echo '&#149; '.$name.'<BR><SPAN STYLE="font-size:10px; color:#666666;">'.$subhead.'</SPAN>';
					echo '</DIV>'."\n";

				//} //End type if statement

				if($step['type'] == "r"){ $lastr = $i; }
				}
				echo '<DIV ID="tourday_'.($onday+1).'" STYLE="background:#FFFFFF url(\'img/fade_top.jpg\') bottom center repeat-x; border:1px solid #333333; padding:2px; text-align:center; font-size:9pt; color:#333333;">Next Day - Drag/drop an item here to move it to a new day</DIV>'."\n";
			} else {
			echo '<SPAN STYLE="font-family:Arial; font-size:14pt; color:#999999;"><BR>Add items here<BR>from the left column<BR><BR></SPAN>'."\n";
			}
			echo "\n";

			echo '</TD>';
		echo '</TR></TABLE>'."\n\n";

		echo '</DIV>';
		echo '</TD></TR>'."\n\n";


//!CALENDAR
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Calendar / Tour Dates</TD>';
		echo '</TR>'."\n\n";

	echo '<TR STYLE="background:#'.bgcolor('').'" ID="calrow">';
		echo '<TD VALIGN="top" STYLE="padding-top:4px; font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; white-space:nowrap;">Add/Remove individual dates<BR><SPAN STYLE="font-family:Arial; font-size:8pt; font-weight:normal;"><SPAN STYLE="color:#666666;">Click once for Forward runs,<BR>again for Reverse, again to remove.</SPAN><BR><BR>';

		echo '<I>-or-</I><BR><BR>';

		echo '<SPAN STYLE="font-size:10pt; font-weight:bold; cursor:pointer;" onMouseOver="this.style.color=\'#0000FF\';" onMouseOut="this.style.color=\'#000000\';" onClick="cal_showme(\'toggle\',\'runs_allavail\');">Add dates based on routes</SPAN><BR><SPAN STYLE="color:#666666;">Find all dates to run based on current route dates.</SPAN><BR>';
		echo '<DIV ID="runs_allavail">';
		echo '<INPUT TYPE="button" STYLE="font-size:8pt;" VALUE="Add" onClick="runs_allavail()">';
		echo '</DIV><DIV ID="runs_allavail_s" STYLE="display:none;"><SPAN STYLE="color:#0000FF; cursor:pointer;" onClick="cal_showme(\'\',\'runs_allavail\');">Click here</SPAN></DIV><BR>';

		echo '<I>-or-</I><BR><BR>';

		echo '<SPAN STYLE="font-size:10pt; font-weight:bold; cursor:pointer;" onMouseOver="this.style.color=\'#0000FF\';" onMouseOut="this.style.color=\'#000000\';" onClick="cal_showme(\'toggle\',\'runs_addbatch\');">Add/Remove a batch of dates</SPAN><BR><SPAN STYLE="color:#666666;">Use this section to add/remove multiple dates quickly.<BR>Dates already booked will be ignored.</SPAN><BR>';
		echo '<DIV ID="runs_addbatch" STYLE="display:none;">';
		echo 'Add/Remove: <SELECT ID="bulk_setto" STYLE="font-size:8pt;"><OPTION VALUE="f">Add forward runs</OPTION><OPTION VALUE="r">Add reverse runs</OPTION><OPTION VALUE="0">Remove dates</OPTION></SELECT><BR>';
		echo '<NOBR>Starting: <SELECT ID="bulk_smonth" STYLE="font-size:8pt;" onChange="checkend()">';
			for($ii=1; $ii<13; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("n",$calendar['first']) == $ii ): echo " SELECTED"; endif;
			echo '>'.date("F",mktime("0","0","0",$ii,"1","2005")).'</OPTION>';
			}
			echo '</SELECT> / <SELECT ID="bulk_sday" STYLE="font-size:8pt;" onChange="checkend()">';
			for($ii=1; $ii<32; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("j",$calendar['first']) == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT> / <SELECT ID="bulk_syear" STYLE="font-size:8pt;" onChange="checkend()">';
			for($ii=2009; $ii<(date("Y",$time)+6); $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("Y",$calendar['first']) == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT></NOBR><BR>';
		echo '<NOBR>Ending: <SELECT ID="bulk_emonth" STYLE="font-size:8pt;" onChange="checkend()">';
			for($ii=1; $ii<13; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("n",$calendar['first']) == $ii ): echo " SELECTED"; endif;
			echo '>'.date("F",mktime("0","0","0",$ii,"1","2005")).'</OPTION>';
			}
			echo '</SELECT> / <SELECT ID="bulk_eday" STYLE="font-size:8pt;" onChange="checkend()">';
			for($ii=1; $ii<32; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("j",$calendar['first']) == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT> / <SELECT ID="bulk_eyear" STYLE="font-size:8pt;" onChange="checkend()">';
			for($ii=2009; $ii<(date("Y",$time)+6); $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("Y",$calendar['first']) == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT></NOBR><BR>';
		echo 'Frequency: <SELECT ID="bulk_frequency" STYLE="font-size:8pt;"><OPTION VALUE="1">Every day</OPTION><OPTION VALUE="2">Every other day</OPTION><OPTION VALUE="3">Every 3rd day</OPTION><OPTION VALUE="4">Every 4th day</OPTION><OPTION VALUE="5">Every 5th day</OPTION><OPTION VALUE="6">Every 6th day</OPTION><OPTION VALUE="sun">Every Sunday</OPTION><OPTION VALUE="mon">Every Monday</OPTION><OPTION VALUE="tue">Every Tuesday</OPTION><OPTION VALUE="wed">Every Wednesday</OPTION><OPTION VALUE="thu">Every Thursday</OPTION><OPTION VALUE="fri">Every Friday</OPTION><OPTION VALUE="sat">Every Saturday</OPTION></SELECT><BR>';
		echo '<INPUT TYPE="button" STYLE="font-size:8pt;" VALUE="Update calendar" onClick="bulkchange()">';
		echo '</SPAN></DIV><DIV ID="runs_addbatch_s"><SPAN STYLE="color:#0000FF; cursor:pointer;" onClick="cal_showme(\'\',\'runs_addbatch\');">Click here</SPAN></DIV><BR>';

		echo '</TD><TD VALIGN="top">';
		$svbg = $GLOBALS['bgcolor'];
		$GLOBALS['bgcolor'] = 'FFFFFF';

		echo '<DIV STYLE="float:left; padding-right:10px;"><TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0" ID="calendar" WIDTH="'.$calw.'" STYLE="border:1px solid #7F9DB9;">';
		echo '<TR><TD COLSPAN="7" ALIGN="center" STYLE="padding-top:4px; padding-bottom:4px;">';
			echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;">';
				echo '<TD ALIGN="left" STYLE="padding-right:8px;"><INPUT TYPE="button" STYLE="font-size:8pt;" VALUE="&lt;&lt;" onClick="movecal(-1);"></TD>';
				echo '<TD ALIGN="center" STYLE="font-size:8pt;"><SELECT ID="choose_month" STYLE="font-size:8pt;" onChange="buildcal(document.getElementById(\'choose_year\').value,document.getElementById(\'choose_month\').value);">';
			for($ii=1; $ii<13; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( $calendar['m'] == $ii ): echo " SELECTED"; endif;
			echo '>'.date("F",mktime("0","0","0",$ii,"1","2005")).'</OPTION>';
			}
			echo '</SELECT> <SELECT ID="choose_year" STYLE="font-size:8pt;" onChange="buildcal(document.getElementById(\'choose_year\').value,document.getElementById(\'choose_month\').value);">';
			for($ii=2009; $ii<(date("Y",$time)+6); $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( $calendar['y'] == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT></TD>';
		echo '<TD ALIGN="right" STYLE="padding-left:8px;"><INPUT TYPE="button" STYLE="font-size:8pt;" VALUE="&gt;&gt;" onClick="movecal(1);"></TD>';
		echo '</TABLE>';
		echo '</TD></TR>'."\n\n";

echo '<TR BGCOLOR="#CCCCCC">
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">S</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">M</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">T</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">W</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">T</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">F</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">S</TD>
	</TR>

<TR BGCOLOR="#FFFFFF">'; //'.bgcolor('DDDDDD').'

$day = 0;
$col = $calendar['start'];

if($calendar['start'] > 0): echo '<TD BGCOLOR="#FFFFFF" COLSPAN="'.$calendar['start'].'" STYLE="height:'.ceil($calw/7).'px; background-color:#DDDDDD; font-size:4px; border-top:1px solid #999999; border-right:1px solid #999999;">&nbsp;</TD>'; endif;

for($i=0; $day<$calendar['days']; $i++){
	$col++;
	++$day;
	if(isset($dates[$calendar['y']][$calendar['m']][$day])){ $thisdate = $dates[$calendar['y']][$calendar['m']][$day]; } else { $thisdate = 0; }
	if(isset($tours_blocked[$calendar['y']][$calendar['m']][$day]) && $tours_blocked[$calendar['y']][$calendar['m']][$day] > 0){ $thisblock = $tours_blocked[$calendar['y']][$calendar['m']][$day]; } else { $thisblock = 0; }

	echo '	<TD ALIGN="left" VALIGN="top" ID="d'.$day.'" ABBR="'.$day.'" STYLE="';
		if($thisdate != "0"){ echo 'background-color:#a5c7ff; '; }
		echo 'padding:3px; width:'.ceil($calw/7).'px; height:'.ceil($calw/7).'px; font-family:Arial; font-size:11px; border-top:1px solid #999999;';
		if($col < 7){ echo ' border-right:1px solid #999999;'; }
		if($thisblock > 0){
			echo ' cursor:help;" onMouseOver="show_booked(this);" onMouseOut="hide_booked();">'.$day; // TITLE="'.$thisblock.' seats booked.  Can\'t be removed."
			} else {
			echo ' cursor:pointer;" onClick="toggle_run(\''.$calendar['y'].'\',\''.$calendar['m'].'\',\''.$day.'\'); hl_runs(\''.$calendar['y'].'\',\''.$calendar['m'].'\'); update_runs();">'.$day;
			}
		if(isset($availdates[$calendar['y']][$calendar['m']][$day]) && $availdates[$calendar['y']][$calendar['m']][$day] != "0"){
			echo '<DIV STYLE="float:right; color:#339900; vertical-align:top; text-transform:uppercase;">'.$availdates[$calendar['y']][$calendar['m']][$day].'</DIV>';
			echo '<BR STYLE="clear:both;">';
			}
		echo '<DIV STYLE="text-align:center; padding-top:3px; font-size:12px;">';
			if($thisblock > 0){ echo '<SPAN STYLE="color:#666666;">(<SPAN STYLE="color:#000000; font-weight:bold;">'.$thisblock.'</SPAN>)</SPAN> '; }
			if(isset($dates[$calendar['y']][$calendar['m']][$day]) && $dates[$calendar['y']][$calendar['m']][$day] == 'r'){
				echo '<SPAN TITLE="Runs in reverse">R</SPAN>';
				} elseif(isset($dates[$calendar['y']][$calendar['m']][$day]) && $dates[$calendar['y']][$calendar['m']][$day] != "0") {
				echo '<SPAN TITLE="Runs forward">F</SPAN>';
				}
			echo '</DIV>';
		echo '</TD>'."\n";

	if($col == 7){
		//$rowbg = bgcolor('');
		echo '</TR>'."\n".'<TR BGCOLOR="#FFFFFF">'; //'.$rowbg.'
		$col = 0;
		}

	}

	if($col > 0 && $col < 7): echo '<TD BGCOLOR="#FFFFFF" COLSPAN="'.(7 - $col).'" STYLE="height:'.ceil($calw/7).'px; font-size:4px; background-color:#DDDDDD; border-top:1px solid #999999;">&nbsp;</TD>'."\n"; endif;

	echo '</TR></TABLE></DIV>'."\n\n";

echo '<SPAN STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">Summary</SPAN><BR>';
echo '<DIV ID="run_summary"><TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100px;">';
	$totalruns = 0;
	foreach($dates as $y => $year){
		$ysubtotal = 0; foreach($year as $m => $month){ $ysubtotal = ($ysubtotal+count($month)); }
		echo '<TR BGCOLOR="#DDDDDD" onClick="sum_show_hide(\''.$y.'\');"><TD STYLE="font-family:Arial; font-size:9pt; border-bottom:1px solid #999999; cursor:pointer;">'.$y.'</TD><TD ALIGN="right" STYLE="font-family:Arial; font-size:10px; color:#666666; border-bottom:1px solid #999999; cursor:pointer;">'.$ysubtotal.' dates</TD></TR>';
		foreach($year as $m => $month){
			echo '<TR><TD ID="rs_'.$y.'_'.$m.'_1" STYLE="font-family:Arial; font-size:10px; text-align:right; padding-left:10px; padding-right:18px; cursor:pointer;';
			if($y != $calendar['y']){ echo ' display:none;'; }
			echo '" onMouseOver="hl_sumline(\'rs_'.$y.'_'.$m.'_\',\'#0000FF\');" onMouseOut="hl_sumline(\'rs_'.$y.'_'.$m.'_\',\'#000000\');" onClick="chgcal_sumline(\''.$y.'\',\''.$m.'\');">'.date("M",mktime(0,0,0,$m,1,$y)).'</TD><TD ID="rs_'.$y.'_'.$m.'_2" STYLE="font-family:Arial; font-size:10px; text-align:right; cursor:pointer;';
			if($y != $calendar['y']){ echo ' display:none;'; }
			echo '" onMouseOver="hl_sumline(\'rs_'.$y.'_'.$m.'_\',\'#0000FF\');" onMouseOut="hl_sumline(\'rs_'.$y.'_'.$m.'_\',\'#000000\');" onClick="chgcal_sumline(\''.$y.'\',\''.$m.'\');">'.count($month).' dates</TD></TR>';
			$totalruns = ($totalruns+count($month));
			}
		}
	echo '<TR><TD STYLE="border-top:1px solid #666666; font-family:Arial; font-size:9pt; text-align:right; padding-right:18px;">Total</TD><TD ALIGN="right" STYLE="border-top:1px solid #666666; font-family:Arial; font-size:10px;">'.$totalruns.' dates</TD></TR>';
	echo '<TR><TD COLSPAN="2"><INPUT TYPE="button" VALUE="Clear all dates" onClick="runs_rmvall();" STYLE="font-size:8pt;"></TD></TR>';
	echo '</TABLE>';
	echo '</DIV>'."\n\n";

echo '<DIV ID="run_fields">';
foreach($dates as $y => $years){
	foreach($years as $m => $months){
		foreach($months as $d => $dir){
			echo '<INPUT TYPE="hidden" NAME="runs[]" VALUE="'.$y.'|'.$m.'|'.$d.'|'.$dir.'">';
			}
		echo "\n";
		}
	echo "\n";
	}
	echo '</DIV>'."\n\n";

$GLOBALS['bgcolor'] = $svbg;
		echo '</TD></TR>'."\n";


//!EZ Tour Finder / Tour Listings / Meta Tags
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">EZ Tour Finder / Tour Listings / Meta Tags</TD>';
		echo '</TR>'."\n\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">EZ title<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">(if different from tour title)</SPAN></TD><TD><INPUT TYPE="text" NAME="eztitle" STYLE="width:400px;" VALUE="'.getval('eztitle').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Short description</TD><TD><TEXTAREA NAME="short_desc" STYLE="width:400px; height:100px;">'.getval('short_desc').'</TEXTAREA></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Means of transportation<BR><INPUT TYPE="button" VALUE="Add another" STYLE="font-size:8pt;" onClick="addmeans();"></TD><TD>';
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" ID="meanstable">';
		if(isset($fillform['means']) && count($fillform['means']) > 0){
			foreach($fillform['means'] AS $thistype){
				echo '<TR><TD STYLE="vertical-align:middle;"><SELECT NAME="means[]" STYLE="font-size:8pt;"><OPTION VALUE="">n/a</OPTION>';
				foreach($means as $row){
					echo '<OPTION VALUE="'.$row['id'].'"';
					if($row['id'] == $thistype['transid']): echo ' SELECTED'; endif;
					echo '>'.$row['name'].'</OPTION>';
					}
				echo '</SELECT></TD><TD STYLE="vertical-align:middle; padding-left:3px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>';
				} //End For Each
		} else {
			echo '<TR><TD STYLE="vertical-align:middle;"><SELECT NAME="means[]" STYLE="font-size:8pt;"><OPTION VALUE="">n/a</OPTION>';
				foreach($means as $row){
					echo '<OPTION VALUE="'.$row['id'].'">'.$row['name'].'</OPTION>';
					}
				echo '</SELECT></TD><TD STYLE="vertical-align:center; padding-left:3px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>';
		} //End Not New If Statement
		echo '</TABLE>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Similar tours<BR><INPUT TYPE="button" VALUE="Add another" STYLE="font-size:8pt;" onClick="addsimilar();"></TD><TD>';
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" ID="similartable">';
		if(isset($fillform['similar']) && count($fillform['similar']) > 0){
			foreach($fillform['similar'] AS $sim){
				echo '<TR><TD STYLE="vertical-align:middle;"><SELECT NAME="similar[]" STYLE="font-size:8pt; width:400px;"><OPTION VALUE="">n/a</OPTION>'."\n";
				foreach($tourlist as $row){
					echo "\t".'<OPTION VALUE="'.$row['id'].'"';
					if($row['id'] == $sim['simid']): echo ' SELECTED'; endif;
					echo '>'.$row['id'].' : '.$row['title'].'</OPTION>'."\n";
					}
				echo '</SELECT></TD><TD STYLE="vertical-align:middle; padding-left:3px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>';
				} //End For Each
		} else {
			echo '<TR><TD STYLE="vertical-align:middle;"><SELECT NAME="similar[]" STYLE="font-size:8pt; width:400px;"><OPTION VALUE="">n/a</OPTION>';
				foreach($tourlist as $row){
					echo '<OPTION VALUE="'.$row['id'].'">'.$row['id'].' : '.$row['title'].'</OPTION>';
					}
				echo '</SELECT></TD><TD STYLE="vertical-align:middle; padding-left:3px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>';
		} //End Not New If Statement
		echo '</TABLE>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Upsell tours<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">100% = no discount.</SPAN><BR><INPUT TYPE="button" VALUE="Add another" STYLE="font-size:8pt;" onClick="addupsell();"></TD><TD>';
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" ID="upselltable">';
			echo '<TR><TD STYLE="font-family:Helvetica; font-size:8pt; font-weight:bold;">Tour</TD><TD STYLE="font-family:Helvetica; font-size:8pt; font-weight:bold; padding-left:4px;">Discount</TD></TR>'."\n";
		if(isset($fillform['upsell']) && count($fillform['upsell']) > 0){
			foreach($fillform['upsell'] AS $up){
				echo '<TR><TD STYLE="vertical-align:middle;"><SELECT NAME="upsell[]" STYLE="font-size:8pt; width:320px;"><OPTION VALUE="">n/a</OPTION>'."\n";
				foreach($tourlist as $row){
					echo "\t".'<OPTION VALUE="'.$row['id'].'"';
					if($row['id'] == $up['upsellid']): echo ' SELECTED'; endif;
					echo '>'.$row['id'].' : '.$row['title'].'</OPTION>'."\n";
					}
				echo '</SELECT></TD><TD STYLE="font-family:Helvetica; font-size:10pt;"><INPUT TYPE="text" NAME="upsell_discount[]" VALUE="'.$up['discount'].'" STYLE="margin-left:4px; text-align:right; width:60px;">%</TD><TD STYLE="vertical-align:middle; padding-left:3px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>';
				} //End For Each
		} else {
			echo '<TR><TD STYLE="vertical-align:middle;"><SELECT NAME="upsell[]" STYLE="font-size:8pt; width:320px;"><OPTION VALUE="">n/a</OPTION>';
				foreach($tourlist as $row){
					echo '<OPTION VALUE="'.$row['id'].'">'.$row['id'].' : '.$row['title'].'</OPTION>';
					}
				echo '</SELECT></TD><TD STYLE="font-family:Helvetica; font-size:10pt;"><INPUT TYPE="text" NAME="upsell_discount[]" VALUE="100.0" STYLE="margin-left:4px; text-align:right; width:60px;">%</TD><TD STYLE="vertical-align:middle; padding-left:3px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>';
		} //End Not New If Statement
		echo '</TABLE>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Meta title<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">(if different from tour title)</SPAN></TD><TD><INPUT TYPE="text" NAME="meta_title" STYLE="width:400px;" VALUE="'.getval('meta_title').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Meta description<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">(if different from short description)</SPAN></TD><TD><TEXTAREA NAME="meta_desc" STYLE="width:400px; height:100px;">'.getval('meta_desc').'</TEXTAREA></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Upsell headline<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">(if different from tour title)</SPAN></TD><TD><INPUT TYPE="text" NAME="upsell_headline" STYLE="width:400px;" VALUE="'.getval('upsell_headline').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Upsell description<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">(if different from short description)</SPAN></TD><TD><TEXTAREA NAME="upsell_desc" STYLE="width:400px; height:100px;">'.getval('upsell_desc').'</TEXTAREA></TD></TR>'."\n";


//!Pricing / Ordering
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Pricing / Ordering</TD>';
		echo '</TR>'."\n\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Price per person<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">If multi-day, double occupancy is assumed.</SPAN></TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="perguest" ID="perguest" STYLE="width:60px;" VALUE="'.getval('perguest').'" onKeyUp="cl_single(); cl_triple(); cl_quad();"></TD></TR>'."\n";
	//echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Volume discount<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">0\'s for no discount.</SPAN></TD><TD STYLE="font-family:Arial; font-size:11pt;">Starting at <INPUT TYPE="text" NAME="volstart" VALUE="'.getval('volstart').'" STYLE="width:50px;"> guests, charge $<INPUT TYPE="text" NAME="volrate" VALUE="'.getval('volrate').'" STYLE="width:60px;"> per person.</TD></TR>'."\n";
	//echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Agent price per person<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">If set, will override agent discount rate.</SPAN></TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="agent_perguest" STYLE="width:60px;" VALUE="'.getval('agent_perguest').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Single occupancy<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">Surcharge</SPAN></TD><TD>';
		$pp_single = number_format((getval('perguest')+getval('single')),2,'.','');
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0"><TR><TD STYLE="font-family:Arial; font-size:10pt; text-align:right; width:70px;" ID="pp_single">$'.$pp_single.'</TD><TD STYLE="font-family:Arial; font-size:10pt; text-align:right; padding-right:4px; width:20px;">+</TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="single" ID="single" STYLE="width:60px;" VALUE="'.getval('single').'" onKeyUp="cl_single()"></TD></TR></TABLE>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Triple occupancy<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">Discount</SPAN></TD><TD>';
		$pp_triple = number_format((((getval('perguest')*3)-getval('triple'))/3),2,'.','');
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0"><TR><TD STYLE="font-family:Arial; font-size:10pt; text-align:right; width:70px;" ID="pp_triple">$'.$pp_triple.'</TD><TD STYLE="font-family:Arial; font-size:10pt; text-align:right; padding-right:4px; width:20px;">-</TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="triple" ID="triple" STYLE="width:60px;" VALUE="'.getval('triple').'" onKeyUp="cl_triple()"></TD></TR></TABLE>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Quad occupancy<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">Discount</SPAN></TD><TD>';
		$pp_quad = number_format((((getval('perguest')*4)-getval('quad'))/4),2,'.','');
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0"><TR><TD STYLE="font-family:Arial; font-size:10pt; text-align:right; width:70px;" ID="pp_quad">$'.$pp_quad.'</TD><TD STYLE="font-family:Arial; font-size:10pt; text-align:right; padding-right:4px; width:20px;">-</TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="quad" ID="quad" STYLE="width:60px;" VALUE="'.getval('quad').'" onKeyUp="cl_quad()"></TD></TR></TABLE>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Master agent discount override<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">100% = no discount. 0% = use agent\'s rate.</SPAN></TD><TD STYLE="font-family:Arial; font-size:11pt;"><INPUT TYPE="text" NAME="agent1_per" STYLE="width:60px; text-align:right;" VALUE="'.getval('agent1_per').'">%</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Travel agent discount override<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">100% = no discount. 0% = use agent\'s rate.</SPAN></TD><TD STYLE="font-family:Arial; font-size:11pt;"><INPUT TYPE="text" NAME="agent2_per" STYLE="width:60px; text-align:right;" VALUE="'.getval('agent2_per').'">%</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Specific costs<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">Only for single day tours.</SPAN><BR><INPUT TYPE="button" VALUE="Add another" STYLE="font-size:8pt;" onClick="addspeccost();"></TD><TD STYLE="font-family:Arial; font-size:9pt;">';
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" ID="speccosts">';
		$ext_pricing = array(':');
		if(getval('ext_pricing') != ""){
			$ext_pricing = trim( getval('ext_pricing') );
			$ext_pricing = explode('|',$ext_pricing);
		}
		foreach($ext_pricing as $pguest){
			$pguest = explode(":",$pguest);
			echo '<TR><TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; padding-bottom:2px;">For <INPUT TYPE="text" STYLE="font-size:9pt; width:40px;" NAME="ext_guest[]" VALUE="'.$pguest[0].'"> guests, set cost to $<INPUT TYPE="text" STYLE="font-size:9pt; width:60px; text-align:right;" NAME="ext_price[]" VALUE="'.$pguest[1].'"></TD><TD STYLE="vertical-align:middle; padding-left:3px; padding-bottom:2px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>';
			}
		echo '</TABLE>';
		echo '</TD></TR>'."\n";
	//echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Minimum cost</TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="mincharge" STYLE="width:60px;" VALUE="'.getval('mincharge').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Fuel surcharge</TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="fuelsurcharge" ID="fuelsurcharge" STYLE="width:60px;" VALUE="'.getval('fuelsurcharge').'"> <SPAN STYLE="font-size:10pt;">per person</SPAN></TD></TR>'."\n";
	//echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="text-align:right; padding-right:10px;"><INPUT TYPE="checkbox" NAME="fuelsurcharge" ID="fuelsurcharge" VALUE="1"';
	//	if(getval('fuelsurcharge') == "1"): echo ' CHECKED'; endif;
	//	echo '></TD><TD STYLE="font-family:Arial; font-size:11pt;"><LABEL FOR="fuelsurcharge">This tour may be subject to a <B>fuel surcharge</B>.</LABEL></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Ask for guest weights?</TD><TD STYLE="font-family:Arial; font-size:11pt;"><INPUT TYPE="radio" NAME="getweights" ID="gwn" VALUE="n"';
		if(getval('getweights') == "n"): echo ' CHECKED'; endif;
		echo '><LABEL FOR="gwn">No</LABEL> / <INPUT TYPE="radio" NAME="getweights" ID="gwy" VALUE="y"';
		if(getval('getweights') == "y"): echo ' CHECKED'; endif;
		echo '><LABEL FOR="gwy">Yes</LABEL></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Ask for lunch type?</TD><TD STYLE="font-family:Arial; font-size:11pt;"><INPUT TYPE="radio" NAME="getlunch" ID="gltn" VALUE="n"';
		if(getval('getlunch') == "n"): echo ' CHECKED'; endif;
		echo '><LABEL FOR="gltn">No</LABEL> / <INPUT TYPE="radio" NAME="getlunch" ID="glty" VALUE="y"';
		if(getval('getlunch') == "y"): echo ' CHECKED'; endif;
		echo '><LABEL FOR="glty">Yes</LABEL></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Preferred time of day?</TD><TD STYLE="font-family:Arial; font-size:11pt;"><INPUT TYPE="radio" NAME="getpreftime" ID="gptn" VALUE="n"';
		if(getval('getpreftime') == "n"): echo ' CHECKED'; endif;
		echo '><LABEL FOR="gptn">No</LABEL> / <INPUT TYPE="radio" NAME="getpreftime" ID="gpty" VALUE="y"';
		if(getval('getpreftime') == "y"): echo ' CHECKED'; endif;
		echo '><LABEL FOR="gpty">Yes</LABEL></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Bundu costs</TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="bunducosts" STYLE="width:60px;" VALUE="'.getval('bunducosts').'"></TD></TR>'."\n";

//!Images / Video
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Images / Video</TD>';
		echo '</TR>'."\n\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD ALIGN="center" COLSPAN="2" STYLE="font-family:Arial; font-size:10pt;">'."\n";
		echo '<DIV ID="images_assoc" STYLE="text-align:center;">';
		if(is_array($fillform['images']) && count($fillform['images']) > 0){
			foreach($fillform['images'] as $key => $row){
				echo ' <INPUT TYPE="hidden" NAME="images[]" VALUE="'.$row['id'].'">';
				echo '<IMG SRC="../images/'.$row['data']['filename'].'" ID="img_'.$key.'" BORDER="0" WIDTH="'.$row['data']['w'].'" HEIGHT="'.$row['data']['h'].'" ALT="'.$row['caption'].'" STYLE="border:4px solid #FFFFFF;" onMouseOver="makeDraggable(1,this); showdelimg(this,\''.$key.'\');" onMouseOut="document.getElementById(\'delimgbtn\').style.display=\'none\';">';
				}
			} //End images_assoc if statement
			echo '</DIV>';
			echo '<CENTER><SPAN STYLE="font-family:Arial; font-size:10px; font-weight:normal; color:#666666;">Hover over an image and click on "Remove" to remove.<BR>Click and drag images to change the order in which they appear.</SPAN><BR><INPUT TYPE="button" VALUE="Add images" STYLE="font-size:9pt;" onClick="findimgs(\'m\',\'addimg\');"></CENTER>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">YouTube title</TD><TD><INPUT TYPE="text" NAME="youtube_title" STYLE="width:400px;" VALUE="'.getval('youtube_title').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">YouTube<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Copy and paste "Embed" code here.</SPAN></TD><TD><TEXTAREA NAME="youtube" STYLE="width:400px; height:100px;">'.getval('youtube').'</TEXTAREA></TD></TR>'."\n";

	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Tour Details / Verbiage</TD>';
		echo '</TR>'."\n\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD ALIGN="center" COLSPAN="2" STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">Highlights <SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">(Will appear as a bulleted list)</SPAN><BR><TEXTAREA NAME="highlights" COLS="64" ROWS="8" STYLE="width:100%; height:200px;">'.htmlspecialchars( getval('highlights') ).'</TEXTAREA></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD ALIGN="center" COLSPAN="2" STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">Details<BR><TEXTAREA NAME="details" COLS="64" ROWS="14" STYLE="width:100%; height:240px;">'.htmlspecialchars( getval('details') ).'</TEXTAREA></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD ALIGN="center" COLSPAN="2" STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">Please note the following... <SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">(Will appear as a bulleted list)</SPAN><BR><TEXTAREA NAME="pleasenote" COLS="64" ROWS="8" STYLE="width:100%; height:200px;">'.htmlspecialchars( getval('pleasenote') ).'</TEXTAREA></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="text-align:right; padding-right:10px;"><INPUT TYPE="checkbox" NAME="zion_warning" ID="zion_warning" VALUE="1"';
		if(getval('zion_warning') == "1"): echo ' CHECKED'; endif;
		echo '></TD><TD STYLE="font-family:Arial; font-size:11pt;"><LABEL FOR="zion_warning">When needed, display zion road closure warning.</LABEL></TD></TR>'."\n";

	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Home Page "Top Grand Canyon Tours"</TD>';
		echo '</TR>'."\n\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="text-align:right; padding-right:10px;"><INPUT TYPE="checkbox" NAME="toptour" ID="toptour" VALUE="1"';
		if(getval('toptour') == "1"): echo ' CHECKED'; endif;
		echo '></TD><TD STYLE="font-family:Arial; font-size:11pt;"><LABEL FOR="toptour">Include this tour on the home page as a top tour</LABEL></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Title<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">(if different from tour title)</SPAN></TD><TD><INPUT TYPE="text" SIZE="50" STYLE="width:400px;" NAME="toptitle" VALUE="'.getval('toptitle').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Short Description<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">(if different from EZ short description)</SPAN></TD><TD><TEXTAREA NAME="topdesc" COLS="50" ROWS="4" STYLE="width:400px; height:100px;">'.getval('topdesc').'</TEXTAREA></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Listing Order<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">(separate from order weight)</SPAN></TD><TD><INPUT TYPE="text" SIZE="5" NAME="toporder" VALUE="'.getval('toporder').'"></TD></TR>'."\n";


	echo '</TABLE><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";

echo '<DIV ID="tourfields" STYLE="display:none;">';
	foreach($fillform['steps']['f'] as $i => $step){
		echo '<INPUT TYPE="hidden" NAME="tour_dir[]" VALUE="'.$step['dir'].'"><INPUT TYPE="hidden" NAME="tour_type[]" VALUE="'.$step['type'].'"><INPUT TYPE="hidden" NAME="tour_id[]" VALUE="'.$step['typeid'].'"><INPUT TYPE="hidden" NAME="tour_adj[]" VALUE="'.$step['adjust'].'"><INPUT TYPE="hidden" NAME="tour_content[]" VALUE="'.$step['content'].'"><INPUT TYPE="hidden" NAME="tour_day[]" VALUE="'.$step['day'].'">'."\n";
		}
	foreach($fillform['steps']['r'] as $i => $step){
		echo '<INPUT TYPE="hidden" NAME="tour_dir[]" VALUE="'.$step['dir'].'"><INPUT TYPE="hidden" NAME="tour_type[]" VALUE="'.$step['type'].'"><INPUT TYPE="hidden" NAME="tour_id[]" VALUE="'.$step['typeid'].'"><INPUT TYPE="hidden" NAME="tour_adj[]" VALUE="'.$step['adjust'].'"><INPUT TYPE="hidden" NAME="tour_content[]" VALUE="'.$step['content'].'"><INPUT TYPE="hidden" NAME="tour_day[]" VALUE="'.$step['day'].'">'."\n";
		}
	echo '</DIV>'."\n\n";
echo '<DIV ID="forshadow" STYLE="display:none; cursor:move;"></DIV>'."\n\n";
echo '<DIV ID="fordel" STYLE="display:none;" onMouseOut="this.style.display=\'none\'"></DIV>'."\n\n";
echo '<DIV ID="delimgbtn" STYLE="display:none;" onMouseOver="this.style.display=\'\';" onMouseOut="this.style.display=\'none\';"></DIV>'."\n\n";
echo '<INPUT TYPE="hidden" NAME="check_complete" VALUE="1">'."\n\n";

if(getval('id') != "*new*"){

echo '</FORM>'."\n\n";

echo '<HR SIZE="1" WIDTH="93%"><BR>'."\n\n";

echo '<INPUT TYPE="button" ID="copybtn" VALUE="Copy this tour" STYLE="width:180px;" onClick="document.getElementById(\'copybtn\').style.display=\'none\'; document.getElementById(\'copytour\').style.display=\'\';"><BR>'."\n";

echo '<FORM METHOD="POST" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="copy">'."\n";
	echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n";
	echo '<DIV ID="copytour" STYLE="display:none;">';
		echo '<SPAN STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">Create a new tour with the following aspects of this tour:<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Be sure to save changes you\'ve made to this tour before copying.</SPAN></SPAN><BR>'."\n";
		echo '<TABLE BORDER="0" CELLSPACING="2" CELLPADDING="0">'."\n\n";
		echo '<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="basic" ID="copy_basic" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="copy_basic">Basic info, pricing, etc.</LABEL></TD></TR>'."\n";
		echo '<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="assoc" ID="copy_assoc" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="copy_assoc">Associated routes/activities/lodging</LABEL></TD></TR>'."\n";
		echo '<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="dates" ID="copy_dates" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="copy_dates">Date scheme</LABEL></TD></TR>'."\n";
		echo '<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="similar" ID="copy_similar" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="copy_similar">Similar tours</LABEL></TD></TR>'."\n";
		echo '<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="transtypes" ID="copy_transtypes" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="copy_transtypes">Means of transportation</LABEL></TD></TR>'."\n";
		echo '<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="images" ID="copy_images" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="copy_images">Images</LABEL></TD></TR>'."\n";
		echo '<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="translations" ID="copy_translations" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="copy_translations">Translations</LABEL></TD></TR>'."\n";
		echo '</TABLE>'."\n\n";
		echo '<INPUT TYPE="submit" VALUE="Copy" STYLE="width:180px;"><BR>'."\n";
		echo '</DIV>'."\n";
	echo '</FORM>'."\n\n";

	echo '<FORM NAME="archiveform" ID="archiveform" METHOD="POST" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return confirm(\'Archive tour '.getval('id').'?\');">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" ID="utaction" VALUE="archive">'."\n";
	echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n";
	echo '<INPUT TYPE="submit" VALUE="Archive '.getval('id').'" STYLE="color:#000088; width:180px;">'."\n";
	echo '</FORM>'."\n\n";

	echo '<FORM NAME="deleteform" ID="deleteform" METHOD="POST" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return confirm(\'Delete tour '.getval('id').'?\');">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" ID="utaction" VALUE="delete">'."\n";
	echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n";
	echo '<INPUT TYPE="submit" VALUE="Delete '.getval('id').'" STYLE="color:#FF0000; width:180px;">'."\n";
	echo '</FORM>'."\n\n";

} //End *new* if statement


} //End Archived if statement


} else {
//!TOUR INDEX PAGE ****************************************//


//GET TOURS
$tours = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS tours.*, COUNT(tours_dates.`tourid`) AS `upcoming`, vendors.`name` AS `vendor_name` FROM (`tours` LEFT JOIN `tours_dates` ON tours.`id` = tours_dates.`tourid` AND tours_dates.`date` > '.$time.') LEFT JOIN `vendors` ON tours.`vendor` = vendors.`id` WHERE 1 = 1';
	if($_SESSION['tours']['showhidden'] != "1"): $query .= ' AND tours.`hidden` = "0"'; endif;
	if(isset($_SESSION['tours']['filter']) && $_SESSION['tours']['filter'] == "available"){
		$query .= ' AND tours.`archived` = "0"';
		} elseif(isset($_SESSION['tours']['filter']) && $_SESSION['tours']['filter'] == "archived"){
		$query .= ' AND tours.`archived` > 0';
		}
	$query .= ' GROUP BY tours.`id`';
	$query .= ' ORDER BY '.$_SESSION['tours']['sortby'].' '.$_SESSION['tours']['sortdir'].', tours.`title` ASC';
	$query .= ' LIMIT '.(($_SESSION['tours']['p']-1)*$_SESSION['tours']['limit']).','.$_SESSION['tours']['limit'];
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tours,$row);
	}

$numitems = @mysqlQuery('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['tours']['limit']);
if($numpages > 0 && $_SESSION['tours']['p'] > $numpages): $_SESSION['tours']['p'] = $numpages; endif;


//echo '<INPUT TYPE="button" VALUE="Design a New Tour" STYLE="width:200px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';

echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";

	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">View:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="filter" STYLE="font-size:9pt;">';
		echo '<OPTION VALUE="*all*">All tours</OPTION>';
		foreach($filters as $key => $val){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['tours']['filter'] == $key): echo " SELECTED"; endif;
			echo '>'.$val.'</OPTION>'."\n";
			}
		echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['tours']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['tours']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['tours']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:0px;"><LABEL FOR="showhidden">Show hidden:</LABEL></TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="checkbox" NAME="showhidden" ID="showhidden" VALUE="1"';
		if($_SESSION['tours']['showhidden'] == "1"): echo ' CHECKED'; endif;
		echo '></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['tours']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Go" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


echo '<FORM METHOD="post" NAME="listing" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['tours']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['tours']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['tours']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['tours']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['tours']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	//echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">ID</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Title</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-right:3px; cursor:help;" TITLE="Length of tour in days">Days</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-right:3px; cursor:help;" TITLE="Number of scheduled future dates">Dates</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Vendor</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF; padding-right:3px;">Edit</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF; padding-right:3px;" TITLE="View on Bundu Bashers website">View</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($tours as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	//echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-left:6px; padding-right:8px; font-family:Arial; font-size:10pt;'.tohide($row['hidden']).'"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'">'.$row['id'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt; '.tohide($row['hidden']).'"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'" STYLE="color:inherit; text-decoration:none;">';
		if($row['hidden'] == '1'): echo '[HIDDEN] '; endif;
		echo $row['title'].'</A></TD>';
	echo '<TD ALIGN="right" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;'.tohide($row['hidden']).'">'.$row['numdays'].'</TD>';
	echo '<TD ALIGN="right" STYLE="padding-right:10px; font-family:Arial; font-size:10pt; cursor:help;'.tohide($row['hidden']).'" TITLE="Number of scheduled future dates">'.$row['upcoming'].'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:9pt; '.tohide($row['hidden']).'">'.$row['vendor_name'].'</TD>';
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '<TD ALIGN="center"><A HREF="http://www.bundubashers.com/tour.php?id='.urlencode($row['alias']).'" TARGET="_blank"><IMG SRC="img/viewico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['tours']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['tours']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['tours']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['tours']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['tours']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR><INPUT TYPE="button" VALUE="Design a New Tour" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" VALUE="CSV File" onClick="window.location=\'sups/csvexport_tours.php\'">';
	//echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" VALUE="Delete Selected" STYLE="color:#FF0000;" DISABLED>';
	echo '<BR><BR>'."\n\n";

} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";


require("footer.php");

?>