<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_transtypes";
require("validate.php");
require("header.php");

if(!isset($_SESSION['transtypes']['p'])): $_SESSION['transtypes']['p'] = 1; endif;
if(!isset($_SESSION['transtypes']['sortby'])): $_SESSION['transtypes']['sortby'] = "shuttle_transtypes.`name`"; endif;
	$sortby = array('shuttle_transtypes.`id`'=>'ID','shuttle_transtypes.`name`'=>'Name');
if(!isset($_SESSION['transtypes']['sortdir'])): $_SESSION['transtypes']['sortdir'] = "ASC"; endif;
if(!isset($_SESSION['transtypes']['limit'])): $_SESSION['transtypes']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['transtypes']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['transtypes']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['transtypes']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['transtypes']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['transtypes']['p'] = '1';
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	if($_POST['edit'] == "*new*"){
	//INSERT NEW
	$query = 'INSERT INTO `shuttle_transtypes`(`name`) VALUES("'.$_POST['name'].'")';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new market "'.$_POST['name'].'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

	} else {
	//UPDATE
	$query = 'UPDATE `shuttle_transtypes` SET `name` = "'.$_POST['name'].'" WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Saved transportation type "'.$_POST['name'].'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

	}

	//UPDATE PRICING
	@mysql_query('DELETE FROM `shuttle_pricing` WHERE `trans_type` = "'.$_REQUEST['edit'].'"');
	foreach($_POST['market'] as $key => $row){
			$query = 'INSERT INTO `shuttle_pricing`(`market`,`trans_type`,`pax1`,`pax2`,`adults`,`seniors`,`children`) VALUES("'.$_POST['market'][$key].'","'.$_REQUEST['edit'].'","'.$_POST['pax1'][$key].'","'.$_POST['pax2'][$key].'","'.$_POST['adults'][$key].'","'.$_POST['seniors'][$key].'","'.$_POST['children'][$key].'")';
			@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror != ""){ array_push($errormsg,$thiserror); }
		}
	
} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_POST['selitems']) && count($_POST['selitems']) > 0){

	$query = 'UPDATE `shuttle_transtypes` SET `disable` = 1 WHERE `id` = "'.implode('" OR `id` = "',$_POST['selitems']).'"';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,mysql_affected_rows().' transportation types were deleted.'); else: array_push($errormsg,$thiserror); endif;

}


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Shuttle Transportation Types</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){

if($_REQUEST['edit'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['edit'] == "*new*"){
	$fillform = array(
		'id' => '*new*'
		);
	} else {
	$query = 'SELECT * FROM `shuttle_transtypes` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
	$result = mysql_query($query);
	$fillform = mysql_fetch_assoc($result);
	}


//GET PRICING
$pricing = array();
$query = 'SELECT shuttle_pricing.* FROM `shuttle_pricing` LEFT JOIN `markets` ON shuttle_pricing.`market` = markets.`id` WHERE `trans_type` = "'.$_REQUEST['edit'].'" ORDER BY markets.`name` ASC, `pax1` ASC, `pax2` ASC, `adults` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($pricing,$row);
	}
	//echo '<PRE>'; print_r($pricing); echo '</PRE>';

//GET MARKETS
$markets = array();
$query = 'SELECT * FROM `markets` ORDER BY markets.`name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($markets,$row);
	}


?><SCRIPT><!--

function addline(){
	var t = document.getElementById('pricingtable');
		var r = t.insertRow(t.rows.length);

		var d = r.insertCell(0);
		d.style.verticalAlign = 'middle';
		d.innerHTML = '<TD STYLE="vertical-align:middle; padding-right:5px;"><SELECT NAME="market[]" STYLE="font-size:8pt;"><?
		foreach($markets as $row){
			echo '<OPTION VALUE="'.$row['id'].'">'.$row['name'].'</OPTION>';
			}
			echo '</SELECT>'; ?>';

		var d = r.insertCell(1);
		d.style.verticalAlign = 'middle';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '9pt';
		d.style.paddingRight = '5px';
		d.innerHTML = '<INPUT TYPE="text" NAME="pax1[]" STYLE="font-size:9pt; width:30px;"> to <INPUT TYPE="text" NAME="pax2[]" STYLE="font-size:9pt; width:30px;">';

		var d = r.insertCell(2);
		d.style.verticalAlign = 'middle';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '9pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '5px';
		d.innerHTML = '=';
		
		var d = r.insertCell(3);
		d.style.verticalAlign = 'middle';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '9pt';
		d.style.paddingRight = '5px';
		d.innerHTML = '$<INPUT TYPE="text" NAME="adults[]" VALUE="0.00" STYLE="font-size:9pt; width:60px;">';

		var d = r.insertCell(4);
		d.style.verticalAlign = 'middle';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '9pt';
		d.style.paddingRight = '5px';
		d.innerHTML = '$<INPUT TYPE="text" NAME="seniors[]" VALUE="0.00" STYLE="font-size:9pt; width:60px;">';

		var d = r.insertCell(5);
		d.style.verticalAlign = 'middle';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '9pt';
		d.innerHTML = '$<INPUT TYPE="text" NAME="children[]" VALUE="0.00" STYLE="font-size:9pt; width:60px;">';
		
		var d = r.insertCell(6);
		d.style.verticalAlign = 'middle';
		d.style.paddingLeft = '3px';
		d.innerHTML = '<INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);">';
	}

//--></SCRIPT><?


bgcolor('');

echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";

echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Name</TD><TD><INPUT TYPE="text" NAME="name" STYLE="width:400px;" VALUE="'.getval('name').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Pricing<BR><INPUT TYPE="button" VALUE="Add another" STYLE="font-size:8pt;" onClick="addline()"></TD><TD STYLE="font-family:Arial; font-size:9pt;">';
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" ID="pricingtable">';
		echo '<TR><TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt;">Market</TD><TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-right:5px;">Pax to Pax</TD><TD ALIGN="center" STYLE="padding-right:5px; font-family:Arial; font-size:9pt; font-weight:bold;">=</TD><TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt;">Adults</TD><TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt;">Seniors</TD><TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt;">Children</TD></TR>'."\n";
		if(isset($pricing) && count($pricing) > 0){
			foreach($pricing as $thisrow){
				echo '<TR><TD STYLE="vertical-align:middle; padding-right:5px;"><SELECT NAME="market[]" STYLE="font-size:8pt;">';
				foreach($markets as $row){
					echo '<OPTION VALUE="'.$row['id'].'"';
					if($row['id'] == $thisrow['market']): echo ' SELECTED'; endif;
					echo '>'.$row['name'].'</OPTION>';
					}
				echo '</SELECT></TD>';
				echo '<TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; padding-right:5px;"><INPUT TYPE="text" NAME="pax1[]" VALUE="'.$thisrow['pax1'].'" STYLE="font-size:9pt; width:30px;"> to <INPUT TYPE="text" NAME="pax2[]" VALUE="'.$thisrow['pax2'].'" STYLE="font-size:9pt; width:30px;"></TD>';
				echo '<TD ALIGN="center" STYLE="padding-right:5px; font-family:Arial; font-size:9pt; font-weight:bold;">=</TD>';
				echo '<TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; padding-right:5px;">$<INPUT TYPE="text" NAME="adults[]" VALUE="'.$thisrow['adults'].'" STYLE="font-size:9pt; width:60px;"></TD>';
				echo '<TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; padding-right:5px;">$<INPUT TYPE="text" NAME="seniors[]" VALUE="'.$thisrow['seniors'].'" STYLE="font-size:9pt; width:60px;"></TD>';
				echo '<TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle;">$<INPUT TYPE="text" NAME="children[]" VALUE="'.$thisrow['children'].'" STYLE="font-size:9pt; width:60px;"></TD>';
				echo '<TD STYLE="vertical-align:middle; padding-left:3px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>';
				} //End For Each
		} else {
			echo '<TR><TD STYLE="vertical-align:middle; padding-right:5px;"><SELECT NAME="market[]" STYLE="font-size:8pt;">';
				foreach($markets as $row){
					echo '<OPTION VALUE="'.$row['id'].'">'.$row['name'].'</OPTION>';
					}
				echo '</SELECT></TD>';
				echo '<TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; padding-right:5px;"><INPUT TYPE="text" NAME="pax1[]" STYLE="font-size:9pt; width:30px;"> to <INPUT TYPE="text" NAME="pax2[]" STYLE="font-size:9pt; width:30px;"></TD>';
				echo '<TD ALIGN="center" STYLE="padding-right:5px; font-family:Arial; font-size:9pt; font-weight:bold;">=</TD>';
				echo '<TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; padding-right:5px;">$<INPUT TYPE="text" NAME="adults[]" VALUE="0.00" STYLE="font-size:9pt; width:60px;"></TD>';
				echo '<TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; padding-right:5px;">$<INPUT TYPE="text" NAME="seniors[]" VALUE="0.00" STYLE="font-size:9pt; width:60px;"></TD>';
				echo '<TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle;">$<INPUT TYPE="text" NAME="children[]" VALUE="0.00" STYLE="font-size:9pt; width:60px;"></TD>';
				echo '<TD STYLE="vertical-align:center; padding-left:3px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>';
		} //End Not New If Statement
		echo '</TABLE>';
		echo '</TD></TR>'."\n";

	echo '</TABLE><BR>'."\n\n";


echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";


} else {


?><SCRIPT><!--

function selectall(){
	i=0;
	while(document.getElementById("sel"+i)){
		document.getElementById("sel"+i).checked = document.getElementById("selall").checked;
		i++;
		}
}

function del(){
	var r=confirm("Delete checked transportation types?");
	return r;
}

//--></SCRIPT><?


//GET TRANSPORTATION TYPES
$transtypes = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS * FROM `shuttle_transtypes`';
	$query .= ' WHERE shuttle_transtypes.`disable` != 1';
	$query .= ' ORDER BY '.$_SESSION['transtypes']['sortby'].' '.$_SESSION['transtypes']['sortdir'].', shuttle_transtypes.`name` ASC';
	$query .= ' LIMIT '.(($_SESSION['transtypes']['p']-1)*$_SESSION['transtypes']['limit']).','.$_SESSION['transtypes']['limit'];
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($transtypes,$row);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['transtypes']['limit']);
if($numpages > 0 && $_SESSION['transtypes']['p'] > $numpages): $_SESSION['transtypes']['p'] = $numpages; endif;


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['transtypes']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['transtypes']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['transtypes']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['transtypes']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['transtypes']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['transtypes']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['transtypes']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['transtypes']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['transtypes']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" ID="indextable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Name</TD>';
	echo '<TD></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($transtypes as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'">'.$row['name'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:8pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'">Edit Pricing</A></TD>';
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['transtypes']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['transtypes']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['transtypes']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['transtypes']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['transtypes']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR><INPUT TYPE="button" VALUE="New Transportation Type" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" VALUE="Delete Selected" STYLE="color:#FF0000;">';
	echo '<BR><BR>'."\n\n";

} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";


require("footer.php");

?>