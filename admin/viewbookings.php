<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com


if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	$pageid = "3_bookings";
	require("validate.php");
	}


$assoc = array();
$query = 'SELECT reservations.`id`, reservations_assoc.`reference`, reservations_assoc.`type`, reservations_assoc.`name`, reservations_assoc.`date`, reservations_assoc.`dep_time`, vendors.`name` AS `vendor`';
	//$query .= ', reservations_assoc.`vendorconf`';
	$query .= ', reservations_assoc.`trans_type`, (reservations_assoc.`adults`+reservations_assoc.`seniors`+reservations_assoc.`children`) as `totalpax`';
	//$query .= ', (SELECT tours_dates.`dir` FROM `tours_dates` WHERE tours_dates.`tourid` = reservations_assoc.`tourid` AND reservations_assoc.`date` = tours_dates.`date` LIMIT 1) as `dir`';
	$query .= ' FROM (`reservations_assoc` JOIN `reservations` ON reservations_assoc.`reservation` = reservations.`id` AND reservations.`canceled` = 0) LEFT JOIN `vendors` ON reservations_assoc.`vendor` = vendors.`id`';
	$query .= ' WHERE reservations_assoc.`canceled` = 0';
		$query .= ' AND (reservations_assoc.`vendor` > 1 AND reservations_assoc.`canceled` = 0 AND reservations_assoc.`vendorconf` LIKE "")';
		$query .= ' AND reservations_assoc.`date` >= "'.$today.'"';
	$query .= ' ORDER BY `date` ASC, `dep_time` ASC';
	//echo '<SPAN STYLE="font-size:10px;">'.$query.'<BR></SPAN>';
$result = mysql_query($query);
$needed = mysql_num_rows($result);
	for($i=0; $i<$needed; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($assoc,$row);
	}

$html = '';

if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	$html .= '<HTML>'."\n\n";
	$html .= '<HEAD><TITLE>Needed Bookings</TITLE><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><LINK HREF="stylesheet.css" REL="stylesheet" TYPE="text/css"></HEAD>'."\n\n";
	$html .= '<BODY BGCOLOR="#FFFFFF" TOPMARGIN="0" LEFTMARGIN="0" onLoad="javascript:window.print();">'."\n\n";
	$html .= '<CENTER>'."\n\n";
	}


//echo '</CENTER><PRE>'; print_r($assoc); echo '</PRE>';

$html .= '<TABLE BORDER="0" WIDTH="94%" CELLSPACING="0" CELLPADDING="2" STYLE="empty-cells:show">'."\n";

if($needed == 0){

$html .= '<TR><TD COLSPAN="3" ALIGN="center" STYLE="font-family:Arial; font-size:12pt;"><I>- No needed bookings found. -</I></TD></TR>'."\n\n";

} else {

//CONVERSION ARRAYS
$conv_binary = array('No','Yes');
$conv_types = array(
	'a' => 'Activity/Tour option',
	'r' => 'Bundu route',
	't' => 'Bundu tour',
	'l' => 'Lodging',
	's' => 'Shuttle',
	'p' => 'Tour transport',
	'o' => 'Other'
	);

//GET SHUTTLE TRANS TYPES
$transtypes = array();
$query = 'SELECT * FROM `shuttle_transtypes` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$transtypes['t'.$row['id']] = $row;
	}

bgcolor('');

$html .= '<TR STYLE="background:#FFFFFF url(\'https://www.bundubashers.com/admin/img/toplinksback.jpg\') center center repeat-x;"><TD ALIGN="center" COLSPAN="6" STYLE="border-bottom:solid 1px #000000; padding:6px; font-family:Arial; font-size:10pt;">';
	$html .= '<I>The following subreservations do not have vendor confirmation numbers.</I>';
	$html .= '</TD></TR>'."\n\n";

foreach($assoc as $key => $row){
	$html .= '<TR STYLE="background:#'.bgcolor('').'">';
		$html .= '<TD VALIGN="top" CLASS="viewsched" STYLE="padding-left:4px;"><SPAN STYLE="font-weight:bold;';
			if($row['date'] < mktime(0,0,0,date("n"),(date("j")+7),date("Y"))){ $html .= ' color:#FF0000;'; }
			$html .= '">'.date("m/d/Y",$row['date']).'</SPAN><BR><SPAN STYLE="font-size:9pt;"><I>'.date("g:ia",$row['dep_time']).'</I></SPAN></TD>';
		$html .= '<TD VALIGN="top" CLASS="viewsched" STYLE="padding-left:4px;"><B>'.$row['name'].'</B><BR><SPAN STYLE="font-size:9pt;"><I><A HREF="https://www.bundubashers.com/admin/3_reservations.php?view='.$row['id'].'" TITLE="View Reservation">'.$row['id'].'</A></I></SPAN></TD>';
		$html .= '<TD VALIGN="top" CLASS="viewsched" STYLE="padding-left:4px; font-size:9pt;">';
			if($row['type'] == "s"){
				$html .= $transtypes['t'.$row['trans_type']]['name'].'<BR>';
				} else {
				$html .= $conv_types[$row['type']].'<BR>';
				}
			$html .= 'Pax: '.$row['totalpax'];
			$html .= '</TD>';
		$html .= '<TD VALIGN="top" CLASS="viewsched" STYLE="padding-left:4px; font-size:9pt;">';
			if(strlen($row['reference']) > 100){
				$html .= substr($row['reference'],0,100).'...';
				} else {
				$html .= $row['reference'];
				}
			$html .= '</TD>';
		$html .= '<TD VALIGN="top" CLASS="viewsched" STYLE="padding-left:4px;">'.$row['vendor'].'</TD>';
		if(!isset($_REQUEST['print']) || $_REQUEST['print'] != "y"){
			$html .= '<TD VALIGN="top" ALIGN="center" CLASS="viewsched">';
				$html .= '<A HREF="https://www.bundubashers.com/admin/3_reservations.php?view='.$row['id'].'" TITLE="View Reservation"><IMG SRC="https://www.bundubashers.com/admin/img/viewico.gif" BORDER="0"></A>';
				$html .= '<BR>';
				$html .= '<A HREF="https://www.bundubashers.com/admin/3_reservations.php?edit='.$row['id'].'" TITLE="Edit Reservation"><IMG SRC="https://www.bundubashers.com/admin/img/editico.gif" BORDER="0"></A>';
				$html .= '</TD>';
			}
		$html .= '</TR>'."\n";

	} //End ForEach


} //End $needed if statement

$html .= '</TABLE><BR>'."\n\n";


if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	$html .= '</CENTER>'."\n\n";
	$html .= '</BODY>'."\n\n";
	$html .= '</HTML>'."\n\n";
	}


if(!isset($email) || $email != "y"){
	echo $html;
	}



?>