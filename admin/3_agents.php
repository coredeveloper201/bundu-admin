<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

//$_REQUEST['edit'] = '1144'; echo 'REMOVE THIS LINE!!!!!!';

$working = 0;

$pageid = "3_agents";
require("validate.php");
require("header.php");
//require_once("../common.inc.php");

if(!isset($_SESSION['agents']['p'])): $_SESSION['agents']['p'] = 1; endif;
if(!isset($_SESSION['agents']['sortby'])): $_SESSION['agents']['sortby'] = "agents.`name`"; endif;
	$sortby = array('agents.`id`'=>'ID','agents.`name`'=>'Business name','agents.`tier`'=>'Type','agents.`discount`'=>'Discount');
if(!isset($_SESSION['agents']['sortdir'])): $_SESSION['agents']['sortdir'] = "ASC"; endif;
if(!isset($_SESSION['agents']['limit'])): $_SESSION['agents']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['agents']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['agents']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['agents']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['agents']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['agents']['p'] = '1';
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	if($_POST['edit'] == "*new*"){
		//INSERT NEW
		$_POST['pass'] = md5($_POST['pass']);
		$query = 'INSERT INTO `agents`(`name`,`user`,`pass`,`discount`,`tier`,`contact`,`contact_last`,`phone`,`email`,`address`,`address2`,`city`,`state`,`zip`,`country`,`iata`)';
			$query .= ' VALUES("'.$_POST['name'].'","'.$_POST['user'].'","'.$_POST['pass'].'","'.$_POST['discount'].'","'.$_POST['tier'].'","'.$_POST['contact'].'","'.$_POST['contact_last'].'","'.$_POST['phone'].'","'.$_POST['email'].'","'.$_POST['address'].'","'.$_POST['address2'].'","'.$_POST['city'].'","'.$_POST['state'].'","'.$_POST['zup'].'","'.$_POST['country'].'","'.$_POST['iata'].'")';
			@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror == ""){ $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new agent "'.$_POST['name'].'" ('.$_REQUEST['edit'].').'); } else { array_push($errormsg,$thiserror); }

	} else {
		//UPDATE
		$query = 'UPDATE `agents` SET `name` = "'.$_POST['name'].'", `user` = "'.$_POST['user'].'", `discount` = "'.$_POST['discount'].'", `tier` = "'.$_POST['tier'].'", `contact` = "'.$_POST['contact'].'", `contact_last` = "'.$_POST['contact_last'].'", `phone` = "'.$_POST['phone'].'", `email` = "'.$_POST['email'].'", `address` = "'.$_POST['address'].'", `address2` = "'.$_POST['address2'].'", `city` = "'.$_POST['city'].'", `state` = "'.$_POST['state'].'", `city` = "'.$_POST['city'].'", `state` = "'.$_POST['state'].'", `zip` = "'.$_POST['zip'].'", `country` = "'.$_POST['country'].'", `iata` = "'.$_POST['iata'].'"';
			if(trim($_POST['pass']) != ""){
				$query .= ', `pass` = "'.md5($_POST['pass']).'"';
				array_push($successmsg,'Changed password for agent "'.$_POST['name'].'" ('.$_REQUEST['edit'].').');
				}
			$query .= ' WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
			@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror == ""){ array_push($successmsg,'Saved agent "'.$_POST['name'].'" ('.$_REQUEST['edit'].').'); } else { array_push($errormsg,$thiserror); }
	}

	foreach($_POST['subagent_id'] as $key => $id_subagent) {
		if(@$_POST['subagent_user'][$key] == "") {
			continue;
		}
		$subagentObj = new subagent($id_subagent);
		$subagentObj->setIDagent($_REQUEST['edit']);
		$subagentObj->setName(@$_POST['subagent_name'][$key]);
		$subagentObj->setUser($_POST['subagent_user'][$key]);
		if(trim(@$_POST['subagent_pass'][$key]) != "") {
			$subagentObj->setPass($_POST['subagent_pass'][$key]);
		}
		$subagentObj->save();
		$successmsg[] = 'Saved subagent "'.@$_POST['subagent_name'][$key].'" ('.$subagentObj->id.')';
		unset($subagentObj);
	}

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_POST['selitems']) && count($_POST['selitems']) > 0){

	$query = 'DELETE FROM `agents` WHERE `id` = "'.implode('" OR `id` = "',$_POST['selitems']).'"';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,mysql_affected_rows().' agents were deleted.'); else: array_push($errormsg,$thiserror); endif;

}


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Agents</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){

if($_REQUEST['edit'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['edit'] == "*new*"){
	$fillform = array(
		'id' => '*new*'
		);
	} else {
	$query = 'SELECT * FROM `agents` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
	$result = mysql_query($query);
	$fillform = mysql_fetch_assoc($result);
	}


bgcolor('');

echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";

echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">ID</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.getval('id').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Business name</TD><TD><INPUT TYPE="text" NAME="name" STYLE="width:300px;" VALUE="'.getval('name').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">IATA number</TD><TD><INPUT TYPE="text" NAME="iata" STYLE="width:300px;" VALUE="'.getval('iata').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Type</TD><TD><SELECT ID="tier" NAME="tier">';
	$tiers = array(
		"1" => "Master agent",
		"2" => "Travel agent"
		);
	foreach($tiers as $key => $val){
		echo '<OPTION VALUE="'.$key.'"';
		if($key == getval('tier')): echo ' SELECTED'; endif;
		echo '>'.$val.'</OPTION>';
		}
	echo '</SELECT></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Discount<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">% of regular price: 100% = no discount</SPAN></TD><TD STYLE="font-family:Arial; font-size:12pt;"><INPUT TYPE="text" NAME="discount" STYLE="width:60px;" VALUE="'.getval('discount').'">%</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Username</TD><TD><INPUT TYPE="text" NAME="user" STYLE="width:300px;" VALUE="'.getval('user').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Password<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;"></SPAN></TD><TD><INPUT TYPE="password" NAME="pass" STYLE="width:300px;"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Contact\'s First Name</TD><TD><INPUT TYPE="text" NAME="contact" STYLE="width:300px;" VALUE="'.getval('contact').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Contact\'s Last Name</TD><TD><INPUT TYPE="text" NAME="contact_last" STYLE="width:300px;" VALUE="'.getval('contact_last').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Phone</TD><TD><INPUT TYPE="text" NAME="phone" STYLE="width:300px;" VALUE="'.getval('phone').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Email</TD><TD><INPUT TYPE="text" NAME="email" STYLE="width:300px;" VALUE="'.getval('email').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Address</TD><TD><INPUT TYPE="text" NAME="address" STYLE="width:300px;" VALUE="'.getval('address').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Address 2</TD><TD><INPUT TYPE="text" NAME="address2" STYLE="width:300px;" VALUE="'.getval('address2').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">City</TD><TD><INPUT TYPE="text" NAME="city" STYLE="width:300px;" VALUE="'.getval('city').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">State/Province</TD><TD><INPUT TYPE="text" NAME="state" STYLE="width:300px;" VALUE="'.getval('state').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Zip/Postal Code</TD><TD><INPUT TYPE="text" NAME="zip" STYLE="width:300px;" VALUE="'.getval('zip').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Country</TD><TD><INPUT TYPE="text" NAME="country" STYLE="width:300px;" VALUE="'.getval('country').'"></TD></TR>'."\n";

	echo '</TABLE>'."\n\n";


$query = 'SELECT * FROM subagents WHERE id_agent = "'.getval('id').'"';
$result = mysql_query($query);
$subagents = array();
while($row = @mysql_fetch_assoc($result)) {
	$subagents[] = $row;
}
//Add a blank entry to allow new subagents
$subagents[] = array();

?><br>

<style>
	.subagent_tbl th {
		text-align: left;
	}
</style>

<b>Subagents:</b>
<table class="subagent_tbl" border="0" width="93%" cellspacing="0" cellpadding="3" style="font-size:11px;">
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Username</th>
		<th>Change Password</th>
	</tr>

	<? if(count($subagents) == 0) { ?> 
		<tr id="subagent_none_tr">
			<td colspan="4" align="center"><i>-None-</i></td>
		</tr>
	<? } ?> 

	<? foreach($subagents as $row) { ?> 
		<tr style="background-color:#<?=bgcolor('')?>">
			<td align="right">
				<?=($row['id']>0?$row['id']:'New:')?> 
				<input type="hidden" name="subagent_id[]" value="<?=$row['id']?>">
			</td>
			<td>
				<input type="text" name="subagent_name[]" value="<?=$row['name']?>" style="width:180px;">
			</td>
			<td>
				<input type="text" name="subagent_user[]" value="<?=$row['user']?>" style="width:180px;">
			</td>
			<td>
				<input type="password" name="subagent_pass[]" value="" style="width:180px;">
			</td>
		</tr>
	<? } ?> 
</table><br>

<?
echo '<DIV STYLE="font-family:Arial; font-size:11pt; color:red; font-style:italic; margin-bottom:8px;">Note: You must provide a unique Username before you can save an Agent or Subagent.</DIV>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";


} else {


?><SCRIPT><!--

function selectall(){
	i=0;
	while(document.getElementById("sel"+i)){
		document.getElementById("sel"+i).checked = document.getElementById("selall").checked;
		i++;
		}
}

function del(){
	var r=confirm("Delete checked agents?");
	return r;
}

//--></SCRIPT><?


//GET AGENTS
$agents = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS * FROM `agents`';
	$query .= ' ORDER BY '.$_SESSION['agents']['sortby'].' '.$_SESSION['agents']['sortdir'].', agents.`name` ASC';
	$query .= ' LIMIT '.(($_SESSION['agents']['p']-1)*$_SESSION['agents']['limit']).','.$_SESSION['agents']['limit'];
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($agents,$row);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['agents']['limit']);
if($numpages > 0 && $_SESSION['agents']['p'] > $numpages): $_SESSION['agents']['p'] = $numpages; endif;


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['agents']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['agents']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['agents']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['agents']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['agents']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['agents']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['agents']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['agents']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['agents']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" ID="indextable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Business name</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Type</TD>';
	//echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Username</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Discount</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Contact</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">View Res.</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

$tiers = array(
	"1" => "Master",
	"2" => "Travel"
	);
		
foreach($agents as $key => $row){
	$thisemail = strtolower($row['email']);
		if(isset($row['contact']) && $row['contact'] != ""){ $thisemail = $row['contact']; }
	if(strlen($thisemail) > 20){ $thisemail = substr($thisemail,0,18).'...'; }
echo '<TR STYLE="background:#'.bgcolor('').'">';
	echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'">'.$row['name'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$tiers[$row['tier']].'</TD>';
	//echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['user'].'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt; text-align:right;">'.$row['discount'].'%</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;"><A HREF="mailto:'.$row['email'].'">'.$thisemail.'</A><BR>'.$row['phone'].'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:9pt; white-space:nowrap;"><A HREF="3_agents_reservations.php?utaction=changesort&view=unconfirmed&agent='.$row['id'].'">Unconfirmed</A> / <A HREF="3_agents_reservations.php?utaction=changesort&view=confirmed&agent='.$row['id'].'">Confirmed</A> / <A HREF="3_agents_reservations.php?utaction=changesort&view=*&agent='.$row['id'].'">All</A></TD>';
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['agents']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['agents']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['agents']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['agents']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['agents']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR><INPUT TYPE="button" VALUE="New Agent" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" VALUE="Delete Selected" STYLE="color:#FF0000;">';
	echo '<BR><BR>'."\n\n";


//GET RATES
$rates = array();
$query = 'SELECT DISTINCT `discount`,`tier` FROM `agents` WHERE `discount` > 0 ORDER BY `discount` ASC, `tier` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($rates,$row);
	}

echo '<BR><FONT FACE="Arial" SIZE="5"><U>Tour Price Sheets</U></FONT>';

echo '<TABLE BORDER="0" CELLSPACING="0" ID="indextable" STYLE="width:60%">'."\n";
echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Discount</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Type</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Download</TD>';
	echo '</TR>'."\n\n";

foreach($rates as $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt;">'.$row['discount'].'%</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt;">'.$tiers[$row['tier']].'</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; white-space:nowrap;"><A HREF="sups/csvexport_agents.php?discount='.$row['discount'].'&type='.$row['tier'].'">Download</A></TD>';
	echo '</TR>'."\n\n";
	}

	
} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";


require("footer.php");

?>