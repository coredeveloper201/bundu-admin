<?  // This custom utility created by Dominick Bernal - www.bernalwebservices.com

if(!isset($path_to_common)) {
	$path_to_common = '../';
}
require_once $path_to_common.'common.inc.php';

//error_reporting('E_ALL'); ini_set('display_errors','1');

/*
if(!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on"){
	$redirect = 'https://'.$_SERVER["HTTP_HOST"].$_SERVER["PHP_SELF"];
	if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != ""){ $redirect .= '?'.$_SERVER['QUERY_STRING']; }
	header('Location: '.$redirect);
	exit;
}
*/

$time = time();
$today = mktime(0,0,0,date("n",$time),date("j",$time),date("Y",$time));

session_start();

if(!isset($_SESSION['valid_user']) && isset($_COOKIE['UTASess']) && $_COOKIE['UTASess'] != ""){
	$refresh = explode('|',$_COOKIE['UTASess']);
	$_SESSION['valid_user']['id'] = $refresh[0];
	$_SESSION['valid_user']['user'] = $refresh[1];
	$_SESSION['valid_user']['passcr'] = $refresh[2];
	//echo 'session refreshed<BR>';
}

//GET AUTHORIZATION
if(isset($thisuser)): unset($thisuser); endif;
@mysql_query('SET time_zone = "'.date("P").'"');
$query = 'select * from `users` where `username` = "'.$_SESSION['valid_user']['user'].'" and `password` = "'.$_SESSION['valid_user']['passcr'].'" limit 1';
@$result = mysql_query($query);
@$num_results = mysql_num_rows($result);
$thisuser = mysql_fetch_assoc($result);

if($num_results != 1){
	header("Location: index.php?logout=y");
	exit;
} elseif(!isset($_SESSION['valid_user'])){
	header("Location: index.php");
	exit;
}


setcookie("UTASess",$thisuser['userid'].'|'.$thisuser['username'].'|'.$thisuser['password'].'|'.($time+21600),(time()+21600),dirname($_SERVER["PHP_SELF"]),$_SERVER["HTTP_HOST"],TRUE);


//RESTRICTED ACCESS
if($thisuser['allow'] != "*"){
	$thisuser['allow'] = explode('|',$thisuser['allow']);
	$check = $pageid;
	//For reservations page, if user doesn't have full edit access, try basic edit access
	//if($check == "3_reservations_edit" && !in_array($check,$thisuser['allow'])){
	//	$check = '3_reservations_basic_edit';
	//}
	if( !in_array($check,$thisuser['allow']) ){
		require_once("header.php");
		echo '<CENTER><BR><BR><FONT FACE="Arial" SIZE="2" COLOR="#FF0000"><B>Your access to this page is restricted.</B></FONT><BR></CENTER>';
		require_once("footer.php");
		exit;
	}
}

//LANGUAGES
if($thisuser['languages'] != "*"){
	$thisuser['languages'] = explode('|',$thisuser['languages']);
	}

//WORKING...
if(isset($working) && $working == 1 && $thisuser['username'] != "dominick"){
	header("Location: /bundubashers.com/admin/working.php");
	exit;
} elseif(isset($working) && $working == 1) {
	echo '<FONT FACE="Arial" SIZE="2" COLOR="#FF0000"><B>This page is currently down for maintenance.<BR>Please make no changes until this message disappears.</B></FONT><BR>';
}

//PREPARE MULTIPLE USER WARNING
if(isset($pageid) && trim($pageid) != "" && isset($_REQUEST['edit']) && trim($_REQUEST['edit']) != "" && $_REQUEST['edit'] != "*new*"){
	$query = 'UPDATE `users` SET `last_tool` = "'.trim($pageid).'", `last_edit` = "'.trim($_REQUEST['edit']).'", `modified` = CURRENT_TIMESTAMP WHERE `userid` = "'.$thisuser['userid'].'" LIMIT 1';
	@mysql_query($query);
	//echo mysql_error();
	}


//GLOBAL FUNCTIONS
function printmsgs($successmsg,$errormsg){
	if(isset($successmsg) && count($successmsg) > 0 || isset($errormsg) && count($errormsg) > 0): echo '<IMG SRC="spacer.gif" BORDER="0"><BR>'."\n"; endif;
	if(isset($successmsg) && count($successmsg) > 0){
	echo '<TABLE BORDER="0" BGCOLOR="#000C7F" CELLSPACING="1" CELLPADDING="2" WIDTH="600">
	<TR BGCOLOR="#FFFFFF"><TD ALIGN="center"><FONT FACE="Arial" SIZE="3">Update Successful</FONT></TD></TR>
	<TR BGCOLOR="#D9DBEC"><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><I>'.implode("<BR>",$successmsg).'</I><BR></FONT></TD></TR>
	</TABLE><BR>'."\n\n";
	}
	if(isset($errormsg) && count($errormsg) > 0){
	echo '<TABLE BORDER="0" BGCOLOR="#B00000" CELLSPACING="1" CELLPADDING="2" WIDTH="600">
	<TR BGCOLOR="#FFFFFF"><TD ALIGN="center"><FONT FACE="Arial" SIZE="3">Error:</FONT></TD></TR>
	<TR BGCOLOR="#F3D9D9"><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><I>'.implode("<BR>",$errormsg).'</I></FONT></TD></TR>
	</TABLE><BR>'."\n\n";
	}
}


function bgcolor($i=""){
	if(!isset($GLOBALS['bgcolor']) || $i == "reset"): $GLOBALS['bgcolor'] = "FFFFFF"; endif;
	if($i != "reset" && $i != ""): $GLOBALS['bgcolor'] = $i; endif;
	if($GLOBALS['bgcolor'] == "FFFFFF"): $GLOBALS['bgcolor'] = "EEEEEE"; else: $GLOBALS['bgcolor'] = "FFFFFF"; endif;
	return $GLOBALS['bgcolor'];
}


function imgform($filename,$maxw,$maxh){
	global $_SERVER;
	$checksmll = explode(".",$filename);
	if( file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.reset($checksmll).'_small.'.end($checksmll)) ){
		$thisimage = reset($checksmll).'_small.'.end($checksmll);
		} else {
		$thisimage = $filename;
		}
	$imgsize = getimagesize($_SERVER['DOCUMENT_ROOT'].'/images/'.$thisimage);
		if($imgsize[0] > $maxw){
			$newsize[0] = $maxw;
			$d = ($newsize[0] / $imgsize[0]);
			$newsize[1] = ceil($imgsize[1] * $d);
			$imgsize = $newsize;
		}
		if($imgsize[1] > $maxh){
			$newsize[1] = $maxh;
			$d = ($newsize[1] / $imgsize[1]);
			$newsize[0] = ceil($imgsize[0] * $d);
			$imgsize = $newsize;
		}
	$thisimage = array(
		"filename" => $thisimage,
		"w" => $imgsize[0],
		"h" => $imgsize[1]
		);
	//echo '<PRE>'; print_r($thisimage); echo '</PRE>';
	return $thisimage;
}


function imgformat($thisimage,$maxw,$maxh){
	global $_SERVER;
	if( !file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.$thisimage) ): $thisimage = 'noimg.gif'; endif;
	$imgsize = getimagesize($_SERVER['DOCUMENT_ROOT'].'/images/'.$thisimage);
		if($imgsize[0] > $maxw){
			$newsize[0] = $maxw;
			$d = ($newsize[0] / $imgsize[0]);
			$newsize[1] = ceil($imgsize[1] * $d);
			$imgsize = $newsize;
		}
		if($imgsize[1] > $maxh){
			$newsize[1] = $maxh;
			$d = ($newsize[1] / $imgsize[1]);
			$newsize[0] = ceil($imgsize[0] * $d);
			$imgsize = $newsize;
		}
	$thisimage = array(
		"filename" => $thisimage,
		"w" => $imgsize[0],
		"h" => $imgsize[1]
		);
	//echo '<PRE>'; print_r($thisimage); echo '</PRE>';
	return $thisimage;
}


function getval($f){
	global $fillform;
	if(isset($fillform[$f]) && trim($fillform[$f]) != ""){
	return trim($fillform[$f]);
	}
}

function getval2($f){
	global $fillform;
	if(isset($fillform[$f]) && trim($fillform[$f]) != ""){
	$val = $fillform[$f];
	$val = str_replace('"','&quot;',$val);
	return trim($val);
	}
}


?>
