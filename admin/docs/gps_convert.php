<?

$coords = array(
	array("Yellowstone Pizza and Internet, 3 Yellowstone Avenue, West Yellowstone MT 59758","44|39|31.32","111|5|50.55"),
	array("Bright Angel Lodge, Grand Canyon, AZ","36|8|16.35","113|24|3.16"),
	array("Salt Lake Plaza Hotel at Temple Square, 122 W South Temple, Salt Lake City, UT","40|46|11.25","111|53|43.67"),
	array("Ruby's Inn, Bryce Canyon","37|37|37.98","112|10|20.16"),
	array("Antelope Canyon Tours, 22 S Lake Powell Blvd, Page AZ","36|55|6.62","111|27|34.02"),
	array("Sol Foods, 95 Zion Park Blvd. Springdale, UT","37|12|0.05","112|59|23.97"),
	array("Furnace Creek Ranch, Highway 190, Death Valley, CA","36|27|22.80","116|52|5.68"),
	array("Goulding's Lodge, Monument Valley","37|6|18.73","110|35|46.56"),
	array("Tsegi, AZ","36|39|0.05","110|25|44.78"),
	array("San Francisco Fisherman|s Wharf","37|48|31.20","122|24|54.60"),
	array("Yosemite Lodge parking lot","37|44|39.73","119|35|50.62"),
	array("Kanab Denny's Wigwam","37|2|52.44","112|31|36.88"),
	array("Moab City Market","38|34|1.48","109|33|2.23"),
	array("Stratosphere Las Vegas","36|8|52.71","115|9|19.18"),
	array("Albertson's Jackson","43|28|21.21","110|47|17.48"),
	array("St. George Chevron gas station","37|4|53.34","113|34|54.94"),
	array("Mimi's Restaurant Phoenix","33|40|39.13","111|58|38.18"),
	array("Sedona Chamber of Commerce","34|52|7.63","111|45|42.49"),
	array("99 Ranch Market","33|59|46.55","117|53|19.83"),
	array("Tropicana Hotel tour bus parking lot","36|5|56.54","115|10|13.59")
	
	);

echo '<PRE>';

foreach($coords as $stop){
	$stop[1] = explode('|',$stop[1]);
	$stop[2] = explode('|',$stop[2]);
	echo $stop[0]."\t".($stop[1][0] + $stop[1][1]/60 + $stop[1][2]/3600)."\t".'-'.($stop[2][0] + $stop[2][1]/60 + $stop[2][2]/3600)."\n";
	}

echo '</PRE>';

?>