<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_bbtickets";
require("validate.php");
require("header.php");

if(!isset($_SESSION['bbtickets']['p'])): $_SESSION['bbtickets']['p'] = 1; endif;
if(!isset($_SESSION['bbtickets']['sortby'])): $_SESSION['bbtickets']['sortby'] = 'bundubus_tickets.`id`'; endif;
	$sortby = array('bundubus_tickets.`id`'=>'Ticket ID','bundubus_tickets.`date_booked`'=>'Date booked','bundubus_tickets.`firstname`'=>'Name','bundubus_tickets.`days`'=>'Number of days','`start`'=>'Start date/time','`numtrips`'=>'Routes reserved');
if(!isset($_SESSION['bbtickets']['sortdir'])): $_SESSION['bbtickets']['sortdir'] = "DESC"; endif;
if(!isset($_SESSION['bbtickets']['limit'])): $_SESSION['bbtickets']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['bbtickets']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['bbtickets']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['bbtickets']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['bbtickets']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['bbtickets']['p'] = '1';
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	$_POST['date_booked'] = mktime(0,0,0,$_POST['date_booked_month'],$_POST['date_booked_day'],$_POST['date_booked_year']);

	if($_POST['edit'] == "*new*"){
	//INSERT NEW
	$query = 'INSERT INTO `bundubus_tickets`(`regid`,`email`,`firstname`,`lastname`,`phone_homebus`,`phone_cell`,`cell_country`,`days`,`cc_num`,`cc_expdate`,`cc_scode`,`cc_name`,`amount`,`pay_method`,`conf_details`,`pnp_orderid`,`notes`,`booker`,`date_booked`)';
		$query .= ' VALUES("'.$_POST['regid'].'","'.$_POST['email'].'","'.$_POST['firstname'].'","'.$_POST['lastname'].'","'.$_POST['phone_homebus'].'","'.$_POST['phone_cell'].'","'.$_POST['cell_country'].'","'.$_POST['days'].'","'.$_POST['cc_num'].'","'.$_POST['cc_expdate'].'","'.$_POST['cc_scode'].'","'.$_POST['cc_name'].'","'.$_POST['amount'].'","'.$_POST['pay_method'].'","'.$_POST['conf_details'].'","'.$_POST['pnp_orderid'].'","'.$_POST['notes'].'","'.$_POST['booker'].'","'.$_POST['date_booked'].'")';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new HOHO ticket '.$_REQUEST['edit'].' for '.$_POST['firstname'].' '.$_POST['lastname'].'.'); else: array_push($errormsg,$thiserror); endif;
	} else {
	//UPDATE
	$query = 'UPDATE `bundubus_tickets` SET `regid` = "'.$_POST['regid'].'", `email` = "'.$_POST['email'].'", `firstname` = "'.$_POST['firstname'].'", `lastname` = "'.$_POST['lastname'].'", `phone_homebus` = "'.$_POST['phone_homebus'].'", `phone_cell` = "'.$_POST['phone_cell'].'", `cell_country` = "'.$_POST['cell_country'].'", `days` = "'.$_POST['days'].'", `cc_num` = "'.$_POST['cc_num'].'", `cc_expdate` = "'.$_POST['cc_expdate'].'", `cc_scode` = "'.$_POST['cc_scode'].'", `cc_name` = "'.$_POST['cc_name'].'", `amount` = "'.$_POST['amount'].'", `pay_method` = "'.$_POST['pay_method'].'", `conf_details` = "'.$_POST['conf_details'].'", `pnp_orderid` = "'.$_POST['pnp_orderid'].'", `notes` = "'.$_POST['notes'].'", `booker` = "'.$_POST['booker'].'", `date_booked` = "'.$_POST['date_booked'].'"';
		$query .= ' WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Saved HOHO ticket '.$_REQUEST['edit'].' for '.$_POST['firstname'].' '.$_POST['lastname'].'.'); else: array_push($errormsg,$thiserror); endif;
	}

	//RUN CREDIT CARD -------------------------
	if(isset($_POST['runcard']) && $_POST['runcard'] == "y"){
		$pnp_publisher_name = "utahtransp";
		$pnp_publisher_password = "1q2w3e4r5t";

		if(isset($_POST['authtype']) && $_POST['authtype'] == "cc_prevcharge"){
		$pnp_mode = "authprev";
		$pnp_reauthtype = "authpostauth";
		$pnp_prevorderid = $_POST['pnp_orderid'];

		} elseif(isset($_POST['authtype']) && $_POST['authtype'] == "cc_refund"){
		$pnp_mode = "return";
		$pnp_order_id = $_POST['pnp_orderid'];

		} elseif(isset($_POST['authtype']) && $_POST['authtype'] == "cc_refund6mo"){
		$pnp_mode = "returnprev";
		$pnp_prevorderid = $_POST['pnp_orderid'];

		} else {
		$pnp_mode = "auth";
		$pnp_authtype = "authpostauth";
		$pnp_card_name = $_POST['cc_name'];
		$pnp_card_number = $_POST['cc_num'];
		$pnp_card_exp = $_POST['cc_expdate'];
		$pnp_card_cvv = $_POST['cc_scode'];
		$pnp_email = $_POST['email'];
		}

		$pnp_card_amount = $_POST['cardamount'];
		include_once("plugnpay.php");
		//echo '<PRE>'; print_r($pnp_transaction_array); echo '</PRE><BR>';
		$conf_details = $_POST['conf_details'];

	if($pnp_transaction_array['FinalStatus'] == "success" || $pnp_transaction_array['FinalStatus'] == "pending"){
		array_push($successmsg,$_REQUEST['edit'].' Credit Card: "Transaction was successful."');
		if(trim($_POST['conf_details']) != ""): $conf_details .= "\n\n"; endif;
		$conf_details .= 'api '.$pnp_mode;
		if(isset($pnp_card_name) && $pnp_card_name != ""): $conf_details .= ' '.$pnp_card_name; endif;
		$conf_details .= ' '.$pnp_transaction_array['FinalStatus'].' '.$pnp_transaction_array['orderID'].' '.date("n/d/Y",$time).' '.date("H:i:s",$time);
		if(isset($pnp_card_number) && $pnp_card_number != ""): $conf_details .= ' '.substr($pnp_card_number,0,4).'**'.substr($pnp_card_number,-2); endif;
		$conf_details .= ' '.$pnp_card_exp.' usd '.$pnp_card_amount.' '.$pnp_transaction_array['auth-code'];

		$query = 'UPDATE `bundubus_tickets` SET';
			if(isset($pnp_card_number) && $pnp_card_number != ""){ $query .= ' `cc_num` = "'.substr($pnp_card_number,0,4).'**'.substr($pnp_card_number,-2).'",'; }
			$query .= ' `conf_details` = "'.$conf_details.'", `pnp_orderid` = "'.$pnp_transaction_array['orderID'].'", `card_run` = "'.$time.'" WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
			@mysql_query($query);

		} elseif($pnp_transaction_array['FinalStatus'] == "badcard"){
		$thiserror = trim($pnp_transaction_array['resp-code'].' '.$pnp_transaction_array['MErrMsg'].' '.$pnp_transaction_array['Errdetails'].' '.$pnp_transaction_array['aux-msg']);
		array_push($errormsg,$_REQUEST['edit'].' Credit Card: "Transaction failed due to decline from processor."<BR>'.$thiserror);

		} elseif($pnp_transaction_array['FinalStatus'] == "fraud"){
		$thiserror = trim($pnp_transaction_array['resp-code'].' '.$pnp_transaction_array['MErrMsg'].' '.$pnp_transaction_array['Errdetails'].' '.$pnp_transaction_array['aux-msg']);
		array_push($errormsg,$_REQUEST['edit'].' Credit Card: "Transaction failed due to failure of FraudTrack2 filter settings."<BR>'.$thiserror);

		} elseif($pnp_transaction_array['FinalStatus'] == "problem"){
		$thiserror = trim($pnp_transaction_array['resp-code'].' '.$pnp_transaction_array['MErrMsg'].' '.$pnp_transaction_array['Errdetails'].' '.$pnp_transaction_array['aux-msg']);
		array_push($errormsg,$_REQUEST['edit'].' Credit Card: "Transaction failed due to problem unrelated to credit card."<BR>'.$thiserror);

		} else {
	        // this should not happen
		$thiserror = trim($pnp_transaction_array['resp-code'].' '.$pnp_transaction_array['MErrMsg'].' '.$pnp_transaction_array['Errdetails'].' '.$pnp_transaction_array['aux-msg']);
		array_push($errormsg,'Credit Card: "Transaction failed due to an unknown error."<BR>'.$thiserror);
		} //End Trans Result If Statement

	} //End $_POST['runcard'] if statement

}


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Bundu Bus Hop On, Hop Off Tickets</U></FONT><BR>'."\n\n";


echo '<DIV STYLE="margin-top:20px; font-family:Helvetica; font-size:11pt; color:red; font-style:italic;">Attention: HOHO tickets have been moved into the reservation form.<BR>To create a new ticket, click on "New Reservation", then choose "HOHO Pass" as the type of a subreservation.  The old tickets are listed here for reference, but are not available to customers through the Bundu Bus Login.</DIV>';

printmsgs($successmsg,$errormsg);


if(isset($_REQUEST['view']) && $_REQUEST['view'] != ""){  //*********** VIEW TICKET ************************************************


echo '<BR>'."\n\n";

$query = 'SELECT `id` FROM `bundubus_tickets` WHERE `id` = "'.$_REQUEST['view'].'" LIMIT 1';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
	if($num_results == 1){

	//Previous Ticket
	$query = 'SELECT `id` FROM `bundubus_tickets` WHERE `id` < "'.$_REQUEST['view'].'" ORDER BY `id` DESC LIMIT 1';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		if($num_results == 1){
			$row = mysql_fetch_array($result);
			$prevticket = $row['id'];
			}

	//Next Ticket
	$query = 'SELECT `id` FROM `bundubus_tickets` WHERE `id` > "'.$_REQUEST['view'].'" ORDER BY `id` ASC LIMIT 1';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		if($num_results == 1){
			$row = mysql_fetch_array($result);
			$nextticket = $row['id'];
			}

	echo '<SPAN STYLE="font-family:Arial; font-size:12pt;">';
		echo '<A HREF="viewticket.php?view='.$_REQUEST['view'].'&print=y" TARGET="_blank">Print Ticket</A>';
		echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$_REQUEST['view'].'">Edit Ticket</A>';
		echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'">Ticket Index</A>';
		if(isset($prevticket) && $prevticket != ""){ echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?view='.$prevticket.'">&lt; Prev</A>'; }
		if(isset($nextticket) && $nextticket != ""){ echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?view='.$nextticket.'">Next &gt;</A>'; }
		echo '</SPAN><BR><BR>'."\n\n";

	include('viewticket.php');

	echo '<SPAN STYLE="font-family:Arial; font-size:12pt;">';
		echo '<A HREF="viewticket.php?view='.$_REQUEST['view'].'&print=y" TARGET="_blank">Print Ticket</A>';
		echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$_REQUEST['view'].'">Edit Ticket</A>';
		echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'">Ticket Index</A>';
		if(isset($prevticket) && $prevticket != ""){ echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?view='.$prevticket.'">&lt; Prev</A>'; }
		if(isset($nextticket) && $nextticket != ""){ echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?view='.$nextticket.'">Next &gt;</A>'; }
		echo '</SPAN><BR><BR>'."\n\n";

	} else {
	echo '<SPAN STYLE="font-family:Arial; font-size:10pt;">Error: Unable to find the ticket you requested.<BR><BR><A HREF="'.$_SERVER['PHP_SELF'].'">Please click here for the ticket index</A></SPAN>'."\n\n";
	}


} elseif(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){  //*********** EDIT TICKET ************************************************


if($_REQUEST['edit'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['edit'] == "*new*"){
	$fillform = array(
		'id' => '*new*',
		'type' => '1',
		'date_booked' => $time
		);
	} else {
	$query = 'SELECT * FROM `bundubus_tickets` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
	$result = mysql_query($query);
	$fillform = mysql_fetch_assoc($result);
	}

//GET BUNDU BUS STOPS
$stops = array();
$query = 'SELECT * FROM `locations` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$stops['s'.$row['id']] = $row;
	}

//GET REGISTERED USERS
$regusers = array();
$query = 'SELECT `id`,`firstname`,`lastname`,`email` FROM `registered` ORDER BY `lastname` ASC, `firstname` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($regusers,$row);
	}

//GET ASSOCIATED ROUTES
$assoc = array();
$query = 'SELECT reservations_assoc.`id` as `tripid`, reservations_assoc.*, routes.`details` FROM `reservations_assoc`,`routes` WHERE reservations_assoc.`bbticket` > 0 AND reservations_assoc.`bbticket` = "'.getval('id').'" AND reservations_assoc.`routeid` = routes.`id` ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($assoc,$row);
	}

?><script language="javascript"><!--

function clk_runcard(){
	if(document.getElementById("runcard").checked){
		document.getElementById("cc_newtrans").style.display = '';
		if(document.getElementById("cardamount").value == ""){
			document.getElementById("cardamount").value = document.getElementById("amount").value;
			}
		} else {
		document.getElementById("cc_newtrans").style.display = 'none';
		}

	if(document.getElementById("pnp_orderid") != undefined && document.getElementById("pnp_orderid").value != ""){
		document.getElementById("cc_prevcharge").disabled = 0;
		document.getElementById("lab_cc_prevcharge").style.color = '#000000';
		document.getElementById("cc_refund").disabled = 0;
		document.getElementById("lab_cc_refund").style.color = '#000000';
		} else {
		document.getElementById("cc_newcharge").checked = 1;
		document.getElementById("cc_prevcharge").disabled = 1;
		document.getElementById("lab_cc_prevcharge").style.color = '#C7C7C7';
		document.getElementById("cc_refund").disabled = 1;
		document.getElementById("lab_cc_refund").style.color = '#C7C7C7';
		}
	}

function totalsubs(){
	var total = 0;
	var assocrow = 0;
	while(document.getElementById('assocamount'+assocrow) != undefined){
		if(document.getElementById('assocamount'+assocrow).value != "" && document.getElementById('assocamount'+assocrow).value > 0 && eval(document.getElementById('canceled'+assocrow).value) == 0){
			total = eval(total + eval(document.getElementById('assocamount'+assocrow).value) );
			}
		assocrow++;
		}
	document.getElementById('amount').value = total.toFixed(2);
	}

//--></script><? echo "\n\n";


bgcolor('');

echo '<BR>'."\n\n";

echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";

echo '<TABLE BORDER="0" WIDTH="94%" CELLSPACING="0" CELLPADDING="2">'."\n";

//BASIC
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Basic / Contact Info</TD>';
		echo '</TR>'."\n\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Ticket ID</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.getval('id').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Days</TD><TD><INPUT TYPE="text" NAME="days" STYLE="width:40px;" VALUE="'.getval('days').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Registered user</TD><TD><SELECT NAME="regid" STYLE="font-size:9pt; width:400px;"><OPTION VALUE="0">None</OPTION>'."\n";
		foreach($regusers as $row){
			echo '<OPTION VALUE="'.$row['id'].'"';
			if($row['id'] == getval('regid')): echo ' SELECTED'; endif;
			echo '>'.$row['lastname'].', '.$row['firstname'].' '.$row['email'].'</OPTION>'."\n";
			}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">First Name</TD><TD><INPUT TYPE="text" ID="firstname" NAME="firstname" STYLE="width:300px;" VALUE="'.getval('firstname').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Last Name</TD><TD><INPUT TYPE="text" ID="lastname" NAME="lastname" STYLE="width:300px;" VALUE="'.getval('lastname').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Home/business phone</TD><TD><INPUT TYPE="text" NAME="phone_homebus" STYLE="width:300px;" VALUE="'.getval('phone_homebus').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Cell phone</TD><TD><INPUT TYPE="text" NAME="phone_cell" STYLE="width:300px;" VALUE="'.getval('phone_cell').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Cell country</TD><TD><INPUT TYPE="text" NAME="cell_country" STYLE="width:300px;" VALUE="'.getval('cell_country').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Email</TD><TD><INPUT TYPE="text" NAME="email" STYLE="width:300px;" VALUE="'.getval('email').'"></TD></TR>'."\n";
	//echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Special instructions<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">This will be included in the customer email confirmation.</SPAN></TD><TD><TEXTAREA NAME="email_inst" STYLE="height:100px; width:400px;">'.getval('email_inst').'</TEXTAREA></TD></TR>'."\n";
	//if(getval('email_conf') > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Email confirmation was sent</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.date("g:ia n/d/Y",getval('email_conf')).'</TD></TR>'."\n"; }
	//echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="text-align:right; padding-right:10px;"><INPUT TYPE="checkbox" NAME="sendconf" ID="sendconf" VALUE="y"></TD><TD STYLE="font-family:Arial; font-size:11pt;"><LABEL FOR="sendconf">Send email confirmation on "Save"</LABEL></TD></TR>'."\n";
	echo "\n";

//ASSOCIATED ROUTES
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Routes Reserved</TD>';
		echo '</TR>'."\n\n";
	echo '<TR><TD COLSPAN="2" STYLE="padding-left:0px; padding-right:0px;">';
		if(count($assoc) > 0){
		echo '<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" WIDTH="100%">';
		foreach($assoc as $row){
			if($row['canceled'] > 0){ $line = ' color:#666666; text-decoration: line-through;'; } else { $line = ''; }
			if(is_numeric($row['dep_loc'])){ $row['dep_loc'] = $stops['s'.$row['dep_loc']]['name']; }
			if(is_numeric($row['arr_loc'])){ $row['arr_loc'] = $stops['s'.$row['arr_loc']]['name']; }
			$row['legname'] = date("g:ia",$row['dep_time']).' '.$row['dep_loc'].' <I>-to-</I> '.date("g:ia",$row['arr_time']).' '.$row['arr_loc'];
			$row['legname'] = str_replace("\n",' ',$row['legname']);
			$row['legname'] = str_replace('"','',$row['legname']);
			echo '<TR STYLE="background:#'.bgcolor('').'">';
			echo '<TD STYLE="font-family:Arial; font-size:9pt; padding-left:5px; padding-right:8px;"><A HREF="3_reservations.php?view='.$row['reservation'].'">'.$row['reservation'].'</A></TD>';
			echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-right:8px;"><A HREF="3_schedule.php?start='.$row['date'].'" STYLE="color:#000000;">'.date("M j, Y",$row['date']).'</A></TD>';
			echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-right:8px;">'.$row['legname'].'</TD>';
			echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:8pt; padding-left:4px; color:#666666;">';
				if($row['canceled'] > 0){ echo '<I>Canceled</I>'; }
				echo '</TD>';
			echo '<TD ALIGN="center"><A HREF="3_reservations.php?edit='.$row['reservation'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
			echo '</TR>';
			}
		echo '</TABLE>';
		} else {
		echo '<DIV STYLE="padding:3px; font-family:Arial; font-size:10pt; text-align:center;">None</DIV>';
		} //End count if statement
		echo '</TD></TR>'."\n";

//PAYMENTS
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Total / Payments</TD>';
		echo '</TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Total</TD><TD STYLE="font-family:Arial; font-size:14pt; font-weight:bold;">$<INPUT TYPE="text" NAME="amount" ID="amount" VALUE="'.getval('amount').'" STYLE="font-size:14pt; font-weight:bold; width:110px;"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Credit card name<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; cursor:pointer;" onClick="document.getElementById(\'cc_name\').value=document.getElementById(\'firstname\').value+\' \'+document.getElementById(\'lastname\').value">Click here to copy from above</SPAN></TD><TD><INPUT TYPE="text" ID="cc_name" NAME="cc_name" STYLE="width:300px;" VALUE="'.getval('cc_name').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Credit card number</TD><TD><INPUT TYPE="text" ID="cc_num" NAME="cc_num" STYLE="width:300px;" VALUE="'.getval('cc_num').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Expiration date</TD><TD STYLE="font-family:Arial; font-size:11pt;"><INPUT TYPE="text" ID="cc_expdate" NAME="cc_expdate" STYLE="width:100px;" VALUE="'.getval('cc_expdate').'"> MM/YY</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Security code</TD><TD><INPUT TYPE="text" ID="cc_scode" NAME="cc_scode" STYLE="width:100px;" VALUE="'.getval('cc_scode').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Method of payment</TD><TD><INPUT TYPE="text" ID="pay_method" NAME="pay_method" STYLE="width:300px;" VALUE="'.getval('pay_method').'"></TD></TR>'."\n";
	if(getval('card_run') != "" && getval('card_run') > 0){
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Card was run</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.date("g:ia n/d/Y",getval('card_run')).'</TD></TR>'."\n";
		}
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; white-space:nowrap;">PlugnPay transaction ID<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">This can be used to charge a new amount<BR>to a previously charged card.</SPAN></TD><TD><INPUT TYPE="text" ID="pnp_orderid" NAME="pnp_orderid" STYLE="width:300px;" VALUE="'.getval('pnp_orderid').'" onFocus="clk_runcard();" onChange="clk_runcard();"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD VALIGN="top" STYLE="text-align:right; padding-right:10px;"><INPUT TYPE="checkbox" NAME="runcard" ID="runcard" VALUE="y" onClick="clk_runcard()"></TD><TD STYLE="font-family:Arial; font-size:11pt; padding-top:4px; padding-bottom:4px;"><LABEL FOR="runcard" onClick="clk_runcard()">Run new credit card transaction on "Save"</LABEL><BR><DIV ID="cc_newtrans" STYLE="display:none;">';
		echo '<TABLE BORDER="0" CELLSPACING="3" CELLPADDING="0">';
		echo '<TR><TD ALIGN="right" VALIGN="middle"><INPUT TYPE="radio" NAME="authtype" ID="cc_newcharge" VALUE="cc_newcharge"';
			if(trim(getval('pnp_orderid')) == ""){ echo ' CHECKED'; }
			echo ' onClick="document.getElementById(\'runcard\').checked=1; clk_runcard();"></TD><TD VALIGN="middle" STYLE="padding-top:4px; font-family:Arial; font-size:9pt;"><LABEL FOR="cc_newcharge" ID="lab_cc_newcharge" onClick="document.getElementById(\'runcard\').checked=1; clk_runcard();">New credit card charge</LABEL></TD></TR>'."\n";
		echo '<TR><TD ALIGN="right" VALIGN="middle"><INPUT TYPE="radio" NAME="authtype" ID="cc_prevcharge" VALUE="cc_prevcharge"';
			if(trim(getval('pnp_orderid')) != ""){ echo ' CHECKED'; }
			echo ' onClick="document.getElementById(\'runcard\').checked=1; clk_runcard();"></TD><TD VALIGN="middle" STYLE="padding-top:4px; font-family:Arial; font-size:9pt;"><LABEL FOR="cc_prevcharge" ID="lab_cc_prevcharge" onClick="document.getElementById(\'runcard\').checked=1; clk_runcard();">Charge card from previous transaction</LABEL></TD></TR>'."\n";
		echo '<TR><TD ALIGN="right" VALIGN="middle"><INPUT TYPE="radio" NAME="authtype" ID="cc_refund" VALUE="cc_refund';
			if(getval('card_run') > 0 && getval('card_run') < ($time-15552000)){ echo '6mos'; }
			echo '" onClick="document.getElementById(\'runcard\').checked=1; clk_runcard();"></TD><TD VALIGN="middle" STYLE="padding-top:4px; font-family:Arial; font-size:9pt;"><LABEL FOR="cc_refund" ID="lab_cc_refund" onClick="document.getElementById(\'runcard\').checked=1; clk_runcard();">Refund credit card <SPAN STYLE="font-size:8pt;">(any amount not larger than previous charge)</SPAN></LABEL></TD></TR>'."\n";
		echo '<TR><TD></TD><TD VALIGN="middle" STYLE="font-family:Arial; font-size:10pt;">Transaction Amount<BR>$<INPUT TYPE="text" NAME="cardamount" ID="cardamount" VALUE="" STYLE="width:100px;" onClick="document.getElementById(\'runcard\').checked=1;"> <SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; cursor:pointer;" onClick="document.getElementById(\'cardamount\').value=document.getElementById(\'amount\').value;">Copy reservation total</SPAN></TD></TR>'."\n";
		echo '</TABLE>';
		echo '</DIV></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; white-space:nowrap;">PlugnPay confirmation details</TD><TD STYLE="font-family:Arial; font-size:9pt;"><TEXTAREA NAME="conf_details" STYLE="height:100px; width:400px;">'.getval('conf_details').'</TEXTAREA></TD></TR>'."\n";
	echo "\n";

//NOTES/ETC.
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Notes / Etc.</TD>';
		echo '</TR>'."\n";
	if(getval('comments') != ""){
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">User comments<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Entered online while making reservation.</SPAN></TD><TD STYLE="font-family:Arial; font-size:10pt;"><DIV STYLE="width:400px;">'.getval('comments').'</DIV></TD></TR>'."\n";
		}
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Booker</TD><TD><INPUT TYPE="text" ID="booker" NAME="booker" STYLE="width:300px;" VALUE="'.getval('booker').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Date booked</TD><TD>';
		echo '<SELECT NAME="date_booked_month">';
		for($i=1; $i<13; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( date("n",getval('date_booked')) == $i ){ echo " SELECTED"; }
			echo '>'.$i.' ('.date("M",mktime("0","0","0",$i,"1","2005")).')</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="date_booked_day">';
		for($i=1; $i<32; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( date("j",getval('date_booked')) == $i ){ echo " SELECTED"; }
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="date_booked_year">';
		for($i=2006; $i<(date("Y",$time)+11); $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( date("Y",getval('date_booked')) == $i ){ echo " SELECTED"; }
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Notes</TD><TD><TEXTAREA NAME="notes" STYLE="height:180px; width:400px;">'.getval('notes').'</TEXTAREA></TD></TR>'."\n";
	echo "\n";

	echo '</TABLE><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR>'."\n\n";

echo '</FORM>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Ticket index</A>'."\n\n";


} else {  //******************************** TICKET INDEX *****************************************************************


?><SCRIPT><!--

function selectall(){
	i=0;
	while(document.getElementById("sel"+i)){
		document.getElementById("sel"+i).checked = document.getElementById("selall").checked;
		i++;
		}
}

//--></SCRIPT><?


//GET TICKETS
$tickets = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS bundubus_tickets.*, COUNT(reservations_assoc.`id`) AS `numtrips`, MIN(reservations_assoc.`dep_time`) AS `start` FROM `bundubus_tickets`';
	$query .= ' LEFT JOIN `reservations_assoc` ON reservations_assoc.`bbticket` = bundubus_tickets.`id` AND reservations_assoc.`canceled` = 0';
	$query .= ' WHERE 1 = 1';
	$query .= ' GROUP BY bundubus_tickets.`id`';
	$query .= ' ORDER BY '.$_SESSION['bbtickets']['sortby'].' '.$_SESSION['bbtickets']['sortdir'].', bundubus_tickets.`id` '.$_SESSION['bbtickets']['sortdir'];
	$query .= ' LIMIT '.(($_SESSION['bbtickets']['p']-1)*$_SESSION['bbtickets']['limit']).','.$_SESSION['bbtickets']['limit'];
	//echo $query.'<BR>';
$result = mysql_query($query);
echo mysql_error().'<BR>';
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tickets,$row);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['bbtickets']['limit']);
if($numpages > 0 && $_SESSION['bbtickets']['p'] > $numpages): $_SESSION['bbtickets']['p'] = $numpages; endif;


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['bbtickets']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['bbtickets']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['bbtickets']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['bbtickets']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['bbtickets']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['bbtickets']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['bbtickets']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['bbtickets']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['bbtickets']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	//echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">ID</TD>';
	//echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Booked</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Name</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Email</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Days</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Starts</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; cursor:help;" TITLE="Number of routes reserved">Routes</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Expires</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($tickets as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	$thisemail = strtolower($row['email']);
	if(strlen($thisemail) > 20){ $thisemail = substr($thisemail,0,18).'...'; }
	//echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-left:5px; padding-right:10px; font-family:Arial; font-size:10pt; font-weight:bold;"><A HREF="'.$_SERVER['PHP_SELF'].'?view='.$row['id'].'">'.$row['id'].'</A></TD>';
	//echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.date("m/d/y",$row['date_booked']).'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['firstname'].' '.$row['lastname'].'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;"><A HREF="mailto:'.$row['email'].'">'.$thisemail.'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['days'].'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">';
		if($row['start'] > 0){ echo date("n/j/y g:ia",$row['start']); } else { echo '<SPAN STYLE="cursor:help;" TITLE="To be determined">TBD</SPAN>'; }
		echo '</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['numtrips'].'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">';
		if($row['start'] > 0){
			$expires = mktime(date("G",$row['start']),date("i",$row['start']),0,date("n",$row['start']),(date("j",$row['start'])+$row['days']),date("Y",$row['start']));
			if($expires <= $time){ echo '<SPAN STYLE="color:#FF0000;">'; }
			echo date("n/j/y g:ia",$expires);
			if($expires <= $time){ echo '</SPAN>'; }
			} else {
			echo '<SPAN STYLE="cursor:help;" TITLE="To be determined">TBD</SPAN>';
			}
		echo '</TD>';
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['bbtickets']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['bbtickets']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['bbtickets']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['bbtickets']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['bbtickets']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR><INPUT TYPE="button" VALUE="New Ticket" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';
	echo '<BR><BR>'."\n\n";

echo '</FORM>'."\n\n";


} //END EDIT IF STATEMENT


require("footer.php");

?>