<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

function tourvaliddates($tourid=0,$routes=array(),$showdebug='n'){

$debug = '';
$availdates = array('r'=>array(),'f'=>array());

//GET ROUTES
if(!is_array($routes) || !isset($routes['f']) || (count($routes['f'])+count($routes['r'])) == 0){
	$routes = array('f'=>array(),'r'=>array());
	$query = 'SELECT * FROM `tours_assoc` WHERE `tourid` = "'.$tourid.'" AND `tourid` != 0 AND `type` = "r" ORDER BY `dir` ASC, `day` ASC, `order` ASC';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		array_push($routes[$row['dir']],$row);
		}
	} //End if statement
	//echo '<PRE>'; print_r($routes); echo '</PRE>';

foreach($routes as $dir => $steps){ if(count($steps) > 0){

	//ESTABLISH FIRST ROUTE
	$route1 = array_shift($steps);
	$debug .= '<SPAN STYLE="font-size:14pt;">Established first route: '.$route1['typeid'].' running in direction: '.$dir.'</SPAN>'."\n";
	
	//GET FIRST RUNS
	$route1['runs'] = array();
	$query = 'SELECT * FROM `routes_dates` WHERE `routeid` = "'.$route1['typeid'].'" ORDER BY `date` ASC';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		array_push($route1['runs'],$row['date']);
		}
		//echo '<PRE>'; print_r($route1); echo '</PRE>';
	
	foreach($route1['runs'] as $startrun){
		$tryday = mktime(0,0,0,date("n",$startrun),(date("j",$startrun)-($route1['day']-1)),date("Y",$startrun)); //($startrun - (($route1['day']-1)*86400));
		$debug .= 'Trying day <B>'.date("n/j/Y G",$tryday).'</B> ('.$tryday.')'."\n";
		$debug .= 'First route ('.$route1['typeid'].') runs on '.date("n/j/Y G",$startrun).' ('.$startrun.').'."\n";
	
		if(count($steps) == 0){
			$debug .= '<SPAN STYLE="color:#339900; font-weight:bold;">End of line: Date '.date("n/j/Y G",$tryday).' ('.$tryday.') is available!</SPAN>'."\n";					
			array_push($availdates[$dir],$tryday);
		} else {
		foreach($steps as $key => $route){
			$debug .= 'Next route ('.$route['typeid'].')';
			if(!isset($steps[$key]['runs'])){
				$debug .= ' (loading runs now)';
				$steps[$key]['runs'] = array();
				$query = 'SELECT * FROM `routes_dates` WHERE `routeid` = "'.$route['typeid'].'" ORDER BY `date` ASC';
				$result = mysql_query($query);
				$num_results = mysql_num_rows($result);
					for($i=0; $i<$num_results; $i++){
					$row = mysql_fetch_assoc($result);
					array_push($steps[$key]['runs'],$row['date']);
					}
				} //End Runs Array If Statement
			$thisday = mktime(0,0,0,date("n",$tryday),(date("j",$tryday)+($route['day']-1)),date("Y",$tryday)); //($tryday + (($route['day']-1)*86400));
			$debug .= ' should run on '.date("n/j/Y G",$thisday).' ('.$thisday.'):';
				if( array_search($thisday,$steps[$key]['runs']) === FALSE ){
					$debug .= ' <SPAN STYLE="color:#FF0000;">NOT FOUND</SPAN>'."\n";
					break;
					} else {
					$debug .= ' <SPAN STYLE="color:#339900;">FOUND</SPAN>'."\n";
					if($key == (count($steps)-1)){
						$debug .= '<SPAN STYLE="color:#339900; font-weight:bold;">End of line: Date '.date("n/j/Y G",$tryday).' ('.$tryday.') is available!</SPAN>'."\n";					
						array_push($availdates[$dir],$tryday);
						}
					}
			} //End Routes Foreach
		} //End count if statement
		$debug .= "\n";
		}
	
	}} //End Direction for loop

	if($showdebug == "y"){ echo '<PRE>'.$debug.'</PRE>'; }
	return $availdates;
}


if(isset($_REQUEST['get_dates_for_tour']) && trim($_REQUEST['get_dates_for_tour']) != ""){

$pageid = "3_tours";
require_once("validate.php");

if(!isset($_REQUEST['debug']) || trim($_REQUEST['debug']) == ""){ $_REQUEST['debug'] = 'n'; }

$useroutes = array('f'=>array(),'r'=>array()); $w = 1;
while(isset($_REQUEST['useroutes'.$w]) && trim($_REQUEST['useroutes'.$w]) != ""){
	$_REQUEST['useroutes'.$w] = explode('|', trim($_REQUEST['useroutes'.$w]) );
	if(!isset($_REQUEST['useroutes'.$w][2]) || $_REQUEST['useroutes'.$w][2] != 'r'){ $_REQUEST['useroutes'.$w][2] = 'f'; }
	$addroute = array('typeid'=>$_REQUEST['useroutes'.$w][0],'day'=>$_REQUEST['useroutes'.$w][1],'dir'=>$_REQUEST['useroutes'.$w][2]);
	array_push($useroutes[$_REQUEST['useroutes'.$w][2]],$addroute);
	$w++;
	}

$founddates = tourvaliddates($_REQUEST['get_dates_for_tour'],$useroutes,$_REQUEST['debug']);
//echo implode("\n",$founddates)."\n";

echo '||BEGIN||'."\n";
foreach($founddates as $key => $thisdir){
	foreach($thisdir as $thisdate){
		echo date("Y",$thisdate).'|'.date("n",$thisdate).'|'.date("j",$thisdate).'|'.$key."\n";
		}
	}
	echo '||END||'."\n";

} //End ID if statement

?>