<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$pageid = '3_tours';
$path_to_common = '../../';
require_once("../validate.php");


//$_REQUEST['discount'] = '75';

//GET TOURS
$tours = array();
$query = 'SELECT * FROM `tours`';
	$query .= ' WHERE tours.`pricesheet` = 1 AND tours.`archived` = 0 AND tours.`hidden` != "1"';
	$query .= ' ORDER BY tours.`id` DESC';
	//echo $query.'<BR>';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			array_push($tours,$row);
			}
		//echo '<PRE STYLE="text-align:left;">'; print_r($tours); echo '</PRE>';

//GET DATES
$dates = array();
$query = 'SELECT tours_dates.`tourid`,tours_dates.`date` FROM `tours`,`tours_dates`';
	$query .= ' WHERE tours.`id` = tours_dates.`tourid` AND tours.`pricesheet` = 1 AND tours.`archived` = 0 AND tours.`hidden` != "1"';
	$query .= ' AND tours_dates.`date` >= '.time(); //.' AND tours_dates.`date` < '.strtotime('April 1, 2011');
	$query .= ' ORDER BY tours_dates.`tourid` DESC, tours_dates.`date` ASC';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			if(!isset($dates[$row['tourid']])){ $dates[$row['tourid']] = array(); }
			array_push($dates[$row['tourid']],$row['date']);
			}
		//echo '<PRE STYLE="text-align:left;">'; print_r($dates); echo '</PRE>';

//GET HOHO TICKETS
$hoho = array();
$query = 'SELECT * FROM `bundubus_hopon` WHERE `flexible` = "0" ORDER BY `days` ASC';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			array_push($hoho,$row);
			}
$hoho_flex = array();
$query = 'SELECT * FROM `bundubus_hopon` WHERE `flexible` = "1" ORDER BY `days` ASC';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			array_push($hoho_flex,$row);
			}

	mysql_close($db);


$csvout = '"Tour #","Long name","Short name","Rack Double Occupancy","Net Double Occupancy","Rack Single","Net Single","Rack Triple","Net Triple","Rack Quad","Net Quad","Fuel Surcharge","URL","Dates"'."\r\n";
foreach($tours as $row){
	foreach($row as $key => $val){
		$row[$key] = str_replace("\r",'',$row[$key]);
		//$row[$key] = str_replace("\n",'\n',$row[$key]);
		$row[$key] = str_replace('"','""',$row[$key]);
		}
	$rate = ($_REQUEST['discount']*0.01);
		if(isset($row['agent'.$_REQUEST['type'].'_per']) && $row['agent'.$_REQUEST['type'].'_per'] > 0){ $rate = ($row['agent'.$_REQUEST['type'].'_per']*0.01); }
	$csvout .= '"'.$row['id'].'","'.$row['title'].'","'.$row['nickname'].'"';
		//Double
		$csvout .= ',"$'.$row['perguest'].'"';
		$csvout .= ',"$'.number_format(($row['perguest']*$rate),2,'.',',').'"';

		//Single
		if($row['single'] > 0){
			$csvout .= ',"$'.number_format(($row['perguest']+$row['single']),2,'.',',').'"';
			$csvout .= ',"$'.number_format((($row['perguest']+$row['single'])*$rate),2,'.',',').'"';
			} else {
			$csvout .= ',"",""';
			}

		//Triple
		if($row['triple'] > 0){
			$csvout .= ',"$'.number_format((($row['perguest']*3-$row['triple'])/3),2,'.',',').'"';
			$csvout .= ',"$'.number_format(((($row['perguest']*3-$row['triple'])/3)*$rate),2,'.',',').'"';
			} else {
			$csvout .= ',"",""';
			}

		//Quad
		if($row['quad'] > 0){
			$csvout .= ',"$'.number_format((($row['perguest']*4-$row['quad'])/4),2,'.',',').'"';
			$csvout .= ',"$'.number_format(((($row['perguest']*4-$row['quad'])/4)*$rate),2,'.',',').'"';
			} else {
			$csvout .= ',"",""';
			}

		//Fuel Surcharge
		$surcharge = $row['fuelsurcharge'];
			if($surcharge != "" && $surcharge > 0){
				$surcharge = '$'.$surcharge;
			} else {
				$surcharge = '';
			}
		$csvout .= ',"'.$surcharge.'"';

		//URL
		$csvout .= ',"http://www.bundubashers.com/tour.php?id='.urlencode($row['alias']).'"';
		
		//Dates
		if(isset($dates[$row['id']])){
			$compstr = '';
			$blocks = array();
			foreach($dates[$row['id']] as $key => $ts){
				$y = date("Y",$ts);
				$m = date("M",$ts);
				$d = date("j",$ts);
				if(!isset($blocks[$y])){ $blocks[$y] = array(); }
				if(!isset($blocks[$y][$m])){ $blocks[$y][$m] = array(); $p = -1; }
				if($d != ($p+1)){
					array_push($blocks[$y][$m], array($d) );
					} else {
					array_push($blocks[$y][$m][count($blocks[$y][$m])-1],$d);
					}
					$p = $d;
				$compstr .= date("n/j/y",$ts).' ';
				}
			$datestr = '';
			foreach($blocks as $y => $year){
				if($year !== reset($blocks)){ $datestr .= ';  '; }
				$datestr .= $y.':';
				foreach($year as $m => $month){
					if($month !== reset($year)){ $datestr .= ';'; }
					$datestr .= ' '.$m;
					foreach($month as $db => $days){
						if($db > 0){ $datestr .= ','; }
						if(count($days) == 1){
							$datestr .= ' '.$days[0];
							} else {
							$datestr .= ' '.reset($days).'-'.end($days);
							}
						}
					}
				}
			$csvout .= ',"'.$datestr.'"';
			//echo '<PRE STYLE="text-align:left;">'; print_r($blocks); echo '</PRE>';
			//echo '['.$row['id'].'] '.$compstr.'<BR>';
			//echo '['.$row['id'].'] '.$datestr.'<BR><BR>';
			}

		$csvout .= "\r\n";
	}
	$csvout .= "\r\n";

$csvout .= '"","Bundu Bus"'."\r\n";
$csvout .= '"","Hop On Hop Off Pass (HOHO)"'."\r\n";
$csvout .= '"","Consecutive Days","Rack","Net"'."\r\n";
foreach($hoho as $row){
	foreach($row as $key => $val){
		$row[$key] = str_replace("\r",'',$row[$key]);
		//$row[$key] = str_replace("\n",'\n',$row[$key]);
		$row[$key] = str_replace('"','""',$row[$key]);
		}
	$csvout .= '"","'.$row['title'].'","$'.$row['price'].'","$'.number_format(($row['price']*($_REQUEST['discount']*0.01)),2,'.',',').'"'."\r\n";
	}
	$csvout .= "\r\n";

$csvout .= '"","Flexible"'."\r\n";
foreach($hoho_flex as $row){
	foreach($row as $key => $val){
		$row[$key] = str_replace("\r",'',$row[$key]);
		//$row[$key] = str_replace("\n",'\n',$row[$key]);
		$row[$key] = str_replace('"','""',$row[$key]);
		}
	$csvout .= '"","'.$row['title'].'","$'.$row['price'].'","$'.number_format(($row['price']*($_REQUEST['discount']*0.01)),2,'.',',').'"'."\r\n";
	}
	$csvout .= "\r\n";

$csvout .= '"","Individual Routes"'."\r\n";
$csvout .= '"","Commissionable at '.(100-$_REQUEST['discount']).'%"'."\r\n";

//echo '<PRE>';
//	echo $csvout;
//	echo '</PRE>';
//	echo mb_strlen($csvout,'latin1');

$_REQUEST['discount'] = str_replace('.','_',$_REQUEST['discount']);
header('Content-Disposition: attachment; filename="tour_pricesheet'.$_REQUEST['discount'].'.csv"');
header('Content-Length: '.mb_strlen($csvout,'latin1'));
header('Content-Type: application/octet-stream;');
header('Cache-Control: no-cache, must-revalidate');
header('Cache-Control: private');
header('Pragma: private');
echo $csvout;
exit;

?>