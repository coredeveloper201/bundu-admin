<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_bussettings";
require("validate.php");
require("header.php");

//echo '<PRE>'; print_r($_POST); echo '</PRE>';

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['setting']) && count($_POST['setting']) > 0){

foreach($_POST['setting'] as $row){
	$query = 'UPDATE `bundubus_settings` SET `var` = "'.$_POST[$row].'" WHERE `setting` = "'.$row.'" LIMIT 1';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror != ""): array_push($errormsg,$thiserror); endif;
	}

	array_push($successmsg,'Bundu Bus settings updated.');
}


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Bundu Bus Settings</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


//GET SETTINGS
$fillform = array();
$query = 'SELECT `setting`,`var` FROM `bundubus_settings`';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$fillform[$row['setting']] = $row['var'];
	}

bgcolor('');

echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";

echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Default # of seats</TD><TD STYLE="width:60%;"><INPUT TYPE="hidden" NAME="setting[]" VALUE="seats"><INPUT TYPE="text" NAME="seats" STYLE="width:80px;" VALUE="'.getval('seats').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Cost per mile</TD><TD><INPUT TYPE="hidden" NAME="setting[]" VALUE="cpm">$<INPUT TYPE="text" NAME="cpm" STYLE="width:80px;" VALUE="'.getval('cpm').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Discount (%) for 2 legs<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">100 = No Discount</SPAN></TD><TD><INPUT TYPE="hidden" NAME="setting[]" VALUE="2legs"><INPUT TYPE="text" NAME="2legs" STYLE="width:80px;" VALUE="'.getval('2legs').'">%</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Discount (%) for 3 or more legs<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">100 = No Discount</SPAN></TD><TD><INPUT TYPE="hidden" NAME="setting[]" VALUE="3legs"><INPUT TYPE="text" NAME="3legs" STYLE="width:80px;" VALUE="'.getval('3legs').'">%</TD></TR>'."\n";
	echo '</TABLE><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";


echo '</FORM>'."\n\n";


require("footer.php");

?>