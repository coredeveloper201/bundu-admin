<? //error_reporting('E_ALL'); ini_set('display_errors','1');

// http://www.bundubashers.com/googletransit/gen.php

@date_default_timezone_set('America/Denver');
//$today = mktime(0,0,0,date("n"),date("j"),date("Y"));
$today = strtotime("today");

if(@!$db){ $db = @MYSQL_CONNECT("localhost","sessel_bbsite","?R&zTQ=UdFX$"); }
@mysql_select_db("sessel_bundubashers");
@mysql_query("SET NAMES utf8");
@mysql_query('SET time_zone = "'.date("P").'"');

function fd($txt){
	$txt = str_replace("\r",'',$txt);
	$txt = str_replace("\n",' ',$txt);	
	$txt = str_replace('"','""',$txt);
	return $txt;
	}

$folder = $_SERVER['DOCUMENT_ROOT'].'/googletransit/20140424/';
@mkdir($folder);


//GET TIME OFFSET FUNCTION
include_once('../admin/sups/timezones.php');

function gettimediff1($origin_tz,$origin_datetime,$remote_tz,$remote_datetime){
    $origin_dtz = new DateTimeZone($origin_tz);
    $remote_dtz = new DateTimeZone($remote_tz);
    $origin_dt = new DateTime($origin_datetime, $origin_dtz);
    $remote_dt = new DateTime($remote_datetime, $remote_dtz);
    $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
	return $offset;
	}

function fromsecs1($secs){
	$out = array('h'=>0,'m'=>0,'s'=>0);
	$out['h'] = intval($secs / 3600);
		$secs_left = ($secs - ($out['h'] * 3600));
	$out['m'] = intval($secs_left / 60);
	$out['s'] = ($secs_left - ($out['m'] * 60));
	return $out;
	}

function hoursfrombase($basetime,$newtime,$incl_secs=true){
	$fromsecs = fromsecs1(($newtime - $basetime));
	if($fromsecs['h'] < 10){ $fromsecs['h'] = '0'.$fromsecs['h']; }
	if($fromsecs['m'] < 10){ $fromsecs['m'] = '0'.$fromsecs['m']; }
	if($fromsecs['s'] < 10){ $fromsecs['s'] = '0'.$fromsecs['s']; }
	$time = $fromsecs['h'].':'.$fromsecs['m'];
		if($incl_secs){ $time .= ':'.$fromsecs['s']; }
	return $time;
	}



//GATHER DATA
$exclude_locs = array(14,15);
	//Find locs missing lat/long
	$query = 'SELECT DISTINCT id FROM locations WHERE stop_lat = "" OR stop_lat IS NULL OR stop_lon = "" OR stop_lon IS NULL';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$exclude_locs[] = $row['id'];
	}
$exclude_routes = array(55,59); //34,35

$routes = array();
$getstops = array();
$query = 'SELECT `id`,`name`,`dep_loc`,`dep_time`,`arr_loc`,`arr_time`,`travel_time`,`anchor` FROM `routes` WHERE `bbincl` != "n"';
	if(isset($exclude_locs) && count($exclude_locs) > 0){ $query .= ' AND (`dep_loc` != "'.implode('" AND `dep_loc` != "',$exclude_locs).'") AND (`arr_loc` != "'.implode('" AND `arr_loc` != "',$exclude_locs).'")'; }
	if(isset($exclude_routes) && count($exclude_routes) > 0){ $query .= ' AND (`id` != "'.implode('" AND `id` != "',$exclude_routes).'")'; }
	$query .= ' ORDER BY `id` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($routes,$row);
	array_push($getstops,$row['dep_loc']);
	array_push($getstops,$row['arr_loc']);
	}

$stops = array();
$getstops = array_unique($getstops);
$query = 'SELECT * FROM `locations`';
	$query .= ' WHERE (`id` = "'.implode('" OR `id` = "',$getstops).'")';
	if(isset($exclude_locs) && count($exclude_locs) > 0){ $query .= ' AND (`id` != "'.implode('" AND `id` != "',$exclude_locs).'")'; }
	$query .= ' ORDER BY `id` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	//array_push($stops,$row);
	$stops['s'.$row['id']] = $row;
	}

$calendar = array();
$query = 'SELECT routes_dates.`id`,routes_dates.`routeid`,routes_dates.`date`,routes_dates.`dep_time`,routes_dates.`arr_time`';
	$query .= ' FROM `routes`,`routes_dates`';
	$query .= ' WHERE routes_dates.`routeid` = routes.`id`';
	//$query .= ' AND routes.`id` = 77';
	$query .= ' AND routes.`bbincl` = "a"';
	$query .= ' AND routes_dates.`date` >= '.$today;
	$query .= ' AND routes_dates.`dep_time` = -1 AND routes_dates.`arr_time` = -1';
	if(isset($exclude_locs) && count($exclude_locs) > 0){ $query .= ' AND (routes.`dep_loc` != "'.implode('" AND routes.`dep_loc` != "',$exclude_locs).'") AND (routes.`arr_loc` != "'.implode('" AND routes.`arr_loc` != "',$exclude_locs).'")'; }
	if(isset($exclude_routes) && count($exclude_routes) > 0){ $query .= ' AND (routes_dates.`routeid` != "'.implode('" AND routes_dates.`routeid` != "',$exclude_routes).'")'; }
	$query .= ' ORDER BY routes_dates.`date` ASC, routes_dates.`id` ASC';
	//echo '<PRE STYLE="text-align:left;">'.$query.'</PRE>';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($calendar,$row);
	}
	//print_r($calendar);

$stop_times_exceptions = array();
$query = 'SELECT DISTINCT routes_dates.`routeid`,routes.`dep_loc`,routes.`arr_loc`,IF(routes_dates.`dep_time`=-1,routes.`dep_time`,routes_dates.`dep_time`) AS `dep_time`,IF(routes_dates.`arr_time`=-1,routes.`arr_time`,routes_dates.`arr_time`) AS `arr_time`,`travel_time`,`anchor`';
	$query .= ' FROM `routes`,`routes_dates`';
	$query .= ' WHERE routes_dates.`routeid` = routes.`id`';
	//$query .= ' AND routes.`id` = 77';
	$query .= ' AND routes.`bbincl` = "a"';
	$query .= ' AND routes_dates.`date` >= '.$today;
	$query .= ' AND (routes_dates.`dep_time` > -1 OR routes_dates.`arr_time` > -1)';
	if(isset($exclude_locs) && count($exclude_locs) > 0){ $query .= ' AND (routes.`dep_loc` != "'.implode('" AND routes.`dep_loc` != "',$exclude_locs).'") AND (routes.`arr_loc` != "'.implode('" AND routes.`arr_loc` != "',$exclude_locs).'")'; }
	if(isset($exclude_routes) && count($exclude_routes) > 0){ $query .= ' AND (routes_dates.`routeid` != "'.implode('" AND routes_dates.`routeid` != "',$exclude_routes).'")'; }
	$query .= ' ORDER BY routes_dates.`date` ASC, routes_dates.`id` ASC';
	//echo '<PRE STYLE="text-align:left;">'.$query.'</PRE>';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($stop_times_exceptions,$row);
	}
	//print_r($stop_times_exceptions);

$cal_exceptions = array();
$query = 'SELECT routes_dates.`id`,routes_dates.`routeid`,routes_dates.`date`,IF(routes_dates.`dep_time`=-1,routes.`dep_time`,routes_dates.`dep_time`) AS `dep_time`,IF(routes_dates.`arr_time`=-1,routes.`arr_time`,routes_dates.`arr_time`) AS `arr_time`';
	$query .= ' FROM `routes`,`routes_dates`';
	$query .= ' WHERE routes_dates.`routeid` = routes.`id`';
	//$query .= ' AND routes.`id` = 77';
	$query .= ' AND routes.`bbincl` = "a"';
	$query .= ' AND routes_dates.`date` >= '.$today;
	$query .= ' AND (routes_dates.`dep_time` > -1 OR routes_dates.`arr_time` > -1)';
	if(isset($exclude_locs) && count($exclude_locs) > 0){ $query .= ' AND (routes.`dep_loc` != "'.implode('" AND routes.`dep_loc` != "',$exclude_locs).'") AND (routes.`arr_loc` != "'.implode('" AND routes.`arr_loc` != "',$exclude_locs).'")'; }
	if(isset($exclude_routes) && count($exclude_routes) > 0){ $query .= ' AND (routes_dates.`routeid` != "'.implode('" AND routes_dates.`routeid` != "',$exclude_routes).'")'; }
	$query .= ' ORDER BY routes_dates.`date` ASC, routes_dates.`id` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($cal_exceptions,$row);
	}
	echo '<PRE STYLE="text-align:left;">'.$query.'</PRE>';
	//print_r($cal_exceptions);

$shapes = array(
	'34' => array('44.658800,-111.097738','44.657535,-111.09329','44.65354,-111.06950','44.65226,-111.06118','44.65014,-111.00453','44.65238,-110.99522','44.66667,-110.98691','44.645426,-110.858483','44.64948,-110.77369','44.70252,-110.74519','44.71643,-110.71352','44.72734,-110.6966','44.75399,-110.72536','44.80321,-110.74262','44.92580,-110.73249','44.96316,-110.70914','44.97603,-110.70168','44.94734,-110.65773','44.94485,-110.61881','44.946523,-110.45130','44.921664,-110.44331','44.873336,-110.38241','44.813696,-110.44603','44.769459,-110.45443','44.75451,-110.48234','44.73597,-110.49319','44.70405,-110.60263','44.72734,-110.6966','44.658800,-111.097738'),
	'35' => array('44.658800,-111.097738','44.657535,-111.09329','44.65354,-111.06950','44.65226,-111.06118','44.65014,-111.00453','44.65238,-110.99522','44.66667,-110.98691','44.645426,-110.858483','44.6218,-110.8575','44.578081,-110.829075','44.55516,-110.80755','44.548395,-110.806775','44.535346,-110.81776','44.533426,-110.82597','44.52913,-110.83531','44.51735,-110.82647','44.47232,-110.85797','44.45189,-110.82407','44.41401,-110.57847','44.42422,-110.57544','44.45746,-110.55872','44.47532,-110.54997','44.47409,-110.52637','44.45281,-110.50229','44.49902,-110.42054','44.55742,-110.39856','44.59658,-110.38758','44.60813,-110.38929','44.63040,-110.43946','44.67137,-110.47401','44.73597,-110.49319','44.70405,-110.60263','44.72734,-110.69657','44.71643,-110.71352','44.70252,-110.74519','44.64948,-110.77369','44.645426,-110.858483','44.658800,-111.097738')
	);

echo '<PRE>';

//CREATE ZIP FILE
$zip = new ZipArchive;
$res = $zip->open($folder.'google_transit.zip', ZipArchive::CREATE);
if($res === TRUE){

//agency.txt
$output  = '"agency_id","agency_name","agency_url","agency_timezone","agency_lang","agency_phone"'."\n";
$output .= '"bundubus-saltlakecity-ut-us","Bundu Bus","http://www.bundubus.com/","'.date_default_timezone_get().'","en","702-381-3550"'."\n";
	$zip->addFromString('agency.txt',$output);

//stops.txt
$output = '"stop_id","stop_name","stop_desc","stop_lat","stop_lon","stop_timezone","stop_url"'."\n";
	foreach($stops as $thisstop){
		$output .= '"stp'.$thisstop['id'].'","'.fd($thisstop['name']).'","'.fd($thisstop['desc']).'","'.$thisstop['stop_lat'].'","'.$thisstop['stop_lon'].'","'.$thisstop['timezone_id'].'","'.fd($thisstop['stop_url']).'"'."\n";
		}
	$zip->addFromString('stops.txt',$output);
	$count_stops = explode("\n",$output); $count_stops = count($count_stops)-2;

//routes.txt
$output = '"route_id","route_short_name","route_long_name","route_desc","route_type"'."\n";
	foreach($routes as $thisroute){
		$output .= '"r'.$thisroute['id'].'","","'.fd($thisroute['name']).'","","3"'."\n";
		}
	$zip->addFromString('routes.txt',$output);
	$count_routes = explode("\n",$output); $count_routes = count($count_routes)-2;

//calendar.txt
//$output = '"service_id","sunday","monday","tuesday","wednesday","thursday","friday","saturday","start_date","end_date"'."\n";
	//$zip->addFromString('calendar.txt',$output);

//calendar_dates.txt
$needed_ids = array();
$output = '"service_id","date","exception_type"'."\n";
	foreach($calendar as $date){
		if(date("I",($date['date']+7200)) == 1){ $season = 'dst'; } else { $season = 'st'; }
		$srvid = 'srv_'.$season.'_'.$date['routeid'];
		$output .= '"'.$srvid.'","'.date("Ymd",$date['date']).'","1"'."\n";
		if(!in_array($season.'_'.$date['routeid'],$needed_ids,1)){ array_push($needed_ids,$season.'_'.$date['routeid']); }
		}
	foreach($cal_exceptions as $date){
		if(date("I",($date['date']+7200)) == 1){ $season = 'dst'; } else { $season = 'st'; }
		$srvid = 'srv_'.$season.'_'.$date['routeid'].'_'.$date['dep_time'];
		$output .= '"'.$srvid.'","'.date("Ymd",$date['date']).'","1"'."\n";

		if(!in_array($season.'_'.$date['routeid'].'_'.$date['dep_time'],$needed_ids,1)){ array_push($needed_ids,$season.'_'.$date['routeid'].'_'.$date['dep_time']); }

		}
	$zip->addFromString('calendar_dates.txt',$output);
	$count_calendar_dates = explode("\n",$output); $count_calendar_dates = count($count_calendar_dates)-2;
	//echo '<PRE>'; print_r($needed_ids); echo '</PRE>';

//trips.txt
$output = '"route_id","service_id","trip_id","trip_headsign","shape_id"'."\n";
	foreach($routes as $thisroute){
		if(in_array('st_'.$thisroute['id'],$needed_ids,1)){
			$output .= '"r'.$thisroute['id'].'","srv_st_'.$thisroute['id'].'","t_st_'.$thisroute['id'].'","'.$stops['s'.$thisroute['arr_loc']]['name'].'","';
			if(isset($shapes[$thisroute['id']])){ $output .= 'shp'.$thisroute['id']; }
			$output .= '"'."\n";
			}
		if(in_array('dst_'.$thisroute['id'],$needed_ids,1)){
			$output .= '"r'.$thisroute['id'].'","srv_dst_'.$thisroute['id'].'","t_dst_'.$thisroute['id'].'","'.$stops['s'.$thisroute['arr_loc']]['name'].'","';
			if(isset($shapes[$thisroute['id']])){ $output .= 'shp'.$thisroute['id']; }
			$output .= '"'."\n";
			}
		}
	foreach($stop_times_exceptions as $thistrip){
		if(in_array('st_'.$thistrip['routeid'].'_'.$thistrip['dep_time'],$needed_ids,1)){
			$output .= '"r'.$thistrip['routeid'].'","srv_st_'.$thistrip['routeid'].'_'.$thistrip['dep_time'].'","t_st_'.$thistrip['routeid'].'_'.$thistrip['dep_time'].'","'.$stops['s'.$thistrip['arr_loc']]['name'].'","';
			if(isset($shapes[$thistrip['routeid']])){ $output .= 'shp'.$thistrip['routeid']; }
			$output .= '"'."\n";
			}
		if(in_array('dst_'.$thistrip['routeid'].'_'.$thistrip['dep_time'],$needed_ids,1)){
			$output .= '"r'.$thistrip['routeid'].'","srv_dst_'.$thistrip['routeid'].'_'.$thistrip['dep_time'].'","t_dst_'.$thistrip['routeid'].'_'.$thistrip['dep_time'].'","'.$stops['s'.$thistrip['arr_loc']]['name'].'","';
			if(isset($shapes[$thistrip['routeid']])){ $output .= 'shp'.$thistrip['routeid']; }
			$output .= '"'."\n";
			}
		}
	$zip->addFromString('trips.txt',$output);
	$count_trips = explode("\n",$output); $count_trips = count($count_trips)-2;

//stop_times.txt
$basestamp = strtotime(date("Y").'-1-1');
$output = '"trip_id","arrival_time","departure_time","stop_id","stop_sequence"'."\n";
	echo "<B>".substr('Anchor Time -------------------------',0,21);
		echo "\t".substr('Dep. Location -------------------------',0,14);
		echo "\t".substr('Agency Time Zone -------------------------',0,28);
		echo "\t".substr('Travel Time-------------------------',0,14);
		echo "\t".substr('Arr. Location -------------------------',0,14);
		echo "\t".substr('Agency Time Zone -------------------------',0,28);
		echo "</B>\n";
	foreach($routes as $thistrip){
		//$thistrip['dep_time'] = ($thistrip['dep_time']+43200);
		//$thistrip['travel_time'] = ($thistrip['travel_time']+21600);
		if(!isset($thistrip['anchor']) || $thistrip['anchor'] != 'arr'){ $thistrip['anchor'] = 'dep'; }
		if($thistrip['anchor'] == 'arr'){
			$arr_st_offset = gettimediff1(date_default_timezone_get(),"2010-1-1",$stops['s'.$thistrip['arr_loc']]['timezone_id'],"2010-1-1");
			$arr_dst_offset = gettimediff1(date_default_timezone_get(),"2010-6-1",$stops['s'.$thistrip['arr_loc']]['timezone_id'],"2010-6-1");
			$arr_st_time = ($basestamp+$thistrip['arr_time']+$arr_st_offset);
			$arr_dst_time = ($basestamp+$thistrip['arr_time']+$arr_dst_offset);
			$dep_st_time = ($arr_st_time-$thistrip['travel_time']);
			$dep_dst_time = ($arr_dst_time-$thistrip['travel_time']);
			} else {
			$dep_st_offset = gettimediff1(date_default_timezone_get(),"2010-1-1",$stops['s'.$thistrip['dep_loc']]['timezone_id'],"2010-1-1");
			$dep_dst_offset = gettimediff1(date_default_timezone_get(),"2010-6-1",$stops['s'.$thistrip['dep_loc']]['timezone_id'],"2010-6-1");
			$dep_st_time = ($basestamp+$thistrip['dep_time']+$dep_st_offset);
			$dep_dst_time = ($basestamp+$thistrip['dep_time']+$dep_dst_offset);
			$arr_st_time = ($dep_st_time+$thistrip['travel_time']);
			$arr_dst_time = ($dep_dst_time+$thistrip['travel_time']);
			}
		echo strtoupper($thistrip['anchor']).' '.substr($stops['s'.$thistrip[$thistrip['anchor'].'_loc']]['timezone_id'].' -------------------------',8,10).'&gt;';
			echo "\t".date("H:i",($basestamp+$thistrip[$thistrip['anchor'].'_time']));
			echo "\t".substr($stops['s'.$thistrip['dep_loc']]['name'].' -------------------------',0,14);
			echo "\t".substr(date_default_timezone_get().'      ',8,6);
			echo "\t".hoursfrombase($basestamp,$dep_st_time,0).'<SPAN STYLE="color:#999999;">st</SPAN> / '.hoursfrombase($basestamp,$dep_dst_time,0).'<SPAN STYLE="color:#999999;">dst</SPAN>'; //.' '.date("H:i",$dep_st_time)
			$traveldesc = fromsecs1($thistrip['travel_time']);
			echo "\t".substr($traveldesc['h'].'h, '.$traveldesc['m'].'m                  ',0,14);
			echo "\t".substr($stops['s'.$thistrip['arr_loc']]['name'].' -------------------------',0,14);
			echo "\t".substr(date_default_timezone_get().'      ',8,6);
			echo "\t".hoursfrombase($basestamp,$arr_st_time,0).'<SPAN STYLE="color:#999999;">st</SPAN> / '.hoursfrombase($basestamp,$arr_dst_time,0).'<SPAN STYLE="color:#999999;">dst</SPAN>';
			echo "\n";
		if(in_array('st_'.$thistrip['id'],$needed_ids,1)){
			$output .= '"t_st_'.$thistrip['id'].'","'.hoursfrombase($basestamp,$dep_st_time).'","'.hoursfrombase($basestamp,$dep_st_time).'","stp'.$thistrip['dep_loc'].'","1"'."\n";
			$output .= '"t_st_'.$thistrip['id'].'","'.hoursfrombase($basestamp,$arr_st_time).'","'.hoursfrombase($basestamp,$arr_st_time).'","stp'.$thistrip['arr_loc'].'","2"'."\n";
			}
		if(in_array('dst_'.$thistrip['id'],$needed_ids,1)){
			$output .= '"t_dst_'.$thistrip['id'].'","'.hoursfrombase($basestamp,$dep_dst_time).'","'.hoursfrombase($basestamp,$dep_dst_time).'","stp'.$thistrip['dep_loc'].'","1"'."\n";
			$output .= '"t_dst_'.$thistrip['id'].'","'.hoursfrombase($basestamp,$arr_dst_time).'","'.hoursfrombase($basestamp,$arr_dst_time).'","stp'.$thistrip['arr_loc'].'","2"'."\n";
			}
		}
	echo "  Route times exceptions-------------\n";
	foreach($stop_times_exceptions as $thistrip){
		if(!isset($thistrip['anchor']) || $thistrip['anchor'] != 'arr'){ $thistrip['anchor'] = 'dep'; }
		if($thistrip['anchor'] == 'arr'){
			$arr_st_offset = gettimediff1(date_default_timezone_get(),"2010-1-1",$stops['s'.$thistrip['arr_loc']]['timezone_id'],"2010-1-1");
			$arr_dst_offset = gettimediff1(date_default_timezone_get(),"2010-6-1",$stops['s'.$thistrip['arr_loc']]['timezone_id'],"2010-6-1");
			$arr_st_time = ($basestamp+$thistrip['arr_time']+$arr_st_offset);
			$arr_dst_time = ($basestamp+$thistrip['arr_time']+$arr_dst_offset);
			$dep_st_time = ($arr_st_time-$thistrip['travel_time']);
			$dep_dst_time = ($arr_dst_time-$thistrip['travel_time']);
			} else {
			$dep_st_offset = gettimediff1(date_default_timezone_get(),"2010-1-1",$stops['s'.$thistrip['dep_loc']]['timezone_id'],"2010-1-1");
			$dep_dst_offset = gettimediff1(date_default_timezone_get(),"2010-6-1",$stops['s'.$thistrip['dep_loc']]['timezone_id'],"2010-6-1");
			$dep_st_time = ($basestamp+$thistrip['dep_time']+$dep_st_offset);
			$dep_dst_time = ($basestamp+$thistrip['dep_time']+$dep_dst_offset);
			$arr_st_time = ($dep_st_time+$thistrip['travel_time']);
			$arr_dst_time = ($dep_dst_time+$thistrip['travel_time']);
			}
		echo strtoupper($thistrip['anchor']).' '.substr($stops['s'.$thistrip[$thistrip['anchor'].'_loc']]['timezone_id'].' -------------------------',8,10).'&gt;';
			echo "\t".date("H:i",($basestamp+$thistrip[$thistrip['anchor'].'_time']));
			echo "\t".substr($stops['s'.$thistrip['dep_loc']]['name'].' -------------------------',0,14);
			echo "\t".substr(date_default_timezone_get().'      ',8,6);
			echo "\t".hoursfrombase($basestamp,$dep_st_time,0).'<SPAN STYLE="color:#999999;">st</SPAN> / '.hoursfrombase($basestamp,$dep_dst_time,0).'<SPAN STYLE="color:#999999;">dst</SPAN>'; //.' '.date("H:i",$dep_st_time)
			$traveldesc = fromsecs1($thistrip['travel_time']);
			echo "\t".substr($traveldesc['h'].'h, '.$traveldesc['m'].'m                  ',0,14);
			echo "\t".substr($stops['s'.$thistrip['arr_loc']]['name'].' -------------------------',0,14);
			echo "\t".substr(date_default_timezone_get().'      ',8,6);
			echo "\t".hoursfrombase($basestamp,$arr_st_time,0).'<SPAN STYLE="color:#999999;">st</SPAN> / '.hoursfrombase($basestamp,$arr_dst_time,0).'<SPAN STYLE="color:#999999;">dst</SPAN>';
			echo "\n";
		if(in_array('st_'.$thistrip['routeid'].'_'.$thistrip['dep_time'],$needed_ids,1)){
			$output .= '"t_st_'.$thistrip['routeid'].'_'.$thistrip['dep_time'].'","'.hoursfrombase($basestamp,$dep_st_time).'","'.hoursfrombase($basestamp,$dep_st_time).'","stp'.$thistrip['dep_loc'].'","1"'."\n";
			$output .= '"t_st_'.$thistrip['routeid'].'_'.$thistrip['dep_time'].'","'.hoursfrombase($basestamp,$arr_st_time).'","'.hoursfrombase($basestamp,$arr_st_time).'","stp'.$thistrip['arr_loc'].'","2"'."\n";
			}
		if(in_array('dst_'.$thistrip['routeid'].'_'.$thistrip['dep_time'],$needed_ids,1)){
			$output .= '"t_dst_'.$thistrip['routeid'].'_'.$thistrip['dep_time'].'","'.hoursfrombase($basestamp,$dep_dst_time).'","'.hoursfrombase($basestamp,$dep_dst_time).'","stp'.$thistrip['dep_loc'].'","1"'."\n";
			$output .= '"t_dst_'.$thistrip['routeid'].'_'.$thistrip['dep_time'].'","'.hoursfrombase($basestamp,$arr_dst_time).'","'.hoursfrombase($basestamp,$arr_dst_time).'","stp'.$thistrip['arr_loc'].'","2"'."\n";
			}
		}

	echo "\n";
	$zip->addFromString('stop_times.txt',$output);
	$count_stop_times = explode("\n",$output); $count_stop_times = count($count_stop_times)-2;

//shapes.txt
$output = '"shape_id","shape_pt_lat","shape_pt_lon","shape_pt_sequence"'."\n";
	foreach($shapes as $id => $shape){
		foreach($shape as $key => $coord){
			$coord = explode(',',$coord);
			$output .= '"shp'.$id.'","'.$coord[0].'","'.$coord[1].'","'.$key.'"'."\n";
			}
		}
	$zip->addFromString('shapes.txt',$output);
	$count_shapes = explode("\n",$output); $count_shapes = count($count_shapes)-2;

$zip->close();

	$msg = 'Updated Google Transit feed: '.$count_stops.' stops, '.$count_routes.' routes, '.$count_trips.' trips, '.$count_stop_times.' stop times, '.$count_calendar_dates.' calendar entries, '.$count_shapes.' shape coordinates';
	if(isset($successmsg) && is_array($successmsg)){ array_push($successmsg,$msg); } else { echo $msg; }

} else {
	$msg = 'Failed to update Google Transit feed.';
	if(isset($errormsg) && is_array($errormsg)){ array_push($errormsg,$mg); } else { echo $msg; }
}

	//@touch($folder.'calendar.txt'); @chmod($folder.'calendar.txt', 0777);
	//$fp=fopen($folder.'calendar.txt',"w"); fwrite($fp,$output); fclose($fp);

?>