<?

@ini_set("session.gc_maxlifetime","10800");
session_start();

include($_SERVER['DOCUMENT_ROOT'].'/header.php');

echo '<center>'."\n\n";

//echo '<PRE>'; print_r($_POST); echo '</PRE>';

function printmsgs($successmsg,$errormsg){
	if(isset($successmsg) && count($successmsg) > 0 || isset($errormsg) && count($errormsg) > 0): echo '<BR>'."\n"; endif;
	if(isset($successmsg) && count($successmsg) > 0){
	echo '<TABLE BORDER="0" BGCOLOR="#000C7F" CELLSPACING="1" CELLPADDING="2" WIDTH="600">
	<TR BGCOLOR="#FFFFFF"><TD ALIGN="center"><FONT FACE="Arial" SIZE="3">Success</FONT></TD></TR>
	<TR BGCOLOR="#D9DBEC"><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><I>'.implode("<BR>",$successmsg).'</I><BR></FONT></TD></TR>
	</TABLE>';
	if(isset($errormsg) && count($errormsg) > 0): echo '<BR>'; endif;
	echo "\n\n";
	}
	if(isset($errormsg) && count($errormsg) > 0){
	echo '<TABLE BORDER="0" BGCOLOR="#B00000" CELLSPACING="1" CELLPADDING="2" WIDTH="600">
	<TR BGCOLOR="#FFFFFF"><TD ALIGN="center"><FONT FACE="Arial" SIZE="3">Error:</FONT></TD></TR>
	<TR BGCOLOR="#F3D9D9"><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><I>'.implode("<BR>",$errormsg).'</I></FONT></TD></TR>
	</TABLE>'."\n\n";
	}
	if(isset($successmsg) && count($successmsg) > 0 || isset($errormsg) && count($errormsg) > 0): echo '<BR>'."\n"; endif;
}




echo '<FORM METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";


//Setup ez session
if(!isset($_SESSION['ez_scity'])){ $_SESSION['ez_scity'] = 1; }
if(!isset($_SESSION['ez_whereto'])){ $_SESSION['ez_whereto'] = array(); }
if(!isset($_SESSION['ez_ecity'])){ $_SESSION['ez_ecity'] = 1; }
if(!isset($_SESSION['ez_smonth'])){ $_SESSION['ez_smonth'] = date("n"); }
if(!isset($_SESSION['ez_sday'])){ $_SESSION['ez_sday'] = date("j"); }
if(!isset($_SESSION['ez_syear'])){ $_SESSION['ez_syear'] = date("Y"); }
if(!isset($_SESSION['ez_emonth'])){ $_SESSION['ez_emonth'] = date("n",strtotime("+1 year")); }
if(!isset($_SESSION['ez_eday'])){ $_SESSION['ez_eday'] = date("j",strtotime("+1 year")); }
if(!isset($_SESSION['ez_eyear'])){ $_SESSION['ez_eyear'] = date("Y",strtotime("+1 year")); }

//Save new values
if(isset($_REQUEST['ez_scity'])){ $_SESSION['ez_scity'] = $_REQUEST['ez_scity']; }
if(isset($_REQUEST['ez_whereto'])){
	$_SESSION['ez_whereto'] = $_REQUEST['ez_whereto'];
	} elseif(isset($_REQUEST['ez_whereto_post']) && $_REQUEST['ez_whereto_post'] == 1){
	$_SESSION['ez_whereto'] = array();
	}
if(isset($_REQUEST['ez_ecity'])){ $_SESSION['ez_ecity'] = $_REQUEST['ez_ecity']; }
if(isset($_REQUEST['ez_smonth'])){ $_SESSION['ez_smonth'] = $_REQUEST['ez_smonth']; }
if(isset($_REQUEST['ez_sday'])){ $_SESSION['ez_sday'] = $_REQUEST['ez_sday']; }
if(isset($_REQUEST['ez_syear'])){ $_SESSION['ez_syear'] = $_REQUEST['ez_syear']; }
if(isset($_REQUEST['ez_emonth'])){ $_SESSION['ez_emonth'] = $_REQUEST['ez_emonth']; }
if(isset($_REQUEST['ez_eday'])){ $_SESSION['ez_eday'] = $_REQUEST['ez_eday']; }
if(isset($_REQUEST['ez_eyear'])){ $_SESSION['ez_eyear'] = $_REQUEST['ez_eyear']; }
	//echo '<PRE STYLE="text-align:left;">'; print_r($_SESSION['ez_whereto']); echo '</PRE>';

//GET STARTING CITIES
$scity = array();
$query = 'SELECT DISTINCT locations.`id`, locations.`name` FROM `locations`,`tours` WHERE tours.`archived` = 0 AND tours.`hidden` != "1" AND locations.`ez_start` = "1" AND locations.`id` = ( SELECT routes.`dep_loc` FROM `tours_assoc`,`routes` WHERE tours_assoc.`tourid` = tours.`id` AND tours_assoc.`type` = "r" AND tours_assoc.`dir` = "f" AND tours_assoc.`typeid` = routes.`id` ORDER BY tours_assoc.`day` ASC, tours_assoc.`order` ASC LIMIT 1 ) ORDER BY locations.`order_weight` ASC, locations.`name` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	//array_push($scity,$row);
	$scity['s'.$row['id']] = $row;
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($scity); echo '</PRE>';

//GET DESTINATIONS
$whereto = array();
$query = 'SELECT DISTINCT locations.`id`, locations.`name` FROM `locations`,`tours_assoc`,`routes` WHERE locations.`ez_destination` = "1" AND tours_assoc.`type` = "r" AND tours_assoc.`typeid` = routes.`id` AND routes.`arr_loc` = locations.`id` ORDER BY locations.`name` ASC'; // locations.`order_weight` ASC,
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	//array_push($whereto,$row);
	$whereto['w'.$row['id']] = $row;
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($whereto); echo '</PRE>';

//GET ENDING CITIES
$ecity = array();
$query = 'SELECT DISTINCT locations.`id`, locations.`name` FROM `locations`,`tours` WHERE tours.`archived` = 0 AND tours.`hidden` != "1" AND locations.`ez_start` = "1" AND locations.`id` = ( SELECT routes.`arr_loc` FROM `tours_assoc`,`routes` WHERE tours_assoc.`tourid` = tours.`id` AND tours_assoc.`type` = "r" AND tours_assoc.`dir` = "f" AND tours_assoc.`typeid` = routes.`id` ORDER BY tours_assoc.`day` DESC, tours_assoc.`order` DESC LIMIT 1 ) ORDER BY locations.`order_weight` ASC, locations.`name` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	//array_push($scity,$row);
	$ecity['e'.$row['id']] = $row;
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($ecity); echo '</PRE>';



echo '<DIV STYLE="margin-top:20px; font-family:Arial; font-size:14pt;">Where would you like to start?</DIV>'."\n\n";

echo '<SELECT NAME="ez_scity" STYLE="font-size:12pt; width:220px;">';
	echo '<OPTION VALUE="0">Anywhere</OPTION>';
	foreach($scity as $row){
		echo '<OPTION VALUE="'.$row['id'].'"';
		if($_SESSION['ez_scity'] == $row['id']){ echo ' SELECTED'; }
		echo '>'.$row['name'].'</OPTION>';
		}
	echo '</SELECT>'."\n\n";


echo '<DIV STYLE="margin-top:20px; font-family:Arial; font-size:14pt;">Where would you like to go?</DIV>'."\n\n";

echo '<TABLE BORDER="0"><TR>';
	$col = 1;
	foreach($whereto as $row){
		if($col == 3){ echo '</TR><TR>'; $col = 1; }
		echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:11pt; white-space:nowrap; padding-right:10px;"><INPUT TYPE="checkbox" NAME="ez_whereto[]" ID="w'.$row['id'].'" VALUE="'.$row['id'].'"';
			if(in_array($row['id'],$_SESSION['ez_whereto'])){ echo ' CHECKED'; }
			echo '> <LABEL FOR="w'.$row['id'].'">'.$row['name'].'</LABEL></TD>';
		$col++;
		}
	echo '</TR></TABLE>'."\n";
	echo '<INPUT TYPE="hidden" NAME="ez_whereto_post" VALUE="1">'."\n\n";


echo '<DIV STYLE="margin-top:20px; font-family:Arial; font-size:14pt;">Where would you like to finish?</DIV>'."\n\n";

echo '<SELECT NAME="ez_ecity" STYLE="font-size:12pt; width:220px;">';
	echo '<OPTION VALUE="0">Anywhere</OPTION>';
	foreach($ecity as $row){
		echo '<OPTION VALUE="'.$row['id'].'"';
		if($_SESSION['ez_ecity'] == $row['id']){ echo ' SELECTED'; }
		echo '>'.$row['name'].'</OPTION>';
		}
	echo '</SELECT>'."\n\n";


echo '<DIV STYLE="margin-top:20px; font-family:Arial; font-size:14pt;">What dates are you available?</DIV>'."\n\n";

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0">';
	echo '<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; padding-right:6px;">Starting on or after:</TD><TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt;">';
		echo '<SELECT NAME="ez_smonth" ID="ez_smonth">';
		for($i=1; $i<=12; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if($_SESSION['ez_smonth'] == $i){ echo ' SELECTED'; }
			echo '>'.date("F",mktime(0,0,0,$i,1,2010)).'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="ez_sday" ID="ez_sday">';
		for($i=1; $i<=31; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if($_SESSION['ez_sday'] == $i){ echo ' SELECTED'; }
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="ez_syear" ID="ez_syear">';
		for($i=date("Y"); $i<=(date("Y")+2); $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if($_SESSION['ez_syear'] == $i){ echo ' SELECTED'; }
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT></TD></TR>';

	echo '<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; padding-right:6px;">Ending on or before:</TD><TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt;">';
		echo '<SELECT NAME="ez_emonth" ID="ez_emonth">';
		for($i=1; $i<=12; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if($_SESSION['ez_emonth'] == $i){ echo ' SELECTED'; }
			echo '>'.date("F",mktime(0,0,0,$i,1,2010)).'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="ez_eday" ID="ez_eday">';
		for($i=1; $i<=31; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if($_SESSION['ez_eday'] == $i){ echo ' SELECTED'; }
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="ez_eyear" ID="ez_eyear">';
		for($i=date("Y"); $i<=(date("Y")+2); $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if($_SESSION['ez_eyear'] == $i){ echo ' SELECTED'; }
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT></TD></TR>';

	echo '</TABLE>'."\n\n";


echo '<BR><BR>';

echo '<INPUT TYPE="submit" VALUE="Find tours" STYLE="width:120px; font-size:14pt;">'."\n\n";

echo '</FORM>'."\n\n";







$query = 'SELECT DISTINCT tours.`id`, tours.`alias`, tours.`title`, tours.`numdays`, tours.`short_desc`, (SELECT routes.`dep_loc` FROM `tours_assoc`,`routes` WHERE tours_assoc.`tourid` = tours.`id` AND tours_assoc.`type` = "r" AND tours_assoc.`dir` = "f" AND tours_assoc.`typeid` = routes.`id` ORDER BY tours_assoc.`day` ASC, tours_assoc.`order` ASC LIMIT 1) AS `scity`, (SELECT routes.`arr_loc` FROM `tours_assoc`,`routes` WHERE tours_assoc.`tourid` = tours.`id` AND tours_assoc.`type` = "r" AND tours_assoc.`dir` = "f" AND tours_assoc.`typeid` = routes.`id` ORDER BY tours_assoc.`day` DESC, tours_assoc.`order` DESC LIMIT 1) AS `ecity`, tours.`perguest`, tours.`mincharge`, tours.`maxcharge`, tours.`volstart`, tours.`volrate`, tours.`ext_pricing`';
		if($_SESSION['lang'] != "1"): $query .= ', tours_translations.`title` AS `trans_title`, tours_translations.`short_desc` AS `trans_short_desc`'; endif;
		//if($list['ar_trans'] == "y"): $query .= ', tours_transtypes_assoc.`transid`'; endif;
	$query .= ' FROM';
		if($_SESSION['lang'] == "1"){ $query .= ' `tours`'; } else { $query .= ' (`tours` LEFT JOIN `tours_translations` ON tours.`id` = tours_translations.`tourid` AND tours_translations.`lang` = "'.$_SESSION['lang'].'")'; }
		//if($list['destination'] > 0): $query .= ',`tours_assoc`,`routes`'; endif;
		//if($list['trans_type'] > 0 || $list['ar_trans'] == "y"): $query .= ',`tours_transtypes_assoc`'; endif;
	$query .= ' WHERE tours.`archived` = 0 AND tours.`hidden` != "1"';
	if($_SESSION['ez_scity'] != 0){ $query .= ' AND (SELECT routes.`dep_loc` FROM `tours_assoc`,`routes` WHERE tours_assoc.`tourid` = tours.`id` AND tours_assoc.`type` = "r" AND tours_assoc.`dir` = "f" AND tours_assoc.`typeid` = routes.`id` ORDER BY tours_assoc.`day` ASC, tours_assoc.`order` ASC LIMIT 1) = "'.$_SESSION['ez_scity'].'"'; }
	if(count($_SESSION['ez_whereto']) > 0){
		$query .= ' AND (SELECT COUNT(DISTINCT routes.`arr_loc`) FROM `tours_assoc`,`routes` WHERE tours_assoc.`tourid` = tours.`id` AND tours_assoc.`type` = "r" AND tours_assoc.`dir` = "f" AND tours_assoc.`typeid` = routes.`id`';
		$query .= ' AND (routes.`arr_loc` = "'.implode('" OR routes.`arr_loc` = "',$_SESSION['ez_whereto']).'")';
		$query .= ' GROUP BY tours_assoc.`tourid` LIMIT 1) = '.count($_SESSION['ez_whereto']);
		}
	if($_SESSION['ez_ecity'] != 0){ $query .= ' AND (SELECT routes.`arr_loc` FROM `tours_assoc`,`routes` WHERE tours_assoc.`tourid` = tours.`id` AND tours_assoc.`type` = "r" AND tours_assoc.`dir` = "f" AND tours_assoc.`typeid` = routes.`id` ORDER BY tours_assoc.`day` DESC, tours_assoc.`order` DESC LIMIT 1) = "'.$_SESSION['ez_ecity'].'"'; }
	$query .= ' AND EXISTS (SELECT * FROM `tours_dates` WHERE tours_dates.`tourid` = tours.`id` AND tours_dates.`date` >= "'.mktime(0,0,0,$_SESSION['ez_smonth'],$_SESSION['ez_sday'],$_SESSION['ez_syear']).'" AND tours_dates.`date` <= "'.mktime(0,0,0,$_SESSION['ez_emonth'],$_SESSION['ez_eday'],$_SESSION['ez_eyear']).'")';
	//if($list['destination'] > 0): $query .= ' AND (tours.`id` = tours_assoc.`tourid` AND tours_assoc.`type` = "r" AND tours_assoc.`typeid` = routes.`id` AND routes.`arr_loc` = "'.$list['destination'].'")'; endif;
	//if($list['trans_type'] > 0 || $list['ar_trans'] == "y"): $query .= ' AND tours.`id` = tours_transtypes_assoc.`tourid`'; endif;
	//if($list['trans_type'] > 0): $query .= ' AND (tours.`id` = tours_transtypes_assoc.`tourid` AND tours_transtypes_assoc.`transid` = "'.$list['trans_type'].'")'; endif;
	$query .= ' ORDER BY';
		if($_SESSION['ez_scity'] != 0){ $query .= ' `scity` ASC,'; }
		//if($list['ar_trans'] == "y"): $query .= ' tours_transtypes_assoc.`transid` ASC,'; endif;
		$query .= ' tours.`order_weight` ASC, tours.`numdays` ASC, tours.`perguest` ASC, tours.`title` ASC';
	//echo $query.'<BR><BR>'."\n\n";
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);
	//echo mysql_error();

	$tour_results = array();
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		if(isset($row['trans_title']) && $row['trans_title'] != ""): $row['title'] = $row['trans_title']; endif;
		if(isset($row['trans_short_desc']) && $row['trans_short_desc'] != ""): $row['short_desc'] = $row['trans_short_desc']; endif;
		array_push($tour_results,$row);
		}
		//echo '<PRE STYLE="text-align:left;">'; print_r($tour_results); echo '</PRE>';





echo '<DIV ID="tour_results" STYLE="margin-top:20px;">';



	echo '<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%" CLASS="tourlisttbl">'."\n";
	echo '<TR BGCOLOR="#CCCCCC">';
		echo '<TD ALIGN="center" CLASS="tourlisthd"><FONT FACE="Arial" SIZE="2"><B>'.gettrans('Title').'</B></FONT></TD>';
		echo '<TD ALIGN="center" CLASS="tourlisthd"><FONT FACE="Arial" SIZE="2"><B>'.gettrans('Days').'</B></FONT></TD>';
		echo '<TD ALIGN="center" CLASS="tourlisthd"><FONT FACE="Arial" SIZE="2"><B>'.gettrans('Price').'</B></FONT></TD>';
		echo '<TD ALIGN="center" CLASS="tourlisthd" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>'.gettrans('Brief Description').'</B></FONT></TD>';
		echo '</TR>'."\n";

	foreach($tour_results as $row){
		$thisvarlink = '/tour.php?id='.urlencode($row['alias']);
		echo '<TR CLASS="tourlistrow">';
		echo '<TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><B><A HREF="'.$thisvarlink.'">'.$row['title'].'</A></B></FONT></TD>';
		echo '<TD ALIGN="center"><FONT FACE="Arial" SIZE="2">'.$row['numdays'].'</FONT></TD>';
		echo '<TD ALIGN="center"><FONT FACE="Arial" SIZE="2">';
			if($row['volrate'] > 0): echo '$'.$row['volrate'].' to '; endif;
			echo '$'.$row['perguest'].'&nbsp;</FONT></TD>';
		echo '<TD ALIGN="left" CLASS="tourlistdesc"><FONT FACE="Arial" SIZE="2">'.$row['short_desc'].'</FONT></TD>';
		echo '</TR>'."\n";
		echo '<TR><TD COLSPAN="5" STYLE="padding:0px;"><HR SIZE="1" COLOR="#999999"></TD></TR>'."\n";
		}

	echo '</TABLE>';




	echo '</DIV>';



include($_SERVER['DOCUMENT_ROOT'].'/footer.php');

?>