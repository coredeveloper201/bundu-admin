<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

function getamount($item){
	global $SESSION;
	$amount = 0;
	$item['numguests'] = ($item['adults']+$item['seniors']+$item['children']);

	if($item['type'] == "t"){

	$query = 'SELECT * FROM `tours` WHERE `id` = "'.$item['tourid'].'" AND `archived` = 0 AND `hidden` != "1" LIMIT 1';
		$result = @mysql_query($query);
		$num_results = @mysql_num_rows($result);
		if(isset($num_results) && $num_results == 1){
			$tourinfo = mysql_fetch_assoc($result);

			$ext = array();
			$tourinfo['ext_pricing'] = explode('|',$tourinfo['ext_pricing']);
			foreach($tourinfo['ext_pricing'] as $val){
				$val = explode(':',$val);
				$ext[$val[0]] = $val[1];
				}

			if(isset($ext[$item['numguests']]) && $ext[$item['numguests']] > 0){
				$amount = $ext[$item['numguests']];
				} elseif($tourinfo['volstart'] > 0 && $tourinfo['volrate'] > 0 && $item['numguests'] >= $tourinfo['volstart']){
				$amount = ($item['numguests']*$tourinfo['volrate']);
				} else {
				$amount = ($item['numguests']*$tourinfo['perguest']);
				}

			if($tourinfo['mincharge'] > 0 && $amount < $tourinfo['mincharge']): $amount = $tourinfo['mincharge']; endif;
			if($tourinfo['maxcharge'] > 0 && $amount > $tourinfo['maxcharge']): $amount = $tourinfo['maxcharge']; endif;

			//$amount += ($item['numguests']*$tourinfo['fuelsurcharge']);

		} //End Tour Num Results if statement

	} elseif($item['type'] == "a"){

		$query = 'SELECT * FROM `activities` WHERE `id` = "'.$item['activityid'].'" LIMIT 1';
		$result = @mysql_query($query);
		$num_results = @mysql_num_rows($result);
		if(isset($num_results) && $num_results == 1){
			$actinfo = mysql_fetch_assoc($result);
			$amount = ($item['numguests']*$actinfo['price']);
			}

	} elseif($item['type'] == "r"){

		$cpm = 0.40;
		$query = 'SELECT `setting`,`var` FROM `bundubus_settings` WHERE `setting` = "cpm" LIMIT 1';
			$result = @mysql_query($query);
			$num_results = @mysql_num_rows($result);
			if(isset($num_results) && $num_results == 1){
				$default = mysql_fetch_assoc($result);
				$cpm = $default['var'];
				}

		$query = 'SELECT * FROM `routes` WHERE `id` = "'.$item['routeid'].'" AND `hidden` != "1" LIMIT 1';
			$result = @mysql_query($query);
			$num_results = @mysql_num_rows($result);
			if(isset($num_results) && $num_results == 1){
			$routeinfo = mysql_fetch_assoc($result);

			if(isset($item['bbticket']) && $item['bbticket'] != "" && $item['bbticket'] > 0){
				$amount = 0;
				} elseif($routeinfo['bbprice'] > 0){
				$amount = ($routeinfo['bbprice']*$item['numguests']);
				} else {
				$amount = (($cpm*$routeinfo['miles'])*$item['numguests']);
				}

			} //End Route Num Results if statement

	} elseif($item['type'] == "s"){
	
		$adults = $item['adults'];
	
		//ARRIVAL SENIORS
		if($item['seniors'] > 0){
		$query = 'SELECT * FROM `shuttle_pricing` WHERE `market` = "'.$item['market'].'" AND `trans_type` = "'.$item['trans_type'].'" AND `pax1` <= "'.$item['seniors'].'" AND `pax2` >= "'.$item['seniors'].'" AND `seniors` > 0 ORDER BY `market` ASC, `pax1` ASC, `pax2` ASC, `adults` ASC LIMIT 1';
			$result = mysql_query($query);
			$num_results = mysql_num_rows($result);
			if($num_results == 1){
				$thisprice = mysql_fetch_assoc($result);
				$amount = ($amount+$thisprice['seniors']);
				} else {
				$adults = ($adults+$item['seniors']);
				}
			}
	
		//ARRIVAL CHILDREN
		if($item['children'] > 0){
		$query = 'SELECT * FROM `shuttle_pricing` WHERE `market` = "'.$item['market'].'" AND `trans_type` = "'.$item['trans_type'].'" AND `pax1` <= "'.$item['children'].'" AND `pax2` >= "'.$item['children'].'" AND `children` > 0 ORDER BY `market` ASC, `pax1` ASC, `pax2` ASC, `adults` ASC LIMIT 1';
			$result = mysql_query($query);
			$num_results = mysql_num_rows($result);
			if($num_results == 1){
				$thisprice = mysql_fetch_assoc($result);
				$amount = ($amount+$thisprice['children']);
				} else {
				$adults = ($adults+$item['children']);
				}
			}
	
		//ARRIVAL ADULTS
		$query = 'SELECT * FROM `shuttle_pricing` WHERE `market` = "'.$item['market'].'" AND `trans_type` = "'.$item['trans_type'].'" AND `pax1` <= "'.$adults.'" AND `pax2` >= "'.$adults.'" AND `adults` > 0 ORDER BY `market` ASC, `pax1` ASC, `pax2` ASC, `adults` ASC LIMIT 1';
			$result = mysql_query($query);
			$num_results = mysql_num_rows($result);
			$thisprice = mysql_fetch_assoc($result);
			$amount = ($amount+$thisprice['adults']);

	} elseif($item['type'] == "l"){

		$start = mktime(0,0,0,$_REQUEST['date_month'],$_REQUEST['date_day'],$_REQUEST['date_year']);
		$end = mktime(0,0,0,$_REQUEST['date_month'],($_REQUEST['date_day']+$_REQUEST['nights']),$_REQUEST['date_year']);

		$query = 'SELECT * FROM `lodging_pricing` WHERE `lodgeid` = "'.$_REQUEST['lodgeid'].'" AND `startdate` <= "'.$start.'" AND `enddate` >= "'.$end.'" AND `min_nights` <= "'.$_REQUEST['nights'].'" AND `max_nights` >= "'.$_REQUEST['nights'].'" ORDER BY `price` DESC LIMIT 1';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		if($num_results == 1){
			$pricing = mysql_fetch_assoc($result);
			if($pricing['calc'] == "*"){
				$amount = ($_REQUEST['nights'] * $pricing['price']);
				} else {
				$amount = $pricing['price'];
				}
			}

	} //End Type If Statement

	return $amount;
	} //End getamount()

if(isset($_REQUEST['getamount'])){

	@date_default_timezone_set('America/Denver');
	$time = mktime();

	if(!$db){ $db = @MYSQL_CONNECT("localhost","sessel_bbsite","?R&zTQ=UdFX$"); }
	@mysql_select_db("sessel_bundubashers");
	@mysql_query("SET NAMES utf8");

	foreach($_REQUEST as $key => $row){
		$_REQUEST[$key] = trim($_REQUEST[$key]);
		$_REQUEST[$key] = strip_tags($_REQUEST[$key]);
		}

	echo '||BEGIN||'."\n";
	echo getamount($_REQUEST)."\n";
	echo '||END||'."\n";
	
	}

?>