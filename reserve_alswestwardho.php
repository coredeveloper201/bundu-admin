<? // This script written by Dominick Bernal - www.bernalwebservices.com

if($_SERVER["HTTP_HOST"] != "www.bundubashers.com" || !isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on"){
	//header("Location: https://www.bundubashers.com/reserve_alswestwardho.php?".$_SERVER["QUERY_STRING"]);
	//exit;
	}

@date_default_timezone_set('America/Denver');
$time = mktime();

@ini_set("session.gc_maxlifetime","10800");
if(!isset($_SESSION)){ session_start();	}

if(!isset($_SESSION['http_referer'])){ $_SESSION['http_referer'] = ''; }
if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != "" && parse_url($_SERVER['HTTP_REFERER'],PHP_URL_HOST) != $_SERVER["HTTP_HOST"]){
	$_SESSION['http_referer'] = $_SERVER['HTTP_REFERER'];
	}

if(@!$db){ $db = @MYSQL_CONNECT("localhost","sessel_bbresrv","~vb)b9[tMLrA"); }
@mysql_select_db("sessel_bundubashers");
@mysql_query("SET NAMES utf8");
@mysql_query('SET time_zone = "'.date("P").'"');

$fullwidth = "780";
$tablewidth = "720";
$leftcol = "300";
$rightcol = ($tablewidth-$leftcol);

function printmsgs($successmsg,$errormsg){
	if(isset($successmsg) && count($successmsg) > 0 || isset($errormsg) && count($errormsg) > 0): echo '<IMG SRC="spacer.gif" BORDER="0"><BR>'."\n"; endif;
	if(isset($successmsg) && count($successmsg) > 0){
	echo '<TABLE BORDER="0" BGCOLOR="#000C7F" CELLSPACING="1" CELLPADDING="2" WIDTH="600">
	<TR BGCOLOR="#FFFFFF"><TD ALIGN="center"><FONT FACE="Arial" SIZE="3">Update Successful</FONT></TD></TR>
	<TR BGCOLOR="#D9DBEC"><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><I>'.implode("<BR>",$successmsg).'</I><BR></FONT></TD></TR>
	</TABLE>';
	if(isset($errormsg) && count($errormsg) > 0): echo '<BR>'; endif;
	echo "\n\n";
	}
	if(isset($errormsg) && count($errormsg) > 0){
	echo '<TABLE BORDER="0" BGCOLOR="#B00000" CELLSPACING="1" CELLPADDING="2" WIDTH="600">
	<TR BGCOLOR="#FFFFFF"><TD ALIGN="center"><FONT FACE="Arial" SIZE="3">Error:</FONT></TD></TR>
	<TR BGCOLOR="#F3D9D9"><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><I>'.implode("<BR>",$errormsg).'</I></FONT></TD></TR>
	</TABLE>'."\n\n";
	}
}

function bgcolor($i){
	if(!isset($GLOBALS['bgcolor']) || $i == "reset"): $GLOBALS['bgcolor'] = "DDDDDD"; endif;
	if($i != "reset" && $i != ""): $GLOBALS['bgcolor'] = $i; endif;
	if($GLOBALS['bgcolor'] == "FFFFFF"): $GLOBALS['bgcolor'] = "DDDDDD"; else: $GLOBALS['bgcolor'] = "FFFFFF"; endif;
	return $GLOBALS['bgcolor'];
}

$successmsg = array();
$errormsg = array();
$adminsuccessmsg = array();
$adminerrormsg = array();

if(isset($_REQUEST['start_year']) && $_REQUEST['start_year'] != ""): $syear = $_REQUEST['start_year']; else: $syear = date("Y",$time); endif;

$seasons = array("5/1","6/14","9/23","10/7");
	for($i=0; $i<count($seasons); $i++){
	$seasons[$i] = explode("/",$seasons[$i]);
	$seasons[$i] = mktime(0,0,0,$seasons[$i][0],$seasons[$i][1],$syear);
	}

$roomtypes = array(
	//"Description","Season1","Season2","Season 3"
	"1" => array("Single Room (1 Queen Bed)","70","84","70"),
	"2" => array("Single Room (2 Queen Beds)","90","95","80"),
	"3" => array("Two Room Suite (3 or 4 Beds)","105","115","105")
	);


echo '<HTML>

<HEAD>
	<TITLE>Al'."'".'s Westward Ho</TITLE>
	<style><!--
	.res1 { font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: #000000; }
	--></style>
</HEAD>

<BODY BGCOLOR="#E1E1E1" TEXT="#000000" TOPMARGIN="0">

<CENTER>

<TABLE BORDER="0" WIDTH="'.$fullwidth.'" CELLPADDING="0" CELLSPACING="2" BGCOLOR="#FFFFFF">
<TR><TD ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>
<TR><TD ALIGN="center">
	<TABLE BORDER="0" WIDTH="'.$tablewidth.'" CELLPADDING="0" CELLSPACING="2"><TR><TD ALIGN="center"><IMG SRC="alswestwardho.gif" ALT="Al'."'".'s Westward Ho"></TD></TR></TABLE>
	<HR SIZE="1" WIDTH="'.$tablewidth.'">'."\n";

		//VALIDATE INPUT
		if(isset($_POST['utaction']) && $_POST['utaction'] == "process"){
			if(!isset($_REQUEST['name']) || trim($_REQUEST['name']) == "" || !isset($_REQUEST['phone_homebus']) || trim($_REQUEST['phone_homebus']) == "" || !isset($_REQUEST['email']) || trim($_REQUEST['email']) == "" || !isset($_REQUEST['cc_num']) || trim($_REQUEST['cc_num']) == "" || !isset($_REQUEST['cc_scode']) || trim($_REQUEST['cc_scode']) == ""){
			@array_push($errormsg,'One or more required fields were not filled in.  Please fill in all required fields.');
			$_POST['utaction'] = "validateres";
			$_REQUEST['utaction'] = "validateres";
			$step2 = "y";
			}
		}
		//VALIDATE DATES
		if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "validateres"){
		$start = mktime(0,0,0,$_REQUEST['start_month'],$_REQUEST['start_day'],$_REQUEST['start_year']);
		if(!is_numeric($_REQUEST['nights'])): $_REQUEST['nights'] = 1; else: $_REQUEST['nights'] = number_format($_REQUEST['nights'],0,'',''); endif;
		$end = $start+($_REQUEST['nights']*86400);
		//echo $start.' : '.$seasons[0].'<BR>'.$end.' : '.end($seasons);
		if($start < $seasons[0] || $start > (end($seasons)+86400) || $end < $seasons[0] || $end > (end($seasons)+86400)): array_push($errormsg,'One or more dates chosen are not available.<BR>Please choose dates between '.date("M j",$seasons[0]).' and '.date("M j",end($seasons)).'.  Thank you.'); else: $step2 = "y"; endif;
		}

		//PRINT SUCCESS/ERROR MESSAGES
		printmsgs($successmsg,$errormsg);

		//PROCESS
		if(isset($_POST['utaction']) && $_POST['utaction'] == "process"){
		$als_checkin = mktime(0,0,0,$_POST['start_month'],$_POST['start_day'],$_POST['start_year']);
		$_POST['total'] = number_format($_POST['total'], 2, '.', '');
		$_POST['cc_num'] = trim($_POST['cc_num']);
		$_POST['cc_num'] = str_replace(' ','',$_POST['cc_num']);
		$_POST['cc_num'] = str_replace('-','',$_POST['cc_num']);
		$_POST['cc_expdate'] = $_POST['cc_expdate_month'].'/'.$_POST['cc_expdate_year'];

		$query = 'INSERT INTO `reservations`(`name`,`phone_homebus`,`phone_cell`,`email`,`amount`,`cc_name`,`cc_num`,`cc_expdate`,`cc_scode`,`pay_method`,`comments`,`http_referer`,`booker`,`date_booked`)';
			$query .= ' VALUES("'.$_POST['name'].'","'.$_POST['phone_homebus'].'","'.$_POST['phone_cell'].'","'.$_POST['email'].'","'.$_POST['total'].'","'.$_POST['name'].'","'.$_POST['cc_num'].'","'.$_POST['cc_expdate'].'","'.$_POST['cc_scode'].'","C/C","'.$_POST['comments'].'","'.$_SESSION['http_referer'].'","Website","'.$time.'")';
			@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror != ""){

			echo '<FONT FACE="Arial" SIZE="3"><BR>The system encountered an error while processing your order.<BR>We apologize for the inconvenience.<BR>Please contact us to complete your reservation.<BR><B>Toll Free: (866) 646-1118</B><BR>Your order number: <B>'.$confnum.'</B><BR><BR></FONT>'."\n\n";
			array_push($adminerrormsg,$thiserror);
	
		} else {
	
		//SUBRESERVATIONS
			$confnum = mysql_insert_id();
			echo '<FONT FACE="Arial" SIZE="3"><BR>Your order number: <B>'.$confnum.'</B><BR><BR>Thank you for your order.  If you placed the order within regular business hours, you can expect an email from us within a couple of hours.<BR><BR>If the order were placed outside regular business hours, we will be in touch after we re-open in the morning.<BR><BR></FONT>'."\n\n";
			array_push($adminsuccessmsg,'Saved new reservation '.$confnum.' for '.$_POST['name'].'.');

			$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`date`,`onlydateavl`,`lodgeid`,`nights`,`amount`,`vendor`)';
				$query .= ' VALUES("'.$confnum.'","'.$roomtypes[$_POST['roomtype']][0].'","l","'.$_POST['name'].'","'.$als_checkin.'",1,"'.$_POST['roomtype'].'","'.$_POST['nights'].'","'.$_POST['total'].'","13")';
				@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror != ""){ array_push($adminerrormsg,$thiserror); }

			$heading = 'New Lodging Reservation #'.$confnum;
			$headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= "To: Bundu Bashers Tours <bundubashers@gmail.com>\r\n";
			//$headers .= "Bcc: BWS Testing <dominick@bernalwebservices.com>\r\n";
			$headers .= "From: ".$_POST['name']." <".$_POST['email'].">\r\n";
			$message = "A customer has ordered a new lodging reservation.<BR><BR>\n\n".$confnum;
			if(!mail('',$heading,$message,$headers)){
				array_push($errormsg,'Unable to send merchant notification.');
				}

		} //End Confnum/Error If Statement

		//echo '<FONT FACE="Arial" SIZE="2"><B>Return to:&nbsp;&nbsp;<A HREF="http://www.yellowstonelodging.biz">West Yellowstone Lodging</A></B><BR><BR></FONT>'."\n\n";

		//PRINT FORMS
		} elseif(isset($step2) && $step2 == "y"){

		$prices = array();
		//Price out dates
		for($i=$start; $i<$end; $i=($i+86400)){
			if($i >= $seasons[0] && $i <= $seasons[1]){ array_push($prices,$roomtypes[$_REQUEST['roomtype']][1]); }
			elseif($i > $seasons[1] && $i <= $seasons[2]){ array_push($prices,$roomtypes[$_REQUEST['roomtype']][2]); }
			elseif($i > $seasons[2] && $i <= ($seasons[3]+86400)){ array_push($prices,$roomtypes[$_REQUEST['roomtype']][3]); }
		}
		//echo '<PRE>'; print_r($prices); echo '</PRE>';
		$total = array_sum($prices);
		$total = number_format($total, 2, '.', '');

		echo '<BR><TABLE BORDER="0" WIDTH="550" CELLPADDING="2" CELLSPACING="1">'."\n";
		echo '<TR BGCOLOR="#CCCCCC"><TD ALIGN="center" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>Your Reservation:</B></FONT></TD></TR>'."\n";

		echo '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="3"><B>Room:</B></FONT></TD><TD ALIGN="left"><FONT FACE="Arial" SIZE="3">'.$roomtypes[$_REQUEST['roomtype']][0].'</FONT></TD></TR>'."\n";
		echo '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="3"><B>Dates:</B></FONT></TD><TD ALIGN="left"><FONT FACE="Arial" SIZE="3">'.date("M j, Y",$start);
			if($start != ($end-86400)): echo ' - '.date("M j, Y",($end-86400)).' ('.$_REQUEST['nights'].' Nights)'; endif;
			echo '</FONT></TD></TR>'."\n";

		echo '<TR BGCOLOR="#E1E1E1"><TD ALIGN="right" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>Subtotal:&nbsp;&nbsp;$'.$total.'&nbsp;&nbsp;</B></FONT></TD></TR>'."\n";

		$tax = ($total*.10);
		echo '<TR BGCOLOR="#E1E1E1"><TD ALIGN="right" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>Tax:&nbsp;&nbsp;&nbsp;$'.number_format($tax, 2, '.', '').'&nbsp;&nbsp;</B></FONT></TD></TR>'."\n";

		$total = $total+$tax;
		$total = number_format($total, 2, '.', '');
		echo '<TR BGCOLOR="#CCCCCC"><TD ALIGN="right" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>Total:&nbsp;&nbsp;$'.$total.'&nbsp;&nbsp;</B></FONT></TD></TR>'."\n";

		echo '</TABLE>'."\n\n";
		echo '<FORM NAME="resform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="roomtype" VALUE="'.$_REQUEST['roomtype'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="start_month" VALUE="'.$_REQUEST['start_month'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="start_day" VALUE="'.$_REQUEST['start_day'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="start_year" VALUE="'.$_REQUEST['start_year'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="nights" VALUE="'.$_REQUEST['nights'].'">'."\n";
		echo '<INPUT TYPE="submit" VALUE="&lt;&lt; Change">'."\n";
		echo '</FORM>'."\n\n";

		echo '<FORM NAME="resform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="process">'."\n";
		echo '<INPUT TYPE="hidden" NAME="roomtype" VALUE="'.$_REQUEST['roomtype'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="start_month" VALUE="'.$_REQUEST['start_month'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="start_day" VALUE="'.$_REQUEST['start_day'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="start_year" VALUE="'.$_REQUEST['start_year'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="nights" VALUE="'.$_REQUEST['nights'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="total" VALUE="'.$total.'">'."\n";
		echo '	<TABLE BORDER="0" WIDTH="'.$tablewidth.'" CELLPADDING="0" CELLSPACING="2">'."\n";
		//echo '	<TR BGCOLOR="#CCCCCC"><TD COLSPAN="2" ALIGN="center"><FONT FACE="Arial" SIZE="2"><B>Contact / Payment Information</B></FONT></TD></TR>'."\n";
		echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";
		echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Name</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="30" NAME="name" VALUE="'; if(isset($_REQUEST['name'])): echo $_REQUEST['name']; endif; echo '"></TD></TR>'."\n";
		echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Home/Business Phone</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="30" NAME="phone_homebus" VALUE="'; if(isset($_REQUEST['phone_homebus'])): echo $_REQUEST['phone_homebus']; endif; echo '"></TD></TR>'."\n";
		echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Cell Phone</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="30" NAME="phone_cell" VALUE="'; if(isset($_REQUEST['phone_cell'])): echo $_REQUEST['phone_cell']; endif; echo '"></TD></TR>'."\n";
		echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Email Address</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="30" NAME="email" VALUE="'; if(isset($_REQUEST['email'])): echo $_REQUEST['email']; endif; echo '"></TD></TR>'."\n";
		echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";
		echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Credit card number</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="30" NAME="cc_num" VALUE="'; if(isset($_REQUEST['cc_num'])): echo $_REQUEST['cc_num']; endif; echo '"></TD></TR>'."\n";
		echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Expiration date</B>&nbsp;</FONT></TD><TD><SELECT NAME="cc_expdate_month">';   for($i=1; $i<13; $i++){   echo '<OPTION VALUE="'.date("m",mktime("0","0","0",$i,"1","2005")).'"'; if( isset($_REQUEST['cc_expdate_month']) && $_REQUEST['cc_expdate_month'] == $i ): echo " SELECTED"; endif; echo '>'.$i.' ('.date("M",mktime("0","0","0",$i,"1","2005")).')</OPTION>';   }   echo '</SELECT> / <SELECT NAME="cc_expdate_year">';   for($i=date("Y",$time); $i<(date("Y",$time)+11); $i++){   echo '<OPTION VALUE="'.date("y",mktime("0","0","0","1","1",$i)).'"'; if( isset($_REQUEST['cc_expdate_year']) && $_REQUEST['cc_expdate_year'] == date("y",mktime("0","0","0","1","1",$i)) ): echo " SELECTED"; endif; echo '>'.$i.'</OPTION>';   }   echo '</SELECT></TD></TR>'."\n";
		echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Security code</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="5" NAME="cc_scode" VALUE="'; if(isset($_REQUEST['cc_scode'])): echo $_REQUEST['cc_scode']; endif; echo '"></TD></TR>'."\n";
		echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";
		echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Comments</B>&nbsp;</FONT></TD><TD ALIGN="left" WIDTH="'.$rightcol.'"><TEXTAREA NAME="comments" COLS="30" ROWS="3">'; if(isset($_REQUEST['comments'])): echo $_REQUEST['comments']; endif; echo '</TEXTAREA></TD></TR>'."\n";
		echo '	<TR><TD COLSPAN="2" ALIGN="center"><INPUT TYPE="submit" VALUE="Submit Reservation"></TD></TR>'."\n";
		echo '	</TABLE>'."\n";
		echo '</FORM>'."\n";
		} else {
		echo '<FORM NAME="resform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="validateres">'."\n";
		echo '	<TABLE BORDER="0" WIDTH="'.$tablewidth.'" CELLPADDING="0" CELLSPACING="2">'."\n";
		echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Room Type</B>&nbsp;</FONT></TD><TD><SELECT NAME="roomtype">';   foreach($roomtypes as $key => $thistype){   echo '<OPTION VALUE="'.$key.'"'; if( isset($_REQUEST['roomtype']) && $_REQUEST['roomtype'] == $key ): echo " SELECTED"; endif; echo '>'.$thistype[0].'</OPTION>'; } echo '</SELECT></TD></TR>'."\n";
		echo '	<TR><TD></TD><TD><FONT FACE="Arial" SIZE="1">Rooms may be available from '.date("M. j, Y",$seasons[0]).' through '.date("M. j, Y",end($seasons)).'.</FONT></TD></TR>'."\n";
		echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Check In Date</B>&nbsp;</FONT></TD><TD><SELECT NAME="start_month">';   for($i=date("n",$seasons[0]); $i<(date("n",end($seasons))+1); $i++){   echo '<OPTION VALUE="'.$i.'"'; if( isset($_REQUEST['start_month']) && $_REQUEST['start_month'] == $i ): echo " SELECTED"; endif; echo '>'.date("F",mktime("0","0","0",$i,"1","2005")).'</OPTION>';   }   echo '</SELECT> / <SELECT NAME="start_day">';   for($i=1; $i<32; $i++){   echo '<OPTION VALUE="'.$i.'"'; if( isset($_REQUEST['start_day']) && $_REQUEST['start_day'] == $i ): echo " SELECTED"; endif; echo '>'.$i.'</OPTION>';   }   echo '</SELECT> / <SELECT NAME="start_year">';   for($i=date("Y",$time); $i<(date("Y",$time)+11); $i++){   echo '<OPTION VALUE="'.date("y",mktime("0","0","0","1","1",$i)).'"'; if( isset($_REQUEST['start_year']) && $_REQUEST['start_year'] == date("y",mktime("0","0","0","1","1",$i)) ): echo " SELECTED"; endif; echo '>'.$i.'</OPTION>';   }   echo '</SELECT></TD></TR>'."\n";
		echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Number of Nights</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="5" NAME="nights" VALUE="'; if(isset($_REQUEST['nights'])): echo $_REQUEST['nights']; else: echo '1'; endif; echo '"></TD></TR>'."\n";
		echo '	<TR><TD></TD><TD ALIGN="left"><INPUT TYPE="submit" VALUE="Continue &gt;&gt;"></TD></TR>'."\n";
		echo '	</TABLE>'."\n";
		echo '</FORM>'."\n";
		}

echo '

<script language="javascript">
<!--

function openhelp(){
     open("alswestwardhelp.html", "helpwin", "width=450,height=150,location=no,menubar=no,resizable=yes,status=no,toolbar=no");
}

//-->
</script>

<FONT FACE="Arial" SIZE="3">Need <A HREF="null" onClick="openhelp(); return false">help</A>?<BR>

';

echo '	</TD></TR>'."\n\n";

echo '<TR><TD ALIGN="center"><HR SIZE="1" WIDTH="'.$tablewidth.'">
<FONT FACE="Arial" SIZE="2"><B>Return to:&nbsp;&nbsp;<A HREF="http://www.yellowstonelodging.biz">West Yellowstone Lodging</A></B><BR></FONT>
<FONT FACE="Arial" SIZE="1"><I>West Yellowstone Lodging will not be bound by any prices that are generated maliciously or in error, and that are not its regular prices as detailed on its web sites.</I></FONT><BR><BR>
</TD></TR>'."\n";

echo '</TABLE>

</FORM>


</CENTER>

</BODY>

</HTML>'."\n\n";


  if(isset($adminsuccessmsg) && count($adminsuccessmsg) > 0 || isset($adminerrormsg) && count($adminerrormsg) > 0){
	if(isset($adminsuccessmsg) && count($adminsuccessmsg) > 0){
	$logentry = implode("\n",$adminsuccessmsg);
	$logentry = addslashes($logentry);
	@mysql_query('insert into `logs`(`time`,`user`,`page`,`log`) values("'.$time.'","Web User","'.$pageid.'","'.$logentry.'")');
	}
	if(isset($adminerrormsg) && count($adminerrormsg) > 0){
	$logentry = implode("\n",$adminerrormsg);
	$logentry = addslashes($logentry);
	@mysql_query('insert into `logs`(`time`,`user`,`page`,`log`) values("'.$time.'","Web User","'.$pageid.'","Error: '.$logentry.'")');
	}
  }

?>