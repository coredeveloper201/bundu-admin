<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com



include_once 'functions/datetime.inc.php';


function getblocked($routes='*',$start='*',$end='*',$showdebug='n'){
	$blocked = array();
	$debug = '';

	//PROCESS VARIABLES
	if($routes != '*' && !is_array($routes)){
		$routes = str_replace('|',',',$routes);
		$routes = explode(',',$routes);
		}
	if(is_array($start) || strpos($start,'|') !== false || strpos($start,',') !== false){
		if(!is_array($start)){
			$start = str_replace('|',',',$start);
			$start = explode(',',$start);
			}
		sort($start,SORT_NUMERIC);
		}

	$debug .= 'Routes: '.print_r($routes,TRUE)."\n";
		$debug .= 'Start: '; if(is_numeric($start)){ $debug .= date("n/j/Y G",$start); } else { $debug .= $start; } $debug .= "\n";
		$debug .= 'End: '; if(is_numeric($end)){ $debug .= date("n/j/Y G",$end); } else { $debug .= $end; } $debug .= "\n";
		$debug .= "\n";

	//Find earliest/latest date
	$earliest = time();
	$latest = time();
	if(is_array($start) && count($start) > 0){
		foreach($start as $date){
			if($date < $earliest){
				$earliest = $date;
			}
			if($date > $latest){
				$latest = $date;
			}
		}
	} else {
		$earliest = $start;
		$latest = $end;
	}

	//COLLECT TOUR SEATS
	$query = 'SELECT blocks.*, reservations_assoc.reservation
			FROM blocks
			LEFT JOIN reservations_assoc ON reservations_assoc.id = blocks.id_reservation_assoc
			WHERE id_tour IS NOT NULL';
			if(is_array($routes)){
				$query .= ' AND blocks.id_route IN ("'.implode('", "', $routes).'")';
			}
			if($earliest != '*') {
				$query .= ' AND blocks.date >= "'.mysqlDate($earliest).'"';
			}
			if($latest != '*') {
				$query .= ' AND blocks.date <= "'.mysqlDate($latest).'"';
			}
		$debug .= 'Starting on tour seats...'."\n";
		$debug .= $query."\n\n";
	$result = @mysql_query($query);
	$thiserror = @mysql_error(); if($thiserror != ""){ $debug .= 'Error with route seats query: '.$thiserror."\n"; }
	$num_results = @mysql_num_rows($result);
	$debug .= 'Found '.$num_results.' route/date sets.'."\n";
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);

		//Patches to old timestamp based system
		$row['date'] = strtotime($row['date']);
		$row['routeid'] = $row['id_route'];
		$row['guests'] = $row['seats'];

		if(!isset($blocked['r'.$row['routeid']])){ $blocked['r'.$row['routeid']] = array(); }
		if(!isset($blocked['r'.$row['routeid']]['d'.$row['date']])){
			$blocked['r'.$row['routeid']]['d'.$row['date']] = array(
																'route'=>$row['routeid'],
																'date'=>$row['date'],
																'tour_seats'=>0,
																'route_seats'=>0,
																'reservations'=>array()
																);
			}
			$debug .= "\t".'Adding from '.$row['id_tour'].' - Route: '.$row['id_route'].' Date: '.date("n/j/Y G",$row['date']).' Tour Seats: '.$row['guests']."\n";
		$blocked['r'.$row['routeid']]['d'.$row['date']]['tour_seats'] = ($blocked['r'.$row['routeid']]['d'.$row['date']]['tour_seats']+$row['guests']);
		array_push($blocked['r'.$row['routeid']]['d'.$row['date']]['reservations'],$row['reservation']);
	}
	$debug .= "\n";

	//COLLECT ROUTE SEATS
	$query = 'SELECT * FROM blocks
			WHERE id_tour IS NULL';
			if(is_array($routes)){
				$query .= ' AND id_route IN ("'.implode('", "', $routes).'")';
			}
			if($earliest != '*') {
				$query .= ' AND date >= "'.mysqlDate($earliest).'"';
			}
			if($latest != '*') {
				$query .= ' AND date <= "'.mysqlDate($latest).'"';
			}
		$debug .= 'Starting on route seats...'."\n";
		//$debug .= $query."\n\n";
	$result = @mysql_query($query);
	$thiserror = @mysql_error(); if($thiserror != ""){ $debug .= 'Error with route seats query: '.$thiserror."\n"; }
	$num_results = @mysql_num_rows($result);
	$debug .= 'Found '.$num_results.' route/date sets.'."\n";
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);

		//Patches to old timestamp based system
		$row['date'] = strtotime($row['date']);
		$row['routeid'] = $row['id_route'];
		$row['guests'] = $row['seats'];

		if(!isset($blocked['r'.$row['routeid']])){ $blocked['r'.$row['routeid']] = array(); }
		if(!isset($blocked['r'.$row['routeid']]['d'.$row['date']])){
			$blocked['r'.$row['routeid']]['d'.$row['date']] = array('route'=>$row['routeid'],'date'=>$row['date'],'tour_seats'=>0,'route_seats'=>0);
			}
			$debug .= "\t".'Adding - Route: '.$row['routeid'].' Date: '.date("n/j/Y G",$row['date']).' Route Seats: '.$row['guests']."\n";
		$blocked['r'.$row['routeid']]['d'.$row['date']]['route_seats'] = ($blocked['r'.$row['routeid']]['d'.$row['date']]['route_seats']+$row['guests']);
	}
	//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($blocked,true)).'</PRE>';

	if($showdebug == "y"){ echo '<PRE>'.$debug.'</PRE>'; }
	return $blocked;

} //END getblocked()


/*
function getblocked($routes='*',$start='*',$end='*',$showdebug='n'){
	$blocked = array();
	$debug = '';

	//PROCESS VARIABLES
	if($routes != '*' && !is_array($routes)){
		$routes = str_replace('|',',',$routes);
		$routes = explode(',',$routes);
		}
	if(is_array($start) || strpos($start,'|') !== false || strpos($start,',') !== false){
		if(!is_array($start)){
			$start = str_replace('|',',',$start);
			$start = explode(',',$start);
			}
		sort($start,SORT_NUMERIC);
		}

	$debug .= 'Routes: '.print_r($routes,TRUE)."\n";
		$debug .= 'Start: '; if(is_numeric($start)){ $debug .= date("n/j/Y G",$start); } else { $debug .= $start; } $debug .= "\n";
		$debug .= 'End: '; if(is_numeric($end)){ $debug .= date("n/j/Y G",$end); } else { $debug .= $end; } $debug .= "\n";
		$debug .= "\n";

	//Find earliest/latest date
	$earliest = mktime();
	$latest = mktime();
	if(is_array($start) && count($start) > 0){
		foreach($start as $date){
			if($date < $earliest){
				$earliest = $date;
			}
			if($date > $latest){
				$latest = $date;
			}
		}
	} else {
		$earliest = $start;
		$latest = $end;
	}

	//Find longest tour
	$query = 'SELECT MAX(`numdays`) AS `longest` FROM `tours`';
	$result = @mysql_query($query);
	$thiserror = @mysql_error(); if($thiserror != ""){ $debug .= 'Error with longest tour query: '.$thiserror."\n"; }
	$row = @mysql_fetch_assoc($result);
	$longest_tour = $row['longest'];

	//Find longest extension
	$query = 'SELECT MAX(`nights`) AS `longest` FROM `reservations_assoc` WHERE `extlodging` = 1 AND `canceled` < 1';
	$result = @mysql_query($query);
	$thiserror = @mysql_error(); if($thiserror != ""){ $debug .= 'Error with longest extension query: '.$thiserror."\n"; }
	$row = @mysql_fetch_assoc($result);
	$longest_ext = $row['longest'];

	$earliest = $earliest - ($longest_tour * 86400) - 3600;
	$latest += (($longest_tour + $longest_ext) * 86400) + 3600;

	//GET ALL TOUR RESERVATIONS IN DATE RANGE
	$candidate_tours = array();
	$candidate_res_ids = array();
	$query = 'SELECT `reservation`, `tourid`, `date`
		FROM `reservations_assoc`
		WHERE (`type` = "t" OR `type` = "o") AND `tourid` > 1 AND `canceled` < 1'."\n";
		//Starting date is offset by longest tour currently available
		//Ending date is extended by longest tour and longest extension
		if(is_array($start) && count($start) > 0){
			$query .= '	AND `date` >= "'.$earliest.'"'."\n";
			$query .= '	AND `date` <= "'.$latest.'"'."\n";
		} else {
			if(is_numeric($start)){
				$query .= '	AND `date` >= "'.$earliest.'"'."\n";
			}
			if(is_numeric($end)){
				$query .= '	AND `date` <= "'.$latest.'"'."\n";
			}
		}
	$result = @mysql_query($query);
	$thiserror = @mysql_error(); if($thiserror != ""){ $debug .= 'Error with candidate tours query: '.$thiserror."\n"; }
	while($row = @mysql_fetch_assoc($result)){
		array_push($candidate_tours,$row);
		array_push($candidate_res_ids,$row['reservation']);
	}
	//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($candidate_res_ids,true)).'</PRE>';

	//GET ALL LODGING EXTENSIONS FOR CANDIDATE TOURS
	$ext_lodging = array();
	$query = 'SELECT `reservation`, `date`, `nights`
		FROM `reservations_assoc`
		WHERE `extlodging` = 1 AND `canceled` < 1
		AND `reservation` IN ('.implode(',',$candidate_res_ids).')';
	$result = @mysql_query($query);
	$thiserror = @mysql_error(); if($thiserror != ""){ $debug .= 'Error with extended lodging query: '.$thiserror."\n"; }
	while($row = @mysql_fetch_assoc($result)){
		array_push($ext_lodging,$row);
	}
	//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($ext_lodging,true)).'</PRE>';

	//COLLECT TOUR SEATS
	$query = 'SELECT reservations_assoc.`reservation`, reservations_assoc.`tourid`, routes_dates.`routeid`, routes_dates.`date`, (reservations_assoc.`adults`+reservations_assoc.`seniors`+reservations_assoc.`children`) AS `guests`
		FROM `reservations_assoc`,`tours_assoc`,`tours_dates`,`routes_dates`
		WHERE reservations_assoc.`canceled` = 0
			AND (reservations_assoc.`type` = "t" OR reservations_assoc.`type` = "o")
			AND reservations_assoc.`tourid` = tours_assoc.`tourid`
			AND tours_dates.`tourid` = tours_assoc.`tourid`
			AND tours_assoc.`dir` = tours_dates.`dir`
			AND tours_assoc.`type` = "r"
			AND routes_dates.`routeid` = tours_assoc.`typeid`
			AND reservations_assoc.`reservation` IN ('.implode(',',$candidate_res_ids).')'."\n";
			if(is_array($routes)){
				$query .= '			AND routes_dates.`routeid` IN ('.implode(',',$routes).')'."\n";
			}
			
			//No longer need to limit dates, instead we need all potential dates for candidate tours
			//if(is_array($start) && count($start) > 0){
			//	$query .= ' AND (routes_dates.`date` = "'.implode('" OR routes_dates.`date` = "',$start).'")';
			//} else {
			//	if(is_numeric($start)){ $query .= ' AND routes_dates.`date` >= '.$start; }
			//	if(is_numeric($end)){ $query .= ' AND routes_dates.`date` <= '.$end; }
			//}

			$query .= ' AND tours_dates.`date` = reservations_assoc.`date`';
			$query .= ' AND tours_dates.`date` >= (routes_dates.`date` - ((tours_assoc.`day`-1)*86400) - 3600)';
			$query .= ' AND tours_dates.`date` <= (routes_dates.`date` - ((tours_assoc.`day`-1)*86400) + 3600)';

		//$query .= ' GROUP BY routes_dates.`routeid`, routes_dates.`date` ORDER BY NULL';
		$debug .= 'Starting on tour seats...'."\n";
		$debug .= $query."\n\n";
	$result = @mysql_query($query);
	$thiserror = @mysql_error(); if($thiserror != ""){ $debug .= 'Error with route seats query: '.$thiserror."\n"; }
	$num_results = @mysql_num_rows($result);
	$debug .= 'Found '.$num_results.' route/date sets.'."\n";
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($row,true)).'</PRE>';

		//Compensate for extensions
		foreach($ext_lodging as $ext){
			if($ext['reservation'] == $row['reservation'] && $ext['date'] <= $row['date']){
				$debug .= '	Lodging extension before this route runs, moving '.$row['guests'].' pax from '.date("n/j/Y",$row['date']);
				$row['date'] = strtotime('+ '.$ext['nights'].' days',$row['date']);
				$debug .= ' to '.date("n/j/Y",$row['date'])."\n";
			}
		}

		//Don't include dates outside of specified range
		$add = false;
		if(is_array($start) && count($start) > 0 && in_array($row['date'],$start)){
			$add = true;
		} elseif(is_numeric($start) && is_numeric($end) && $row['date'] >= $start && $row['date'] <= $end){
			$add = true;
		} elseif(is_numeric($start) && !is_numeric($end) && $row['date'] >= $start){
			$add = true;
		} elseif(!is_numeric($start) && is_numeric($end) && $row['date'] <= $end){
			$add = true;
		} elseif(!is_numeric($start) && !is_numeric($end)){
			$add = true;
		}

		if($add){
			if(!isset($blocked['r'.$row['routeid']])){ $blocked['r'.$row['routeid']] = array(); }
			if(!isset($blocked['r'.$row['routeid']]['d'.$row['date']])){
				$blocked['r'.$row['routeid']]['d'.$row['date']] = array('route'=>$row['routeid'],'date'=>$row['date'],'tour_seats'=>0,'route_seats'=>0,'reservations'=>array());
				}
				$debug .= "\t".'Adding from '.$row['reservation'].' - Route: '.$row['routeid'].' Date: '.date("n/j/Y G",$row['date']).' Tour Seats: '.$row['guests']."\n";
			$blocked['r'.$row['routeid']]['d'.$row['date']]['tour_seats'] = ($blocked['r'.$row['routeid']]['d'.$row['date']]['tour_seats']+$row['guests']);
			array_push($blocked['r'.$row['routeid']]['d'.$row['date']]['reservations'],$row['reservation']);
		}
	}
		$debug .= "\n";

	//COLLECT ROUTE SEATS
	$query = 'SELECT routes_dates.`routeid`, routes_dates.`date`, SUM((reservations_assoc.`adults`+reservations_assoc.`seniors`+reservations_assoc.`children`)) AS `guests`';
		$query .= ' FROM `reservations_assoc`,`routes_dates`';
		$query .= ' WHERE reservations_assoc.`canceled` = 0';
			$query .= ' AND (reservations_assoc.`type` = "r" OR reservations_assoc.`type` = "o")';
			$query .= ' AND reservations_assoc.`date` = routes_dates.`date`';
			$query .= ' AND reservations_assoc.`routeid` = routes_dates.`routeid`';
			if(is_array($routes)){ $query .= ' AND (routes_dates.`routeid` = "'.implode('" OR routes_dates.`routeid` = "',$routes).'")'; }
			if(is_array($start) && count($start) > 0){
				$query .= ' AND (routes_dates.`date` = "'.implode('" OR routes_dates.`date` = "',$start).'")';
				} else {
				if(is_numeric($start)){ $query .= ' AND routes_dates.`date` >= '.$start; }
				if(is_numeric($end)){ $query .= ' AND routes_dates.`date` <= '.$end; }
				}
		$query .= ' GROUP BY routes_dates.`routeid`, routes_dates.`date` ORDER BY NULL';
		$debug .= 'Starting on route seats...'."\n";
		//$debug .= $query."\n\n";
	$result = @mysql_query($query);
	$thiserror = @mysql_error(); if($thiserror != ""){ $debug .= 'Error with route seats query: '.$thiserror."\n"; }
	$num_results = @mysql_num_rows($result);
	$debug .= 'Found '.$num_results.' route/date sets.'."\n";
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		if(!isset($blocked['r'.$row['routeid']])){ $blocked['r'.$row['routeid']] = array(); }
		if(!isset($blocked['r'.$row['routeid']]['d'.$row['date']])){
			$blocked['r'.$row['routeid']]['d'.$row['date']] = array('route'=>$row['routeid'],'date'=>$row['date'],'tour_seats'=>0,'route_seats'=>0);
			}
			$debug .= "\t".'Adding - Route: '.$row['routeid'].' Date: '.date("n/j/Y G",$row['date']).' Route Seats: '.$row['guests']."\n";
		$blocked['r'.$row['routeid']]['d'.$row['date']]['route_seats'] = ($blocked['r'.$row['routeid']]['d'.$row['date']]['route_seats']+$row['guests']);
	}
	//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($blocked,true)).'</PRE>';

	if($showdebug == "y"){ echo '<PRE>'.$debug.'</PRE>'; }
	return $blocked;

} //END getblocked()
*/


function getblocked_tours($tours='*',$start='*',$end='*',$showdebug='n'){

$blocked = array();
$debug = '';

	//PROCESS VARIABLES
	if($tours != '*' && !is_array($tours)){
		$tours = str_replace('|',',',$tours);
		$tours = explode(',',$tours);
		}
	if(is_array($start) || strpos($start,'|') !== false || strpos($start,',') !== false){
		if(!is_array($start)){
			$start = str_replace('|',',',$start);
			$start = explode(',',start);
			}
		sort($start,SORT_NUMERIC);
		}
	$debug .= 'Tours: '.print_r($tours,TRUE)."\n";
		$debug .= 'Start: '; if(is_numeric($start)){ $debug .= date("n/j/Y G",$start); } else { $debug .= $start; } $debug .= "\n";
		$debug .= 'End: '; if(is_numeric($end)){ $debug .= date("n/j/Y G",$end); } else { $debug .= $end; } $debug .= "\n";
		$debug .= "\n";

	//COLLECT TOUR SEATS
	$query = 'SELECT reservations_assoc.`tourid`,
		GROUP_CONCAT(DISTINCT reservations_assoc.`reservation` ORDER BY reservations_assoc.`reservation` ASC SEPARATOR ",") AS `reservations`,
		reservations_assoc.`date`, SUM((reservations_assoc.`adults`+reservations_assoc.`seniors`+reservations_assoc.`children`)) AS `guests`';
		$query .= ' FROM `reservations_assoc`';
		$query .= ' WHERE reservations_assoc.`canceled` = 0';
			$query .= ' AND (reservations_assoc.`type` = "t" OR reservations_assoc.`type` = "o")';
			$query .= ' AND reservations_assoc.`tourid` > 0';
			if(is_array($tours)){ $query .= ' AND (reservations_assoc.`tourid` = "'.implode('" OR reservations_assoc.`tourid` = "',$tours).'")'; }

			if(is_array($start) && count($start) > 0){
				$query .= ' AND (reservations_assoc.`date` = "'.implode('" OR reservations_assoc.`date` = "',$start).'")';
				} else {
				if(is_numeric($start)){ $query .= ' AND reservations_assoc.`date` >= '.$start; }
				if(is_numeric($end)){ $query .= ' AND reservations_assoc.`date` <= '.$end; }
				}

		$query .= ' GROUP BY reservations_assoc.`tourid`, reservations_assoc.`date` ORDER BY NULL';
		$debug .= 'Finding tour seats...'."\n";
		//$debug .= $query."\n\n";
	$result = @mysql_query($query);
	$thiserror = @mysql_error(); if($thiserror != ""){ $debug .= 'Error with tour seats query: '.$thiserror."\n"; }
	$num_results = @mysql_num_rows($result);
	$debug .= 'Found '.$num_results.' tour/date sets.'."\n";
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		if(!isset($blocked['t'.$row['tourid']])){ $blocked['t'.$row['tourid']] = array(); }
		if(!isset($blocked['t'.$row['tourid']]['d'.$row['date']])){
			$debug .= "\t".'Adding - Tour: '.$row['tourid'].' Date: '.date("n/j/Y G",$row['date']).' Blocked: '.$row['guests']."\n";
			$blocked['t'.$row['tourid']]['d'.$row['date']] = array(
				'tour'=>$row['tourid'],
				'date'=>$row['date'],
				'blocked'=>$row['guests'],
				'reservations'=> explode(',', $row['reservations'])
				);
			}
		}
		$debug .= "\n";

	if($showdebug == "y"){ echo '<PRE>'.$debug.'</PRE>'; }
	return $blocked;

} //END getblocked_tours()


if(isset($_REQUEST['getblocked_routes'])){

	@date_default_timezone_set('America/Denver');
	$time = time();

	if(!@$db){ $db = @MYSQL_CONNECT("localhost","sessel_bbsite","?R&zTQ=UdFX$"); }
	@mysql_select_db("sessel_bundubashers");
	@mysql_query("SET NAMES utf8");

	foreach($_REQUEST as $key => $row){
		$_REQUEST[$key] = trim($_REQUEST[$key]);
		$_REQUEST[$key] = strip_tags($_REQUEST[$key]);
	}
	
	if(!isset($_REQUEST['getblocked_start']) || trim($_REQUEST['getblocked_start']) == ""){ $_REQUEST['getblocked_start'] = '*'; }
	if(!isset($_REQUEST['getblocked_end']) || trim($_REQUEST['getblocked_end']) == ""){ $_REQUEST['getblocked_end'] = '*'; }
	if(!isset($_REQUEST['getblocked_debug']) || trim($_REQUEST['getblocked_debug']) == ""){ $_REQUEST['getblocked_debug'] = 'n'; }

	$blocked = getblocked($_REQUEST['getblocked_routes'],$_REQUEST['getblocked_start'],$_REQUEST['getblocked_end'],$_REQUEST['getblocked_debug']);
		//echo '<PRE>'; print_r($blocked); echo '</PRE>';

	echo '||BEGIN||'."\n";
	foreach($blocked as $key => $route){
		foreach($route as $thisdate){
			echo date("Y",$thisdate['date']).'|'.date("n",$thisdate['date']).'|'.date("j",$thisdate['date']).'|'.$thisdate['route'].'|'.$thisdate['tour_seats'].'|'.$thisdate['route_seats']."\n";
		}
	}
	echo '||END||'."\n";

} //End ID if statement

?>