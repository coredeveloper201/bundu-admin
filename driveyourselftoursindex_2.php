<? include_once('header.php'); ?>








<div align="center">
	<table border="0" width="94%" cellspacing="0" cellpadding="8">
		<tr>
			<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
				<tr>
					<td colspan="3">
					<p align="center"><b>
					<font face="Matisse ITC" color="#FF0000" style="font-size: 20pt">Drive 
					Yourself Tours</font><font size="2" face="Arial"><br>
					Order Here</font></b></td>
				</tr>
				<tr>
					<td width="21%" valign="top">
					<p align="center">
					<img border="0" src="drive%20yourself%203a.gif" width="149" height="120" lowsrc="Drive%20yourself%20Monument%20Valley%20tours" alt="Drive yourself Grand Canyon tours"></td>
					<td width="58%" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
				<tr>
					<td><font face="Arial" size="2">
					<img border="0" src="drive_yourself_grand-canyon_tours.gif" width="12" height="12" lowsrc="Drive%20yourself%20Grand%20Canyon%20tours" alt="Drive yourself tours"> 
					</font><font face="Arial">
					Many thousands of people have taken one of our
					<a target="_blank" href="http://www.bundubashers.com/listing.php?id=36&title=All+Grand+Canyon+tours">
					package tours</a> to the great sights of the southwestern 
					United States</font></td>
				</tr>
				<tr>
					<td><font face="Arial" size="2">
					<img border="0" src="drive_yourself_grand-canyon_tours.gif" width="12" height="12" lowsrc="Drive%20yourself%20Grand%20Canyon%20tours" alt="Drive yourself tours"> 
					</font><font face="Arial">
					Please see what some of them have
					<a target="_blank" href="http://www.bundubashers.com/references_grand_canyon_tours.php">
					said about us</a></font></td>
				</tr>
				<tr>
					<td><font face="Arial" size="2">
					<img border="0" src="drive_yourself_grand-canyon_tours.gif" width="12" height="12" lowsrc="Drive%20yourself%20Grand%20Canyon%20tours" alt="Drive yourself tours"> 
					</font><font face="Arial">
					Now you can use our knowledge and skills for your own drive 
					yourself tour to the Grand Canyon and much more!</font></td>
				</tr>
			</table>
					</td>
					<td width="20%" valign="top">
					<p align="center">
					<font face="Matisse ITC" color="#CC3300" size="4">A division 
					of </font>
					<img border="0" src="Bundu_Bashers_logo_drive_yourself2.jpg" width="148" height="97" lowsrc="Drive%20yourself%20tours" alt="Drive yourself Grand Canyon tours"></td>
				</tr>
				<tr>
					<td colspan="3">
					<p align="center"><font face="Matisse ITC" color="#CC3300">
					How It Works</font></td>
				</tr>
				<tr>
					<td colspan="3"><font face="Arial" size="2">It's pretty 
					simple actually.&nbsp; No one knows as much about the Grand 
					Canyon, Monument Valley, Bryce Canyon and the rest of the 
					southwest as we do.&nbsp; We'll plan and organize your 
					vacation for you and all you have to worry about is having a 
					good time!</font><div align="center">
						<table border="0" width="100%" cellspacing="0" cellpadding="2">
							<tr>
								<td colspan="2">
								<p align="center">
								<font face="Matisse ITC" color="#CC3300">Why It 
								Works!&nbsp;&nbsp; We ...</font></td>
							</tr>
							<tr>
								<td width="44%">
								<img border="0" src="drive_yourself_grand_canyon_tours.gif" width="11" height="11" lowsrc="Drive%20yourself%20tour" alt="Drive yourself Grand Canyon tours"><font face="Arial" size="2"> 
								Design the best routes for you to take </font>
								</td>
								<td width="55%">
								<img border="0" src="drive_yourself_grand_canyon_tours.gif" width="11" height="11" lowsrc="Drive%20yourself%20tour" alt="Drive yourself Grand Canyon tours"><font face="Arial" size="2"> 
								Use our group discounts to get you killer rates 
								at hotels ..</font></td>
							</tr>
							<tr>
								<td width="44%">
								<img border="0" src="drive_yourself_grand_canyon_tours.gif" width="11" height="11" lowsrc="Drive%20yourself%20tour" alt="Drive yourself Grand Canyon tours"><font face="Arial" size="2"> 
								Clue you in on locals' secrets at the parks</font></td>
								<td width="55%">
								<img border="0" src="drive_yourself_grand_canyon_tours.gif" width="11" height="11" lowsrc="Drive%20yourself%20tour" alt="Drive yourself Grand Canyon tours"><font face="Arial" size="2"> 
								Use our group discounts to get you discount 
								rates on activities</font></td>
							</tr>
							<tr>
								<td width="44%">
								<img border="0" src="drive_yourself_grand_canyon_tours.gif" width="11" height="11" lowsrc="Drive%20yourself%20tour" alt="Drive yourself Grand Canyon tours"><font face="Arial" size="2"> 
								Advise which companies to use for tours &amp; 
								activities</font></td>
								<td width="55%">
								<img border="0" src="drive_yourself_grand_canyon_tours.gif" width="11" height="11" lowsrc="Drive%20yourself%20tour" alt="Drive yourself Grand Canyon tours"><font face="Arial" size="2"> 
								Show you how to find spots you'd never find on 
								your own</font></td>
							</tr>
							<tr>
								<td width="44%">
								<img border="0" src="drive_yourself_grand_canyon_tours.gif" width="11" height="11" lowsrc="Drive%20yourself%20tour" alt="Drive yourself Grand Canyon tours"><font face="Arial" size="2"> 
								Explain where the locals like to eat and play</font></td>
								<td width="55%"><font size="2" face="Arial">
								<img border="0" src="drive_yourself_grand_canyon_tours.gif" width="11" height="11" lowsrc="Drive%20yourself%20tour" alt="Drive yourself Grand Canyon tours"> 
								Generally ensure that we do the work and you 
								have the fun!</font></td>
							</tr>
							<tr>
								<td width="99%" colspan="2">
								<p align="center">
								<font face="Matisse ITC" color="#CC3300">What It 
								Costs ...</font></td>
							</tr>
							<tr>
								<td width="99%" colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td width="99%" colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td width="99%" colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td width="99%" colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td width="99%" colspan="2">
								<p align="center">
								<font face="Matisse ITC" color="#CC3300">It's As 
								Easy As 1 ... 2 ... 3...</font></td>
							</tr>
						</table>
					</div>
					</td>
				</tr>
				<tr>
					<td colspan="3" height="626" valign="top">
					<table border="0" width="100%" cellspacing="0" cellpadding="2">
						<tr>
							<td width="28" align="center" valign="top">
							<img border="1" src="drive%20yourself%20grand%20canyon%20tours%201.jpg" width="80" height="80"></td>
							<td valign="top"><font face="Arial" size="2">Use the 
							form below to tell us how many people there are, 
							where you want to go, what you want to do, how 
							active you are, what sort of activities may be on 
							interest and how many days you want to spend on your 
							tour.&nbsp; If you're not sure, please take a moment 
							to look at some of our
							<a target="_blank" href="http://www.bundubashers.com/">
							tours</a>, especially the
							<a target="_blank" href="http://www.bundubashers.com/tour.php?id=grand canyon tours">
							six day Grand Canyon, Yellowstone and more tour</a>.&nbsp; 
							This will give you a good idea of some of your 
							options</font></td>
						</tr>
						<tr>
							<td width="28" align="center" valign="top">
							<img border="1" src="drive%20yourself%20grand%20canyon%20tours%202.jpg" width="80" height="80" lowsrc="Drive%20yourself%20Yellowstone%20tours" alt="Drive yourself Grand Canyon and Yellowstone tours"></td>
							<td valign="top"><font face="Arial" size="2">We'll 
							design an itinerary etc</font></td>
						</tr>
						<tr>
							<td width="28" align="center" valign="top">
							<img border="1" src="drive%20yourself%20grand%20canyon%20tours%203.jpg" width="80" height="80" lowsrc="Grand%20Canyon%20self%20drive%20tours" alt="Grand Canyon and more self drive tours"></td>
							<td valign="top"><font face="Arial" size="2">We'll 
							send it back to you.</font></td>
						</tr>
						<tr>
							<td width="28" align="center">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
					<p><font face="Arial" size="2">. </font></td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</div>










<? include('footer.php'); ?>