<?php

class block {
	var $id;
	var $tableName = 'blocks';
	var $data = array();


	function __construct($params=array()){
		if(!is_array($params)){
			$params = array('id'=>$params);
		}
		if($params['id'] != ""){
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)){
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getIDreservationAssoc(){
		return $this->data['id_reservation_assoc'];
	}

	function getIDroute(){
		return $this->data['id_route'];
	}

	function getIDtour(){
		return $this->data['id_tour'];
	}

	function getDate(){
		return $this->data['date'];
	}

	function getTime(){
		return $this->data['time'];
	}

	function getSeats(){
		return $this->data['seats'];
	}


	function setIDreservationAssoc($input){
		$this->data['id_reservation_assoc'] = $input;
	}

	function setIDroute($input){
		$this->data['id_route'] = $input;
	}

	function setIDtour($input){
		$this->data['id_tour'] = $input;
	}

	function setDate($input){
		$this->data['date'] = mysqlDate($input);
	}

	function setTime($input){
		$this->data['time'] = mysqlTime($input);
	}

	function setSeats($input){
		$this->data['seats'] = $input;
	}


	function dataChecks(){
		if($this->data['datetime_created'] == ""){
			$this->data['datetime_created'] = mysqlDateTime();
		}
	}

	function save(){
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete(){
		object_delete($this->tableName, $this->id);
	}
}

?>