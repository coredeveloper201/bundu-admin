<?php

class tour {
	var $id;
	var $tableName = 'tours';

	var $data = array();
	var $steps = false;

	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.encodeSQL($params['id']).'" LIMIT 1';
		}
		if(isset($query)) {
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getData($fieldName) {
		return $this->data[$fieldName];
	}

	function getTitle() {
		return $this->data['title'];
	}

	function getNickname() {
		return $this->data['nickname'];
	}

	function getNumDays() {
		return $this->data['numdays'];
	}

	function getPricePerGuest() {
		return $this->data['perguest'];
	}

	function getPriceSingle() {
		return $this->data['single'];
	}

	function getPriceTriple() {
		return $this->data['triple'];
	}

	function getPriceQuad() {
		return $this->data['quad'];
	}

	function getExtPricing() {
		return $this->data['ext_pricing'];
	}

	function getExtPricingArry() {
		$out = array();
		$ext_pricing = explode('|', $this->getExtPricing());
		foreach($ext_pricing as $val) {
			$val = explode(':',$val);
			$out[$val[0]] = $val[1];
		}
		return $out;
	}

	function getFuelSurcharge() {
		return $this->data['fuelsurcharge'];
	}

	function getTwoBedSurcharge() {
		return $this->data['twobed_surcharge'];
	}

	function getAgentTierPercent($tier=0) {
		return @$this->data['agent'.$tier.'_per'];
	}

	function getAgent1Percent() {
		return $this->data['agent1_per'];
	}

	function getAgent2Percent() {
		return $this->data['agent2_per'];
	}

	function getIDvendor() {
		return $this->data['vendor'];
	}


	function setData($fieldName, $input) {
		$this->data[$fieldName] = $input;
	}
	
	function get_steps($params=array()) {
		if($this->steps === false) {
			$this->steps = array(
				'f' => array(),
				'r' => array()
				);
			$query = 'SELECT * FROM `tours_assoc`
						WHERE tourid = "'.$this->id.'"
							'.(@$params['type']!=""?'AND type = "'.encodeSQL($params['type']).'"':'').'
						ORDER BY `dir` ASC, `day` ASC, `order` ASC';
			$result = mysql_query($query);
			while($row = @mysql_fetch_assoc($result)) {
				array_push($this->steps[$row['dir']], $row);
			}
		}
		return $this->steps;
	}

	function get_routes($params=array()) {
		if($params['returnKind'] == "") {
			$params['returnKind'] = 'assoc';
		}

		$routes = array();
		$query = 'SELECT * FROM `tours_assoc` WHERE tourid = "'.$this->id.'" AND type = "r"';
			if($params['direction'] != "") {
				$query .= ' AND dir = "'.encodeSQL($params['direction']).'"';
			}
			$query .= ' ORDER BY `dir` ASC, `day` ASC, `order` ASC';
		$result = mysql_query($query);
		while($row = @mysql_fetch_assoc($result)) {
			if($params['returnKind'] == "ids") {
				$routes[$row['dir']][] = $row['typeid'];

			} elseif($params['returnKind'] == "objects") {
				$routes[$row['dir']][] = new route($row['typeid']);

			} else {
				$routes[$row['dir']][] = $row;
			}
		}

		if(@$params['dir'] != "") {
			return $routes[$params['dir']];
		}
		return $routes;
	}


	function dataChecks() {
		//None... yet
	}

	function save() {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete() {
		if($this->id > 0) {
			$delete = 'DELETE FROM '.$this->tableName.' WHERE id = "'.$this->id.'" LIMIT 1';
			$result = mysql_query($delete);
		}
	}
}

?>