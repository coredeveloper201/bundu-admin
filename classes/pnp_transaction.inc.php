<?php

class pnp_transaction {
	var $id;
	var $tableName = 'pnp_transactions';
	var $data = array();
        var $publisherName;

	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)) {
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getIDreservation() {
		return $this->data['id_reservation'];
	}

	function getCCname() {
		return $this->data['cc_name'];
	}

	function getCCnumber() {
		return $this->data['cc_num'];
	}

	function getCCexpDate() {
		return $this->data['cc_expdate'];
	}

	function getCCsecurityCode() {
		return $this->data['cc_scode'];
	}

	function getPNPorderID() {
		return $this->data['pnp_orderid'];
	}

	function getEmail() {
		return $this->data['email'];
	}

	function getAuthType() {
		return $this->data['auth_type'];
	}

	function getAmount() {
		return $this->data['amount'];
	}

	function getConfDetails() {
		return $this->data['conf_details'];
	}


	function setIDreservation($input) {
		$this->data['id_reservation'] = $input;
	}

	function setCCname($input) {
		$this->data['cc_name'] = $input;
	}

	function setCCnumber($input) {
		$this->data['cc_num'] = $input;
	}

	function setCCexpDate($input) {
		$this->data['cc_expdate'] = $input;
	}

	function setCCsecurityCode($input) {
		$this->data['cc_scode'] = $input;
	}

	function setPNPorderID($input) {
		$this->data['pnp_orderid'] = $input;
	}

	function setAuthCode($input) {
		$this->data['auth_code'] = $input;
	}

	function setErrorMessage($input) {
		$this->data['error_msg'] = $input;
	}

	function setEmail($input) {
		$this->data['email'] = $input;
	}

	function setAuthType($input) {
		$this->data['auth_type'] = $input;
	}

	function setAmount($input) {
		$this->data['amount'] = $input;
	}

	function setFinalStatus($input) {
		$this->data['final_status'] = $input;
	}

	function setConfDetails($input) {
		$this->data['conf_details'] = $input;
	}

	function setDateTimeRun($input) {
		$this->data['datetime_run'] = mysqlDateTime($input);
	}
        
        function setPublisherName($input) {
		$this->publisherName = $input;
	}
        
        function getPublisherName() {
		return $this->publisherName;
	}


	function runTransaction() {
		$pnp_post_url = "https://pay1.plugnpay.com/payment/pnpremote.cgi";

		$post = array();
		
                $publisher_name = $this->getPublisherName();
                
                switch($publisher_name) {
                    case 'utahtransp':
                        $post['publisher-name'] = 'utahtransp';
                        $post['publisher-password'] = '1q2w3e4r5t';
                        break;
                    case 'yellowston2':
                        $post['publisher-name'] = 'yellowston2';
                        $post['publisher-password'] = 'c0872a2abd8be31b';
                        break;
                    default:
                        $post['publisher-name'] = 'utahtransp';
                        $post['publisher-password'] = '1q2w3e4r5t';
                }
		
		$post['card-amount'] = $this->getAmount();
		if($this->getAuthType() == "cc_prevcharge") {
			$post['mode'] = "authprev";
			$post['reauthtype'] = "authpostauth";
			$post['prevorderid'] = $this->getPNPorderID();

		} elseif($this->getAuthType() == "cc_refund") {
			$post['mode'] = "return";
			$post['orderID'] = $this->getPNPorderID();

		} elseif($this->getAuthType() == "cc_refund6mo") {
			$post['mode'] = "returnprev";
			$post['prevorderid'] = $this->getPNPorderID();

		} else {
			$post['mode'] = "auth";
			$post['authtype'] = "authpostauth";
			$post['card-name'] = $this->getCCname();
			$post['card-number'] = $this->getCCnumber();
			$post['card-exp'] = $this->getCCexpDate();
			$post['card-cvv'] = $this->getCCsecurityCode();
			$post['email'] = $this->getEmail();
			$post['ipaddress'] = $this->getEmail();
		}

		$post_string = http_build_query($post);
		//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($post,true)).'</PRE>'; echo $post_string; exit;

		$pnp_ch = curl_init($pnp_post_url);
		//curl_setopt($pnp_ch, CURLOPT_INTERFACE, '198.57.241.81');
		curl_setopt($pnp_ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($pnp_ch, CURLOPT_POSTFIELDS, $post_string);
		#curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  // Upon problem, uncomment for additional Windows 2003 compatibility

		$pnp_result_page = curl_exec($pnp_ch);
		$pnp_result_decoded = urldecode($pnp_result_page);

		// decode the result page and put it into transaction_array
		$pnp_transaction_array = array();
		$pnp_temp_array = explode('&', $pnp_result_decoded);
		foreach($pnp_temp_array as $entry) {
			list($name, $value) = explode('=', $entry);
			$pnp_transaction_array[$name] = $value;
		}

		$error_msg = trim($pnp_transaction_array['resp-code'].' '.$pnp_transaction_array['MErrMsg'].' '.$pnp_transaction_array['Errdetails'].' '.$pnp_transaction_array['aux-msg']);

		$datetime_transaction = time();

		$cc_num_masked = substr($post['card-number'], 0, 4).'**'.substr($post['card-number'], -2);

		$conf_details = '';
		$conf_details .= 'api '.$post['mode'];
		if(@$post['card-name'] != "") {
			$conf_details .= ' '.$post['card-name'];
		}
		$conf_details .= ' '.$pnp_transaction_array['FinalStatus'];
		$conf_details .= ' '.$pnp_transaction_array['orderID'];
		$conf_details .= ' '.date("n/d/Y", $datetime_transaction).' '.date("H:i:s", $datetime_transaction);
		if(@$post['card-number'] != "") {
			$conf_details .= ' '.$cc_num_masked;
		}
		if(@$post['card-exp'] != "") {
			$conf_details .= ' '.$post['card-exp'];
		}
		$conf_details .= ' usd '.$post['card-amount'].' '.$pnp_transaction_array['auth-code'];

		$this->setFinalStatus($pnp_transaction_array['FinalStatus']);
		$this->setPNPorderID($pnp_transaction_array['orderID']);
		$this->setAuthCode($pnp_transaction_array['auth-code']);
		$this->setErrorMessage($error_msg);
		$this->setDateTimeRun($datetime_transaction);
		$this->setConfDetails($conf_details);
		$this->setCCnumber($cc_num_masked);
		$this->setCCsecurityCode(null);
		$this->save();

		log_audit(array(
			'primary_id' => $this->id,
			'table' => $this->tableName,
			'data' => $pnp_result_page,
			'comment' => 'curl_result'
			));

		$pnp_transaction_array['conf_string'] = $conf_details;

		return $pnp_transaction_array;
	}


	function dataChecks() {
		if($this->id == "" && $this->data['datetime_created'] == "") {
			$this->data['datetime_created'] = mysqlDateTime();
		}
		if($this->id == "" && $this->data['id_creator'] == "") {
			$this->data['id_creator'] = @$_SESSION['valid_user']['id'];
		}
	}

	function save() {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete() {
		object_delete($this->tableName, $this->id);
	}
}

?>