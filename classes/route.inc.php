<?php

// route.inc.php

class route {
	var $id;
	var $data = array();
	var $tableName = 'routes';

	var $depLocationObj = false;
	var $arrLocationObj = false;

	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)) {
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getName() {
		return $this->data['name'];
	}

	function getNumSeats() {
		return $this->data['seats'];
	}

	function getMinimum() {
		return $this->data['minimum'];
	}

	function getIDdepLocation() {
		return $this->data['dep_loc'];
	}
	function getDepLocation() {
		return $this->getIDdepLocation();
	}

	function getDepLocationName() {
		$this->setupDepLoctionObj();
		return $this->depLocationObj->getName();
	}

	function getDepTimezone() {
		$this->setupDepLoctionObj();
		return $this->depLocationObj->getTimezone();
	}

	function getDepTime() {
		$this->syncDates();
		return $this->data['dep_time_v2'];
	}

	function getIDarrLocation() {
		return $this->data['arr_loc'];
	}
	function getArrLocation() {
		return $this->getIDarrLocation();
	}

	function getArrLocationName() {
		$this->setupArrLoctionObj();
		return $this->arrLocationObj->getName();
	}

	function getArrTimezone() {
		$this->setupArrLoctionObj();
		return $this->arrLocationObj->getTimezone();
	}

	function getArrTime() {
		$this->syncDates();
		return $this->data['arr_time_v2'];
	}

	function getTravelTime() {
		return $this->data['travel_time'];
	}

	function getAnchor() {
		return $this->data['anchor'];
	}

	function getMiles() {
		return $this->data['miles'];
	}

	function getDescription() {
		return $this->data['description'];
	}

	function getDetails() {
		return $this->data['details'];
	}

	function getBBprice() {
		return $this->data['bbprice'];
	}

	function getAPIinclude() {
		return $this->data['bbincl'];
	}

	function getIDvendor() {
		return $this->data['vendor'];
	}


	function setupDepLoctionObj() {
		if($this->depLocationObj === false) {
			$this->depLocationObj = new location($this->getIDdepLocation());
		}
	}

	function setupArrLoctionObj() {
		if($this->arrLocationObj === false) {
			$this->arrLocationObj = new location($this->getIDarrLocation());
		}
	}

	function usedby_tours($params=array()) {
		$tourIDs = array();

		if($this->id > 0) {
			$query = 'SELECT DISTINCT tours_assoc.tourid AS id_tour
				FROM tours_assoc
				JOIN tours ON tours_assoc.tourid = tours.id
				WHERE type = "r" AND typeid = "'.$this->id.'"';
					if($params['direction'] != "") {
						$query .= ' AND tours_assoc.dir = "'.mysql_real_escape_string($params['direction']).'"'."\n";
					}
					$query .= "\n";
				$query .= '	ORDER BY tours_assoc.tourid ASC';
			$result = @mysql_query($query);
			while($row = @mysql_fetch_assoc($result)) {
				$tourIDs[] = $row['id_tour'];
			}
		}

		if($params['return_type'] == "objects") {
			$objs = array();
			foreach($tourIDs as $id) {
				$objs[] = new tour($id);
			}
			return $objs;

		} else {
			return $tourIDs;
		}
	}


	function syncDates() {
		//Make sure both old and new date fields are accurate
		if($this->data['dep_time'] > -1 && $this->data['dep_time_v2'] == "") {
			$this->data['dep_time_v2'] = mysqlTime(strtotime('2010-1-1') + $this->data['dep_time']);
		}
		if($this->data['dep_time'] <= 0 && $this->data['dep_time_v2'] != "") {
			$this->data['dep_time'] = strtotime($this->data['dep_time_v2']);
		}
		if($this->data['dep_time'] == "" && $this->data['dep_time_v2'] == "") {
			$this->data['dep_time'] = 0;
		}

		if($this->data['arr_time'] > -1 && $this->data['arr_time_v2'] == "") {
			$this->data['arr_time_v2'] = mysqlTime(strtotime('2010-1-1') + $this->data['arr_time']);
		}
		if($this->data['arr_time'] <= 0 && $this->data['arr_time_v2'] != "") {
			$this->data['arr_time'] = strtotime($this->data['arr_time_v2']);
		}
		if($this->data['arr_time'] == "" && $this->data['arr_time_v2'] == "") {
			$this->data['arr_time'] = 0;
		}	
	}

	function dataChecks() {
		$this->syncDates();
	}

	function save() {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete() {
		if($this->id > 0) {
			$delete = 'DELETE FROM '.$this->tableName.' WHERE id = "'.$this->id.'" LIMIT 1';
			$result = mysql_query($delete);
		}
	}
}

?>