<?php

class subagent {
	var $id;
	var $data = array();
	var $tableName = 'subagents';

	function __construct($params=array()){
		if(!is_array($params)){
			$params = array('id'=>$params);
		}
		if($params['id'] != ""){
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)){
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getIDagent(){
		return $this->data['id_agent'];
	}

	function getName(){
		return $this->data['name'];
	}

	function getUser(){
		return $this->data['user'];
	}

	function getPass(){
		return $this->data['pass'];
	}


	function setIDagent($input){
		$this->data['id_agent'] = $input;
	}

	function setName($input){
		$this->data['name'] = $input;
	}

	function setUser($input){
		$this->data['user'] = $input;
	}

	function setPass($input){
		$input = trim($input);
		$input = md5($input);
		$this->data['pass'] = $input;
	}


	function dataChecks(){
		if($this->id == "" && $this->data['id_author'] == ""){
			$this->data['id_author'] = @$_SESSION['valid_user']['id'];
		}
		if($this->id == "" && $this->data['datetime_created'] == ""){
			$this->data['datetime_created'] = mysqlDateTime();
		}
	}

	function save(){
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete(){
		if($this->id > 0){
			$delete = 'DELETE FROM '.$this->tableName.' WHERE id = "'.$this->id.'" LIMIT 1';
			$result = mysql_query($delete);
		}
	}
}

?>