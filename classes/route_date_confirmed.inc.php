<?php

// route_date_confirmed.inc.php

class route_date_confirmed {
	var $id;
	var $data = array();
	var $tableName = 'route_dates_confirmed';

	function __construct($params=array()){
		if(!is_array($params)){
			$params = array('id'=>$params);
		}
		if($params['id'] != ""){
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';

		} elseif($params['id_route'] != "" && $params['date'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.'
						WHERE routeid = "'.mysql_real_escape_string($params['id_route']).'"
							AND date_v2 = "'.mysqlDate($params['date']).'"
						LIMIT 1';
		}
		if(isset($query)) {
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getIDroute(){
		return $this->data['routeid'];
	}

	function getDate(){
		$this->syncDates();
		return $this->data['date_v2'];
	}

	function getIDtourDate(){
		return $this->data['id_tour_date'];
	}


	function setIDroute($input){
		$this->data['routeid'] = $input;
	}

	function setDate($input){
		$this->data['date_v2'] = $input;
		$this->data['date'] = null;
	}

	function setIDtourDate($input){
		$this->data['id_tour_date'] = $input;
	}


	function syncDates(){
		//Make sure both old and new date fields are accurate
		if($this->data['date'] > 0 && $this->data['date_v2'] == ""){
			$this->data['date_v2'] = mysqlDate($this->data['date']);
		}
		if($this->data['date'] <= 0 && $this->data['date_v2'] != ""){
			$this->data['date'] = strtotime($this->data['date_v2']);
		}
		if($this->data['date'] == "" && $this->data['date_v2'] == ""){
			$this->data['date'] = 0;
		}
	}


	function dataChecks(){
		$this->syncDates();
	}

	function save(){
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete(){
		if($this->id > 0){
			$delete = 'DELETE FROM '.$this->tableName.' WHERE id = "'.$this->id.'" LIMIT 1';
			$result = mysql_query($delete);
		}
	}
}

?>