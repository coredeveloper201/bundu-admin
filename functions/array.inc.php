<?php

function make_array($input='') {
	if(is_array($input)) {
		return $input;
	}
	if(is_bool($input)) {
		return array();
	}
	if(is_string($input)) {
		$input = str_replace("\n", ',', $input);
		$input = str_replace('|', ',', $input);
		$input = explode(',', $input);
		array_walk($input, 'trim');
		return $input;
	}
	if(!is_array($input)) {
		return array($input);
	}
}

function arrayPrepForSQL($array=array()) {
	$array = array_unique($array);
	$array = array_filter($array);
	array_walk($array, 'encodeSQL');
	return $array;
}

?>