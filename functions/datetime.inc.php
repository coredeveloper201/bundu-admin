<?php

function dateFormat($input='now', $format='m/d/Y g:ia') {
	if(is_null($input) || trim($input) == ""){
		return null;
	}
	if(!is_int($input) && !is_numeric($input)){
		$input = strtotime($input);
	}
	$out = date($format, $input);
	return $out;	
}

function is_date($input='') {
	if($input == "0000-00-00") {
		return false;
	}
	if(strtotime($input) === false) {
		return false;
	}
	return true;
}

function mysqlDateTime($input='now'){
	if(is_null($input) || trim($input) == ""){
		return null;
	}
	if(!is_int($input) && !is_numeric($input)){
		$input = strtotime($input);
	}
	$out = date("Y-m-d H:i:s", $input);
	return $out;
}

function mysqlDate($input='now'){
	if(is_null($input) || trim($input) == ""){
		return null;
	}
	if(!is_int($input) && !is_numeric($input)){
		$input = strtotime($input);
	}
	$out = date("Y-m-d", $input);
	return $out;
}

function mysqlTime($input='now'){
	if(is_null($input) || trim($input) == ""){
		return null;
	}
	if(!is_int($input) && !is_numeric($input)){
		$input = strtotime($input);
	}
	$out = date("H:i:s", $input);
	return $out;
}

function dateDiff($startDateTime='now', $endDateTime='now'){	
	$diff = strtotime($endDateTime) - strtotime($startDateTime);
	return $diff;
}

//echo mysqlDate( time() );

?>