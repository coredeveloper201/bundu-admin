<?php

// Author: Dominick Bernal - bernalwebservices.com
// http://bundubashers.com/staging/api/tours.php

require_once '../common.inc.php';
require_once 'auth.inc.php';


$summary = array(
	'found' => 0
	);

$xmlObj = new DOMDocument();
$xmlObj->formatOutput = true;
$xmlObj->encoding = 'UTF-8';

$resultTag = $xmlObj->createElement('result');
$xmlObj->appendChild($resultTag);

if(isset($_REQUEST['id_tour']) && $_REQUEST['id_tour'] != ""){
    $query = 'SELECT * FROM `tours` WHERE (`alias` = "'.$_REQUEST['id_tour'].'" OR `id` = "'.$_REQUEST['id_tour'].'") AND `hidden` != "1" LIMIT 1';
    //echo $query."<BR>\n";
    $result = @mysql_query($query);
    $num_results = @mysql_num_rows($result);
}

if(isset($num_results) && $num_results > 0){
    $tourinfo = mysql_fetch_assoc($result);
}
/*echo '<pre>';
print_r($tourinfo);
echo '</pre>';*/

$tourTag = $xmlObj->createElement('tour');

$tag = $xmlObj->createElement('id');
$tag->appendChild($xmlObj->createTextNode($tourinfo['id']));

$tourTag->appendChild($tag);

$tag = $xmlObj->createElement('perguest');
$tag->appendChild($xmlObj->createTextNode($tourinfo['perguest']));

$tourTag->appendChild($tag);

$resultTag->appendChild($tourTag);

header ("Content-Type:text/xml");

echo $xmlObj->saveXML();


?>