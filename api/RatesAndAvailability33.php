<?php
header("Access-Control-Allow-Origin: *");
require_once '../common.inc.php';

if (empty($_REQUEST['start_date']) || empty($_REQUEST['nights'])) {
    echo json_encode(['status' => 5000, 'error' => 'Date is not selected.']);
    exit;
}

$start = strtotime($_REQUEST['start_date']);
$end = $_REQUEST['nights'];
$data = [];

$query = 'SELECT * FROM `lodging` WHERE `type` = "y" order by `sort` asc ';
$result = mysql_query($query);

$number_of_lodge = mysql_num_rows($result);
if ($number_of_lodge > 0) {
    while ($row = mysql_fetch_assoc($result)) {
        $dateData = [];
        for ($i = 0; $i < $end; $i++) {
            $query = 'SELECT lodging.booked, lodging.available, lodging.name, lodging.description, lodging.capacity, lodging.url, lodging_pricing.price, lodging_pricing.lodgeid, lodging_pricing.startdate, lodging_pricing.enddate, lodging_pricing.min_nights, lodging_pricing.max_nights FROM `lodging` LEFT JOIN lodging_pricing ON lodging_pricing.lodgeid = lodging.id WHERE lodging.id = ' . $row['id'] . ' && lodging_pricing.startdate <= ' . $start . ' AND lodging_pricing.enddate >= ' . $start . ' ORDER BY lodging.id ASC';
            $eachData = [];
            $new_result = mysql_query($query);
            $number_of_lodge = mysql_num_rows($new_result);
            if ($number_of_lodge > 0) {
                while ($new_row = mysql_fetch_assoc($new_result)) {
                    $eachData[] = $new_row;
                }
            }
            $dateData[date('M j Y',$start)] = $eachData;
            $start = strtotime('+1 day', $start);
        }
        $row['info'] = $dateData;
        $data[] = $row;
    }
}
$newData = [];
$newData1 = [];
$newData2 = [];
$i = 0;
foreach ($data as $id=>$item) {
    foreach ($item['info'] as $date => $info) {
        $count = count($info);
        if ($count == 1){
            $info[0]['id'] = $item['id'];
            $newData[] = $info[0];
        }
        if ($count == 2){
            $info[0]['id'] = $item['id'];
            $info[1]['id'] = $item['id'];
            $newData[] = $info[0];
            $newData[] = $info[1];
        }
        if ($count == 3){
            $info[0]['id'] = $item['id'];
            $info[1]['id'] = $item['id'];
            $info[2]['id'] = $item['id'];
            $newData[] = $info[0];
            $newData[] = $info[1];
            $newData[] = $info[2];
        }

    }
}

//echo json_encode($newData);
//exit();

$dataResult = [];
$id = 0;
foreach ($newData as $newDatum) {
    if ($newDatum['max_nights'] == "31" and $newDatum['min_nights'] == "7"){
        $key = $newDatum['id'].$newDatum['min_nights'];
        if ($id != $newDatum['id']){
            $id = $newDatum['id'];
            $dataResult[$key]['id'] =  $newDatum['id'];
            $dataResult[$key]['name'] =  $newDatum['name'];
            $dataResult[$key]['max_min'] =  "( Max-Night : ".$newDatum['max_nights']."- Min_night : ".$newDatum['min_nights'].")";
            $dataResult[$key]['data'][] =  $newDatum;
        }elseif ($id == $newDatum['id']){
            if (!$dataResult[$key]['name']){
                $dataResult[$key]['id'] =  $newDatum['id'];
                $dataResult[$key]['name'] =  $newDatum['name'];
                $dataResult[$key]['max_min'] =   "( Max-Night : ".$newDatum['max_nights']."- Min_night : ".$newDatum['min_nights'].")";
            }
            $dataResult[$key]['data'][] =  $newDatum;
        }
    } elseif ($newDatum['max_nights'] == "6" and $newDatum['min_nights'] == "2"){
        $key = $newDatum['id'].$newDatum['min_nights'];
        if ($id != $newDatum['id']){
            $id = $newDatum['id'];
            $dataResult[$key]['id'] =  $newDatum['id'];
            $dataResult[$key]['name'] =  $newDatum['name'];
            $dataResult[$key]['max_min'] =   "( Max-Night : ".$newDatum['max_nights']."- Min_night : ".$newDatum['min_nights'].")";
            $dataResult[$key]['data'][] =  $newDatum;
        }elseif ($id == $newDatum['id']){
            if (!$dataResult[$key]['name']){
                $dataResult[$key]['id'] =  $newDatum['id'];
                $dataResult[$key]['name'] =  $newDatum['name'];
                $dataResult[$key]['max_min'] =   "( Max-Night : ".$newDatum['max_nights']."- Min_night : ".$newDatum['min_nights'].")";
            }
            $dataResult[$key]['data'][] =  $newDatum;
        }
    } elseif ($newDatum['max_nights'] == "1" and $newDatum['min_nights'] == "1"){
        $key = $newDatum['id'].$newDatum['min_nights'];
        if ($id != $newDatum['id']){
            $id = $newDatum['id'];
            $dataResult[$key]['id'] =  $newDatum['id'];
            $dataResult[$key]['name'] =  $newDatum['name'];
            $dataResult[$key]['max_min'] =   "( Max-Night : ".$newDatum['max_nights']."- Min_night : ".$newDatum['min_nights'].")";
            $dataResult[$key]['data'][] =  $newDatum;
        }elseif ($id == $newDatum['id']){
            if (!$dataResult[$key]['name']){
                $dataResult[$key]['id'] =  $newDatum['id'];
                $dataResult[$key]['name'] =  $newDatum['name'];
                $dataResult[$key]['max_min'] =  "( Max-Night : ".$newDatum['max_nights']."- Min_night : ".$newDatum['min_nights'].")";
            }
            $dataResult[$key]['data'][] =  $newDatum;
        }
    }



}



/*for ($i = 0; $i < $end; $i++){
    $query = 'SELECT lodging.booked, lodging.available, lodging.name, lodging.description, lodging.capacity, lodging.url, lodging_pricing.price, lodging_pricing.lodgeid, lodging_pricing.startdate, lodging_pricing.enddate, lodging_pricing.min_nights, lodging_pricing.max_nights FROM `lodging` LEFT JOIN lodging_pricing ON lodging_pricing.lodgeid = lodging.id WHERE lodging.type = "y" && lodging_pricing.startdate <= '.$start.' AND lodging_pricing.enddate >= '.$start.' ORDER BY lodging.id ASC';
    $start = strtotime('+1 day', $start);
    $result = mysql_query($query);

    $number_of_lodge = mysql_num_rows($result);
    if ($number_of_lodge>0){
        while ($row = mysql_fetch_assoc($result)) {
            $data[] = $row;
        }
    }
}*/

if (count($dataResult)) {
    echo json_encode(['status' => 2000, 'data' => $dataResult]);
    exit();
} else {
    echo json_encode(['status' => 5000, 'error' => 'No results found!']);
}
