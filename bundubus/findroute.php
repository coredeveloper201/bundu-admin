<?  //error_reporting('E_ALL'); ini_set('display_errors','1');

require_once('../common.inc.php');


$time = time(); //$time = mktime(2,0,0,11,20,2010);


@ini_set("session.gc_maxlifetime","10800");
if(!isset($_SESSION)){ session_start();	}

$aday = 86400;
$weekdays = array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
$maxlegs = array("Direct routes only","2 legs","3 legs","4 legs","5 legs","6 legs","7 legs","8 legs","9 legs","10 legs");
$c = 0;

$output = '';
if(!isset($_REQUEST['return'])): $_REQUEST['return'] = 'b'; endif;
if(!isset($_REQUEST['formsize'])): $_REQUEST['formsize'] = 'l'; endif;
if(!isset($_REQUEST['rettype'])): $_REQUEST['rettype'] = 'html'; endif;
if(!isset($_REQUEST['disablejs'])): $_REQUEST['disablejs'] = 'n'; endif;
if(isset($_REQUEST['jenabled']) && $_REQUEST['jenabled'] == "n"): $_REQUEST['disablejs'] = 'y'; endif;
if(isset($_REQUEST['trip']) && !is_array($_REQUEST['trip'])){ $_REQUEST['trip'] = explode('|',$_REQUEST['trip']); }
//if(!isset($_REQUEST['hidep'])): $_REQUEST['hidep'] = '0'; endif;

//GET DEFAULT SETTINGS
$defaults = array();
$query = 'SELECT `setting`,`var` FROM `bundubus_settings`';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$defaults[$row['setting']] = $row['var'];
	}
//$defaults['chgcutoff'] = ($defaults['chgcutoff']*3600);
$defaults['chgcutoff_secs'] = ($defaults['chgcutoff']*3600);

$today = mktime(0,0,0,date("n",$time),date("j",$time),date("Y",$time));
	if($today < 1235890800): $today = 1235890800; endif;

if(!isset($_REQUEST['act'])){ $_REQUEST['act'] = $_SERVER['PHP_SELF']; }
if(!isset($_REQUEST['btn'])){ $_REQUEST['btn'] = ''; }
if(!isset($_REQUEST['smonth']) || !isset($_REQUEST['sday']) || !isset($_REQUEST['syear'])){
	$finddate = $today;
	} else {
	$finddate = mktime(0,0,0,$_REQUEST['smonth'],$_REQUEST['sday'],$_REQUEST['syear']);
	}
	$startdate = mktime(0,0,0,date("n",$finddate),(date("j",$finddate)-2),date("Y",$finddate));
if(isset($rticket) || !isset($_REQUEST['pax']) || !is_numeric($_REQUEST['pax']) || $_REQUEST['pax'] < 1){ $_REQUEST['pax'] = 1; }
if(!isset($_REQUEST['maxlegs']) || !is_numeric($_REQUEST['maxlegs'])): $_REQUEST['maxlegs'] = 10; endif;
if(!isset($_REQUEST['maxdays']) || !is_numeric($_REQUEST['maxdays'])): $_REQUEST['maxdays'] = 5; endif;
if(!isset($_REQUEST['start'])): $_REQUEST['start'] = 1; endif;
if(!isset($_REQUEST['end'])): $_REQUEST['end'] = 2; endif;
$enddate = mktime(0,0,0,date("n",$finddate),(date("j",$finddate)+($_REQUEST['maxdays']+1)),date("Y",$finddate));
//echo date("n/j/y G",$startdate).' '.date("n/j/y G",$finddate).' '.date("n/j/y G",$enddate).'<BR>';

//echo '<PRE STYLE="text-align:left;">'; print_r($_REQUEST); echo '</PRE>';

if(isset($_REQUEST['ticket']) && $_REQUEST['ticket'] != "" && isset($_SESSION['bb_member'])){
	$query = 'SELECT * FROM `reservations_assoc` WHERE `id` = "'.$_REQUEST['ticket'].'" AND `username` = "'.$_SESSION['bb_member']['username'].'" AND `password` = "'.$_SESSION['bb_member']['password'].'" LIMIT 1';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
	if($num_results == 1){
		$rticket = mysql_fetch_assoc($result);
		$query = 'SELECT reservations_assoc.`date`, reservations_assoc.`dep_time` FROM `reservations_assoc`,`routes` WHERE reservations_assoc.`reservation` = "'.$rticket['reservation'].'" AND reservations_assoc.`routeid` = routes.`id` AND reservations_assoc.`canceled` = 0 ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC LIMIT 1';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		if($num_results == 1){
			$row = mysql_fetch_assoc($result);
			$rticket['start'] = $row['dep_time'];
			if($startdate < $rticket['start']){
				$startdate = $rticket['start'];
				if($finddate < $startdate){ $finddate = $startdate; }
				$enddate = mktime(date("G",$finddate),date("i",$finddate),0,date("n",$finddate),(date("j",$finddate)+($_REQUEST['maxdays']+1)),date("Y",$finddate));
				}
			$rticket['expires'] = mktime(date("G",$rticket['start']),date("i",$rticket['start']),0,date("n",$rticket['start']),(date("j",$rticket['start'])+$rticket['days']),date("Y",$rticket['start']));
			if($enddate > $rticket['expires']){ $enddate = $rticket['expires']; }
			}
		if($rticket['type'] == "m"){
			$limroutes = array(0);
			$query = 'SELECT routes.`id` FROM `miniroutes_assoc`,`routes` WHERE';
				$query .= ' `minirouteid` = "'.$rticket['miniroute'].'" AND `minirouteid` != 0';
				$query .= ' AND miniroutes_assoc.`type` = "r" AND miniroutes_assoc.`typeid` = routes.`id`';
				$query .= ' ORDER BY `order` ASC';
			$result = @mysql_query($query);
			$num_results = @mysql_num_rows($result);
				for($i=0; $i<$num_results; $i++){
					$row = mysql_fetch_assoc($result);
					array_push($limroutes,$row['id']);
					}

			if($_REQUEST['trip'][0] == "*new*" && count($limroutes) > 0){
				//Removed booked routes for *new* trips
				$usedlegs = array();
				$query = 'SELECT routes.`id` FROM `reservations_assoc`,`routes` WHERE reservations_assoc.`reservation` = "'.$rticket['reservation'].'" AND reservations_assoc.`routeid` = routes.`id`';
					$query .= ' ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC';
				//echo $query.'<BR>';
				$result = mysql_query($query);
				$num_results = mysql_num_rows($result);
					for($i=0; $i<$num_results; $i++){
						$row = mysql_fetch_assoc($result);

						$rmv = false;
						foreach($limroutes as $lkey => $id){
							if($row['id'] == $id){ $rmv = $lkey; }
							}
						if($rmv !== false){ array_splice($limroutes,$rmv,1); }
						}
				}

			}
		if($_REQUEST['maxdays'] > $rticket['days']){ $_REQUEST['maxdays'] = $rticket['days']; }
		}
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r(@$limroutes); echo '</PRE>';
	//echo '<PRE STYLE="text-align:left;">'; print_r($_REQUEST); echo '</PRE>';

//Can't book in the past
//if($startdate < ($time+$defaults['chgcutoff_secs'])){ $startdate = ($time+$defaults['chgcutoff_secs']); }
if($finddate < $startdate){ $finddate = $startdate; }
if($finddate < ($time+$defaults['chgcutoff_secs'])){ $finddate = ($time+$defaults['chgcutoff_secs']); }

//GET DEPARTING LOCATIONS LIST
$dep_locs = array();
$query = 'SELECT DISTINCT locations.* FROM `locations`,`routes`';
	$query .= ' WHERE routes.`bbincl` != "n" AND routes.`hidden` != "1"';
	$query .= ' AND locations.`id` = routes.`dep_loc`';
	$query .= ' ORDER BY locations.`name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$dep_locs['d'.$row['id']] = $row;
	}

//GET ARRIVING LOCATIONS LIST
$arr_locs = array();
$query = 'SELECT DISTINCT locations.* FROM `locations`,`routes`';
	$query .= ' WHERE routes.`bbincl` != "n" AND routes.`hidden` != "1"';
	$query .= ' AND locations.`id` = routes.`arr_loc`';
	$query .= ' ORDER BY locations.`name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$arr_locs['a'.$row['id']] = $row;
	}



if($_REQUEST['return'] != "f"){

//GET BLOCKED
@include_once('../getblocked.php');
$blocked = getblocked('*',$startdate,$enddate);
	//echo '<PRE>'; print_r($blocked); echo '</PRE>';

$loc_tzs = array();
	$query = 'SELECT `id`,`timezone_id` FROM `locations`';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$loc_tzs['l'.$row['id']] = $row['timezone_id'];
		}
	//echo '<PRE STYLE="text-align:left;">'; print_r($loc_tzs); echo '</PRE>';

function tz_offset($dep_loc=1,$arr_loc=1,$datetime="now"){
	global $loc_tzs;

	if(!isset($loc_tzs['l'.$dep_loc])){	$loc_tzs['l'.$dep_loc] = date_default_timezone_get(); }
	if(!isset($loc_tzs['l'.$arr_loc])){ $loc_tzs['l'.$arr_loc] = date_default_timezone_get(); }

    $dep_tz = new DateTimeZone($loc_tzs['l'.$dep_loc]);
    $arr_tz = new DateTimeZone($loc_tzs['l'.$arr_loc]);

    $dep_dt = new DateTime($datetime,$dep_tz);
    $arr_dt = new DateTime($datetime,$arr_tz);

    $offset = ($arr_tz->getOffset($arr_dt) - $dep_tz->getOffset($dep_dt));
    return $offset;
	}

//GET LEGS
$legs = array();
$query = '';
	$query .= '(SELECT routes_dates.`id`, routes_dates.`routeid`, routes_dates.`date`, routes.`dep_loc`, IF(routes_dates.`dep_time`=-1,routes.`dep_time`,routes_dates.`dep_time`) AS `dep_time`, routes.`arr_loc`, IF(routes_dates.`arr_time`=-1,routes.`arr_time`,routes_dates.`arr_time`) AS `arr_time`, routes.`travel_time`, routes.`anchor`, IF(routes_dates.`seats`=-1,routes.`seats`,routes_dates.`seats`) AS `seats`, routes.`bbprice`, routes.`miles`, routes.`details`';
	$query .= ' FROM `routes`,`routes_dates`';
	$query .= ' WHERE routes.`id` = routes_dates.`routeid` AND routes.`bbincl` = "a" AND routes.`hidden` != "1"';
	$query .= ' AND routes_dates.`date` >= '.$startdate.' AND routes_dates.`date` <= '.$enddate;
	if(isset($limroutes) && count($limroutes) > 0){ $query .= ' AND (routes.`id` = "'.implode('" OR routes.`id` = "',$limroutes).'")'; }
	$query .= ')';
	$query .= ' UNION ';
	$query .= '(SELECT routes_dates.`id`, routes_dates.`routeid`, routes_dates.`date`, routes.`dep_loc`, IF(routes_dates.`dep_time`=-1,routes.`dep_time`,routes_dates.`dep_time`) AS `dep_time`, routes.`arr_loc`, IF(routes_dates.`arr_time`=-1,routes.`arr_time`,routes_dates.`arr_time`) AS `arr_time`, routes.`travel_time`, routes.`anchor`, IF(routes_dates.`seats`=-1,routes.`seats`,routes_dates.`seats`) AS `seats`, routes.`bbprice`, routes.`miles`, routes.`details`';
	$query .= ' FROM `routes`,`routes_dates`,`tours_assoc`,`tours_dates`';
	$query .= ' WHERE routes.`id` = routes_dates.`routeid` AND routes.`bbincl` = "c" AND routes.`hidden` != "1"';	//Connect routes and route dates, filter routes that need to be confirmed
	$query .= ' AND routes_dates.`date` >= '.$startdate.' AND routes_dates.`date` <= '.$enddate;					//Filter route dates to within the period requested
	if(isset($limroutes) && count($limroutes) > 0){ $query .= ' AND (routes.`id` = "'.implode('" OR routes.`id` = "',$limroutes).'")'; } //If needed, limit route ids
	$query .= ' AND routes.`id` = tours_assoc.`typeid` AND tours_assoc.`type` = "r"'; 								//Connect route to associated tours, filter to type "r"
	$query .= ' AND tours_dates.`tourid` = tours_assoc.`tourid`'; 													//Connect tour dates and tours assoc
	$query .= ' AND tours_assoc.`dir` = tours_dates.`dir`'; 														//Match the direction of the tour date to the tour assoc
	$query .= ' AND tours_dates.`date` >= (routes_dates.`date` - ((tours_assoc.`day`-1)*86400) - 3600)'; 			//Match the offset route date to the scheduled tour date
	$query .= ' AND tours_dates.`date` <= (routes_dates.`date` - ((tours_assoc.`day`-1)*86400) + 3600)';
	$query .= ' AND tours_dates.`confirmed` = "1"'; 																//Make sure tour date is confirmed
	$query .= ')';
	$query .= ' ORDER BY `date` ASC, `dep_time` ASC, `arr_time` ASC, `routeid` ASC';
	//$output .= $query.'<BR>';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		//echo '<PRE STYLE="text-align:left;">'; print_r($row); echo '</PRE>';
		$row['blocked'] = 0;
		if(isset($blocked['r'.$row['routeid']]['d'.$row['date']])){ $row['blocked'] = ($blocked['r'.$row['routeid']]['d'.$row['date']]['tour_seats'] + $blocked['r'.$row['routeid']]['d'.$row['date']]['route_seats']); }
		if(($row['blocked']+$_REQUEST['pax']) <= $row['seats']){
			$datestr = date("F j, Y",$row['date']);
			if($row['anchor'] == 'arr'){
				$row['arr_time'] = strtotime($datestr. ' '.date("H:i",(1262329200+$row['arr_time'])));
				$row['dep_time'] = ($row['arr_time']-$row['travel_time']+tz_offset($row['arr_loc'],$row['dep_loc'],($datestr.' '.date("H:i",$row['arr_time']-$row['travel_time']))));
				} else {
				$row['dep_time'] = strtotime($datestr. ' '.date("H:i",(1262329200+$row['dep_time'])));
				$row['arr_time'] = ($row['dep_time']+$row['travel_time']+tz_offset($row['dep_loc'],$row['arr_loc'],($datestr.' '.date("H:i",$row['dep_time']+$row['travel_time']))));
				}
				//echo $row['dep_time'].' - '.$row['arr_time'].'<BR>';
			//The following is no longer needed since times are based on travel time
			//if($row['arr_time'] < $row['dep_time']){
			//	$row['arr_time'] = ($row['arr_time']+mktime(0,0,0,date("n",$row['date']),(date("j",$row['date'])+1),date("Y",$row['date'])));
			//	} else {
			//	$row['arr_time'] = ($row['arr_time']+$row['date']);
			//	}
			//$row['dep_time'] = ($row['dep_time']+$row['date']);
			if($row['dep_time'] >= ($time+$defaults['chgcutoff_secs'])){ $legs['l'.$row['id']] = $row; }
			}
		}
	//echo '<PRE STYLE="text-align:left;">'; print_r($legs); echo '</PRE>';

	//$output .= '<PRE>';
	//	$output .= 'Legs: '.count($legs)."\n";
	//	$output .= print_r($legs,TRUE);
	//foreach($legs as $thisleg){
	//	$output .= 'Date: '.date("D n/j/Y",$thisleg['date'])."\t".$thisleg['routeid']."\t".$thisleg['blocked'].'/'.$thisleg['seats']."\t".$dep_locs['d'.$thisleg['dep_loc']]['name']."\t".date("g:ia",$thisleg['dep_time'])."\t".$arr_locs['a'.$thisleg['arr_loc']]['name']."\t".date("g:ia",$thisleg['arr_time'])."\n";
	//	}
	//	$output .= '</PRE>';


function bcolor($i){
	if(!isset($GLOBALS['bcolor']) || $i == "reset"): $GLOBALS['bcolor'] = "FFFFFF"; endif;
	if($i != "reset" && $i != ""): $GLOBALS['bcolor'] = $i; endif;
	if($GLOBALS['bcolor'] == "FFFFFF"): $GLOBALS['bcolor'] = "DDDDDD"; else: $GLOBALS['bcolor'] = "FFFFFF"; endif;
	return $GLOBALS['bcolor'];
}

function addgoroute($legs){
	global $goroutes;
	global $_REQUEST;

	$a = 1;
	$visited = array();
	foreach($legs as $row){
		if($row['dep_loc'] != $row['arr_loc'] && in_array($row['arr_loc'],$visited)){
			//echo 'Found '.$row['arr_loc'].' in visited array.<BR>';
			$a = 0;
			break;
			} elseif(in_array($row['dep_loc'].':'.$row['arr_loc'],$visited)){
			//echo 'Found '.$row['dep_loc'].':'.$row['arr_loc'].' in visited array.<BR>';
			$a = 0;
			break;
			} else {
			//echo $row['dep_loc'].':'.$row['arr_loc'].' not found in visited array.<BR>';
			if($row['dep_loc'] == $row['arr_loc']){ $row['arr_loc'] = $row['dep_loc'].':'.$row['arr_loc']; }
			array_push($visited,$row['arr_loc']);
			}
		}
	//echo '<BR>';

	if($a == 1){ array_push($goroutes,$legs); }
}

function findprime($legs){
	global $finddate;
	$prime = 0;

	if($legs[0]['date'] != $finddate){
		$prime = ($prime+2);
		}

	foreach($legs as $thisleg){
		if($thisleg['date'] != $finddate){
			$prime++;
			break;
			}
		}

	foreach($legs as $thisleg){
		if($thisleg['date'] != $legs[0]['date']){
			$prime++;
			break;
			}
		}

	return $prime;
	}


$output .= '<DIV ID="showbox" STYLE="display:none; position:absolute; z-index:200;"></DIV>'."\n\n";

if($_REQUEST['disablejs'] != "y"){
$output .= '<SCRIPT><!--

function togcheck(id){
	x = document.getElementById(id);
	if(x.checked == true){
		x.checked = false;
		} else {
		x.checked = true;
		}
}'."\n\n";

$output .= 'var legnotes = new Array();'."\n";
	foreach($legs as $thisleg){
	if($thisleg['details'] != ""){
		$thisleg['details'] = str_replace("\r",'',$thisleg['details']);
		$thisleg['details'] = str_replace("\n",'<BR>',$thisleg['details']);
		$thisleg['details'] = str_replace("'",'\\\'',$thisleg['details']);
		$output .= "\t".'legnotes[\'l'.$thisleg['id'].'\'] = \''.$thisleg['details'].'\';'."\n";
		}
	}
	$output .= "\n";

$output .= '// --></SCRIPT>'."\n\n";
} //END $_REQUEST['disablejs'] if statement
} //END $_REQUEST['return'] If STATEMENT


if($_REQUEST['return'] != "r"){
	$output .= '<FORM METHOD="get" ACTION="'.$_REQUEST['act'].'" TARGET="_top">'."\n";
	if(isset($_REQUEST['nodup'])){ $output .= '<INPUT TYPE="hidden" NAME="nodup" VALUE="'.$_REQUEST['nodup'].'">'."\n"; }
	if(isset($_REQUEST['trip'])){ $output .= '<INPUT TYPE="hidden" NAME="trip" VALUE="'.implode('|',$_REQUEST['trip']).'">'."\n"; }
	if(isset($_REQUEST['ticket'])){ $output .= '<INPUT TYPE="hidden" NAME="ticket" VALUE="'.$_REQUEST['ticket'].'">'."\n"; }
	$output .= "\n";


if(isset($_REQUEST['formsize']) && $_REQUEST['formsize'] == "m"){

$output .= '<font face="Arial" size="2"><b>Find routes on or around date:</b></font><br>
<select name="smonth" style="font-size:9pt;">';
	for($i=1; $i<13; $i++){
		$output .= '<option value="'.$i.'"';
		if( date("n",$finddate) == $i ): $output .= " selected"; endif;
		$output .= '>'.date("F",mktime("0","0","0",$i,"1","2005")).'</OPTION>';
		}
	$output .= '</select> / <select name="sday" style="font-size:9pt;">';
	for($i=1; $i<32; $i++){
		$output .= '<option value="'.$i.'"';
		if( date("j",$finddate) == $i ): $output .= " selected"; endif;
		$output .= '>'.$i.'</option>';
		}
	$output .= '</select> / <select name="syear" style="font-size:9pt;">';
	for($i=date("Y",$time); $i<(date("Y",$time)+3); $i++){
		$output .= '<option value="'.$i.'"';
		if( date("Y",$finddate) == $i ): $output .= " selected"; endif;
		$output .= '>'.$i.'</option>';
		}
	$output .= '</select><br>'."\n\n";

$output .= '<table border="0">';

$output .= '<tr><td align="right"><font face="Arial" size="2"><b># of passengers:</b></font></td><td align="left">';
	if(isset($rticket)){
		$output .= '<font face="Arial" size="2">1<input type="hidden" name="pax" value="1"></font>';
		} else {
		$output .= '<input type="text" size="3" name="pax" style="font-size:9pt;" value="'.$_REQUEST['pax'].'">';
		}
	$output .= "</td></tr>\n";

$output .= '<tr><td align="right"><font face="Arial" size="2"><b>Max. # of days:</b></font></td><td align="left"><SELECT NAME="maxdays" STYLE="font-size:9pt;">';
	$max = 8;
		if(isset($rticket['days']) && $rticket['days'] > 0 && $rticket['days'] < $max){ $max = $rticket['days']; }
	for($i=1; $i<=$max; $i++){
		$output .= '<option value="'.$i.'"';
		if( $i == $_REQUEST['maxdays'] ): $output .= " selected"; endif;
		$output .= '>'.$i.' Day'; if($i > 1): $output .= 's'; endif; $output .= '</option>';
		}
	$output .= '</select></td>';
	$output .= "</tr>\n";

$output .= '<tr><td align="right"><font face="Arial" size="2"><b>From:</b></font></td><td align="left"><select name="start" style="font-size:9pt; width:200px;">';
	foreach($dep_locs as $thisstop){
		$output .= '<option value="'.$thisstop['id'].'"';
		if($_REQUEST['start'] == $thisstop['id']): $output .= ' selected'; endif;
		$output .= '>'.$thisstop['name'].'</option>';
		}
		$output .= '</select></td>'."\n\n";
	$output .= "</tr>\n";

$output .= '<tr><td align="right"><font face="Arial" size="2"><b>To:</b></td><td align="left"><select name="end" style="font-size:9pt; width:200px;">';
	foreach($arr_locs as $thisstop){
		$output .= '<option value="'.$thisstop['id'].'"';
		if($_REQUEST['end'] == $thisstop['id']): $output .= ' selected'; endif;
		$output .= '>'.$thisstop['name'].'</option>';
		}
		$output .= '</select></td>'."\n\n";
	$output .= "</tr></table>\n\n";

$output .= '<input type="hidden" name="jenabled" id="jenabled" value="n">'."\n";
$output .= '<input type="hidden" name="formsize" value="m">'."\n";
$output .= '<input type="submit" style="font-size:9pt;" value="Find Routes">'."\n\n";

} elseif(isset($_REQUEST['formsize']) && $_REQUEST['formsize'] == "s"){

$output .= '<TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" STYLE="border:#000000 1px solid; background:#FFFFFF url(\'http://www.bundubashers.com/img/ezdescback.jpg\') left top repeat-y;">
<TR><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FF0000;"><B>C\'mon, give it a go!</B></TD></TR>
<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; padding-right:6px;"><B>On or around:</B></TD>
<TD><SELECT NAME="smonth" STYLE="font-size:9pt;">';
	for($i=1; $i<13; $i++){
		$output .= '<OPTION VALUE="'.$i.'"';
		if( date("n",$finddate) == $i ): $output .= " SELECTED"; endif;
		$output .= '>'.date("F",mktime("0","0","0",$i,"1","2005")).'</OPTION>';
		}
	$output .= '</SELECT> / <SELECT NAME="sday" STYLE="font-size:9pt;">';
	for($i=1; $i<32; $i++){
		$output .= '<OPTION VALUE="'.$i.'"';
		if( date("j",$finddate) == $i ): $output .= " SELECTED"; endif;
		$output .= '>'.$i.'</OPTION>';
		}
	$output .= '</SELECT> / <SELECT NAME="syear" STYLE="font-size:9pt;">';
	for($i=date("Y",$time); $i<(date("Y",$time)+3); $i++){
		$output .= '<OPTION VALUE="'.$i.'"';
		if( date("Y",$finddate) == $i ): $output .= " SELECTED"; endif;
		$output .= '>'.$i.'</OPTION>';
		}
	$output .= '</SELECT></TD></TR>'."\n\n";

$output .= '<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; padding-right:6px;"><B># of passengers:</B></TD><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">';
	if(isset($rticket)){
		$output .= '1<INPUT TYPE="hidden" NAME="pax" VALUE="1">';
		} else {
		$output .= '<INPUT TYPE="text" SIZE="3" NAME="pax" STYLE="font-size:9pt;" VALUE="'.$_REQUEST['pax'].'">';
		}
	$output .= '</TD></TR>'."\n\n";

$output .= '<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; padding-right:6px;"><B>Max. # of days:</B></TD>
<TD><SELECT NAME="maxdays" STYLE="font-size:9pt;">';
	$max = 8;
		if(isset($rticket['days']) && $rticket['days'] > 0 && $rticket['days'] < $max){ $max = $rticket['days']; }
	for($i=1; $i<=$max; $i++){
		$output .= '<OPTION VALUE="'.$i.'"';
		if( $i == $_REQUEST['maxdays'] ): $output .= " SELECTED"; endif;
		$output .= '>'.$i.' Day'; if($i > 1): $output .= 's'; endif; $output .= '</OPTION>';
		}
	$output .= '</SELECT></TD></TR>'."\n\n";

$output .= '<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; padding-right:6px;"><B>From:</B></TD>
<TD>
	<SELECT NAME="start" STYLE="font-size:9pt; width:200px;">';
		foreach($dep_locs as $thisstop){
			$output .= '<OPTION VALUE="'.$thisstop['id'].'"';
			if($_REQUEST['start'] == $thisstop['id']): $output .= ' SELECTED'; endif;
			$output .= '>'.$thisstop['name'].'</OPTION>';
			}
		$output .= '</SELECT></TD></TR>'."\n\n";

$output .= '<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; padding-right:6px;"><B>To:</B></TD>
<TD>
	<SELECT NAME="end" STYLE="font-size:9pt; width:200px;">';
		foreach($arr_locs as $thisstop){
			$output .= '<OPTION VALUE="'.$thisstop['id'].'"';
			if($_REQUEST['end'] == $thisstop['id']): $output .= ' SELECTED'; endif;
			$output .= '>'.$thisstop['name'].'</OPTION>';
			}
		$output .= '</SELECT></TD></TR>
<TR><TD></TD><TD><INPUT TYPE="submit" STYLE="font-size:9pt;" VALUE="Find Routes"></TD></TR>
</TABLE>'."\n";

} else {

$output .= '<TABLE BORDER="0">
<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>On or around:&nbsp;</B></FONT></TD>
<TD><SELECT NAME="smonth">';
	for($i=1; $i<13; $i++){
		$output .= '<OPTION VALUE="'.$i.'"';
		if( date("n",$finddate) == $i ): $output .= " SELECTED"; endif;
		$output .= '>'.date("F",mktime("0","0","0",$i,"1","2005")).'</OPTION>';
		}
	$output .= '</SELECT> / <SELECT NAME="sday">';
	for($i=1; $i<32; $i++){
		$output .= '<OPTION VALUE="'.$i.'"';
		if( date("j",$finddate) == $i ): $output .= " SELECTED"; endif;
		$output .= '>'.$i.'</OPTION>';
		}
	$output .= '</SELECT> / <SELECT NAME="syear">';
	for($i=date("Y",$time); $i<(date("Y",$time)+3); $i++){
		$output .= '<OPTION VALUE="'.$i.'"';
		if( date("Y",$finddate) == $i ): $output .= " SELECTED"; endif;
		$output .= '>'.$i.'</OPTION>';
		}
	$output .= '</SELECT></TD></TR>'."\n\n";

$output .= '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>Number of passengers:&nbsp;</B></FONT></TD><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">';
	if(isset($rticket)){
		$output .= '1<INPUT TYPE="hidden" NAME="pax" VALUE="1">';
		} else {
		$output .= '<INPUT TYPE="text" SIZE="3" NAME="pax" VALUE="'.$_REQUEST['pax'].'">';
		}
	$output .= '</TD></TR>'."\n\n";

/*$output .= '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>Max. number of legs:&nbsp;</B></FONT></TD>
<TD><SELECT NAME="maxlegs">';
	foreach($maxlegs as $key => $opt){
		$output .= '<OPTION VALUE="'.$key.'"';
		if( $key == $_REQUEST['maxlegs'] ): $output .= " SELECTED"; endif;
		$output .= '>'.$opt.'</OPTION>';
		}
	$output .= '</SELECT></TD></TR>'."\n\n";*/

$output .= '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>Max. number of days:&nbsp;</B></FONT></TD>
<TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">';
	//if(isset($rticket)){
	//	$output .= $rticket['days'].' days<INPUT TYPE="hidden" NAME="maxdays" VALUE="'.$rticket['days'].'">';
	//	} else {
		$max = 8;
			if(isset($rticket['days']) && $rticket['days'] > 0 && $rticket['days'] < $max){ $max = $rticket['days']; }
		$output .= '<SELECT NAME="maxdays">';
		for($i=1; $i<=$max; $i++){
			$output .= '<OPTION VALUE="'.$i.'"';
			if( $i == $_REQUEST['maxdays'] ): $output .= " SELECTED"; endif;
			$output .= '>'.$i.' Day'; if($i > 1): $output .= 's'; endif; $output .= '</OPTION>';
			}
		$output .= '</SELECT>';
	//	}
	$output .= '</TD></TR>'."\n\n";

$output .= '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>From:&nbsp;</B></FONT></TD>
<TD>
	<SELECT NAME="start" STYLE="width:200px;">';
		foreach($dep_locs as $thisstop){
			$output .= '<OPTION VALUE="'.$thisstop['id'].'"';
			if($_REQUEST['start'] == $thisstop['id']): $output .= ' SELECTED'; endif;
			$output .= '>'.$thisstop['name'].'</OPTION>';
			}
		$output .= '</SELECT>
	-to-
	<SELECT NAME="end" STYLE="width:200px;">';
		foreach($arr_locs as $thisstop){
			$output .= '<OPTION VALUE="'.$thisstop['id'].'"';
			if($_REQUEST['end'] == $thisstop['id']): $output .= ' SELECTED'; endif;
			$output .= '>'.$thisstop['name'].'</OPTION>';
			}
		$output .= '</SELECT></TD></TR>
<TR><TD></TD><TD><INPUT TYPE="submit" VALUE="Find Routes"></TD></TR>
</TABLE>'."\n";

} //END FORM SIZE IF STATEMENT

$output .= '	</FORM>'."\n\n";

//echo '<PRE STYLE="text-align:left;">'; print_r($rticket); echo '</PRE>';

/*if(isset($rticket)){
	$output .= '<SPAN STYLE="font-family:Arial; font-size:12pt;"><B>Routes for';
		if($rticket['type'] == "h"){ $output .= ' Hop On, Hop Off Pass'; } elseif($rticket['type'] == "m"){ $output .= ' Mini Route Pass'; }
		$output .= ' #'.$rticket['reservation'].'</B></SPAN><BR>';
	if(isset($rticket['start']) && $rticket['start'] > 0){
		$output .= '<SPAN STYLE="font-family:Arial; font-size:10pt;">Valid dates: '.date("n/j/y g:ia",$rticket['start']).' <I>-through-</I> '.date("n/j/y g:ia",$rticket['expires']).'</SPAN><BR><BR>'."\n\n";
		} else {
		$output .= '<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" STYLE="width:600px; border:#B00000 1px solid; background:#FFFFFF;">
		<TR><TD ALIGN="center" STYLE="background:#F3D9D9; font-family:Arial; font-size:10pt;"><B>IMPORTANT: Please book your earliest trip first!!!<BR>The start date can be changed later, but there is an extra fee.</B><BR>Please see our <A HREF="http://www.bundubus.com/faq_grand_canyon_transportation.php" TARGET="_blank">FAQ</A> page for details.</TD></TR>
		</TABLE><BR>'."\n\n";
		}
		//$output .= $mdays.'<BR>';
	}*/

} //END $_REQUEST['return'] if Statement


if($_REQUEST['return'] != "f"){

if(isset($_REQUEST['trip']) && is_array($_REQUEST['trip']) && count($_REQUEST['trip']) > 0 && !in_array('*new*',$_REQUEST['trip']) && isset($_SESSION['bb_member'])){
	$query = 'SELECT SUM(reservations_assoc.`amount`) AS `sum` FROM `reservations_assoc`,`reservations`';
		$query .= ' WHERE reservations.`id` = reservations_assoc.`reservation`';
		$query .= ' AND (reservations_assoc.`id` = "'.implode('" OR reservations_assoc.`id` = "',$_REQUEST['trip']).'")';
		$query .= ' AND reservations.`email` = "'.$_SESSION['bb_member']['email'].'"';
		$query .= ' GROUP BY reservations.`email`';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
	if($num_results == 1){
		$changeadj = mysql_fetch_assoc($result);
		$changeadj = $changeadj['sum'];
		}
	}

$m_start = microtime(true);
$bench = array('start'=>$m_start);

$output .= '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0"';
	if(!isset($_REQUEST['formsize']) || $_REQUEST['formsize'] != "m"): $output .= ' WIDTH="600"'; endif;
	$output .= ' ID="routetable">'."\n";

//PRINT OUT LEGS AND MANIFEST
	$starters = array(); $constart = array(); $constart3 = array(); $constart4 = array(); $constart5 = array();
	$enders = array(); $conend = array(); $conend3 = array(); $conend4 = array(); $conend5 = array();
	$goroutes = array();

	//DIRECT ROUTES *******************************************************************************************************************
	foreach($legs as $thisleg){
		$c++;
		if($thisleg['dep_loc'] == $_REQUEST['start'] && $thisleg['arr_loc'] == $_REQUEST['end']){
			addgoroute(array($thisleg));
			} elseif($thisleg['dep_loc'] == $_REQUEST['start']){
			array_push($starters,$thisleg['id']);
			} elseif($thisleg['arr_loc'] == $_REQUEST['end']){
			array_push($enders,$thisleg['id']);
			}
		} //End ForEach
		$m_end = microtime(true); $bench['direct'] = ($m_end - $m_start); $m_start = $m_end;

	//2 TRIPS *******************************************************************************************************************
	if($_REQUEST['maxlegs'] > 0){
	foreach($starters as $startleg){
		foreach($enders as $endleg){
		$c++;
		if($legs['l'.$startleg]['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $legs['l'.$startleg]['arr_time'] && (date("z",$legs['l'.$endleg]['date'])-date("z",$legs['l'.$startleg]['date'])) < $_REQUEST['maxdays']){
			addgoroute(array($legs['l'.$startleg],$legs['l'.$endleg]));
			}
		}
	} //End ForEach
	$m_end = microtime(true); $bench['2trips'] = ($m_end - $m_start); $m_start = $m_end;
	}

	//3 TRIPS *******************************************************************************************************************
	if($_REQUEST['maxlegs'] > 1){
	foreach($legs as $thisleg){
	if($thisleg['arr_loc'] != $_REQUEST['start'] && $thisleg['arr_loc'] != $_REQUEST['end']){
		foreach($starters as $startleg){
			$c++;
			if($legs['l'.$startleg]['arr_loc'] == $thisleg['dep_loc'] && $thisleg['dep_time'] >= $legs['l'.$startleg]['arr_time'] && $thisleg['arr_loc'] != $legs['l'.$startleg]['dep_loc'] && (date("z",$thisleg['date'])-date("z",$legs['l'.$startleg]['date'])) < $_REQUEST['maxdays']){
			if($_REQUEST['maxlegs'] > 2){ array_push($constart,array($startleg,$thisleg['id'])); }
			foreach($enders as $endleg){
			$c++;
			if($thisleg['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $thisleg['arr_time'] && (date("z",$legs['l'.$endleg]['date'])-date("z",$legs['l'.$startleg]['date'])) < $_REQUEST['maxdays']){
				addgoroute(array($legs['l'.$startleg],$thisleg,$legs['l'.$endleg]));
				}
			} //End ForEach
			} //End Starter If Statement
		} //End ForEach
		if($_REQUEST['maxlegs'] > 2){
		foreach($enders as $endleg){
			$c++;
			if($legs['l'.$endleg]['arr_loc'] != $_REQUEST['start'] && $legs['l'.$endleg]['arr_loc'] != $thisleg['dep_loc'] && $thisleg['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $thisleg['arr_time'] && (date("z",$legs['l'.$endleg]['date'])-date("z",$thisleg['date'])) < $_REQUEST['maxdays']){
			array_push($conend,array($thisleg['id'],$endleg));
			}
		}
		}
	} //End Arrival != 1st Dep/Ult Dest Check
	} //End ForEach
	$m_end = microtime(true); $bench['3trips'] = ($m_end - $m_start); $m_start = $m_end;
	}
	unset($starters,$enders);

	//4 TRIPS *******************************************************************************************************************
	if($_REQUEST['maxlegs'] > 2){
	foreach($constart as $startlegs){
	$startleg = end($startlegs);
	$chksdate = $startlegs[0];
		foreach($conend as $endlegs){
		$endleg = $endlegs[0];
		$chkedate = end($endlegs);
		$c++;
		if($legs['l'.$startleg]['dep_loc'] != $legs['l'.$endleg]['arr_loc'] && $legs['l'.$startleg]['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $legs['l'.$startleg]['arr_time'] && (date("z",$legs['l'.$chkedate]['date'])-date("z",$legs['l'.$chksdate]['date'])) < $_REQUEST['maxdays']){
			$pusharray = array();
			foreach($startlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
			foreach($endlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
			addgoroute($pusharray);
		} //End If Statement
		} //End ForEach
	} //End ForEach
	$m_end = microtime(true); $bench['4trips'] = ($m_end - $m_start); $m_start = $m_end;
	}

	//5 TRIPS *******************************************************************************************************************
	if($_REQUEST['maxlegs'] > 3){
	foreach($legs as $thisleg){
	if($thisleg['arr_loc'] != $_REQUEST['start'] && $thisleg['arr_loc'] != $_REQUEST['end']){
		foreach($constart as $startlegs){
		$startleg = end($startlegs);
		$chksdate = $startlegs[0];
			$c++;
			// && $legs['l'.$endleg]['arr_loc'] != $thisleg['dep_loc']
			if($thisleg['arr_loc'] != $legs['l'.$startleg]['dep_loc'] && $legs['l'.$startleg]['arr_loc'] == $thisleg['dep_loc'] && $thisleg['dep_time'] >= $legs['l'.$startleg]['arr_time'] && (date("z",$thisleg['date'])-date("z",$legs['l'.$startleg]['date'])) < $_REQUEST['maxdays']){
			if($_REQUEST['maxlegs'] > 4){
				$pusharray = array();
				foreach($startlegs as $addthis){ array_push($pusharray,$addthis); } //$output .= $legs['l'.$addthis]['dep_loc'].' : '.$legs['l'.$addthis]['arr_loc'].' > ';
				//$output .= $thisleg['dep_loc'].' : '.$thisleg['arr_loc'].'<BR>';
				array_push($pusharray,$thisleg['id']);
				array_push($constart3,$pusharray);
				}
			foreach($conend as $endlegs){
			$endleg = $endlegs[0];
			$chkedate = end($endlegs);
			$c++;
			if($thisleg['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $thisleg['arr_time'] && (date("z",$legs['l'.$chkedate]['date'])-date("z",$legs['l'.$chksdate]['date'])) < $_REQUEST['maxdays']){
				$pusharray = array();
				//$output .= $legs['l'.$startleg]['arr_loc'].' > '.$thisleg['dep_loc'].' > '.$thisleg['arr_loc'].' > '.$legs['l'.$endleg]['dep_loc'].'<BR>';
				foreach($startlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
				array_push($pusharray,$thisleg);
				foreach($endlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
				addgoroute($pusharray);
				}
			} //End ForEach
			} //End If
		} //End ForEach
		if($_REQUEST['maxlegs'] > 4){
		foreach($conend as $endlegs){
		$endleg = $endlegs[0];
		$chkedate = end($endlegs);
			$c++;
			if($legs['l'.$endleg]['arr_loc'] != $_REQUEST['start'] && $legs['l'.$endleg]['arr_loc'] != $thisleg['dep_loc'] && $thisleg['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $thisleg['arr_time'] && $thisleg['dep_loc'] != $_REQUEST['start'] && (date("z",$legs['l'.$chkedate]['date'])-date("z",$thisleg['date'])) < $_REQUEST['maxdays']){
				$pusharray = array();
				//$output .= $thisleg['dep_loc'].' : '.$thisleg['arr_loc'];
				array_push($pusharray,$thisleg['id']);
				foreach($endlegs as $addthis){ array_push($pusharray,$addthis); } // $output .= ' > '.$legs['l'.$addthis]['dep_loc'].' : '.$legs['l'.$addthis]['arr_loc'];
				//$output .= '<BR>';
				array_push($conend3,$pusharray);
			}
		} //End ConEnd ForEach
		}
	} //End Arrival != 1st Dep/Ult Dest Check
	} //End ForEach
	$m_end = microtime(true); $bench['5trips'] = ($m_end - $m_start); $m_start = $m_end;
	}
	unset($constart,$conend);

	//6 TRIPS *******************************************************************************************************************
	if($_REQUEST['maxlegs'] > 4){
	foreach($constart3 as $startlegs){
	$startleg = end($startlegs);
	$chksdate = $startlegs[0];
		foreach($conend3 as $endlegs){
		$endleg = $endlegs[0];
		$chkedate = end($endlegs);
		$c++;
		if($legs['l'.$startleg]['dep_loc'] != $legs['l'.$endleg]['arr_loc'] && $legs['l'.$startleg]['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $legs['l'.$startleg]['arr_time'] && (date("z",$legs['l'.$chkedate]['date'])-date("z",$legs['l'.$chksdate]['date'])) < $_REQUEST['maxdays']){
			$pusharray = array();
			foreach($startlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
			foreach($endlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
			addgoroute($pusharray);
		} //End If Statement
		} //End ForEach
	} //End ForEach
	$m_end = microtime(true); $bench['6trips'] = ($m_end - $m_start); $m_start = $m_end;
	}

	//7 TRIPS *******************************************************************************************************************
	if($_REQUEST['maxlegs'] > 5){
	foreach($legs as $thisleg){
	if($thisleg['arr_loc'] != $_REQUEST['start'] && $thisleg['arr_loc'] != $_REQUEST['end']){
		foreach($constart3 as $startlegs){
		$startleg = end($startlegs);
		$chksdate = $startlegs[0];
			$c++;
			// && $legs['l'.$endleg]['arr_loc'] != $thisleg['dep_loc']
			if($thisleg['arr_loc'] != $legs['l'.$startleg]['dep_loc'] && $legs['l'.$startleg]['arr_loc'] == $thisleg['dep_loc'] && $thisleg['dep_time'] >= $legs['l'.$startleg]['arr_time'] && (date("z",$thisleg['date'])-date("z",$legs['l'.$startleg]['date'])) < $_REQUEST['maxdays']){
			if($_REQUEST['maxlegs'] > 6 && $thisleg['arr_loc'] != $_REQUEST['end']){
				$pusharray = array();
				foreach($startlegs as $addthis){ array_push($pusharray,$addthis); } //$output .= $legs['l'.$addthis]['dep_loc'].' : '.$legs['l'.$addthis]['arr_loc'].' > ';
				//$output .= $thisleg['dep_loc'].' : '.$thisleg['arr_loc'].'<BR>';
				array_push($pusharray,$thisleg['id']);
				array_push($constart4,$pusharray);
				}
			foreach($conend3 as $endlegs){
			$endleg = $endlegs[0];
			$chkedate = end($endlegs);
			$c++;
			if($thisleg['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $thisleg['arr_time'] && (date("z",$legs['l'.$chkedate]['date'])-date("z",$legs['l'.$chksdate]['date'])) < $_REQUEST['maxdays']){
				$pusharray = array();
				//$output .= $legs['l'.$startleg]['arr_loc'].' > '.$thisleg['dep_loc'].' > '.$thisleg['arr_loc'].' > '.$legs['l'.$endleg]['dep_loc'].'<BR>';
				foreach($startlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
				array_push($pusharray,$thisleg);
				foreach($endlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
				addgoroute($pusharray);
				}
			} //End ForEach
			} //End If
		} //End ForEach
		if($_REQUEST['maxlegs'] > 6){
		foreach($conend3 as $endlegs){
		$endleg = $endlegs[0];
		$chkedate = end($endlegs);
			$c++;
			if($legs['l'.$endleg]['arr_loc'] != $_REQUEST['start'] && $legs['l'.$endleg]['arr_loc'] != $thisleg['dep_loc'] && $thisleg['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $thisleg['arr_time'] && $thisleg['dep_loc'] != $_REQUEST['start'] && (date("z",$legs['l'.$chkedate]['date'])-date("z",$thisleg['date'])) < $_REQUEST['maxdays']){
				$pusharray = array();
				//$output .= $thisleg['dep_loc'].' : '.$thisleg['arr_loc'];
				array_push($pusharray,$thisleg['id']);
				foreach($endlegs as $addthis){ array_push($pusharray,$addthis); } // $output .= ' > '.$legs['l'.$addthis]['dep_loc'].' : '.$legs['l'.$addthis]['arr_loc'];
				//$output .= '<BR>';
				array_push($conend4,$pusharray);
			}
		} //End ConEnd ForEach
		}
	} //End Arrival != 1st Dep/Ult Dest Check
	} //End ForEach
	$m_end = microtime(true); $bench['7trips'] = ($m_end - $m_start); $m_start = $m_end;
	}
	unset($constart3,$conend3);
	//$output .= '<PRE>ConStart4: '; print_r($constart4); $output .= '</PRE>';
	//$output .= '<PRE>ConEnd4: '; print_r($conend4); $output .= '</PRE>';

	//8 TRIPS *******************************************************************************************************************
	if($_REQUEST['maxlegs'] > 6){
	foreach($constart4 as $startlegs){
	$startleg = end($startlegs);
	$chksdate = $startlegs[0];
		foreach($conend4 as $endlegs){
		$endleg = $endlegs[0];
		$chkedate = end($endlegs);
		$c++;
		if($legs['l'.$startleg]['dep_loc'] != $legs['l'.$endleg]['arr_loc'] && $legs['l'.$startleg]['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $legs['l'.$startleg]['arr_time'] && (date("z",$legs['l'.$chkedate]['date'])-date("z",$legs['l'.$chksdate]['date'])) < $_REQUEST['maxdays']){
			$pusharray = array();
			foreach($startlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
			foreach($endlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
			addgoroute($pusharray);
		} //End If Statement
		} //End ForEach
	} //End ForEach
	$m_end = microtime(true); $bench['8trips'] = ($m_end - $m_start); $m_start = $m_end;
	}

	//9 TRIPS *******************************************************************************************************************
	if($_REQUEST['maxlegs'] > 7){
	foreach($legs as $thisleg){
	if($thisleg['arr_loc'] != $_REQUEST['start'] && $thisleg['arr_loc'] != $_REQUEST['end']){
		foreach($constart4 as $startlegs){
		$startleg = end($startlegs);
		$chksdate = $startlegs[0];
			$c++;
			// && $legs['l'.$endleg]['arr_loc'] != $thisleg['dep_loc']
			if($thisleg['arr_loc'] != $legs['l'.$startleg]['dep_loc'] && $legs['l'.$startleg]['arr_loc'] == $thisleg['dep_loc'] && $thisleg['dep_time'] >= $legs['l'.$startleg]['arr_time'] && (date("z",$thisleg['date'])-date("z",$legs['l'.$startleg]['date'])) < $_REQUEST['maxdays']){
			if($_REQUEST['maxlegs'] > 8 && $thisleg['arr_loc'] != $_REQUEST['end']){
				$pusharray = array();
				foreach($startlegs as $addthis){ array_push($pusharray,$addthis); } // $output .= $legs['l'.$addthis]['dep_loc'].' : '.$legs['l'.$addthis]['arr_loc'].' > ';
				//$output .= $thisleg['dep_loc'].' : '.$thisleg['arr_loc'].'<BR>';
				array_push($pusharray,$thisleg['id']);
				array_push($constart5,$pusharray);
				}
			foreach($conend4 as $endlegs){
			$endleg = $endlegs[0];
			$chkedate = end($endlegs);
			$c++;
			if($thisleg['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $thisleg['arr_time'] && (date("z",$legs['l'.$chkedate]['date'])-date("z",$legs['l'.$chksdate]['date'])) < $_REQUEST['maxdays']){
				$pusharray = array();
				//$output .= $legs['l'.$startleg]['arr_loc'].' > '.$thisleg['dep_loc'].' > '.$thisleg['arr_loc'].' > '.$legs['l'.$endleg]['dep_loc'].'<BR>';
				foreach($startlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
				array_push($pusharray,$thisleg);
				foreach($endlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
				addgoroute($pusharray);
				}
			} //End ForEach
			} //End If
		} //End ForEach
		if($_REQUEST['maxlegs'] > 8){
		foreach($conend4 as $endlegs){
		$endleg = $endlegs[0];
		$chkedate = end($endlegs);
			$c++;
			if($legs['l'.$endleg]['arr_loc'] != $_REQUEST['start'] && $legs['l'.$endleg]['arr_loc'] != $thisleg['dep_loc'] && $thisleg['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $thisleg['arr_time'] && $thisleg['dep_loc'] != $_REQUEST['start'] && (date("z",$legs['l'.$chkedate]['date'])-date("z",$thisleg['date'])) < $_REQUEST['maxdays']){
				$pusharray = array();
				//$output .= $thisleg['dep_loc'].' : '.$thisleg['arr_loc'];
				array_push($pusharray,$thisleg['id']);
				foreach($endlegs as $addthis){ array_push($pusharray,$addthis); } // $output .= ' > '.$legs['l'.$addthis]['dep_loc'].' : '.$legs['l'.$addthis]['arr_loc'];
				//$output .= '<BR>';
				array_push($conend5,$pusharray);
			}
		} //End ConEnd ForEach
		}
	} //End Arrival != 1st Dep/Ult Dest Check
	} //End ForEach
	$m_end = microtime(true); $bench['9trips'] = ($m_end - $m_start); $m_start = $m_end;
	}
	unset($constart4,$conend4);
	//$output .= '<PRE>ConStart5: '; print_r($constart5); $output .= '</PRE>';
	//$output .= '<PRE>ConEnd5: '; print_r($conend5); $output .= '</PRE>';

	//10 TRIPS *******************************************************************************************************************
	if($_REQUEST['maxlegs'] > 8){
	foreach($constart5 as $startlegs){
	$startleg = end($startlegs);
	$chksdate = $startlegs[0];
		foreach($conend5 as $endlegs){
		$endleg = $endlegs[0];
		$chkedate = end($endlegs);
		$c++;
		if($legs['l'.$startleg]['dep_loc'] != $legs['l'.$endleg]['arr_loc'] && $legs['l'.$startleg]['arr_loc'] == $legs['l'.$endleg]['dep_loc'] && $legs['l'.$endleg]['dep_time'] >= $legs['l'.$startleg]['arr_time'] && (date("z",$legs['l'.$chkedate]['date'])-date("z",$legs['l'.$chksdate]['date'])) < $_REQUEST['maxdays']){
			$pusharray = array();
			foreach($startlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
			foreach($endlegs as $addthis){ array_push($pusharray,$legs['l'.$addthis]); }
			addgoroute($pusharray);
		} //End If Statement
		} //End ForEach
	} //End ForEach
	$m_end = microtime(true); $bench['10trips'] = ($m_end - $m_start); $m_start = $m_end;
	}
	unset($constart5,$conend5);

/*
	$output .= 'Legs: '.count($legs).'<BR>';
	$output .= 'Starters: '.count($starters).'<BR>';
	$output .= 'Enders: '.count($enders).'<BR>';
	$output .= 'ConStart: '.count($constart).'<BR>';
	$output .= 'ConEnd: '.count($conend).'<BR>';
	$output .= 'ConStart3: '.count($constart3).'<BR>';
	$output .= 'ConEnd3: '.count($conend3).'<BR>';
	$output .= 'ConStart4: '.count($constart4).'<BR>';
	$output .= 'ConEnd4: '.count($conend4).'<BR>';
	$output .= 'ConStart5: '.count($constart5).'<BR>';
	$output .= 'ConEnd5: '.count($conend5).'<BR>';
	$output .= 'Total Comparisons: '.$c.'<BR>';
	//$output .= '<PRE>'; print_r($constart); $output .= '</PRE>';
*/

	if(count($goroutes) > 0){
	//SORT GOROUTES
	$sort0 = array(); //Prime route?, First Leg on Find Date, All on the same day?
	$sort1 = array(); //Number of days
	$sort2 = array(); //Number of legs
	$sort3 = array(); //Date of initial leg
	$sort4 = array(); //Time difference from 1st to 2nd leg
	foreach($goroutes as $key => $thisroute){
		$sort0[$key] = findprime($thisroute);
		$sort1[$key] = ($thisroute[(count($thisroute)-1)]['date'] - $thisroute[0]['date']);
		$sort2[$key] = count($thisroute);
		$sort3[$key] = $thisroute[0]['date'];
		if(isset($thisroute[1])){
			$sort4[$key] = ($thisroute[1]['date']-$thisroute[0]['date']);
			} else {
			$sort4[$key] = 0;
			}
		}
	array_multisort($sort0, SORT_ASC, $sort1, SORT_ASC, $sort2, SORT_ASC, $sort3, SORT_ASC, $sort4, SORT_ASC, $goroutes);
	$m_end = microtime(true); $bench['sorting'] = ($m_end - $m_start);
	$bench['total'] = ($m_end - $bench['start']);

	$output .= '<TR><TD COLSPAN="8" ALIGN="center" STYLE="font-family:Arial; font-size:10pt;">';
	//$output .= '<SPAN STYLE="text-align:left;"><PRE>'; $output .= print_r($bench,TRUE); $output .= '</PRE></SPAN>';
		$ondate = '<SPAN STYLE="font-size:12pt;"><I>No routes are available on your requested date.<BR>Please consider the following options.</I></SPAN><BR><BR>';
		foreach($goroutes as $thisroute){
		if($thisroute[0]['date'] == $finddate): $ondate = ''; break; endif;
		}

		//$output .= '<SPAN STYLE="font-size:10pt;"><I>Please be aware that as a result of road work in Zion the bus will not be able to go through Zion on certain days.  Please <A HREF="mailto:info@bundubus.com">email</A> for specifics.</I></SPAN><BR><BR>';

		$output .= $ondate.'Found <B>'.count($goroutes).'</B> possible routes.';
		$output .= '</TD></TR>'."\n";

	foreach($goroutes as $thisroute){
	bcolor('reset');
	$price = 0;
	$prime = findprime($thisroute);
	$numdays = (date("z",$thisroute[(count($thisroute)-1)]['date'])-date("z",$thisroute[0]['date'])+1);
	$ordtitles = array(); $ordids = array(); $orddates = array(); $ordpax = array();
	$output .= '<TR><TD COLSPAN="8" ALIGN="center" STYLE="font-family:Arial; font-size:12pt; border-bottom:#999999 1px solid;">&nbsp;</TD></TR>'."\n";

	foreach($thisroute as $thisleg){
	array_push($ordtitles,date("g:ia",$thisleg['dep_time']).' '.$dep_locs['d'.$thisleg['dep_loc']]['name'].' to '.date("g:ia",$thisleg['arr_time']).' '.$arr_locs['a'.$thisleg['arr_loc']]['name']);
	array_push($ordids,$thisleg['routeid']);
	array_push($orddates,$thisleg['date']);
	array_push($ordpax,$_REQUEST['pax']);

	if(isset($rticket)){
		$thisprice = '$0.00';
		} elseif($thisleg['bbprice'] > 0){
		$thisprice = $thisleg['bbprice'];
		} else {
		$thisprice = number_format(($defaults['cpm']*$thisleg['miles']),2,'.','');
			if($thisprice == 0){ $thisprice = "n/a"; }
		}

	$output .= "\t".'<TR STYLE="background:#';
			if($prime == "0"): $output .= 'CCCCFF'; else: $output .= bcolor(''); endif; $output .= ';">';
		$output .= '<TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; padding-left:3px; padding-right:6px; border-left:#999999 1px solid; white-space:nowrap;">'.date("D n/j",$thisleg['dep_time']).'</TD>';
		$output .= '<TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; padding-right:6px;">'.date("g:ia",$thisleg['dep_time']);
		if(!isset($_REQUEST['formsize']) || $_REQUEST['formsize'] != "m"){
			$output .= '</TD>';
			$output .= '<TD STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-right:6px;">';
			} else {
			$output .= ' ';
			}
		$output .= $dep_locs['d'.$thisleg['dep_loc']]['name'].'</TD>';
		$output .= '<TD STYLE="font-family:Arial; font-size:9pt; padding-right:6px;"><I>-to-</I></TD>';
		$output .= '<TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; padding-right:6px;">'.date("g:ia",$thisleg['arr_time']);
		if(!isset($_REQUEST['formsize']) || $_REQUEST['formsize'] != "m"){
			$output .= '</TD>';
			$output .= '<TD STYLE="font-family:Arial; font-size:9pt; padding-right:6px;">';
			} else {
			$output .= ' ';
			}
		$output .= $arr_locs['a'.$thisleg['arr_loc']]['name'].'</TD>';
		if(!isset($_REQUEST['formsize']) || $_REQUEST['formsize'] != "m"){
		$output .= '<TD STYLE="font-family:Arial; font-size:9pt; padding-right:6px;">';
			if($thisleg['details'] != ""): $output .= '<A STYLE="color:#0000FF; cursor:pointer;" onMouseOver="shownotes(this,\''.$thisleg['id'].'\')" onMouseOut="hideLyr()">Details</A>'; endif;
			$output .= '</TD>';
			}
		$output .= '<TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; border-right:#999999 1px solid;">';
			if(!isset($_REQUEST['hidep']) || $_REQUEST['hidep'] != 1){
				if(is_numeric($thisprice)): $output .= '$'.$thisprice.'/seat'; else: $output .= $thisprice; endif;
				//$output .= ' <SPAN STYLE="color:#C7C7C7;">('.($thisleg['seats']-$thisleg['blocked']).')</SPAN>';
				//$output .= ' (R:'.$thisleg['route'].' D:'.$thisleg['day'].' Seats:'.$avseats.')';
				//$output .= '<BR> DayOffset:'.$thisleg['dayoffset'].'<BR> RunStart:'.date("n/j",$thisleg['runstart']).'<BR> RouteStart:'.date("n/j",$thisleg['routestart']).'<BR> SubTDays:'.$thisleg['subtractdays'].'<BR> ThisDate:'.date("n/j",$thisleg['thisdate']);
				}
			$output .= '</TD>';
		$output .= '</TR>'."\n";
		if(is_numeric($price) && is_numeric($thisprice)): $price = ($price+$thisprice); else: $price = "n/a"; endif;
	} //End Legs ForEach

	$output .= "\t".'<TR STYLE="';
		if($prime == "0"): $output .= 'background:#CCCCFF;'; endif;
		$output .= '"><TD COLSPAN="2" ALIGN="left" VALIGN="bottom" STYLE="font-family:Arial; font-size:9pt; color:#666666; padding-left:6px; border-left:#999999 1px solid;">';
			if(count($thisroute) > 1){
				$output .= '<I>'.$numdays.' day'; if($numdays > 1): $output .= 's'; endif; $output .= ', '.count($thisroute).' legs</I>';
				} else {
				$output .= '&nbsp;';
				}
			$output .= '</TD><TD COLSPAN="6" ALIGN="right" STYLE="font-family:Arial; font-size:10pt; padding-right:6px; border-right:#999999 1px solid;">';

		if(!isset($_REQUEST['hidep']) || $_REQUEST['hidep'] != 1){
			if(is_numeric($price)){
			$output .= '$'.number_format($price,2,'.','').' x '.$_REQUEST['pax'].' seat(s) = ';
			$price = ($price*$_REQUEST['pax']);
			$output .= '<B>$'.number_format($price, 2, '.', '').'</B>';

			if(isset($changeadj) && $changeadj > 0){
				$output .= '<BR><I>Adjust value of previously reserved routes: $'.number_format($changeadj,2,'.','').'</I>';
				$price = ($price-$changeadj);
				$output .= '&nbsp;&nbsp;&nbsp;New total: <B>$'.number_format($price,2,'.','').'</B>';
				/*} elseif(count($thisroute) > 1){
				$output .= '<BR><SPAN STYLE="font-size:8pt;"><I>A multi leg discount has been automatically applied!</I></SPAN>';
				if(count($thisroute) == 2){
					$price = $price*($defaults['2legs']*0.01);
					} elseif(count($thisroute) > 2){
					$price = $price*($defaults['3legs']*0.01);
					}
				$output .= '&nbsp;&nbsp;&nbsp;Discounted total: <B>$'.number_format($price, 2, '.', '').'</B>';*/
				}

				} elseif(isset($rticket)) {
				//$output .= '<SPAN STYLE="font-size:9pt;"><I>Add to';
				//	if($rticket['type'] == "h"){ $output .= ' Hop On, Hop Off Pass'; } elseif($rticket['type'] == "m"){ $output .= ' Mini Route Pass'; }
				//	$output .= ' #'.$rticket['reservation'].'</I></SPAN>';
				$output .= '&nbsp;';
				} else {
				$output .= '<B><I>Please call for pricing</I></B>';
				}
			}
		$output .= '</TD></TR>'."\n";

	$output .= "\t".'<TR STYLE="';
		if($prime == "0"): $output .= 'background:#CCCCFF;'; endif;
		$output .= '"><TD COLSPAN="8" ALIGN="center" STYLE="font-family:Arial; font-size:1pt; border-bottom:#999999 1px solid; border-left:#999999 1px solid; border-right:#999999 1px solid;">';
		if(isset($rticket) || is_numeric($price)){
			$output .= '<FORM METHOD="POST" ACTION="';
					if(isset($rticket['id']) && $rticket['id'] != "" && in_array('*new*',$_REQUEST['trip'])){
						$output .= $_REQUEST['act'];
						} elseif(isset($_REQUEST['trip']) && count($_REQUEST['trip']) > 0 && !in_array('*new*',$_REQUEST['trip'])){
						$output .= $_REQUEST['act'];
						} elseif(isset($_REQUEST['respage']) && $_REQUEST['respage'] != "") {
						$output .= $_REQUEST['respage'];
						} else {
						$output .= 'https://www.bundubashers.com/reserve.php';
						}
					$output .= '" STYLE="margin-bottom:0px; padding-bottom:0px;">';
				$output .= '<INPUT TYPE="hidden" NAME="utaction" VALUE="addroutes">';
				if(isset($_REQUEST['nodup'])){ $output .= '<INPUT TYPE="hidden" NAME="nodup" VALUE="'.$_REQUEST['nodup'].'">'."\n"; }
				if(isset($rticket['id']) && $rticket['id'] != ""){ $output .= '<INPUT TYPE="hidden" NAME="bbticket" VALUE="'.$rticket['id'].'">'; }
				if(isset($_REQUEST['trip']) && count($_REQUEST['trip']) > 0 && !in_array('*new*',$_REQUEST['trip'])){
					foreach($_REQUEST['trip'] as $id){
						$output .= '<INPUT TYPE="hidden" NAME="remove[]" VALUE="'.$id.'">';
						}
					}
				foreach($ordids as $key => $id){
					$output .= '<INPUT TYPE="hidden" NAME="title[]" VALUE="'.$ordtitles[$key].'">';
					$output .= '<INPUT TYPE="hidden" NAME="routeid[]" VALUE="'.$ordids[$key].'">';
					$output .= '<INPUT TYPE="hidden" NAME="date[]" VALUE="'.$orddates[$key].'">';
					$output .= '<INPUT TYPE="hidden" NAME="pax[]" VALUE="'.$ordpax[$key].'">';
					}
				$output .= '<INPUT TYPE="submit" VALUE="';
					if(isset($_REQUEST['btn']) && trim($_REQUEST['btn']) != ""){
						$output .= $_REQUEST['btn'];
						} elseif(isset($rticket['id']) && $rticket['id'] != "" && in_array('*new*',$_REQUEST['trip'])){
						$output .= 'Add';
						} elseif(isset($_REQUEST['trip']) && count($_REQUEST['trip']) > 0 && !in_array('*new*',$_REQUEST['trip'])){
						$output .= 'Change';
						} else {
						$output .= 'Reserve';
						}
					$output .= '" STYLE="font-size:9pt; width:100px;">';
				$output .= '</FORM>';
			} else {
			$output .= '&nbsp;';
			}
		$output .= '</TD></TR>'."\n";

	} //End GoRoutes ForEach

	} else {
	$output .= '<TR><TD ALIGN="center" STYLE="font-family:Arial; font-size:12pt;"><I>';
		if(isset($rticket)){
		$output .= '- No valid routes found for your ticket -';
		} else {
		$output .= '- No routes found -';
		}
		$output .= '</I></TD></TR>'."\n\n";
	}

$output .= '</TABLE><BR>'."\n\n";

$output .= '<!-- '.$c.' -->'."\n";


//$output .= '<PRE>'; print_r($routeseats); print_r($legseats); $output .= '</PRE>';

if($_REQUEST['disablejs'] != "y"){
$output .= '<script language="javascript"><!--

function hideLyr(){
	document.getElementById("showbox").style.display = "none";
	}

function showLyr(){
	document.getElementById("showbox").style.display = "";
	}

function shownotes(obj,legid){
	if(legnotes[\'l\'+legid]){
	var x = document.getElementById(\'showbox\');

	var newhtml = \'<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:420px;" onMouseOver="showLyr()" onMouseOut="hideLyr()"><TR><TD ALIGN="left" STYLE="background:#CCCCFF url(\\\'http://www.bundubashers.com/img/ezdescback.jpg\\\') right top repeat-y;  border:1px solid #000000; font-family:Arial; font-size:10pt; padding:6px;">\'+legnotes[\'l\'+legid]+\'</TD><TD WIDTH="8">&nbsp;</TD></TR></TABLE>\';
	//alert(newhtml);
	x.innerHTML = newhtml;
	x.style.display = "";

	var boxpos = findPos(obj);
	var w = obj.offsetWidth;
	boxpos[0] = eval(boxpos[0] - 420 + 2);
	boxpos[1] = eval(boxpos[1]);
	var windems = getwindems();
	var scrolloff = getScrollXY();
	//alert(\'WinDems:\'+windems[0]+\'x\'+windems[1]+\' ScrollOff:\'+scrolloff[0]+\'x\'+scrolloff[1]);

	var st = document.getElementById(\'routetable\');
	var tabpos = findPos(st);
	var table_bot = eval(tabpos[1] + st.offsetHeight);
	var win_bot = eval(windems[1] + scrolloff[1]);
	var boxb = eval(boxpos[1] + x.offsetHeight);
	//alert(x.offsetHeight+\' : \'+boxb+\' : \'+table_bot+\' : \'+(boxb-table_bot));
	if(table_bot < win_bot && boxb > table_bot){
		boxpos[1] = eval(boxpos[1]-(boxb-table_bot));
		} else if(boxb > win_bot){
		boxpos[1] = eval(boxpos[1]-(boxb-win_bot));
		} else if(boxb > table_bot){
		boxpos[1] = eval(boxpos[1]-(boxb-table_bot));
		}
	boxpos[1] = eval(boxpos[1]-20);

	x.style.top = boxpos[1] + \'px\';
	x.style.left = boxpos[0] + \'px\';
	}
	}

function findPos(obj){
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return [curleft,curtop];
	}

function getScrollXY(){
	var scrOfX = 0, scrOfY = 0;
	if( typeof( window.pageYOffset ) == \'number\' ) {
		//Netscape compliant
		scrOfY = window.pageYOffset;
		scrOfX = window.pageXOffset;
	} else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ){
		//DOM compliant
		scrOfY = document.body.scrollTop;
		scrOfX = document.body.scrollLeft;
	} else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
		//IE6 standards compliant mode
		scrOfY = document.documentElement.scrollTop;
		scrOfX = document.documentElement.scrollLeft;
	}
	return [ scrOfX, scrOfY ];
	}

function getwindems(){
	var myWidth = 0, myHeight = 0;
	if( typeof( window.innerWidth ) == \'number\' ) {
		//Non-IE
		myWidth = window.innerWidth;
		myHeight = window.innerHeight;
	} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
		//IE 6+ in \'standards compliant mode\'
		myWidth = document.documentElement.clientWidth;
		myHeight = document.documentElement.clientHeight;
	} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
		//IE 4 compatible
		myWidth = document.body.clientWidth;
		myHeight = document.body.clientHeight;
	}
	return [ myWidth, myHeight ];
	}

//--></script>';
} //End $_REQUEST['disablejs'] if statement

} //END $_REQUEST['return'] If Statement

if($_REQUEST['rettype'] == "jfeed"){

	echo '<script language="javascript"><!--'."\n\n";

	$output = str_replace("\r",'',$output);
	$output = str_replace("\n",'',$output);
	$output = str_replace("\t",'',$output);
	$output = str_replace('\'','\\\'',$output);
	echo 'feedcode = \''.$output.'\';'."\n\n";

	echo 'function feedit(){ return feedcode; }'."\n\n";

	if($_REQUEST['disablejs'] == "y"){
	echo 'var legnotes = new Array();'."\n";
		foreach($legs as $thisleg){
		if($thisleg['details'] != ""){
			$thisleg['details'] = str_replace("\r",'',$thisleg['details']);
			$thisleg['details'] = str_replace("\n",'<BR>',$thisleg['details']);
			$thisleg['details'] = str_replace("'",'\\\'',$thisleg['details']);
			echo "\t".'legnotes[\'l'.$thisleg['id'].'\'] = \''.$thisleg['details'].'\';'."\n";
			}
		}
	} //END $_REQUEST['disablejs'] if statement

	echo '//--></script>';

} else {
	echo $output;
}

?>