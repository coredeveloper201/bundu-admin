<?  // This custom utility created by Dominick Bernal - www.bernalwebservices.com

require_once('../common.inc.php');

if(!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on") {
	$redirect = 'https://'.$_SERVER["HTTP_HOST"].$_SERVER["PHP_SELF"];
	if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != ""){ $redirect .= '?'.$_SERVER['QUERY_STRING']; }
	//header('Location: '.$redirect);
	//exit;
}

$time = time();
$today = strtotime("today");

session_set_cookie_params(36000);
session_start();

$successmsg = array();
$errormsg = array();
$pubsuccessmsg = array();
$puberrormsg = array();


if(!isset($_REQUEST['form'])): $_REQUEST['form'] = "cart"; endif;

//GET TICKET TYPES
$ticket_types = array();
$dtype = 0;
$query = 'SELECT * FROM `bundubus_hopon` WHERE `flexible` = 0 ORDER BY `days` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
		$row['id'] = 'h:'.$row['id'];
		$row['title'] = 'Hop On, Hop Off Pass: '.$row['title'];
	$ticket_types[$row['id']] = $row;
	if($i==0){ $dtype = $row['id']; }
	}
$query = 'SELECT * FROM `miniroutes` ORDER BY `title` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
		$row['id'] = 'm:'.$row['id'];
		$row['title'] = 'Mini Route Pass: '.$row['title'];
		$row['days'] = 10;
	$ticket_types[$row['id']] = $row;
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($ticket_types); echo '</PRE>';

$fillform = array("type"=>$dtype);


if($_REQUEST['form'] == "process"){

if(!isset($_POST['acceptterms']) || $_POST['acceptterms'] != "I agree"){
	array_push($puberrormsg,'You must accept the terms and conditions by checking the appropriate box near the end of the form to complete your reservation.');
	}

$copyfields = array("type","name","email","cc_expdate_month","cc_expdate_year","comments");
	foreach($copyfields as $key){
		$_SESSION['reginfo'][$key] = $_POST[$key];
		}
	if($_POST['email'] === $_POST['remail']): $_SESSION['reginfo']['remail'] = $_POST['remail']; else: $_SESSION['reginfo']['remail'] = ''; endif;

$checkfields = array(
	"type" => "choice of pass",
	"name" => "name",
	"email" => "email address",
	"cc_num" => "credit card number",
	"cc_expdate_month" => "credit card expiration month",
	"cc_expdate_year" => "credit card expiration year",
	"cc_scode" => "credit card security code",
	"password" => "password"
	);

	foreach($checkfields as $key => $desc){
		//echo '<!-- Check field '.$key.'|'.$desc.': '.$_POST[$key].' -->'."\n";
		if(!isset($_POST[$key]) || trim($_POST[$key]) == ""){
			//echo '<!-- Found a problem... -->'."\n";
			array_push($puberrormsg,'Please enter your '.$desc.' in the form below.');
			}
		//echo '<!-- '; print_r($puberrormsg); echo ' -->'."\n";
		}

	if(isset($_POST['cc_num']) && $_POST['cc_num'] != "" && substr($_POST['cc_num'],0,1) == "6"){ array_push($puberrormsg,'Credit card number is invalid.  Please enter a valid Visa, MasterCard or American Express card number.'); }

	//CHECK EMAIL
	//$emailchk = @mysql_query('SELECT `email` FROM `registered` WHERE `email` = "'.trim($_POST['email']).'" LIMIT 1');
	//$num_results = mysql_num_rows($emailchk);
	//if($num_results == 1){ $puberrormsg = array('The email address you entered has already been registered.<BR><A HREF="login.php?hoho=*new*">Please log in</A> to purchase more passes with this email address.<BR>If you have misplaced your password, <A HREF="forgotpwd.php">you can have it reset here</A>.'); }

	if(count($puberrormsg) > 0){
		$_REQUEST['form'] = "cart";
		$fillform = $_POST;
		$fillform['password'] = '';
		$fillform['rpassword'] = '';
		}
}


//echo '<PRE>'; print_r($puberrormsg); echo '</PRE>';
//echo '<PRE>'; print_r($_POST); echo '</PRE>';
//echo $_REQUEST['form'];


if($_REQUEST['form'] == "process"){  // PROCESS RESERVATION **********************************************************

	//$_POST['name'] = trim($_POST['firstname']).' '.trim($_POST['lastname']);
	$_POST['cc_num'] = trim($_POST['cc_num']);
	$_POST['cc_num'] = str_replace(' ','',$_POST['cc_num']);
	$_POST['cc_num'] = str_replace('-','',$_POST['cc_num']);
	$_POST['cc_expdate'] = $_POST['cc_expdate_month'].'/'.$_POST['cc_expdate_year'];
	if(isset($_POST['altpaymethod']) && $_POST['altpaymethod'] != ""): $paymethod = 'Alternate'; else: $paymethod = 'C/C'; endif;

	//NEW REGISTRATION
	/*$_POST['password'] = md5($_POST['password']);
	$query = 'INSERT INTO `registered`(`password`,`firstname`,`lastname`,`phone_homebus`,`phone_cell`,`cell_country`,`email`,`regdate`) VALUES("'.$_POST['password'].'","'.$_POST['firstname'].'","'.$_POST['lastname'].'","'.trim($_POST['phone_homebus']).'","'.trim($_POST['phone_cell']).'","'.trim($_POST['cell_country']).'","'.trim($_POST['email']).'","'.$time.'")';
	@mysql_query($query);
	$newuser = mysql_insert_id();
	$thiserror = mysql_error();
	if($thiserror == ""){
		array_push($pubsuccessmsg,'You have been registered with Bundu Bus!  Don\'t forget the email address you used to register.  You will need it to log in to your account in the future.');
		$_SESSION['bb_member']['email'] = trim($_POST['email']);
		$_SESSION['bb_member']['password'] = $_POST['password'];
		array_push($successmsg,'Registered new user: <B>'.$_POST['firstname'].' '.$_POST['lastname'].': '.$_POST['email'].'</B>.');
		} else {
		array_push($puberrormsg,'There was an error in registration.  Please contact us to complete your registration.');
		$newuser = 0;
		$_REQUEST['form'] = "cart";
		array_push($errormsg,$thiserror);
		}

	//SAVE NEW TICKET
	$query = 'INSERT INTO `bundubus_tickets`(`regid`,`type`,`email`,`firstname`,`lastname`,`phone_homebus`,`phone_cell`,`cell_country`,`days`,`cc_num`,`cc_expdate`,`cc_scode`,`cc_name`,`amount`,`pay_method`,`comments`,`booker`,`date_booked`) VALUES("'.$newuser.'","'.$_POST['type'].'","'.$_POST['email'].'","'.$_POST['firstname'].'","'.$_POST['lastname'].'","'.trim($_POST['phone_homebus']).'","'.trim($_POST['phone_cell']).'","'.trim($_POST['cell_country']).'","'.$ticket_types['t'.$_POST['type']]['days'].'","'.$_POST['cc_num'].'","'.$_POST['cc_expdate'].'","'.$_POST['cc_scode'].'","'.$_POST['name'].'","'.$ticket_types['t'.$_POST['type']]['price'].'","'.$paymethod.'","'.$_POST['comments'].'","Website","'.$time.'")';
	@mysql_query($query);
	$newid = mysql_insert_id();
	$thiserror = mysql_error();
	if($thiserror == ""){
		array_push($pubsuccessmsg,'You have created a Hop On, Hop Off Pass! Your new pass number is <B>'.$newid.'</B>.');
		array_push($successmsg,'Saved new Bundu Bus Hop On/Off pass: <B>'.$newid.'</B> for '.$_POST['firstname'].' '.$_POST['lastname']);
		} else {
		array_push($puberrormsg,'There was an error creating your new pass.  Please contact us to complete your purchase.');
		array_push($errormsg,$thiserror);
		}*/

	//NEW RESERVATION
	$query = 'INSERT INTO `reservations`(`name`,`phone_homebus`,`phone_cell`,`cell_country`,`email`,`amount`,`cc_name`,`cc_num`,`cc_expdate`,`cc_scode`,`cc_zip`,`pay_method`,`comments`,`booker`,`date_booked`)';
		$query .= ' VALUES("'.$_POST['name'].'","'.$_POST['phone_homebus'].'","'.$_POST['phone_cell'].'","'.$_POST['cell_country'].'","'.$_POST['email'].'","'.$ticket_types['t'.$_POST['type']]['price'].'","'.$_POST['name'].'","'.$_POST['cc_num'].'","'.$_POST['cc_expdate'].'","'.$_POST['cc_scode'].'","'.$_POST['cc_zip'].'","'.$paymethod.'","'.$_POST['comments'].'","Website","'.$time.'")';
		@mysql_query($query);
	$newid = mysql_insert_id();
	$thiserror = mysql_error();
	if($thiserror != ""){

		array_push($errormsg,$thiserror);

	} else {

		$t = explode(':',$_POST['type']);
		if($t[0] == "m"){
			$thoho = '';
			$tminiroute = $t[1];
			} else {
			$thoho = $t[1];
			$tminiroute = '';
			}

		$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`dep_time`,`arr_time`,`amount`,`bbticket`,`miniroute`,`username`,`password`,`days`)';
			$query .= ' VALUES("'.$newid.'","'.$ticket_types[$_POST['type']]['title'].'","'.$t[0].'","'.$_POST['name'].'",1,0,0,'.time().','.time().','.time().',"'.$ticket_types[$_POST['type']]['price'].'","'.$thoho.'","'.$tminiroute.'","'.trim($_POST['email']).'","'.trim($_POST['password']).'","'.$ticket_types[$_POST['type']]['days'].'")';
			//echo $query.'<BR>';
			@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror != ""){ array_push($errormsg,$thiserror); }

		array_push($pubsuccessmsg,'You have created a Hop On, Hop Off Pass! Your new pass number is <B>'.$newid.'</B>.');
		$_SESSION['bb_member']['username'] = trim($_POST['email']);
		$_SESSION['bb_member']['password'] = trim($_POST['password']);

		$message = '<SPAN STYLE="font-family:Arial; font-size:12pt;">';
		$message .= $_POST['name'].' has purchased a new HOHO Pass:<BR><BR>'."\n\n";
		$message .= 'Pass Id: <B>'.$newid.'</B><BR>'."\n";
		$message .= 'Pass Title: '.$ticket_types['t'.$_POST['type']]['title'].'<BR>'."\n";
		$message .= 'Days: '.$ticket_types['t'.$_POST['type']]['days'].'<BR>'."\n";
		$message .= 'Price: '.$ticket_types['t'.$_POST['type']]['price'].'<BR>'."\n";
		$message .= 'Comments: '.$_POST['comments'].'<BR>'."\n";
		$message .= '</SPAN>'."\n";

		if(count($errormsg) > 0){ $message .= '<BR>The following errors occurred: '.implode("<BR>\n",$errormsg); }

		//SEND MERCHANT NOTIFICATION
		$heading = 'New HOHO Pass #'.$newid;
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		$headers .= "To: Bundu Bus <yellowstonetours@gmail.com>\r\n"; //".$defaults['notaddress']."
		$headers .= "From: ".$_POST['name']." <".$_POST['email'].">\r\n";
		@mail("", $heading, $message, $headers);

		//Temp - run this so blocks get created
		$resObj = new reservation($newid);
		if($resObj->id > 0) {
			$resObj->save();
		}

		$_SERVER['PHP_SELF'] = '/bundubus/login.php';
		include('login.php');
		exit;

	}

}


if($_REQUEST['form'] != "process"){  // REGISTER/PURCHASE *********************************************************************************************************************************


$pagetitle = 'Bundu Bus Hop On, Hop Off Pass';
include_once('../header_reserve.php');

echo '<CENTER><BR>'."\n\n";


printmsgs($pubsuccessmsg,$puberrormsg);


?><SCRIPT><!--

function checkform(){
	var go = true;
	var msg = new Array();
	if(document.getElementById("acceptterms").checked == false){
		msg.push('You must accept the terms and conditions by checking the appropriate box near the end of the form to complete your reservation.');
		go = false;
		}
	if(document.getElementById("email").value != document.getElementById("remail").value){
		msg.push('"Email Address" and "Re-enter Email Address" fields are not the same.  Please double check your email address entries.');
		go = false;
		}
	if(document.getElementById("password").value != document.getElementById("rpassword").value){
		msg.push('"Password" and "Re-enter Password" fields are not the same.  Please re-type your password in both fields.');
		go = false;
		}
	if(document.getElementById("cc_expdate_month").value == "" || document.getElementById("cc_expdate_year").value == ""){
		msg.push('Please choose an expiration date for your credit card.');
		go = false;
		}
	if(document.getElementById("cc_zip").value == ""){
		msg.push('Please enter the zip or postal code associated with your credit card.');
		go = false;
		}
	if(go){
		var tmpyear;
		if(document.getElementById("cc_expdate_year").value > 96){
			tmpyear = "19" + document.getElementById("cc_expdate_year").value;
			} else if(document.getElementById("cc_expdate_year").value < 21){
			tmpyear = "20" + document.getElementById("cc_expdate_year").value;
			} else {
			msg.push('The card expiration year is not valid.');
			go = false;
			}
		tmpmonth = document.getElementById("cc_expdate_month").value;
		if (!(new CardType()).isExpiryDate(tmpyear, tmpmonth)) {
			msg.push('Your credit card has already expired.');
			go = false;
			}
		card = false;
		for(var n = 0; n < Cards.length; n++){
			if(Cards[n].checkCardNumber(document.getElementById("cc_num").value, tmpyear, tmpmonth)){
				card = true;
				break;
				}
			}
		if(!card){
			msg.push('Your credit card number does not appear to be valid.');
			go = false;
			}
		}
	if(msg.length > 0){ alert(msg.join("\n")); }
	return go;
}

function hidepw(){
	var d = '';
	if(document.getElementById('regn').checked == 1){
		d = 'none';
		document.getElementById("password").value = "";
		document.getElementById("rpassword").value = "";
		}

	var pw = 0;
	while(document.getElementById('pw'+pw)){
	document.getElementById('pw'+pw).style.display = d;
	pw++;
	}
}



//CC Validation Script Author: Simon Tneoh (tneohcb@pc.jaring.my)
var Cards = new Array();
Cards[0] = new CardType("MasterCard", "51,52,53,54,55", "16");
var MasterCard = Cards[0];
Cards[1] = new CardType("VisaCard", "4", "13,16");
var VisaCard = Cards[1];
Cards[2] = new CardType("AmExCard", "34,37", "15");
var AmExCard = Cards[2];
//Cards[3] = new CardType("DinersClubCard", "30,36,38", "14");
//var DinersClubCard = Cards[3];
//Cards[4] = new CardType("DiscoverCard", "6011", "16");
//var DiscoverCard = Cards[4];
//Cards[5] = new CardType("enRouteCard", "2014,2149", "15");
//var enRouteCard = Cards[5];
//Cards[6] = new CardType("JCBCard", "3088,3096,3112,3158,3337,3528", "16");
//var JCBCard = Cards[6];
var LuhnCheckSum = Cards[3] = new CardType();

/*************************************************************************\
Object CardType([String cardtype, String rules, String len, int year, 
                                        int month])
cardtype    : type of card, eg: MasterCard, Visa, etc.
rules       : rules of the cardnumber, eg: "4", "6011", "34,37".
len         : valid length of cardnumber, eg: "16,19", "13,16".
year        : year of expiry date.
month       : month of expiry date.
eg:
var VisaCard = new CardType("Visa", "4", "16");
var AmExCard = new CardType("AmEx", "34,37", "15");
\*************************************************************************/
function CardType() {
var n;
var argv = CardType.arguments;
var argc = CardType.arguments.length;

this.objname = "object CardType";

var tmpcardtype = (argc > 0) ? argv[0] : "CardObject";
var tmprules = (argc > 1) ? argv[1] : "0,1,2,3,4,5,6,7,8,9";
var tmplen = (argc > 2) ? argv[2] : "13,14,15,16,19";

this.setCardNumber = setCardNumber;  // set CardNumber method.
this.setCardType = setCardType;  // setCardType method.
this.setLen = setLen;  // setLen method.
this.setRules = setRules;  // setRules method.
this.setExpiryDate = setExpiryDate;  // setExpiryDate method.

this.setCardType(tmpcardtype);
this.setLen(tmplen);
this.setRules(tmprules);
if (argc > 4)
this.setExpiryDate(argv[3], argv[4]);

this.checkCardNumber = checkCardNumber;  // checkCardNumber method.
this.getExpiryDate = getExpiryDate;  // getExpiryDate method.
this.getCardType = getCardType;  // getCardType method.
this.isCardNumber = isCardNumber;  // isCardNumber method.
this.isExpiryDate = isExpiryDate;  // isExpiryDate method.
this.luhnCheck = luhnCheck;// luhnCheck method.
return this;
}

/*************************************************************************\
boolean checkCardNumber([String cardnumber, int year, int month])
return true if cardnumber pass the luhncheck and the expiry date is
valid, else return false.
\*************************************************************************/
function checkCardNumber() {
var argv = checkCardNumber.arguments;
var argc = checkCardNumber.arguments.length;
var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
var year = (argc > 1) ? argv[1] : this.year;
var month = (argc > 2) ? argv[2] : this.month;

this.setCardNumber(cardnumber);
this.setExpiryDate(year, month);

if (!this.isCardNumber())
return false;
if (!this.isExpiryDate())
return false;

return true;
}
/*************************************************************************\
String getCardType()
return the cardtype.
\*************************************************************************/
function getCardType() {
return this.cardtype;
}
/*************************************************************************\
String getExpiryDate()
return the expiry date.
\*************************************************************************/
function getExpiryDate() {
return this.month + "/" + this.year;
}
/*************************************************************************\
boolean isCardNumber([String cardnumber])
return true if cardnumber pass the luhncheck and the rules, else return
false.
\*************************************************************************/
function isCardNumber() {
var argv = isCardNumber.arguments;
var argc = isCardNumber.arguments.length;
var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
if (!this.luhnCheck())
return false;

for (var n = 0; n < this.len.size; n++)
if (cardnumber.toString().length == this.len[n]) {
for (var m = 0; m < this.rules.size; m++) {
var headdigit = cardnumber.substring(0, this.rules[m].toString().length);
if (headdigit == this.rules[m])
return true;
}
return false;
}
return false;
}

/*************************************************************************\
boolean isExpiryDate([int year, int month])
return true if the date is a valid expiry date,
else return false.
\*************************************************************************/
function isExpiryDate() {
var argv = isExpiryDate.arguments;
var argc = isExpiryDate.arguments.length;

year = argc > 0 ? argv[0] : this.year;
month = argc > 1 ? argv[1] : this.month;

if (!isNum(year+""))
return false;
if (!isNum(month+""))
return false;
today = new Date();
expiry = new Date(year, month);
if (today.getTime() > expiry.getTime())
return false;
else
return true;
}

/*************************************************************************\
boolean isNum(String argvalue)
return true if argvalue contains only numeric characters,
else return false.
\*************************************************************************/
function isNum(argvalue) {
argvalue = argvalue.toString();

if (argvalue.length == 0)
return false;

for (var n = 0; n < argvalue.length; n++)
if (argvalue.substring(n, n+1) < "0" || argvalue.substring(n, n+1) > "9")
return false;

return true;
}

/*************************************************************************\
boolean luhnCheck([String CardNumber])
return true if CardNumber pass the luhn check else return false.
Reference: http://www.ling.nwu.edu/~sburke/pub/luhn_lib.pl
\*************************************************************************/
function luhnCheck() {
var argv = luhnCheck.arguments;
var argc = luhnCheck.arguments.length;

var CardNumber = argc > 0 ? argv[0] : this.cardnumber;

if (! isNum(CardNumber)) {
return false;
  }

var no_digit = CardNumber.length;
var oddoeven = no_digit & 1;
var sum = 0;

for (var count = 0; count < no_digit; count++) {
var digit = parseInt(CardNumber.charAt(count));
if (!((count & 1) ^ oddoeven)) {
digit *= 2;
if (digit > 9)
digit -= 9;
}
sum += digit;
}
if (sum % 10 == 0)
return true;
else
return false;
}

/*************************************************************************\
ArrayObject makeArray(int size)
return the array object in the size specified.
\*************************************************************************/
function makeArray(size) {
this.size = size;
return this;
}

/*************************************************************************\
CardType setCardNumber(cardnumber)
return the CardType object.
\*************************************************************************/
function setCardNumber(cardnumber) {
this.cardnumber = cardnumber;
return this;
}

/*************************************************************************\
CardType setCardType(cardtype)
return the CardType object.
\*************************************************************************/
function setCardType(cardtype) {
this.cardtype = cardtype;
return this;
}

/*************************************************************************\
CardType setExpiryDate(year, month)
return the CardType object.
\*************************************************************************/
function setExpiryDate(year, month) {
this.year = year;
this.month = month;
return this;
}

/*************************************************************************\
CardType setLen(len)
return the CardType object.
\*************************************************************************/
function setLen(len) {
// Create the len array.
if (len.length == 0 || len == null)
len = "13,14,15,16,19";

var tmplen = len;
n = 1;
while (tmplen.indexOf(",") != -1) {
tmplen = tmplen.substring(tmplen.indexOf(",") + 1, tmplen.length);
n++;
}
this.len = new makeArray(n);
n = 0;
while (len.indexOf(",") != -1) {
var tmpstr = len.substring(0, len.indexOf(","));
this.len[n] = tmpstr;
len = len.substring(len.indexOf(",") + 1, len.length);
n++;
}
this.len[n] = len;
return this;
}

/*************************************************************************\
CardType setRules()
return the CardType object.
\*************************************************************************/
function setRules(rules) {
// Create the rules array.
if (rules.length == 0 || rules == null)
rules = "0,1,2,3,4,5,6,7,8,9";
  
var tmprules = rules;
n = 1;
while (tmprules.indexOf(",") != -1) {
tmprules = tmprules.substring(tmprules.indexOf(",") + 1, tmprules.length);
n++;
}
this.rules = new makeArray(n);
n = 0;
while (rules.indexOf(",") != -1) {
var tmpstr = rules.substring(0, rules.indexOf(","));
this.rules[n] = tmpstr;
rules = rules.substring(rules.indexOf(",") + 1, rules.length);
n++;
}
this.rules[n] = rules;
return this;
}

// --></SCRIPT><? echo "\n\n";


echo '<FORM NAME="resform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="javascript:return checkform();">'."\n";
echo '<INPUT TYPE="hidden" NAME="form" ID="form" VALUE="process">'."\n";

/*
echo '<SPAN STYLE="font-family:Arial; font-size:14pt;">Would you like to resigister with Bundu Bus?</SPAN><BR>'."\n";
	echo '<TABLE BORDER="0" WIDTH="500">'."\n";
	echo '<TR><TD VALIGN="top" ALIGN="center"><INPUT TYPE="radio" NAME="register" ID="regy" VALUE="y" onClick="hidepw()"';
		if(getval('register') == "y"): echo ' CHECKED'; endif;
		echo '></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="regy" onClick="hidepw()"><B>Yes</B>, I would like to register.  This will allow me to make changes and book trips with my pass online.</LABEL></TD></TR>'."\n";
	echo '<TR><TD VALIGN="top" ALIGN="center"><INPUT TYPE="radio" NAME="register" ID="regn" VALUE="n" onClick="hidepw()"';
		if(getval('register') == "n"): echo ' CHECKED'; endif;
		echo '></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="regn" onClick="hidepw()"><B>No</B> thank you, I prefer not to register.  I understand I will not be able to make changes to my pass online.</LABEL></TD></TR>'."\n";
	echo '<TR><TD></TD><TD STYLE="font-family:Arial; font-size:10pt;"><A HREF="login.php" STYLE="color:#CC0000;"><I><B>Already registered?</B> Click here to log in.</I></A></TD></TR>'."\n";
	echo '</TABLE><BR>'."\n";
*/

echo '<SPAN STYLE="font-family:Arial; font-size:10pt;"><A HREF="login.php?hoho=*new*" STYLE="color:#CC0000;"><I><B>Already registered?</B> Please click here to log in and purchase your new pass.</I></A><BR>Otherwise, continue below.</SPAN><BR><BR>'."\n\n";

	$pw=0;
	echo '	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="2">'."\n";
	//echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('First Name').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:280px;" NAME="firstname" VALUE="'.getval('firstname').'"></TD></TR>'."\n";
	//echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Last Name').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:280px;" NAME="lastname" VALUE="'.getval('lastname').'"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Name').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:280px;" NAME="name" VALUE="'.getval('name').'"></TD></TR>'."\n";

	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Home/Business Phone').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:180px;" NAME="phone_homebus" VALUE="'.getval('phone_homebus').'"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Cell Phone').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:180px;" NAME="phone_cell" VALUE="'.getval('phone_cell').'"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Cell Phone Country').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:180px;" NAME="cell_country" VALUE="'.getval('cell_country').'"></TD></TR>'."\n";

	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Email Address').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:280px;" NAME="email" ID="email" VALUE="'.getval('email').'"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Re-enter Email Address').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:280px;" NAME="remail" ID="remail" VALUE="'.getval('remail').'"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;" ID="pw'.$pw++.'"><B>'.gettrans('Password').'</B>&nbsp;</TD><TD ID="pw'.$pw++.'"><INPUT TYPE="password" SIZE="30" STYLE="width:280px;" ID="password" NAME="password" VALUE="'.getval('password').'"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;" ID="pw'.$pw++.'"><B>'.gettrans('Re-enter Password').'</B>&nbsp;</TD><TD ID="pw'.$pw++.'"><INPUT TYPE="password" SIZE="30" STYLE="width:280px;" ID="rpassword" NAME="rpassword" VALUE="'.getval('rpassword').'"></TD></TR>'."\n";
	echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo '	<TR><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:14pt;">Please choose your pass:</TD></TR>'."\n\n";
	foreach($ticket_types as $thistype){
	echo '<TR><TD ALIGN="right"><INPUT TYPE="radio" NAME="type" ID="type'.$thistype['id'].'" VALUE="'.$thistype['id'].'"';
		if(getval('type') == $thistype['id']): echo ' CHECKED'; endif;
		echo '></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="type'.$thistype['id'].'">'.$thistype['title'].' - $'.$thistype['price'].'</LABEL></TD></TR>'."\n";
	}
	echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo '	<TR><TD ALIGN="right"></TD><TD><FONT FACE="Arial" SIZE="1"><I>'.gettrans('We accept Visa, MasterCard or American Express credit cards.').'</I></FONT></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Credit card number').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" NAME="cc_num" ID="cc_num" VALUE="'.getval('cc_num').'"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Expiration date').'</B>&nbsp;</TD><TD><SELECT NAME="cc_expdate_month" ID="cc_expdate_month"><OPTION VALUE="">Choose...</OPTION>';
		for($i=1; $i<13; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if(getval('cc_expdate_month') == $i): echo " SELECTED"; endif;
			echo '>'.date("F",mktime("0","0","0",$i,"1","2005")).'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="cc_expdate_year" ID="cc_expdate_year"><OPTION VALUE="">Choose...</OPTION>';
		for($i=date("Y",$time); $i<(date("Y",$time)+11); $i++){
			echo '<OPTION VALUE="'.date("y",mktime("0","0","0","1","1",$i)).'"';
			if(getval('cc_expdate_year') == date("y",mktime("0","0","0","1","1",$i))): echo " SELECTED"; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Security code').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="5" NAME="cc_scode" VALUE="'.getval('cc_scode').'"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; width:200px; padding-right:5px;">'.gettrans('Zip/Postal code').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('Please provide the zip or postal code associated with your credit card.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="8" NAME="cc_zip" ID="cc_zip" VALUE="'.getval('cc_zip').'" STYLE="width:80px;"></TD></TR>'."\n";

	echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Comments').'</B>&nbsp;</TD><TD ALIGN="left" WIDTH="'.$rightcol.'"><TEXTAREA NAME="comments" COLS="30" ROWS="3" STYLE="width:280px;">'.getval('comments').'</TEXTAREA></TD></TR>'."\n";

	echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="acceptterms" ID="acceptterms" VALUE="I agree"></TD><TD ALIGN="left" STYLE="font-family:Arial; font-size:10pt; padding-left:4px;"><LABEL FOR="acceptterms">I agree to the terms and conditions as outlined on the<BR><A HREF="http://www.bundubus.com/hop_on_hop_off_grand_canyon_transportation.php" TARGET="_blank">Bundu Bus website</A>.</LABEL></TD></TR>'."\n";

	echo '	</TABLE><BR>'."\n\n";

	echo '<INPUT TYPE="submit" NAME="submit" VALUE="Purchase Pass">'."\n\n";

echo '</FORM>'."\n";


echo '<DIV STYLE="font-family:Arial; font-size:8pt;">'.gettrans('Bundu Bus').' '.gettrans('will not be bound by any prices that are generated maliciously or in error, and that are not its regular prices as detailed on its web sites.').'</DIV>'."\n\n";

echo '</CENTER>'."\n\n";


include_once('../footer.php');

}  //END FORM IF STATEMENT


?>